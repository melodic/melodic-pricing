package org.ow2.morphemic.pricing.rest.api.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Ignore
public class PricingProviderAPIV1ControllerTest {
    @Autowired
    private WebApplicationContext context;

//    @MockBean
//    private PricingDataService pricingDataService;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    /**
     * Simple case 1: 1st fetch all IDs, then fetch some pricing data for given set of IDs.
     */
    @Test
    public void shouldFetchAllPricingDataWhenThereIsNoData() throws Exception {
//        // given
//        when(pricingDataService.fetchAllPricingRecordIds()).thenReturn(Collections.emptyList());

        // when
        MockHttpServletRequestBuilder reqAllPricingsIds = MockMvcRequestBuilders.get(RestAPIV1.FULL_PATH_ALL_PRICING_IDS)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        MvcResult resultAllPricingIds = mockMvc.perform(reqAllPricingsIds).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        // then
        MockHttpServletResponse response = resultAllPricingIds.getResponse();
        assertEquals(200, response.getStatus());
        PricingProviderAPIV1Controller.AllPricingsIds allPricingsIds = objectMapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), PricingProviderAPIV1Controller.AllPricingsIds.class);
        assertNotNull(allPricingsIds);
        assertEquals(0, allPricingsIds.getPricingsCount());
        assertEquals(100, allPricingsIds.getMaxAllowedPricingsToFetchInOneRequest());
        assertNotNull(allPricingsIds.getPricingIds());
        assertTrue(allPricingsIds.getPricingIds().isEmpty());

    }


    /**
     * Simple case 2: 1st fetch all IDs, then fetch some pricing data for given set of IDs.
     */
    @Test
    public void shouldFetchAllPricingDataWhenThereIsData() throws Exception {
//        // given
//        List<UniquePricingId> testIds = createTestIds(150, 2);
//        when(pricingDataService.fetchAllPricingRecordIds()).thenReturn(testIds);

        // when
        MockHttpServletRequestBuilder reqAllPricingsIds = MockMvcRequestBuilders.get(RestAPIV1.FULL_PATH_ALL_PRICING_IDS)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON);

        MvcResult resultAllPricingIds = mockMvc.perform(reqAllPricingsIds).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        // then
        MockHttpServletResponse response = resultAllPricingIds.getResponse();
        assertEquals(200, response.getStatus());
        PricingProviderAPIV1Controller.AllPricingsIds allPricingsIds = objectMapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), PricingProviderAPIV1Controller.AllPricingsIds.class);
        assertNotNull(allPricingsIds);
        assertEquals(150, allPricingsIds.getPricingsCount());
        assertEquals(100, allPricingsIds.getMaxAllowedPricingsToFetchInOneRequest());
        assertNotNull(allPricingsIds.getPricingIds());
        assertEquals(150, allPricingsIds.getPricingIds().size());

//        verify(pricingDataService, times(1)).fetchAllPricingRecordIds();
//        verify(pricingDataService, never()).updateAllPricings();
//        verify(pricingDataService, never()).fetchPricingRecords(any());

        // and
//        ArgumentCaptor<List<UniquePricingId>> capturedPricingIds = ArgumentCaptor.forClass(List.class);
//        doAnswer(invocation -> {
//            List<UniquePricingId> pricingIds = invocation.getArgument(0);
//            return pricingIds.stream()
//                    .map(id -> {
//                        try {
//                            return loadTestPricingRecord(id.getExtId(), id.getCloudProviderId());
//                        } catch (IOException e) {
//                            log.error(e.getMessage(), e);
//                            throw new RuntimeException(e);
//                        }
//                    }).collect(Collectors.toList());
//        }).when(pricingDataService).fetchPricingRecords(any());

        // given
//        final String cloudProvbider0Id = "cloud00";
//        // when: client wants to get pricings for cloud00
//        MockHttpServletRequestBuilder reqPricings = MockMvcRequestBuilders.post(RestAPIV1.FULL_PATH_FETCH_PRICING_IDS)
//                .param(RestAPIV1.PARAM_PRICING_IDS_CLOUD_PROVIDER_ID, cloudProvbider0Id)
//                .param(RestAPIV1.PARAM_PRICING_IDS_PRICING_ID, testIds.stream()
//                        .filter(pricingId -> cloudProvbider0Id.equals(pricingId.getCloudProviderId())).map(UniquePricingId::getExtId).collect(Collectors.toList()).toArray(new String[]{}))
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON);
//
//        MvcResult resultPricings = mockMvc.perform(reqPricings).andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
//                .andReturn();
//
//        // then
//        verify(pricingDataService).fetchPricingRecords(capturedPricingIds.capture());
//
//        response = resultPricings.getResponse();
//        assertEquals(200, response.getStatus());
//
//        List<UniquePricingId> capturedValues = capturedPricingIds.getValue();
//        assertNotNull(capturedValues);
//        assertEquals(75, capturedValues.size());
//        assertEquals(capturedValues.size(), capturedValues.stream().filter(pricingId -> cloudProvbider0Id.equals(pricingId.getCloudProviderId())).count());
//
//        PricingProviderAPIV1Controller.CollectionOfPricingData collectionOfPricingData = objectMapper.readValue(resultPricings.getResponse().getContentAsString(), PricingProviderAPIV1Controller.CollectionOfPricingData.class);
//        assertNotNull(collectionOfPricingData);
//        assertEquals(75, collectionOfPricingData.getPricingCount());
//        assertNotNull(collectionOfPricingData.getPricings());
//        assertEquals(75, collectionOfPricingData.getPricings().size());
//        assertEquals(75, collectionOfPricingData.getPricings().stream()
//                .filter(pricing-> pricing.getId().startsWith("ID0") && cloudProvbider0Id.equals(pricing.getCloudServiceProviderName())).count());

    }

    /**
     * Endpoint must refuse returning pricings, when collection of ids is bigger than allowed size.
     */
    @Test
    public void mustNotAcceptBiggerCollectionOfIdsThanAllowed() throws Exception {
//        // given
//        List<UniquePricingId> testIds = createTestIds(150, 1);
//        final String cloudProvbider0Id = "cloud00";
//
//        // when
//        MockHttpServletRequestBuilder reqPricings = MockMvcRequestBuilders.post(RestAPIV1.FULL_PATH_FETCH_PRICING_IDS)
//                .param(RestAPIV1.PARAM_PRICING_IDS_CLOUD_PROVIDER_ID, cloudProvbider0Id)
//                .param(RestAPIV1.PARAM_PRICING_IDS_PRICING_ID, testIds.stream()
//                        .filter(pricingId -> cloudProvbider0Id.equals(pricingId.getCloudProviderId())).map(UniquePricingId::getExtId).collect(Collectors.toList()).toArray(new String[]{}))
//                .contentType(MediaType.APPLICATION_JSON)
//                .accept(MediaType.APPLICATION_JSON);
//
//        MvcResult resultPricings = mockMvc.perform(reqPricings).andExpect(MockMvcResultMatchers.status().isBadRequest())
//                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
//                .andReturn();
//
//        // then
//        MockHttpServletResponse response = resultPricings.getResponse();
//        assertEquals(400, response.getStatus());
    }


//    private List<UniquePricingId> createTestIds(int idsToGenerateCount, int cloudProvidersCount) {
//        List<UniquePricingId> generatedIds = new ArrayList<>(idsToGenerateCount);
//        for (int i = 0; i < idsToGenerateCount; i++) {
//            int cloudProviderId = (i % cloudProvidersCount);
//            generatedIds.add(new UniquePricingId(String.format("ID%04d", i), String.format("cloud%02d", cloudProviderId)));
//        }
//        return generatedIds;
//    }
//
//    private Pricing loadTestPricingRecord(String newId, String prividerName) throws IOException {
//        Pricing pricing = objectMapper.readValue(Files.readString(Paths.get("src", "test", "data", "testPricingRecord.json")), Pricing.class);
//        pricing.setId(newId);
//        pricing.setCloudServiceProviderName(prividerName);
//        return pricing;
//    }
}