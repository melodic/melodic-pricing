package org.ow2.morphemic.pricing.rest.api.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Provides access to pricing data using REST API. Access is secured by JWT.
 */
@RestController
@RequestMapping(RestAPIV1.BASE_V1_PATH)
@Slf4j
@RequiredArgsConstructor
@SecurityScheme(name = "Authorization", description = "JWT Token in authorization header", in = SecuritySchemeIn.HEADER,
        type = SecuritySchemeType.APIKEY, scheme = "jwt")
@SecurityRequirement(name = "Authorization")
@Tag(name = "pricing", description = "Pricing API")
class PricingProviderAPIV1Controller {

    //private final PricingDataService pricingDataService;
    @Value("${pricing.rest.api.maxAllowedPricingsToFetchInOneRequest:100}")
    private int maxAllowedPricingsToFetchInOneRequest = 100;

    /**
     * Returns all ids of pricing entries. Then client could fetch them using other methods (preferably using packages).
     *
     * @return
     */
    @Operation(summary = "Fetch all pricing IDs.", description = "Use this operation to fetch all pricing record IDs available to download.")
    @ApiResponses(@ApiResponse(responseCode = "200", description = "Ok."))
    @GetMapping(value = RestAPIV1.ALL_PRICING_IDS, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<AllPricingsIds> fetchAllPricingIDs() {

//        List<AllPricingsIds.PricingId> pricingIds = pricingDataService.fetchAllPricingRecordIds().stream().map(uniqPricingId ->
//                new AllPricingsIds.PricingId(uniqPricingId.getExtId(), uniqPricingId.getCloudProviderId())).collect(Collectors.toList());

        return ResponseEntity.ok(AllPricingsIds.builder()
                //.pricingsCount(pricingIds.size())
                //.pricingIds(pricingIds)
                .maxAllowedPricingsToFetchInOneRequest(maxAllowedPricingsToFetchInOneRequest)
                .build());
    }

    /**
     * Fetching pricing data for given set of pricing ids. If id is not found it will be ignored, so: returned count of elements
     * may be smaller than expected. Client can send up to {@link #maxAllowedPricingsToFetchInOneRequest} ids.
     *
     * @param pricingsIDs     set of ids for given cloud provider
     * @param cloudProviderId id of cloud provider
     * @return pricing data
     */
    @Operation(summary = "Fetch all pricing data.", description = "Allows to fetch pricing data for specified set of pricing record ids for given cloud provider.")
    @ApiResponses({@ApiResponse(responseCode = "200", description = "Ok."),
            @ApiResponse(responseCode = "400", description = "Bad request data. Usually too many ids sent.")})
    @PostMapping(value = RestAPIV1.FETCH_PRICING_IDS, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<CollectionOfPricingData> fetchCollectionOfPricingRecords(@RequestParam(RestAPIV1.PARAM_PRICING_IDS_CLOUD_PROVIDER_ID) String cloudProviderId,
                                                                            @RequestParam(RestAPIV1.PARAM_PRICING_IDS_PRICING_ID) Set<String> pricingsIDs) {
        if (pricingsIDs == null || pricingsIDs.size() > maxAllowedPricingsToFetchInOneRequest) {
            log.warn("Client sent wrong number of Pricing IDs: {}", pricingsIDs);
            CollectionOfPricingData dto = CollectionOfPricingData.builder().messages(Collections.singletonList("Client sent wrong number of Pricing IDs")).build();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(dto);
        }

        //List<Pricing> pricings = pricingDataService.fetchPricingRecords(pricingsIDs.stream().map(recId -> new UniquePricingId(recId, cloudProviderId)).collect(Collectors.toList()));

        CollectionOfPricingData dto = CollectionOfPricingData.builder()
                //      .pricings(pricings)
                //      .pricingCount(pricings.size())
                .build();
        return ResponseEntity.ok(dto);
    }

    /**
     * Schedule updating of pricing data for ALL suppliers.
     *
     * @return
     */
    @PostMapping(value = RestAPIV1.UPDATE_PRICING_DATA)
    @Operation(summary = "Schedule updating of pricing data for all cloud providers.", description = "Update will be performed in background. This is async operation.")
    @ApiResponse(responseCode = "200", description = "Scheduled.")
    ResponseEntity<Void> schedulePricingDataUpdate() {
        //pricingDataService.asyncUpdateAllPricings();
        return ResponseEntity.ok().build();
    }


    /**
     * Summary of all pricing IDs data returned by {@link #fetchAllPricingIDs()}.
     */
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Schema(name = "AllPricingsIds", description = "Contains all pricing ids available to download.")
    static class AllPricingsIds {
        /**
         * count of {@link #pricingIds}
         */
        @Schema(description = "Count of all ids")
        private int pricingsCount;
        /**
         * important: how many pricings are allowed to fetch (max) in one call to fetch data operation.
         */
        @Schema(description = "Important: how many pricings are allowed to fetch (max) in one call to fetch data operation.")
        private int maxAllowedPricingsToFetchInOneRequest;
        /**
         * ids of pricings
         */
        @Schema(description = "pricing IDs", name = "pricingIds")
        private List<PricingId> pricingIds;

        /**
         * DTO class: pricing ID.
         */
        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        @Schema(description = "Unique Pricing ID")
        static class PricingId {
            /**
             * unique pricing record for specific cloud provider
             */
            @Schema(description = "Unique pricing record for specific cloud provider.", name = "recordId")
            private String recordId;
            /**
             * cloud provider ID
             */
            @Schema(description = "Cloud provider ID", name = "cloudPrividerId")
            private String cloudPrividerId;
        }
    }

    /**
     * Structure represents pricing data fetched according to client's request.
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Schema(name = "CollectionOfPricingData", description = "Structure represents pricing data fetched according to client's request.")
    static class CollectionOfPricingData {
        /**
         * how many elements in this collection
         */
        @Schema(description = "How many elements in this collection of data.", name = "pricingCount")
        private int pricingCount;

//        /**
//         * data
//         */
//        @Schema(description = "Pricing data.", name = "pricings")
//        private List<Pricing> pricings;

        /**
         * Additional messages.
         */
        @Schema(description = "Additional messages in case of errors / problems.", name = "messages")
        private List<String> messages;
    }
}
