package org.ow2.morphemic.pricing.rest.api.v1;

/**
 * API V1 endpoints definitions.
 */
public interface RestAPIV1 {

    String BASE_V1_PATH = "/rest/v1";

    String ALL_PRICING_IDS = "/pricings";

    String FULL_PATH_ALL_PRICING_IDS = BASE_V1_PATH + ALL_PRICING_IDS;


    String FETCH_PRICING_IDS = "/pricings";
    String FULL_PATH_FETCH_PRICING_IDS = BASE_V1_PATH + FETCH_PRICING_IDS;

    String PARAM_PRICING_IDS_CLOUD_PROVIDER_ID = "cloudProviderId";
    String PARAM_PRICING_IDS_PRICING_ID = "pricing_id";

    String UPDATE_PRICING_DATA = "/update/pricings";
    String FULL_PATH_UPDATE_PRICING_DATA = BASE_V1_PATH + UPDATE_PRICING_DATA;
}
