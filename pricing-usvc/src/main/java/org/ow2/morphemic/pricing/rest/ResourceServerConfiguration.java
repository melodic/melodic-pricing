package org.ow2.morphemic.pricing.rest;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.rest.api.v1.RestAPIV1;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * Turns on and configures OAuth2 resource server.
 */
@Configuration
@EnableResourceServer
@Slf4j
class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Value("${pricing.resourceserver.id:oauth-morphemic-pricing}")
    private String resourceid;

    /**
     * Protect /v1 endpoints.
     *
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(RestAPIV1.BASE_V1_PATH + "/**")
                .authenticated();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        log.info("Use \"{}\" as resourceId", resourceid);
        resources.resourceId(resourceid);
    }

    @Bean
    OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components())
                .info(new Info().title("Pricing API").description(
                        "Pricing API."));
    }
}
