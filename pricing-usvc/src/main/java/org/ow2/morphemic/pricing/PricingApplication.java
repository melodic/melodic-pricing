package org.ow2.morphemic.pricing;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

/**
 * Main class.
 */
@Slf4j
@SpringBootApplication
@EnableConfigurationProperties
@EnableOAuth2Client
@EnableMongoRepositories
@EnableAsync
@EnableScheduling
@PropertySources(
        @PropertySource(value = "git.properties", ignoreResourceNotFound = true)
)
@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class PricingApplication {

    public static void main(String... args) {
        SpringApplication.run(PricingApplication.class, args);
    }
}

