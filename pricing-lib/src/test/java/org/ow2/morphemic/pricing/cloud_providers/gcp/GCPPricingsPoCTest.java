package org.ow2.morphemic.pricing.cloud_providers.gcp;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.cloudbilling.Cloudbilling;
import com.google.api.services.cloudbilling.model.ListServicesResponse;
import com.google.api.services.cloudbilling.model.ListSkusResponse;
import com.google.api.services.cloudbilling.model.Service;
import com.google.api.services.cloudbilling.model.Sku;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Can be used to dump raw data and test whle process of fetching data.
 * To run you have to define property: -Dtest.gcp=true
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@ActiveProfiles({"localtestgcp" , "test"})
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class GCPPricingsPoCTest {

    @Value("${test.gcp.key}")
    private String apiKey;

    @Value("${test.gcp.secret}")
    private String apiSecret;

    @Test
    @IfProfileValue(name = "test.gcp", value = "true")
    public void testFetchPricesForAllServices() throws GeneralSecurityException, IOException {
        JacksonFactory jsonFactory = new JacksonFactory();
        Cloudbilling.Builder builder = createBldr(jsonFactory);
        Cloudbilling cloudBilling = builder.build();

        File gcpresultsDir = new File("target", "gcp");
        gcpresultsDir.mkdirs();

        List<Service> allServices = fetchAllServices(cloudBilling, gcpresultsDir);

        int serviceNum = 0;
        for (Service service : allServices) {
            fetchAllSkusForService(cloudBilling, service, serviceNum, gcpresultsDir);
            serviceNum++;
        }
    }

    private List<Service> fetchAllServices(Cloudbilling cloudBilling, File gcpresultsDir) throws IOException {
        List<Service> allServices = new ArrayList<>();
        Cloudbilling.Services.List listOfServices = cloudBilling.services().list().setKey(apiKey).setPrettyPrint(true);
        ListServicesResponse servicesListResponse = listOfServices.execute();
        boolean hasNextToken = true;
        for (int servicePgNum = 0; hasNextToken; servicePgNum++) {

            String asJson = servicesListResponse.toPrettyString();
            allServices.addAll(servicesListResponse.getServices());
            log.debug("Page {} \n{}", servicePgNum, asJson);

            Files.write(new File(gcpresultsDir, String.format("services_%03d.json", servicePgNum)).toPath(), asJson.getBytes(StandardCharsets.UTF_8));

            String nextPageToken = servicesListResponse.getNextPageToken();
            hasNextToken = StringUtils.isNotBlank(nextPageToken);
            if (hasNextToken) {
                servicesListResponse = listOfServices.setPageToken(nextPageToken).execute();
            }
        }
        return allServices;
    }

    private List<Sku> fetchAllSkusForService(Cloudbilling cloudBilling, Service service, int serviceNum, File gcpresultsDir) throws IOException {
        List<Sku> allSkus = new ArrayList<>();

        Cloudbilling.Services.Skus.List skusListForService = cloudBilling.services().skus().list(service.getName()).setKey(apiKey).setPrettyPrint(true);
        ListSkusResponse listSkusResponse = skusListForService.execute();
        boolean hasNextToken = true;
        for (int pgNum = 0; hasNextToken; pgNum++) {

            String asJson = listSkusResponse.toPrettyString();
            allSkus.addAll(listSkusResponse.getSkus());
            log.debug("Page {} \n{}", pgNum, asJson);

            Files.write(new File(gcpresultsDir, String.format("sku_%03d_%03d.json", serviceNum, pgNum)).toPath(), asJson.getBytes(StandardCharsets.UTF_8));

            String nextPageToken = listSkusResponse.getNextPageToken();
            hasNextToken = StringUtils.isNotBlank(nextPageToken);
            if (hasNextToken) {
                listSkusResponse = skusListForService.setPageToken(nextPageToken).execute();
            }
        }
        return allSkus;
    }


    private Cloudbilling.Builder createBldr(JacksonFactory jsonFactory) throws GeneralSecurityException, IOException {
        return new Cloudbilling.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, null);
    }

}
