package org.ow2.morphemic.pricing.cloud_providers;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class ProductsMetadataServiceTest {

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private GenericProductRecordsRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
    }

    @Test
    public void shouldProvideInformationIfProductMetadataIsRegistered() {
        // given
        ProductId productId = new ProductId(new CloudEnvironmentPricingId(CloudProvider.AWS), "sku-1234");

        // when product is not registered:
        assertFalse(productsMetadataService.isProductMetadataInCache(productId));
        assertTrue(productsMetadataService.findCloudProviderOf(productId).isEmpty());

        // when product is registered
        productsMetadataService.createProductMetadata(productId, CloudProvider.GCP);

        assertTrue(productsMetadataService.isProductMetadataInCache(productId));
        Optional<CloudProvider> maybeCloudProvider = productsMetadataService.findCloudProviderOf(productId);
        assertTrue(maybeCloudProvider.isPresent());

        assertEquals(CloudProvider.GCP, maybeCloudProvider.get());

        // delete
        productsMetadataService.removeAllProducts();

        // then
        assertFalse(productsMetadataService.isProductMetadataInCache(productId));
        assertTrue(productsMetadataService.findCloudProviderOf(productId).isEmpty());

    }


}