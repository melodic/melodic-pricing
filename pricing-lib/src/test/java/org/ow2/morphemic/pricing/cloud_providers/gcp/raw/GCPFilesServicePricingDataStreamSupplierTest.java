package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.services.cloudbilling.model.Sku;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingUpdateService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.model.CloudProvider.GCP;

public class GCPFilesServicePricingDataStreamSupplierTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void shouldLoadDataFromEmptyDir() throws IOException {
        // given
        File scanDir = temporaryFolder.getRoot();

        // when
        List<GCPServicePricingDataStreamSupplier.ServiceWithSkusPrices> prices = new GCPFilesServicePricingDataStreamSupplier(scanDir).createDataStream().collectList().block();

        // then
        assertNotNull(prices);
        assertTrue(prices.isEmpty());
    }

    @Test
    public void shouldLoadSetOfServicesAndSKUsFromDir() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(GCP);
        File scanDir = temporaryFolder.getRoot();
        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), scanDir);
        Files.write(new File(scanDir, GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        // when
        GCPFilesServicePricingDataStreamSupplier gcpFilesServicePricingDataStreamSupplier = new GCPFilesServicePricingDataStreamSupplier(scanDir);
        assertEquals(cloudEnvironmentPricingId, gcpFilesServicePricingDataStreamSupplier.getCloudEnvironmentPricingId().get());
        List<GCPServicePricingDataStreamSupplier.ServiceWithSkusPrices> servicesWithPrices = gcpFilesServicePricingDataStreamSupplier.createDataStream().collectList().block();

        // then
        assertNotNull(servicesWithPrices);
        assertFalse(servicesWithPrices.isEmpty());
        assertEquals(2, servicesWithPrices.size());
        List<Sku> skus = servicesWithPrices.stream().flatMap(sp -> sp.getSkus().stream()).collect(Collectors.toList());
        assertEquals(16, skus.size());

        assertEquals("Directory must not contain any files and dirs after import", 0, scanDir.listFiles().length);
        servicesWithPrices.forEach(serviceWithSkusPrice -> {
            assertNotNull(serviceWithSkusPrice);
            assertNotNull(serviceWithSkusPrice.getSkus());
            serviceWithSkusPrice.getSkus().forEach(sku -> {
                assertNotNull(sku);
                assertNotNull(sku.getSkuId());
                assertNotNull(sku.getDescription());
                assertNotNull(sku.getName());
                assertNotNull(sku.getCategory());
                assertNotNull(sku.getPricingInfo());
                assertFalse(sku.getPricingInfo().isEmpty());
                assertNotNull(sku.getFactory());
            });
            assertNotNull(serviceWithSkusPrice.getServiceDef());
            assertNotNull(serviceWithSkusPrice.getServiceDef().getFactory());
        });
    }
}