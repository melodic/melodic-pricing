package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.cloudbilling.model.Money;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MappingContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPPriceOfferRate;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPriceOfferRecord;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingRecord;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPStandardTermTypes;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GcpProductIdFactory;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.*;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class GCPProductsAndOffersFactoryTest {

    private JsonFactory jsonFactory = new JacksonFactory();

    @Test
    public void shouldConvertFromRawData() throws IOException {
        // given
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("clevpr:" + CloudProvider.GCP.name() + "/test1234"), new VersionNum(1));
        GCPRawServiceSKUDataRecord rawServiceData = new GCPRawServiceSKUDataRecord(new VersionNum(123),
                Files.readString(Paths.get("src/test/data/gcp/service_1C58-CF90-9614/service.json"), UTF_8),
                Stream.of(new File("src/test/data/gcp/service_1C58-CF90-9614").listFiles()).filter(f -> f.getName().startsWith("sku_"))
                        .map(f -> {
                            try {
                                return Files.readString(f.toPath(), UTF_8);
                            } catch (IOException e) {
                                log.error(e.getMessage(), e);
                                throw new RuntimeException(e);
                            }
                        }).collect(Collectors.toList()).toArray(new String[]{}))
                .afterLoad(jsonFactory);
        rawServiceData.setCloudEnvironmentPricingId(versionedCloudEnvPricingId.getCloudEnvironmentPricingId());
        rawServiceData.setVersion(versionedCloudEnvPricingId.getVersionNum());

        GCPRawServicePricing gcpRawServicePricing = rawServiceData.toGCPRawServicePricing();

        // when
        GCPProductsAndOffersFactory prodOffersFactory = new GCPProductsAndOffersFactory(new GcpProductIdFactory());
        List<GCPProductPricingRecord> productPricingRecords = prodOffersFactory.fromRaw(gcpRawServicePricing);

        // use this file to update reference structures, when something changed!
        Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
        File jsonFile = new File("target/GCPProductsAndOffersFactoryTest/products.json");
        jsonFile.getParentFile().mkdirs();
        Files.write(jsonFile.toPath(), gson.toJson(productPricingRecords).getBytes(UTF_8), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);

        // then
        assertNotNull(productPricingRecords);
        assertEquals(2, productPricingRecords.size());
        for (GCPProductPricingRecord productPricingRecord : productPricingRecords) {
            assertEquals(rawServiceData.getVersion(), productPricingRecord.getBasedOnRawVersionNum());
            assertNotNull(productPricingRecord.getProductCode());
            assertNotNull(productPricingRecord.getTaxonomy());
            assertNotNull(productPricingRecord.getTaxonomy().getProductSkuName());
            assertEquals(GCPStandardTermTypes.ON_DEMAND.getOfferTermType(), productPricingRecord.getOfferTermType());

            for (GCPProductPriceOfferRecord offer : productPricingRecord.getOffers()) {
                assertNotNull(offer.getRates());
                assertNotNull(offer.getAggregationInfo());
                assertNotNull(offer.getEffectiveDate());

                Double lastEndRange = 0d;
                GCPPriceOfferRate.GCPUnit group = null;
                for (GCPPriceOfferRate rate : offer.getRates()) {
                    if (group == null || !group.equals(rate.getUsageUnit())) {
                        lastEndRange = 0d;
                        group = rate.getUsageUnit();
                    }

                    assertTrue(rate.getStartUsageAmount() >= 0);
                    assertEquals(lastEndRange, (Double) rate.getStartUsageAmount());
                    assertNotNull(rate.getBaseUnit());
                    assertNotNull(rate.getBaseUnit().getAbbr());
                    assertNotNull(rate.getBaseUnit().getDescription());
                    assertNotNull(rate.getUsageUnit());
                    assertNotNull(rate.getUsageUnit().getAbbr());
                    assertNotNull(rate.getUsageUnit().getDescription());
                    assertNotNull(rate.getPricePerUnitByCurrency());
                    lastEndRange = rate.getEndUsageAmount();
                }
                assertNull(lastEndRange);
            }

        }

        assertEquals(Sets.newHashSet("04F2-6383-80BD", "6BC9-D757-743E"), productPricingRecords.stream()
                .map(GCPProductPricingRecord::getProductCode)
                .collect(Collectors.toSet()));

        // finally: produced structures must be equal to reference structures
        Map<String, GCPProductPricingRecord> refProductsBySkuId = Stream.of(gson.fromJson(Files.readString(Paths.get("src/test/data/gcp_expected_results/GCPProductsAndOffersFactoryTest/test_products_1.json"), UTF_8),
                GCPProductPricingRecord[].class)).collect(Collectors.toMap(GCPProductPricingRecord::getProductCode, p -> p, (v1, v2) -> v2));

        Map<String, GCPProductPricingRecord> productPricingRecordsBySkuId = productPricingRecords.stream().collect(Collectors.toMap(GCPProductPricingRecord::getProductCode, p -> p, (v1, v2) -> v2));

        assertEquals(refProductsBySkuId.size(), productPricingRecordsBySkuId.size());
        assertEquals(refProductsBySkuId.keySet(), productPricingRecordsBySkuId.keySet());
        refProductsBySkuId.forEach((skuId, prod) -> {
            assertEquals("SkuID: " + skuId, prod, productPricingRecordsBySkuId.get(skuId));
        });

//        assertEquals(refProductsBySkuId, productPricingRecordsBySkuId);
    }

    @Test
    public void testCustomConverters() {
        Money price = new Money();
        price.setUnits(0L);
        price.setNanos(1);
        price.setCurrencyCode("USD");

        Map<String, BigDecimal> pricesByCurr = new GCPProductsAndOffersFactory.MoneyToBigDecimalConverter().convert(price, null, new MappingContext(new HashMap<>()));

        assertNotNull(pricesByCurr);
        assertEquals(1, pricesByCurr.size());
        assertEquals(new BigDecimal("0.000000001"), pricesByCurr.get("USD"));
    }

}