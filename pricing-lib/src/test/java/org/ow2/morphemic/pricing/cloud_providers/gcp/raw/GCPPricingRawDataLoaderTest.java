package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.client.json.jackson2.JacksonFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.GCP;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class GCPPricingRawDataLoaderTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private GCPRawServiceSKUDataRecordRepository gcpRawServiceSKUDataRecordRepository;

    @Autowired
    private GCPRawSKURecordRepository gcpRawSKURecordRepository;

    @Before
    public void setUp() {
        gcpRawServiceSKUDataRecordRepository.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    @Test
    public void shouldLoadAllProductsToDBCacheAndRemoveOldVersions() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(GCP);
        JacksonFactory jsonFactory = new JacksonFactory();
        File scanDir = temporaryFolder.getRoot();
        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        // when
        GCPPricingRawDataLoader loader = new GCPPricingRawDataLoader(Collections.singletonList(new GCPFilesServicePricingDataStreamSupplier(scanDir)),
                gcpRawServiceSKUDataRecordRepository, gcpRawSKURecordRepository, dataVersionsService);
        Optional<VersionedCloudEnvPricingId> maybeNewEnvVersionNum = loader.loadRawDataToCache(
                new PricingDataImportStatisticCollector(GCP, "account_id", List.of()));

        // then
        assertNotNull(maybeNewEnvVersionNum);
        assertTrue(maybeNewEnvVersionNum.isPresent());
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = maybeNewEnvVersionNum.get();
        VersionNum newVersionNum = versionedCloudEnvPricingId.getVersionNum();
        assertNotNull(newVersionNum);
        assertEquals(cloudEnvironmentPricingId, versionedCloudEnvPricingId.getCloudEnvironmentPricingId());

        assertEquals(2, gcpRawServiceSKUDataRecordRepository.count());
        for (GCPRawServiceSKUDataRecord gcpRawServiceSKUDataRecord : gcpRawServiceSKUDataRecordRepository.findAll()) {
            gcpRawServiceSKUDataRecord.afterLoad(jsonFactory);
            assertNotNull(gcpRawServiceSKUDataRecord.getGcpService());
            assertNotNull(gcpRawServiceSKUDataRecord.getGcpServiceSkus());
            assertNotNull(gcpRawServiceSKUDataRecord.getSerializedGcpServiceAsJson());
            assertNull(gcpRawServiceSKUDataRecord.getSerializedGcpServiceSkusAsJson());
            assertNotNull(gcpRawServiceSKUDataRecord.getRawSKURecords());
            assertEquals(cloudEnvironmentPricingId, gcpRawServiceSKUDataRecord.getCloudEnvironmentPricingId());
            assertEquals(newVersionNum, gcpRawServiceSKUDataRecord.getVersion());
        }

        Optional<VersionNum> maybeLastVersionNum = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, GCPRawServiceSKUDataRecord.VERSION_COLLECTION_NAME);
        assertNotNull(maybeLastVersionNum);
        assertTrue(maybeLastVersionNum.isPresent());
        assertEquals(newVersionNum, maybeLastVersionNum.get());
        VersionNum lastVersionNum = maybeLastVersionNum.get();

        // when next call when scan dir is empty: import must not erase old version
        maybeNewEnvVersionNum = loader.loadRawDataToCache(new PricingDataImportStatisticCollector(GCP, "account_id", List.of()));

        // then
        assertNotNull(maybeNewEnvVersionNum);
        assertFalse(maybeNewEnvVersionNum.isPresent());
        assertEquals(2, gcpRawServiceSKUDataRecordRepository.count());

        assertEquals(lastVersionNum, dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, GCPRawServiceSKUDataRecord.VERSION_COLLECTION_NAME).get());

        // when - new data import
        FileSystemUtils.copyRecursively(new File("src/test/data/gcp/service_1C58-CF90-9614"), new File(scanDir, "test"));
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        maybeNewEnvVersionNum = loader.loadRawDataToCache(new PricingDataImportStatisticCollector(GCP, "account_id", List.of()));

        // then
        assertNotNull(maybeNewEnvVersionNum);
        assertTrue(maybeNewEnvVersionNum.isPresent());
        versionedCloudEnvPricingId = maybeNewEnvVersionNum.get();

        VersionNum nextNewVersionNum = versionedCloudEnvPricingId.getVersionNum();
        assertNotEquals(newVersionNum, nextNewVersionNum);
        assertEquals(cloudEnvironmentPricingId, versionedCloudEnvPricingId.getCloudEnvironmentPricingId());

        assertEquals(1, gcpRawServiceSKUDataRecordRepository.count());
        for (GCPRawServiceSKUDataRecord gcpRawServiceSKUDataRecord : gcpRawServiceSKUDataRecordRepository.findAll()) {
            gcpRawServiceSKUDataRecord.afterLoad(jsonFactory);
            assertNotNull(gcpRawServiceSKUDataRecord.getGcpService());
            assertNotNull(gcpRawServiceSKUDataRecord.getGcpServiceSkus());
            assertNotNull(gcpRawServiceSKUDataRecord.getSerializedGcpServiceAsJson());
            assertNull(gcpRawServiceSKUDataRecord.getSerializedGcpServiceSkusAsJson());
            assertEquals(cloudEnvironmentPricingId, gcpRawServiceSKUDataRecord.getCloudEnvironmentPricingId());
            assertEquals(nextNewVersionNum, gcpRawServiceSKUDataRecord.getVersion());
        }

        maybeLastVersionNum = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, GCPRawServiceSKUDataRecord.VERSION_COLLECTION_NAME);
        assertNotNull(maybeLastVersionNum);
        assertTrue(maybeLastVersionNum.isPresent());
        assertEquals(nextNewVersionNum, maybeLastVersionNum.get());

    }
}