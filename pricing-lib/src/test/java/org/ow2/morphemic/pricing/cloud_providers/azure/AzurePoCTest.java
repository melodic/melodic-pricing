package org.ow2.morphemic.pricing.cloud_providers.azure;

import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;
import com.microsoft.store.partnercenter.AuthenticationToken;
import com.microsoft.store.partnercenter.IAggregatePartner;
import com.microsoft.store.partnercenter.IPartnerCredentials;
import com.microsoft.store.partnercenter.PartnerService;
import com.microsoft.store.partnercenter.extensions.PartnerCredentials;
import com.microsoft.store.partnercenter.extensions.UserPartnerCredentials;
import com.microsoft.store.partnercenter.models.ratecards.AzureRateCard;
import com.microsoft.store.partnercenter.ratecards.IAzureRateCard;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.naming.ServiceUnavailableException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * PoC for AZURE.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = PricingConfiguration.class)
@ActiveProfiles({"localtestazure", "test"})
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class AzurePoCTest {

    @Value("${test.azure.azureApplicationDomain}")
    private String azureApplicationDomain;

    @Value("${test.azure.secret}")
    private String azureSecret;

    @Value("${test.azure.azureAppId}")
    private String azureAppId;

    @Value("${test.azure.tenantId}")
    private String azureTenantId;

    @Value("${test.azure.subscriptionId}")
    private String azureSubscriptionId;


    /**
     * Documentation: https://docs.microsoft.com/en-us/partner-center/develop/partner-center-authentication
     */
    @Ignore
    @IfProfileValue(name = "test.azure", value = "true")
    public void testAndDowloadPricingData() {
        IAggregatePartner partner = getAppPartnerOperations();
        IAzureRateCard azureRateCard = partner.getRateCards().getAzure();

        AzureRateCard rateCard = azureRateCard.get(null, null);
        log.info("rate card: {}", rateCard);

    }

    @Ignore
    @IfProfileValue(name = "test.azure", value = "true")
    public void testAzurePricingService() throws Exception {
        AzurePoCPricingService azurePoCPricingService = new AzurePoCPricingService(azureTenantId, azureSubscriptionId, azureAppId, azureSecret);
        azurePoCPricingService.getAzurePricing();

    }

    @Test
    @IfProfileValue(name = "test.azure", value = "true")
    public void testAzurePricingService2() throws Exception {
        AzurePoCPricingService azurePoCPricingService = new AzurePoCPricingService(azureTenantId, azureSubscriptionId, azureAppId, azureSecret);
        AuthenticationResult accessTokenFromUserCredentials = azurePoCPricingService.getAccessTokenFromUserCredentials();
        AuthenticationToken authenticationToken = new AuthenticationToken(accessTokenFromUserCredentials.getAccessToken(), new DateTime(accessTokenFromUserCredentials.getExpiresOnDate().getTime()));

        IAggregatePartner partner = PartnerService.getInstance().createPartnerOperations(new UserPartnerCredentials(azureAppId, authenticationToken));

        IAzureRateCard azureRateCard = partner.getRateCards().getAzure();

        AzureRateCard rateCard = azureRateCard.get("EUR", "IE");
        log.info("rate card: {}", rateCard);
    }


    /**
     * https://docs.microsoft.com/en-us/partner-center/develop/partner-center-authentication
     *
     * @return
     */
    private IAggregatePartner getAppPartnerOperations() {
        //new AuthenticationToken()
        IPartnerCredentials appCredentials =
                PartnerCredentials.getInstance().generateByApplicationCredentials(
                        "23452345234523523452",
                        "23542353242354",
                        "23452352354");

        return PartnerService.getInstance().createPartnerOperations(appCredentials);
    }

    /**
     * https://docs.microsoft.com/en-us/partner-center/develop/partner-center-authentication
     *
     * @return
     */
    private IAggregatePartner getAppPartnerOperations(IPartnerCredentials appCredentials) {
        return PartnerService.getInstance().createPartnerOperations(appCredentials);
    }
}

@Slf4j
class AzurePoCPricingService {

    private final String AUTHORITY;
    private final String CLIENT_ID;
    private final String CLIENT_SECRET;
    private final String SKUs_URL;
    private final String RATE_CARD_URL;

    private final String subscriptionId;

    public AzurePoCPricingService(String tenantId, String subscriptionId, String clientId, String clientSecret) throws UnsupportedEncodingException {
        AUTHORITY = String.format("https://login.windows.net/%s", tenantId);
        SKUs_URL = String.format("https://management.azure.com/subscriptions/%s/providers/Microsoft.Compute/skus?api-version=2017-09-01", subscriptionId);
        //RATE_CARD_URL = String.format("https://management.azure.com/subscriptions/%s/providers/Microsoft.Commerce/RateCard?api-version=2016-08-31-preview&%s", subscriptionId, URLEncoder.encode("$filter=OfferDurableId eq 'MS-AZR-0003P' and Currency eq 'USD' and Locale eq 'en-US' and RegionInfo eq 'US'", "UTF-8"));
        RATE_CARD_URL = String.format("https://management.azure.com/subscriptions/%s/providers/Microsoft.Commerce/RateCard?api-version=2016-08-31-preview&$filter=OfferDurableId+eq+'MS-AZR-0003P'+and+Currency+eq+'EUR'+and+Locale+eq+'en-US'+and+RegionInfo+eq+'PL'", subscriptionId);
        CLIENT_ID = clientId;
        CLIENT_SECRET = clientSecret;
        this.subscriptionId = subscriptionId;
    }

    public void getAzurePricing_NOT_USED() {
        log.info("getAzurePricing");

        IPartnerCredentials appCredentials = PartnerCredentials.getInstance().generateByApplicationCredentials("uuid", "uuid", "uuid", "https://login.microsoftonline.com/", "https://graph.microsoft.com/");
        //generateByApplicationCredentials
        log.info("IPartnerCredentials");

        log.info((appCredentials.getExpiresAt()).toString());
        log.info("" + appCredentials.isExpired());

        IAggregatePartner partnerOperations = PartnerService.getInstance().createPartnerOperations(appCredentials);
        log.info("IPartner");

        try {
            AzureRateCard azureRateCard = partnerOperations.getRateCards().getAzure().get("", "");
            //AzureRateCard azureRateCard = partnerOperations.getRateCards().getAzure().get("EUR", "IE");
            log.info("AzureRateCard");
            //log.info(azureRateCard.getCurrency());
            log.info(azureRateCard.toString());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public AuthenticationResult getAccessTokenFromUserCredentials() throws Exception {
        AuthenticationContext context = null;
        AuthenticationResult result = null;
        ExecutorService service = null;
        log.info(RATE_CARD_URL);
        try {
            service = Executors.newFixedThreadPool(1);
            context = new AuthenticationContext(AUTHORITY, false, service);
            ClientCredential credential = new ClientCredential(CLIENT_ID, CLIENT_SECRET);
            Future<AuthenticationResult> future = context.acquireToken("https://management.azure.com/", credential, null);
            result = future.get();
        } finally {
            service.shutdown();
        }
        if (result == null) {
            throw new ServiceUnavailableException("authentication result was null");
        }
        return result;
    }

    public void getAzurePricing() throws Exception {
        AuthenticationResult result = getAccessTokenFromUserCredentials();
        log.info("Access Token - " + result.getAccessToken());
        log.info("Access Token Expires On - " + result.getExpiresOnDate());

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            StringBuilder builder = new StringBuilder();
            HttpGet request = new HttpGet("https://management.azure.com/subscriptions/" + subscriptionId + "?api-version=2019-06-01");
            request.addHeader("Authorization", "Bearer " + result.getAccessToken());
            HttpResponse response = httpClient.execute(request);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            int lines = 0;
            while ((line = rd.readLine()) != null) {
                builder.append(line);
                lines++;
            }
            rd.close();
            log.info("lines: " + lines);
            BufferedWriter writer = new BufferedWriter(new FileWriter("target/azure_rest_out.json"));
            writer.write(builder.toString());
            writer.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
