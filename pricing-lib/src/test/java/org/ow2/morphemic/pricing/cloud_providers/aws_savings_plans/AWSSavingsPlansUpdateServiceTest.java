package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.AWSRawSavingsPlansManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption;
import org.ow2.morphemic.pricing.model.aws_savings_plans.AWSSavingsPlansProductIdFactory;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.model.CloudProvider.AWS;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AWSSavingsPlansUpdateServiceTest {

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AWSSavingsPlansPricingRepository awsSavingsPlansPricingRepository;

    @Autowired
    private AWSSavingsPlansRepository awsSavingsPlansRepository;

    @Autowired
    private AWSSavingsPlansUpdateService awsSavingsPlansUpdateService;
    @Autowired
    private AWSSavingsPlansProductPricingService awsSavingsPlansProductPricingService;


    private final AWSSavingsPlansProductIdFactory awsSavingsPlansProductIdFactory = new AWSSavingsPlansProductIdFactory();


    @Value("${pricing.datadump.aws.savingsplans.dir}")
    private String awsDumpDir;

    private File scanDir;

    @Autowired
    private AWSRawSavingsPlansManager rawManager;

    @Before
    public void setUp() {
        awsSavingsPlansProductPricingService.deleteAll();
        rawManager.deleteAll();

        dataVersionsService.removeAllVersions();
        scanDir = new File(awsDumpDir);
        scanDir.mkdirs();
        var files = scanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }
    }

    @Test
    public void loadPricingDumpDataFromFilesTest_noFile() throws IOException {
        Optional<VersionedCloudEnvPricingId> versionedCloudEnvPricingId = awsSavingsPlansUpdateService.loadPricingDumpDataFromFiles();

        // then
        assertNotNull(versionedCloudEnvPricingId);
        assertTrue(versionedCloudEnvPricingId.isEmpty());
        assertTrue(new File(awsDumpDir).isDirectory());
    }

    @Test
    public void loadPricingDumpDataFromFilesTest() throws IOException {
        AWSSavingsPlansTestUtils.prepareFileToRunLoadPricingFromFiles(scanDir, "src/test/data/aws/savings_plans/v1");

        Optional<VersionedCloudEnvPricingId> versionedCloudEnvPricingId = awsSavingsPlansUpdateService.loadPricingDumpDataFromFiles();

        var files = scanDir.listFiles();
        assertEquals(0, files.length);

        assertNotNull(versionedCloudEnvPricingId);
        assertTrue(versionedCloudEnvPricingId.isPresent());
        assertTrue(new File(awsDumpDir).isDirectory());

        assertEquals(2943, awsSavingsPlansPricingRepository.findAllByVersion(versionedCloudEnvPricingId.get().getVersionNum()).size());

        String awsSavingsPlansSku = "88BSZA59PQRXTMGW";

        assertOneSavingsPlansPricingRecord(versionedCloudEnvPricingId.get().getVersionNum(), awsSavingsPlansSku);
        assertOneSavingsPlansProductRecord(versionedCloudEnvPricingId.get().getVersionNum(), awsSavingsPlansSku);
    }

    /**
     * Proof of concept test for obtain all data and save in db.
     */
    @Ignore
    public void loadPricingDumpDataFromAws() {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AWS);

        awsSavingsPlansUpdateService.updateUsingExternalApi(cloudEnvironmentPricingId, new PricingDataImportStatisticCollector(CloudProvider.AWS,
                "test", List.of()));

        assertTrue(awsSavingsPlansPricingRepository.count() > 0);
        assertTrue(awsSavingsPlansRepository.count() > 0);
    }

    private void assertOneSavingsPlansPricingRecord(VersionNum versionNum, String awsSavingsPlansSku) {
        String awsProductDiscountedSku = "22RRGT3PBVP2XF8D";

        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.AWS, awsSavingsPlansSku);
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, versionNum);

        ProductId productId = awsSavingsPlansProductIdFactory.createSavingsPlansProductId(versionedCloudEnvPricingId, awsProductDiscountedSku);

        Optional<AWSSavingsPlansPricingRecord> savingsPlanRateRecord = awsSavingsPlansPricingRepository.findById(productId);

        assertTrue(savingsPlanRateRecord.isPresent());

        AWSSavingsPlansPricingRecord awsSavingsPlansPricingRecord = savingsPlanRateRecord.get();

        assertNotNull(awsSavingsPlansPricingRecord.getPricePerUnitByCurrency().get("USD"));

        assertEquals(new BigDecimal("6.03"), awsSavingsPlansPricingRecord.getPricePerUnitByCurrency().get("USD"));
        assertEquals(new Unit("Hrs"), awsSavingsPlansPricingRecord.getUnit());
        assertEquals(awsProductDiscountedSku, awsSavingsPlansPricingRecord.getProductSkuId());
        assertEquals(versionedCloudEnvPricingId.getVersionNum(), awsSavingsPlansPricingRecord.getVersion());
        assertEquals(awsSavingsPlansSku + "." + awsProductDiscountedSku, awsSavingsPlansPricingRecord.getRateCode());
    }

    private void assertOneSavingsPlansProductRecord(VersionNum versionNum, String awsSavingsPlansSku) {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.AWS, awsSavingsPlansSku);
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, versionNum);

        Optional<AWSSavingsPlansRecord> savingsPlansProductRecord = awsSavingsPlansRepository.findByCloudEnvironmentPricingIdAndVersion(cloudEnvironmentPricingId, versionNum);

        assertTrue(savingsPlansProductRecord.isPresent());
        AWSSavingsPlansRecord awsSavingsPlansRecord = savingsPlansProductRecord.get();

        assertEquals(versionedCloudEnvPricingId.getVersionNum(), awsSavingsPlansRecord.getVersion());
        assertEquals(versionedCloudEnvPricingId.getCloudEnvironmentPricingId(), awsSavingsPlansRecord.getCloudEnvironmentPricingId());

        AWSPurchaseOption expectedPurchaseOption = AWSPurchaseOption.PARTIAL_UPFRONT;
        Period expectedPurchaseTerm = Period.ofYears(1);
        String expectedInstanceType = "x2iezn";

        assertEquals(expectedPurchaseOption, awsSavingsPlansRecord.getPurchaseOption());
        assertEquals(expectedInstanceType, awsSavingsPlansRecord.getInstanceType());
        assertEquals(expectedPurchaseTerm, awsSavingsPlansRecord.getPurchaseTerm());

        List<VersionedCloudEnvPricingId> savingsPlanIds = awsSavingsPlansProductPricingService.findEc2InstanceSavingsPlansBy(
                expectedPurchaseOption, expectedPurchaseTerm, expectedInstanceType, "us-east-1");
        assertEquals(1, savingsPlanIds.size());
        assertEquals(versionedCloudEnvPricingId, savingsPlanIds.get(0));

        savingsPlanIds = awsSavingsPlansProductPricingService.findComputeSavingsPlansBy(
                AWSPurchaseOption.NO_UPFRONT, expectedPurchaseTerm);
        assertEquals(1, savingsPlanIds.size());

        // test removing
        long elementsCountBeforeDeleteInAwsSavingsPlansRepo = awsSavingsPlansRepository.count();
        long elementsCountBeforeInPricingRepo = awsSavingsPlansPricingRepository.count();

        awsSavingsPlansProductPricingService.removeAllOldVersionsSavingsPlansPricing(1);
        assertEquals(elementsCountBeforeDeleteInAwsSavingsPlansRepo, awsSavingsPlansRepository.count());
        assertEquals(elementsCountBeforeInPricingRepo, awsSavingsPlansPricingRepository.count());

        awsSavingsPlansProductPricingService.removeAllOldVersionsSavingsPlansPricing(0);
        assertEquals(0L, awsSavingsPlansRepository.count());
        assertEquals(0L, awsSavingsPlansPricingRepository.count());

        awsSavingsPlansProductPricingService.removeAllOldVersionsSavingsPlansPricing(-1);
        assertEquals(0L, awsSavingsPlansRepository.count());
        assertEquals(0L, awsSavingsPlansPricingRepository.count());

    }
}
