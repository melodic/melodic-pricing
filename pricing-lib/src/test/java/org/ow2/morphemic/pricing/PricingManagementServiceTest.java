package org.ow2.morphemic.pricing;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSTestingSupportService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureTestingSupportService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPTestingSupportService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class PricingManagementServiceTest {

    @Autowired
    private PricingManagementService pricingManagementService;

    @Autowired
    private GCPTestingSupportService gcpTestingSupportService;

    @Autowired
    private AWSTestingSupportService awsTestingSupportService;

    @Autowired
    private AWSProductPricingUpdateService awsProductPricingUpdateService;

    @Autowired
    private AzureTestingSupportService azureTestingSupportService;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AWSProductPricingService awsProductPricingService;

    @Autowired
    private AzureProductPricingUpdateService azureProductPricingUpdateService;

    @Autowired
    private GCPProductPricingUpdateService gcpProductPricingUpdateService;

    @Autowired
    private AzureProductPricingService azureProductPricingService;

    @Autowired
    private GCPProductPricingService gcpProductPricingService;

    @Autowired
    private GCPAggregatedProductsService gcpAggregatedProductsService;

    @Before
    public void setUp() {
        awsProductPricingService.deleteAll();
        azureProductPricingService.deleteAll();
        gcpAggregatedProductsService.deleteAll();
        gcpProductPricingService.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    /**
     * Deletion of one pricing must not affect others.
     */
    @Test
    public void shouldDeleteOnlyGivenPricing() throws IOException {

        for (CloudProvider testedCloudProvider : CloudProvider.values()) {
            if (testedCloudProvider == CloudProvider.ON_PREMISE) {
                continue;
            }

            Map<CloudProvider, TestScenario> testScenariosByCp = createTestScenarios();
            TestScenario scenario = testScenariosByCp.get(testedCloudProvider);

            List<PricingStats> pricingStatsBefore = pricingManagementService.fetchPricingStats();
            List<CloudProviderStats> cloudProviderStatsBefore = pricingManagementService.fetchCloudProviderStats();


            // when removed pricing for current tested cloud provider

            pricingManagementService.removePricing(scenario.getPricing1());

            List<PricingStats> pricingStatsAfter = pricingManagementService.fetchPricingStats();
            List<CloudProviderStats> cloudProviderStatsAfter = pricingManagementService.fetchCloudProviderStats();

            // then other pricing data must not be affected.
            assertEquals(pricingStatsBefore.size() - 1, pricingStatsAfter.size());
            assertEquals(cloudProviderStatsBefore.size(), cloudProviderStatsAfter.size());


            testScenariosByCp.forEach((cldProv, checkScenario) -> {
                switch (cldProv) {
                    case AWS:
                        if(testedCloudProvider != cldProv) {
                            assertEquals(1, awsProductPricingService.allProductIds(checkScenario.getPricing1()).size());
                        }
                        assertEquals(1, awsProductPricingService.allProductIds(checkScenario.getPricing2()).size());
                        break;
                    case AZURE:
                        if(testedCloudProvider != cldProv) {
                            assertEquals(249, azureProductPricingService.allProductIds(checkScenario.getPricing1()).size());
                        }
                        assertEquals(249, azureProductPricingService.allProductIds(checkScenario.getPricing2()).size());
                        break;
                    case GCP:
                        if(testedCloudProvider != cldProv) {
                            assertEquals(16, gcpProductPricingService.allProductIds(checkScenario.getPricing1()).size());
                            assertEquals(5, gcpAggregatedProductsService.allProductIds(checkScenario.getPricing1()).size());
                        }
                        assertEquals(16, gcpProductPricingService.allProductIds(checkScenario.getPricing2()).size());
                        assertEquals(5, gcpAggregatedProductsService.allProductIds(checkScenario.getPricing2()).size());
                        break;
                }

                assertEquals(dataVersionsService.hasAnyVersion(checkScenario.getPricing1()), (testedCloudProvider != cldProv));
                assertTrue(dataVersionsService.hasAnyVersion(checkScenario.getPricing2()));
            });

            setUp();
        }


    }

    private Map<CloudProvider, TestScenario> createTestScenarios() throws IOException {
        Map<CloudProvider, TestScenario> testScenariosByCp = new LinkedHashMap<>();
        for (CloudProvider cp : CloudProvider.values()) {
            if(cp == CloudProvider.ON_PREMISE) {
                continue;
            }

            TestScenario scenario = new TestScenario(cp);
            testScenariosByCp.put(cp, scenario);

            switch (cp) {
                case AWS:
                    for (CloudEnvironmentPricingId pricingId : scenario.pricingIds()) {
                        awsTestingSupportService.fillSourceDirectoryWithSamplePricingFiles(pricingId, 10, 5);
                        awsProductPricingUpdateService.loadPricingDumpDataFromFiles();
                    }
                    break;
                case AZURE:
                    for (CloudEnvironmentPricingId pricingId : scenario.pricingIds()) {
                        azureTestingSupportService.prepareTestPricingToLoad(pricingId);
                        azureProductPricingUpdateService.loadPricingDumpDataFromFiles();
                    }
                    break;

                case GCP:
                    for (CloudEnvironmentPricingId pricingId : scenario.pricingIds()) {
                        gcpTestingSupportService.prepareTestPricingToLoad(pricingId);
                        gcpProductPricingUpdateService.loadPricingDumpDataFromFiles();
                    }
                    break;
                default:
                    throw new UnsupportedOperationException("Not supported cloud provider: " + cp);
            }
        }
        return testScenariosByCp;
    }

    @Data
    private static class TestScenario {
        private final CloudEnvironmentPricingId pricing1;
        private final CloudEnvironmentPricingId pricing2;

        TestScenario(CloudProvider cp) {
            this.pricing1 = new CloudEnvironmentPricingId(cp);
            this.pricing2 = new CloudEnvironmentPricingId(cp);
        }

        CloudEnvironmentPricingId[] pricingIds() {
            return new CloudEnvironmentPricingId[]{pricing1, pricing2};
        }
    }

}