package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.*;
import static org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSRawPricingDataRecord.VERSION_COLLECTION_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AWS;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AWSPricingRawDataLoaderTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AWSRawPricingDataRepository awsRawPricingDataRepository;

    @Before
    public void setUp() {
        awsRawPricingDataRepository.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    @Test
    public void shouldLoadAllProductsToDBCacheAndRemoveOldVersions() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.AWS);
        File scanDir = temporaryFolder.getRoot();
        String jsonContents = Files.readString(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), UTF_8);
        fillSourceDirectoryWithSamplePricingFiles(cloudEnvironmentPricingId, scanDir, 5, 3);

        // when
        AWSPricingRawDataLoader loader = new AWSPricingRawDataLoader(Collections.singletonList(new AWSFilesPricingDataStreamSupplier(scanDir)),
                awsRawPricingDataRepository, dataVersionsService);
        Optional<VersionedCloudEnvPricingId> maybeNewEnvVersionNum = loader.loadRawDataToCache(new PricingDataImportStatisticCollector(AWS, "account_id", List.of()));

        // then
        assertNotNull(maybeNewEnvVersionNum);
        assertTrue(maybeNewEnvVersionNum.isPresent());
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = maybeNewEnvVersionNum.get();
        assertEquals(cloudEnvironmentPricingId, versionedCloudEnvPricingId.getCloudEnvironmentPricingId());

        assertEquals(15, awsRawPricingDataRepository.count());
        List<AWSRawPricingDataRecord> rawPricingDataRecords = StreamSupport.stream(awsRawPricingDataRepository.findAll().spliterator(), false).collect(Collectors.toList());
        assertEquals(15, rawPricingDataRecords.size());
        Optional<VersionNum> maybeLastVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, VERSION_COLLECTION_NAME);
        assertTrue(maybeLastVersion.isPresent());
        for (AWSRawPricingDataRecord rawPricingDataRecord : rawPricingDataRecords) {
            assertNotNull(rawPricingDataRecord.getId());
            assertEquals(rawPricingDataRecord.getVersion(), maybeLastVersion.get());
            assertEquals(jsonContents, rawPricingDataRecord.getRawContents());
            // TODO
            // assertNotNull(r.getPricingForProduct());
        }

        VersionNum lastVersionNum = versionedCloudEnvPricingId.getVersionNum();
        assertNotNull(lastVersionNum);
        assertEquals(lastVersionNum, maybeLastVersion.get());


        // when next call when scan dir is empty: it must not
        maybeNewEnvVersionNum = loader.loadRawDataToCache(new PricingDataImportStatisticCollector(AWS, "account_id", List.of()));
        // then
        assertNotNull(maybeNewEnvVersionNum);
        assertFalse(maybeNewEnvVersionNum.isPresent());

        assertEquals(15, awsRawPricingDataRepository.count());
        assertEquals(lastVersionNum, dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, VERSION_COLLECTION_NAME).get());

        // given: new dataset: should replace old one
        fillSourceDirectoryWithSamplePricingFiles(cloudEnvironmentPricingId, scanDir, 2, 4);

        // when next call when scan dir is NOT empty: import must erase new version
        maybeNewEnvVersionNum = loader.loadRawDataToCache(new PricingDataImportStatisticCollector(AWS, "account_id", List.of()));

        // then
        assertNotNull(maybeNewEnvVersionNum);
        assertTrue(maybeNewEnvVersionNum.isPresent());
        assertEquals(cloudEnvironmentPricingId, maybeNewEnvVersionNum.get().getCloudEnvironmentPricingId());

        assertEquals(8, awsRawPricingDataRepository.count());
        VersionNum prevVersion = maybeLastVersion.get();
        maybeLastVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, VERSION_COLLECTION_NAME);
        assertTrue(maybeLastVersion.isPresent());
        assertNotEquals(prevVersion, maybeLastVersion.get());
        assertEquals(maybeNewEnvVersionNum.get().getVersionNum(), maybeLastVersion.get());

        rawPricingDataRecords = StreamSupport.stream(awsRawPricingDataRepository.findAll().spliterator(), false).collect(Collectors.toList());
        for (AWSRawPricingDataRecord r : rawPricingDataRecords) {
            assertNotNull(r.getId());
            assertEquals(r.getVersion(), maybeLastVersion.get());
            assertEquals(jsonContents, r.getRawContents());
            // TODO
            // assertNotNull(r.getPricingForProduct());
        }

    }

    private void fillSourceDirectoryWithSamplePricingFiles(CloudEnvironmentPricingId cloudEnvironmentPricingId, File rootDir, int dirsCount, int filesInDir) throws IOException {
        for (int i = 0; i < dirsCount; i++) {
            File packDir = new File(rootDir, String.format("p%02d", i));
            packDir.mkdirs();
            for (int rec = 0; rec < filesInDir; rec++) {
                Files.copy(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), packDir.toPath().resolve(String.format("p_%06d.json", rec)));
            }
        }

        Files.write(new File(rootDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(UTF_8));
    }


}
