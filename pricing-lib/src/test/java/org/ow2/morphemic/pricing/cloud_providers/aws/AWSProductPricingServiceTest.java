package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService.VERSIONED_AWS_PRICING_COLLECTION_NAME;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AWSProductPricingServiceTest {

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AWSProductPricingService productPricingService;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private AWSProductRecordsRepository awsProductRecordsRepository;

    @Autowired
    private AWSProductPricingRecordsRepository awsProductPricingRecordsRepository;

    @Autowired
    private AWSProductPricingUpdateService productPricingUpdateService;

    @Autowired
    private AWSTestingSupportService awsTestingSupportService;

    @Before
    public void setUp() throws IOException {
        awsProductPricingRecordsRepository.deleteAll();
        awsProductRecordsRepository.deleteAll();
        productsMetadataService.removeAllProducts();
    }

    @Test
    public void shouldLoadProductWithPricingParams() {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId("test1234");
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, new VersionNum(1));
        ProductId prodId = new ProductId(cloudEnvironmentPricingId, "ABC1234");

        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, VERSIONED_AWS_PRICING_COLLECTION_NAME);

        AWSProductRecord productRecord = AWSProductRecord.builder()
                .id(prodId)
                .skuId("ABC1234")
                .cloudEnvironmentPricingId(cloudEnvironmentPricingId)
                .basedOnRawVersionNum(versionedCloudEnvPricingId.getVersionNum())
                .serviceCode("AmazonEC2").build();
        awsProductRecordsRepository.save(productRecord);

        AWSProductPricingRecord pricingRecord = AWSProductPricingRecord.builder()
                .id(prodId)
                .productSkuId(productRecord.getSkuId())
                .cloudEnvironmentPricingId(cloudEnvironmentPricingId)
                .basedOnRawVersionNum(versionedCloudEnvPricingId.getVersionNum())
                .offers(Arrays.asList(AWSOffersTermsRecord.builder()
                        .offerTermType(new OfferTermType("OnDemand"))
                        .productSkuId(productRecord.getSkuId()).build())).build();
        awsProductPricingRecordsRepository.save(pricingRecord);

        // when
        Optional<AWSProductPricingService.ProductWithPricing> maybeProductWithPricing = productPricingService
                .findProduct(prodId);

        Set<ProductId> ProductIds = productPricingService.allProductIds(cloudEnvironmentPricingId);

        // then
        assertNotNull(ProductIds);
        assertEquals(1, ProductIds.size());
        assertTrue(ProductIds.contains(prodId));

        assertNotNull(maybeProductWithPricing);
        assertTrue(maybeProductWithPricing.isPresent());
        AWSProductPricingService.ProductWithPricing productWithPricing = maybeProductWithPricing.get();
        assertEquals(productRecord, productWithPricing.getProductRecord());
        assertEquals(pricingRecord, productWithPricing.getProductPricingRecord());
        assertEquals(prodId, productWithPricing.getProductId());
    }

    @Test
    public void shouldReturnProductsFromTheSameProductFamily() {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId("test1234");
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, new VersionNum(1));

        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, VERSIONED_AWS_PRICING_COLLECTION_NAME);

        for (int p = 0; p < 10; p++) {
            ProductId prodId = new ProductId(cloudEnvironmentPricingId, "p:ABC1234_" + p);
            int prodFamilyIdx = p % 2;
            AWSProductRecord productRecord = AWSProductRecord.builder()
                    .id(prodId)
                    .productFamily("Product Family " + prodFamilyIdx)
                    .skuId("ABC1234" + p)
                    .cloudEnvironmentPricingId(cloudEnvironmentPricingId)
                    .basedOnRawVersionNum(versionedCloudEnvPricingId.getVersionNum())
                    .serviceCode("AmazonEC2").build();
            awsProductRecordsRepository.save(productRecord);
        }

        // when
        Set<ProductId> foundProductIds = productPricingService.findProductsByFamilyName(cloudEnvironmentPricingId, "Product Family 0");

        // then
        assertNotNull(foundProductIds);
        assertEquals(5, foundProductIds.size());
        for (int p = 0; p < 10; p += 2) {
            assertTrue(foundProductIds.contains(new ProductId(cloudEnvironmentPricingId, "p:ABC1234_" + p)));
        }
    }

    @Test
    public void shouldReturnProductsFromTheSameProductFamilyForRealData() throws IOException {
        // given
        CloudEnvironmentPricingId pricingId = new CloudEnvironmentPricingId(CloudProvider.AWS);
        awsTestingSupportService.fillSourceDirectoryWithSamplePricingFiles(pricingId, 5, 3);

        productPricingUpdateService.loadPricingDumpDataFromFiles();
        // when

        Set<ProductId> foundProductIds = productPricingService.findProductsByFamilyName(pricingId, "Compute Instance");

        // then
        assertNotNull(foundProductIds);
        assertEquals(1, foundProductIds.size());
    }

    @Test
    public void shouldDeletePricingData() throws IOException {
        // given
        CloudEnvironmentPricingId pricing1 = new CloudEnvironmentPricingId(CloudProvider.AWS);
        CloudEnvironmentPricingId pricing2 = new CloudEnvironmentPricingId(CloudProvider.AWS);
        assertNotEquals(pricing1, pricing2);

        for (CloudEnvironmentPricingId pricingId : new CloudEnvironmentPricingId[]{pricing1, pricing2}) {
            awsTestingSupportService.fillSourceDirectoryWithSamplePricingFiles(pricingId, 5, 3);
            productPricingUpdateService.loadPricingDumpDataFromFiles();
        }
        String familyName = "Compute Instance";

        // when
        assertEquals(1, productPricingService.findProductsByFamilyName(pricing1, familyName).size());
        assertEquals(1, productPricingService.findProductsByFamilyName(pricing2, familyName).size());

        Set<ProductId> productPr1Ids = productPricingService.allProductIds(pricing1);

        productPricingService.deletePricing(pricing1);

        // then
        assertTrue(productPricingService.findProductsByFamilyName(pricing1, familyName).isEmpty());
        assertEquals(1, productPricingService.findProductsByFamilyName(pricing2, familyName).size());

        assertTrue(productPricingService.allProductIds(pricing1).isEmpty());
        Set<ProductId> productPr2Ids = productPricingService.allProductIds(pricing2);
        assertEquals(1, productPr2Ids.size());

        productPr1Ids.forEach(productId -> {
            assertTrue(productsMetadataService.findCloudProviderOf(productId).isEmpty());
        });

        productPr2Ids.forEach(productId -> {
            Optional<CloudProvider> foundClProv = productsMetadataService.findCloudProviderOf(productId);
            assertTrue(foundClProv.isPresent());
            assertEquals(CloudProvider.AWS, foundClProv.get());
        });

        assertFalse(dataVersionsService.hasAnyVersion(pricing1));
        assertTrue(dataVersionsService.hasAnyVersion(pricing2));
    }

}