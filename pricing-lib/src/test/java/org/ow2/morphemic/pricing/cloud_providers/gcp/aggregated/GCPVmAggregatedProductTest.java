package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import org.junit.Test;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class GCPVmAggregatedProductTest {

    @Test
    public void testTypicalMergeOperation() {

        ProductId p1 = new ProductId("p1");
        GCPVmAggregatedProduct product1 = GCPVmAggregatedProduct.builder()
                .productId(p1)
                .offerTermTypes(Set.of(new OfferTermType("OnDemand")))
                .regions(Set.of(new Place("r1")))
                .components(Arrays.asList(
                        GCPBasicProduct.builder().productId(new ProductId("sku1")).regions(Set.of(new Place("r1"))).build(),
                        GCPBasicProduct.builder().productId(new ProductId("sku2")).regions(Set.of(new Place("r1"))).build()))
                .build();

        GCPVmAggregatedProduct product2 = GCPVmAggregatedProduct.builder()
                .productId(p1)
                .offerTermTypes(Set.of(new OfferTermType("Reserved")))
                .regions(Set.of(new Place("r2")))
                .components(Arrays.asList(
                        GCPBasicProduct.builder().productId(new ProductId("sku1")).regions(Set.of(new Place("r2"))).build(),
                        GCPBasicProduct.builder().productId(new ProductId("sku2")).regions(Set.of(new Place("r2"))).build(),
                        GCPBasicProduct.builder().productId(new ProductId("sku3")).regions(Set.of(new Place("r2"))).build(),
                        GCPBasicProduct.builder().productId(new ProductId("sku4")).regions(Set.of(new Place("r2"))).build()))
                .build();

        // when
        product1.merge(product2);

        // then
        assertEquals(Set.of(new Place("r2"), new Place("r1")), product1.getRegions());
        assertEquals(Set.of(new OfferTermType("OnDemand"), new OfferTermType("Reserved")), product1.getOfferTermTypes());

        assertEquals(Set.of(new ProductId("sku1"), new ProductId("sku2"), new ProductId("sku3"), new ProductId("sku4")), product1.getComponents().stream().map(GCPBasicProduct::getProductId).collect(Collectors.toSet()));
    }

}