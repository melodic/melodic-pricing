package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.gcp.GCPAggregatedProductIdFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class GCPAggregatedProductsServiceTest {

    @Autowired
    private GCPAggregatedProductsService aggregatedProductsService;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private GCPAggregatedProductsRepository repository;

    @Before
    public void setUp() {
        productsMetadataService.removeAllProducts();
        repository.deleteAll();
        new File(aggregatedProductsService.getVmFilePath()).delete();
    }

    @Test
    public void testWhenThereIsNoData() {
        assertTrue(aggregatedProductsService.allProductIds(new CloudEnvironmentPricingId("test")).isEmpty());
        assertTrue(aggregatedProductsService.findProduct(new ProductId("unknown")).isEmpty());
    }

    @Test
    public void shouldUpdateVmsFromFile() throws IOException {
        CloudEnvironmentPricingId pricingId = new CloudEnvironmentPricingId();
        // given

        new File(aggregatedProductsService.getVmFilePath()).getParentFile().mkdirs();
        Files.copy(Paths.get("src/test/data/gcp_aggregated_products/test_machines.json"), Paths.get(aggregatedProductsService.getVmFilePath()), StandardCopyOption.REPLACE_EXISTING);

        //
        assertTrue(aggregatedProductsService.allProductIds(pricingId).isEmpty());

        // when
        aggregatedProductsService.updateAggregatedProducts(pricingId);

        // then
        GCPAggregatedProductIdFactory prodIdFactory = new GCPAggregatedProductIdFactory();
        ProductId testProdId = prodIdFactory.createVMId(pricingId, "e2-highcpu-16", new Place("europe-west3"));
        Optional<CloudProvider> maybeFoundCloudProvider = productsMetadataService.findCloudProviderOf(testProdId);
        assertNotNull(maybeFoundCloudProvider);
        assertTrue(maybeFoundCloudProvider.isPresent());
        assertEquals(CloudProvider.GCP, maybeFoundCloudProvider.get());
        Optional<GCPAggregatedProduct> maybeFoundAggregatedProd = repository.findById(testProdId);
        assertTrue(maybeFoundAggregatedProd.isPresent());

        // and when
        Optional<? extends GCPAggregatedProduct> foundTestProd = aggregatedProductsService.findProduct(testProdId);

        // then
        assertNotNull(foundTestProd);
        assertTrue(foundTestProd.isPresent());
        GCPAggregatedProduct gcpAggregatedProduct = foundTestProd.get();
        assertTrue(gcpAggregatedProduct instanceof GCPVmAggregatedProduct);

        assertFalse(aggregatedProductsService.findProduct(prodIdFactory.createVMId(pricingId, "e2-highcpu-16", new Place("europe-west3-a"))).isPresent());
        assertFalse(aggregatedProductsService.findProduct(prodIdFactory.createVMId(pricingId, "e2-highcpu-16", new Place("europe-west"))).isPresent());

        // when
        Set<ProductId> productIds = aggregatedProductsService.allProductIds(pricingId);

        // then
        assertNotNull(productIds);
        assertTrue(productIds.contains(testProdId));
        assertEquals(5, productIds.size());

        // and when
        aggregatedProductsService.deleteAll();

        // then
        assertEquals(0L, repository.count());
        assertTrue(aggregatedProductsService.findProduct(testProdId).isEmpty());

    }

    @Test
    public void shouldDeleteRecordsForGivenPricing() throws Exception {
        // given
        new File(aggregatedProductsService.getVmFilePath()).getParentFile().mkdirs();
        Files.copy(Paths.get("src/test/data/gcp_aggregated_products/test_machines.json"), Paths.get(aggregatedProductsService.getVmFilePath()), StandardCopyOption.REPLACE_EXISTING);
        CloudEnvironmentPricingId pricingId1 = new CloudEnvironmentPricingId();
        CloudEnvironmentPricingId pricingId2 = new CloudEnvironmentPricingId();
        assertTrue(aggregatedProductsService.allProductIds(pricingId1).isEmpty());
        assertTrue(aggregatedProductsService.allProductIds(pricingId2).isEmpty());

        // when
        aggregatedProductsService.updateAggregatedProducts(pricingId1);
        aggregatedProductsService.updateAggregatedProducts(pricingId2);
        Set<ProductId> product1Ids = aggregatedProductsService.allProductIds(pricingId1);
        assertFalse(product1Ids.isEmpty());
        assertFalse(aggregatedProductsService.allProductIds(pricingId2).isEmpty());

        aggregatedProductsService.deletePricing(pricingId1);

        // then
        assertTrue(aggregatedProductsService.allProductIds(pricingId1).isEmpty());
        assertFalse(aggregatedProductsService.allProductIds(pricingId2).isEmpty());

        for (ProductId productId : product1Ids) {
            assertFalse(productsMetadataService.findCloudProviderOf(productId).isPresent());
        }

        // when
        aggregatedProductsService.deleteAll();

        // then
        assertTrue(aggregatedProductsService.allProductIds(pricingId1).isEmpty());
        assertTrue(aggregatedProductsService.allProductIds(pricingId2).isEmpty());

    }

    @Test
    public void shouldMapToAggregatedCustomVmWithProductFamily() {
        // given
        ProductId productId = new ProductId("p:clpr:8eff8329-74ee-41be-82a8-8a290a12e55d/extId:vm:n2-custom-72-262144/r:CONCRETE/europe-west1");

        // when
        Optional<GCPVmAggregatedProduct> maybeResult = aggregatedProductsService.mapToAggregatedCustomVm(productId);

        // then
        assertTrue(maybeResult.isPresent());
        GCPVmAggregatedProduct result = maybeResult.get();
        assertEquals(productId, result.getId());
        assertEquals(new CloudEnvironmentPricingId("clpr:8eff8329-74ee-41be-82a8-8a290a12e55d"), result.getCloudEnvironmentPricingId());
        assertEquals("N2", result.getProductFamily());
        assertEquals(72, result.getCpuCount().getQuantity(), 0);
        assertEquals(256, result.getRamAmountGB().getQuantity(), 0);
        assertEquals(1, result.getRegions().size());
        assertTrue(result.getRegions().contains(new Place("europe-west1")));
    }

    @Test
    public void shouldMapToAggregatedCustomVmFromN1Family() {
        // given
        ProductId productId = new ProductId("p:clpr:8eff8329-74ee-41be-82a8-8a290a12e55d/extId:vm:custom-12-30720/r:CONCRETE/europe-west1");

        // when
        Optional<GCPVmAggregatedProduct> maybeResult = aggregatedProductsService.mapToAggregatedCustomVm(productId);

        // then
        assertTrue(maybeResult.isPresent());
        GCPVmAggregatedProduct result = maybeResult.get();
        assertEquals(productId, result.getId());
        assertEquals(new CloudEnvironmentPricingId("clpr:8eff8329-74ee-41be-82a8-8a290a12e55d"), result.getCloudEnvironmentPricingId());
        assertEquals("N1", result.getProductFamily());
        assertEquals(12, result.getCpuCount().getQuantity(), 0);
        assertEquals(30, result.getRamAmountGB().getQuantity(), 0);
        assertEquals(1, result.getRegions().size());
        assertTrue(result.getRegions().contains(new Place("europe-west1")));
    }


}
