package org.ow2.morphemic.pricing.cloud_providers.gcp;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class GCPProductPricingServiceTest {

    @Autowired
    private GCPProductPricingService pricingService;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private GCPProductPricingRecordsRepository pricingRecordsRepository;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    @Qualifier("gcpProductPricingUpdateService")
    private GCPProductPricingUpdateService gcpProductPricingUpdateService;

    @Autowired
    private GCPTestingSupportService gcpTestingSupportService;

    @Before
    public void setUp() {
        dataVersionsService.removeAllVersions();
        pricingService.deleteAll();
        productsMetadataService.removeAllProducts();
    }

    @Test
    public void shouldReturnEmptySetOfProductIdsWhenThereAreNoGCPProducts() {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.GCP);

        // when
        Set<ProductId> ProductIds = pricingService.allProductIds(cloudEnvironmentPricingId);

        // then
        assertNotNull(ProductIds);
        assertTrue(ProductIds.isEmpty());
    }

    @Test
    public void shouldReturnProductData() {
        // given
        CloudEnvironmentPricingId testClEnvId1 = new CloudEnvironmentPricingId(CloudProvider.GCP);
        CloudEnvironmentPricingId testClEnvId2 = new CloudEnvironmentPricingId(CloudProvider.GCP);

        VersionedCloudEnvPricingId versionedCloudEnvPricingId2 = new VersionedCloudEnvPricingId(testClEnvId2, new VersionNum(2));
        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId2, GCPProductPricingService.VERSIONED_GCP_PRICING_COLLECTION_NAME);

        List<GCPProductPricingRecord> testProducts = new ArrayList<>();

        for (int skuNum = 0; skuNum < 10; skuNum++) {
            List<GCPProductPriceOfferRecord> offers = new ArrayList<>();
            for (int offrNum = 0; offrNum < 5; offrNum++) {
                offers.add(GCPProductPriceOfferRecord.builder()
                        .aggregationInfo(new GCPProductPriceOfferRecord.GCPAggregationInfo(1, "DAILY", "PROJECT"))
                        .description("Descr " + skuNum + " offer " + offrNum)
                        .effectiveDate(new Date())
                        .rates(Arrays.asList(GCPPriceOfferRate.builder()
                                .baseUnit(new GCPPriceOfferRate.GCPUnit("b.sec", "Bytes / sec"))
                                .baseUnitConversionFactor(1.0)
                                .currencyConversionRate(1.0)
                                .displayQuantity(1.0)
                                .startUsageAmount(0)
                                .usageUnit(new GCPPriceOfferRate.GCPUnit("kb/h", "KiBytes / hour"))
                                .pricePerUnitByCurrency(Maps.asMap(Sets.newHashSet("USD"), k -> new BigDecimal(7)))
                                .build()))
                        .build());
            }

            GCPTaxonomy taxonomy = GCPTaxonomy.builder()
                    .businessEntityName("entity/GCP")
                    .productSkuName("SKU_" + skuNum)
                    .resourceFamily("family")
                    .serviceId("serviceId")
                    .resourceGroup("group")
                    .serviceName("service/serviceId")
                    .serviceDisplayName("Service 1")
                    .build();

            GCPProductPricingRecord prod = GCPProductPricingRecord.builder()
                    .id(new ProductId(testClEnvId2, "prod" + skuNum))
                    .productCode("prod" + skuNum)
                    .taxonomy(taxonomy)
                    .cloudEnvironmentPricingId(testClEnvId2)
                    .basedOnRawVersionNum(versionedCloudEnvPricingId2.getVersionNum())
                    .offers(offers)
                    .availableInRegions(Sets.newHashSet("global"))
                    .global(true)
                    .offerTermType(new OfferTermType("OnDemand"))
                    .build();
            testProducts.add(prod);
        }

        pricingRecordsRepository.saveAll(testProducts);

        // when
        Set<ProductId> productIds2 = pricingService.allProductIds(testClEnvId2);

        // then
        assertFalse(productIds2.isEmpty());
        assertEquals(10, productIds2.size());
        assertEquals(testProducts.stream().map(GCPProductPricingRecord::getId).collect(Collectors.toSet()), productIds2);

        Optional<GCPProductPricingRecord> maybeProd1ForCloudEnv1 = pricingService
                .findProduct(new ProductId(testClEnvId1, "prod1"));
        Optional<GCPProductPricingRecord> maybeProd1ForCloudEnv2 = pricingService
                .findProduct(new ProductId(testClEnvId2, "prod1"));


        // then
        assertNotNull(maybeProd1ForCloudEnv1);
        assertNotNull(maybeProd1ForCloudEnv2);

        assertFalse(maybeProd1ForCloudEnv1.isPresent());
        assertTrue(maybeProd1ForCloudEnv2.isPresent());

        GCPProductPricingRecord gcpProductPricingRecord = maybeProd1ForCloudEnv2.get();
        assertEquals("prod1", gcpProductPricingRecord.getProductCode());
        assertNotNull(gcpProductPricingRecord.getOffers());
        assertNotNull(gcpProductPricingRecord.getAvailableInRegions());
        assertEquals(1, gcpProductPricingRecord.getAvailableInRegions().size());
        assertEquals(Sets.newHashSet("global"), gcpProductPricingRecord.getAvailableInRegions());
        gcpProductPricingRecord.getOffers().forEach(offer -> {
            assertNotNull(offer);
            assertNotNull(offer.getRates());
            assertNotNull(offer.getAggregationInfo());
        });
        assertEquals(testProducts.get(1), gcpProductPricingRecord);
    }

    @Test
    public void shouldDeletePricingData() throws IOException {
        CloudEnvironmentPricingId pricing1 = new CloudEnvironmentPricingId(CloudProvider.GCP);
        CloudEnvironmentPricingId pricing2 = new CloudEnvironmentPricingId(CloudProvider.GCP);

        for(CloudEnvironmentPricingId pricing : new CloudEnvironmentPricingId[]{pricing1, pricing2}) {
            for(int ver = 0; ver < 2; ver ++) {
                gcpTestingSupportService.prepareTestPricingToLoad(pricing);
                gcpProductPricingUpdateService.loadPricingDumpDataFromFiles();
            }
        }

        assertEquals(16, pricingService.allProductIds(pricing2).size());
        assertEquals(16, pricingService.allProductIds(pricing1).size());

        // when
        pricingService.deletePricing(pricing1);

        // then
        assertEquals(0, pricingService.allProductIds(pricing1).size());
        assertEquals(16, pricingService.allProductIds(pricing2).size());

        pricingService.deleteAll();

        assertEquals(0, pricingService.allProductIds(pricing1).size());
        assertEquals(0, pricingService.allProductIds(pricing2).size());

    }

}