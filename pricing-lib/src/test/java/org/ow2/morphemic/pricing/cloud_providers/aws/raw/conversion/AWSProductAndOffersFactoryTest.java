package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.platform.commons.util.StringUtils;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSOfferRate;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSOffersTermsRecord;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingRecord;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductRecord;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.*;

@Slf4j
public class AWSProductAndOffersFactoryTest {

    @Test
    public void shouldParseProductAndPricingDataEC2() throws IOException {
        // given
        String jsonContents = Files.readString(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), UTF_8);
        VersionedCloudEnvPricingId verClId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(123));
        Set<OfferTermType> termTypes = availTermTypes();

        // when

        AWSProductAndOffersFactory pf = new AWSProductAndOffersFactory();
        AWSProductAndOffersFactory.ProductAndOffers productAndOffers = pf.fromJson(jsonContents, verClId).get();

        // then

        assertNotNull(productAndOffers);
        AWSProductRecord awsProductRecord = productAndOffers.getAwsProductRecord();
        assertNotNull(awsProductRecord);
        assertEquals("K3CVNEAWU7BY7JWE", awsProductRecord.getSkuId());
        assertEquals("Compute Instance", awsProductRecord.getProductFamily());
        assertEquals("AmazonEC2", awsProductRecord.getServiceCode());
        assertEquals(verClId.getVersionNum(), awsProductRecord.getBasedOnRawVersionNum());
        assertEquals(verClId.getCloudEnvironmentPricingId(), awsProductRecord.getCloudEnvironmentPricingId());
        assertNotNull(awsProductRecord.getAttrsRaw());

        AWSProductPricingRecord awsProductPricing = productAndOffers.getAwsProductPricing();
        assertNotNull(awsProductPricing);
        assertEquals(awsProductRecord.getSkuId(), awsProductPricing.getProductSkuId());
        assertEquals(verClId.getVersionNum(), awsProductPricing.getBasedOnRawVersionNum());
        assertEquals(13, awsProductPricing.getOffers().size());

        for (AWSOffersTermsRecord offer : awsProductPricing.getOffers()) {
            assertNotNull(offer);
            assertNotNull(offer.getEffectiveDate());
            assertNotNull(offer.getExternalOfferTermId());
            assertTrue(termTypes.contains(offer.getOfferTermType()));
            assertEquals(awsProductPricing.getProductSkuId(), offer.getProductSkuId());

            assertNotNull(offer.getRates());
            assertFalse(offer.getRates().isEmpty());
            for (AWSOfferRate rate : offer.getRates()) {
                assertNotNull(rate);
                assertTrue(StringUtils.isNotBlank(rate.getUnit().getUnitName()));
                assertTrue(StringUtils.isNotBlank(rate.getDescription()));
                assertNotNull(rate.getExternalRateCodeId());
                assertTrue(rate.getExternalRateCodeId().startsWith(awsProductPricing.getProductSkuId()));
                assertNotNull(rate.getPricePerUnitByCurrency());
                assertFalse(rate.getPricePerUnitByCurrency().isEmpty());
                assertNotNull(rate.getPricePerUnitByCurrency().get("USD"));
            }

        }

        log.debug("Stricture: {}", productAndOffers);
    }

    protected HashSet<OfferTermType> availTermTypes() {
        return Sets.newHashSet(new OfferTermType("OnDemand"), new OfferTermType("Reserved"));
    }

    @Test
    public void shouldParseProductAndPricingDataRDS() throws IOException {
        // given
        VersionedCloudEnvPricingId verClId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(123));
        String jsonContents = Files.readString(Paths.get("src", "test", "data", "aws", "rds_p_00001.json"), UTF_8);
        VersionNum versionNum = verClId.getVersionNum();
        Set<OfferTermType> termTypes = availTermTypes();

        // when
        AWSProductAndOffersFactory pf = new AWSProductAndOffersFactory();
        AWSProductAndOffersFactory.ProductAndOffers productAndOffers = pf.fromJson(jsonContents, verClId).get();

        // then

        assertNotNull(productAndOffers);
        AWSProductRecord awsProductRecord = productAndOffers.getAwsProductRecord();
        assertNotNull(awsProductRecord);
        assertEquals("227M9XKHRSNMBFJQ", awsProductRecord.getSkuId());
        assertEquals("Database Instance", awsProductRecord.getProductFamily());
        assertEquals("AmazonRDS", awsProductRecord.getServiceCode());
        assertEquals(versionNum, awsProductRecord.getBasedOnRawVersionNum());
        assertNotNull(awsProductRecord.getAttrsRaw());

        AWSProductPricingRecord awsProductPricing = productAndOffers.getAwsProductPricing();
        assertNotNull(awsProductPricing);
        assertEquals(awsProductRecord.getSkuId(), awsProductPricing.getProductSkuId());
        assertEquals(versionNum, awsProductPricing.getBasedOnRawVersionNum());
        assertEquals(6, awsProductPricing.getOffers().size());

        for (AWSOffersTermsRecord offer : awsProductPricing.getOffers()) {
            assertNotNull(offer);
            assertNotNull(offer.getEffectiveDate());
            assertNotNull(offer.getExternalOfferTermId());
            assertTrue(termTypes.contains(offer.getOfferTermType()));
            assertEquals(awsProductPricing.getProductSkuId(), offer.getProductSkuId());

            assertNotNull(offer.getRates());
            assertFalse(offer.getRates().isEmpty());
            for (AWSOfferRate rate : offer.getRates()) {
                assertNotNull(rate);
                assertTrue(StringUtils.isNotBlank(rate.getUnit().getUnitName()));
                assertTrue(StringUtils.isNotBlank(rate.getDescription()));
                assertNotNull(rate.getExternalRateCodeId());
                assertTrue(rate.getExternalRateCodeId().startsWith(awsProductPricing.getProductSkuId()));
                assertNotNull(rate.getPricePerUnitByCurrency());
                assertFalse(rate.getPricePerUnitByCurrency().isEmpty());
                assertNotNull(rate.getPricePerUnitByCurrency().get("USD"));
            }

        }

        log.debug("Stricture: {}", productAndOffers);
    }

    /**
     * Smoke test
     */
    @Test
    public void shouldParseFilesWithoutProblems() throws IOException {
        for (String fileName : new String[]{"rds_p_00001.json", "rds_p_00009.json", "ec2_p_00082.json", "s3_p_00077.json"}) {
            // given
            String jsonContents = Files.readString(Paths.get("src", "test", "data", "aws", fileName), UTF_8);
            VersionedCloudEnvPricingId verClId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum());
            Set<OfferTermType> termTypes = availTermTypes();

            // when
            AWSProductAndOffersFactory pf = new AWSProductAndOffersFactory();
            AWSProductAndOffersFactory.ProductAndOffers productAndOffers = pf.fromJson(jsonContents, verClId).get();

            // then

            assertNotNull(productAndOffers);
            AWSProductRecord awsProductRecord = productAndOffers.getAwsProductRecord();
            assertNotNull(awsProductRecord);
            assertTrue(StringUtils.isNotBlank(awsProductRecord.getSkuId()));
            assertTrue(StringUtils.isNotBlank(awsProductRecord.getServiceCode()));
            if (!"AmazonS3".equals(awsProductRecord.getServiceCode())) {
                assertTrue(fileName, StringUtils.isNotBlank(awsProductRecord.getProductFamily()));
            }
            assertEquals(verClId.getVersionNum(), awsProductRecord.getBasedOnRawVersionNum());
            assertNotNull(awsProductRecord.getAttrsRaw());

            AWSProductPricingRecord awsProductPricing = productAndOffers.getAwsProductPricing();
            assertNotNull(awsProductPricing);
            assertEquals(awsProductRecord.getSkuId(), awsProductPricing.getProductSkuId());
            assertEquals(verClId.getVersionNum(), awsProductPricing.getBasedOnRawVersionNum());
            assertTrue(awsProductPricing.getOffers().size() > 0);

            for (AWSOffersTermsRecord offer : awsProductPricing.getOffers()) {
                assertNotNull(offer);
                assertNotNull(offer.getEffectiveDate());
                assertNotNull(offer.getExternalOfferTermId());
                assertTrue(termTypes.contains(offer.getOfferTermType()));
                assertEquals(awsProductPricing.getProductSkuId(), offer.getProductSkuId());

                assertNotNull(offer.getRates());
                assertFalse(offer.getRates().isEmpty());
                for (AWSOfferRate rate : offer.getRates()) {
                    assertNotNull(rate);
                    assertTrue(StringUtils.isNotBlank(rate.getUnit().getUnitName()));
                    assertTrue(StringUtils.isNotBlank(rate.getDescription()));
                    assertNotNull(rate.getExternalRateCodeId());
                    assertTrue(rate.getExternalRateCodeId().startsWith(awsProductPricing.getProductSkuId()));
                    assertNotNull(rate.getPricePerUnitByCurrency());
                    assertFalse(rate.getPricePerUnitByCurrency().isEmpty());
                    assertNotNull(rate.getPricePerUnitByCurrency().get("USD"));
                }

            }

        }
    }


}