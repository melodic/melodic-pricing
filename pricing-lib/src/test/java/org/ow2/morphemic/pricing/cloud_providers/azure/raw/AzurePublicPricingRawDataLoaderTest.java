package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.ow2.morphemic.pricing.model.CloudProvider.AZURE;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AzurePublicPricingRawDataLoaderTest {

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AzureRawPricingDataRepository azureRawPricingDataRepository;

    @Autowired
    private AzurePublicPricingRawDataLoader loader;

    @Before
    public void setUp() {
        azureRawPricingDataRepository.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    // Really simple test only for checking if public pricing id is available
    @Test
    public void shouldReturnIdOfAzurePublicPricing() {

        // when
        CloudEnvironmentPricingId publicAzurePricingId = loader.getPublicAzurePricingId();

        // then
        assertNotNull(publicAzurePricingId);
    }

    @Ignore
    public void shouldLoadPublicPricingToDBCacheAndRemoveOldVersions() {

        // when - 1st loading of Azure pricing
        Optional<VersionedCloudEnvPricingId> maybeNewVerCloudEnvPricingId = loader.loadRawDataToCache(
                loader.getPublicAzurePricingId(),
                new PricingDataImportStatisticCollector(AZURE, "cloudAccountId", List.of()));

        // then
        assertNotNull(maybeNewVerCloudEnvPricingId);
        assertTrue(maybeNewVerCloudEnvPricingId.isPresent());
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = maybeNewVerCloudEnvPricingId.get();
        assertEquals(loader.getPublicAzurePricingId(), versionedCloudEnvPricingId.getCloudEnvironmentPricingId());

        long pricingItemsNumberFirst = azureRawPricingDataRepository.count();
        assertTrue(pricingItemsNumberFirst > 0);

        List<AzureRawPricingDataRecord> allPricingItems = azureRawPricingDataRepository.findAll();
        assertTrue(allPricingItems.size() > 0);

        assertTrue(dataVersionsService.hasAnyVersion(loader.getPublicAzurePricingId()));

        Optional<VersionNum> maybeLastVersion = dataVersionsService.lastVersionOf(loader.getPublicAzurePricingId(), AzureRawPricingDataRecord.VERSION_COLLECTION_NAME);
        assertTrue(maybeLastVersion.isPresent());
        for (AzureRawPricingDataRecord pricingItem : allPricingItems) {
            assertNotNull(pricingItem.getId());
            assertEquals(pricingItem.getVersion(), maybeLastVersion.get());
            assertNotNull(pricingItem.getRawContent());
            assertNotNull(pricingItem.getRawContent().getArmSkuName());
            assertNotNull(pricingItem.getRawContent().getArmRegionName());
            assertNotNull(pricingItem.getRawContent().getMeterId());
        }

        VersionNum lastVersionNum = versionedCloudEnvPricingId.getVersionNum();
        assertNotNull(lastVersionNum);
        assertEquals(lastVersionNum, maybeLastVersion.get());


        // when - next loading of Azure pricing
        maybeNewVerCloudEnvPricingId = loader.loadRawDataToCache(loader.getPublicAzurePricingId(),
                new PricingDataImportStatisticCollector(AZURE, "cloudAccountId", List.of()));

        // then
        assertNotNull(maybeNewVerCloudEnvPricingId);
        assertTrue(maybeNewVerCloudEnvPricingId.isPresent());
        assertEquals(loader.getPublicAzurePricingId(), maybeNewVerCloudEnvPricingId.get().getCloudEnvironmentPricingId());

        // old data should be removed, only new in DB - the number should be equal
        assertEquals(pricingItemsNumberFirst, azureRawPricingDataRepository.count());

        VersionNum prevVersion = maybeLastVersion.get();

        assertTrue(dataVersionsService.hasAnyVersion(loader.getPublicAzurePricingId()));
        maybeLastVersion = dataVersionsService.lastVersionOf(loader.getPublicAzurePricingId(), AzureRawPricingDataRecord.VERSION_COLLECTION_NAME);
        assertTrue(maybeLastVersion.isPresent());
        assertNotEquals(prevVersion, maybeLastVersion.get());
        assertEquals(maybeNewVerCloudEnvPricingId.get().getVersionNum(), maybeLastVersion.get());

        for (AzureRawPricingDataRecord pricingItem : azureRawPricingDataRepository.findAll()) {
            assertNotNull(pricingItem.getId());
            assertEquals(pricingItem.getVersion(), maybeLastVersion.get());
            assertNotNull(pricingItem.getRawContent());
            assertNotNull(pricingItem.getRawContent().getArmSkuName());
            assertNotNull(pricingItem.getRawContent().getArmRegionName());
            assertNotNull(pricingItem.getRawContent().getMeterId());
        }
    }
}
