package org.ow2.morphemic.pricing.cloud_providers.aws;

import org.junit.Test;

import java.time.Period;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;

public class AWSOffersTermsRecordTest {

    @Test
    public void shouldReturnEmptyLeaseContractLengthWhenThereIsNoSuchData() {
        // 1
        assertTrue(AWSOffersTermsRecord.builder().build().getLeaseContractLength().isEmpty());

        // 2
        AWSOffersTermsRecord offerTerm = AWSOffersTermsRecord.builder().externalOfferTermId("123")
                .termAttributesRaw(new HashMap<>()).build();
        assertTrue(offerTerm.getLeaseContractLength().isEmpty());
    }

    @Test
    public void shouldReturnEmptyLeaseContractLengthWhenThereIsWrongDefinition() {
        // given
        Map<String, String> params = new HashMap<>();
        params.put("LeaseContractLength", "kyr");

        AWSOffersTermsRecord offerTerm = AWSOffersTermsRecord.builder().externalOfferTermId("123")
                .termAttributesRaw(params).build();

        // when , then
        assertTrue(offerTerm.getLeaseContractLength().isEmpty());

        // 2
        params = new HashMap<>();
        params.put("LeaseContractLength", "yr");

        offerTerm = AWSOffersTermsRecord.builder().externalOfferTermId("123")
                .termAttributesRaw(params).build();
        assertTrue(offerTerm.getLeaseContractLength().isEmpty());
    }

    @Test
    public void shouldReturnEmptyLeaseContractLengthWhenThereIsNotSupportedFormat() {
        Map<String, String> params = new HashMap<>();
        params.put("LeaseContractLength", "3mo");

        AWSOffersTermsRecord offerTerm = AWSOffersTermsRecord.builder().externalOfferTermId("123")
                .termAttributesRaw(params).build();

        // when , then
        assertTrue(offerTerm.getLeaseContractLength().isEmpty());

    }

    @Test
    public void shouldReturnLeaseContractLength() {
        Map<String, String> params = new HashMap<>();
        params.put("LeaseContractLength", "3yr");

        AWSOffersTermsRecord offerTerm = AWSOffersTermsRecord.builder().externalOfferTermId("123")
                .termAttributesRaw(params).build();

        // when , then
        Optional<Period> maybeLeaseLen = offerTerm.getLeaseContractLength();
        assertTrue(maybeLeaseLen.isPresent());
        assertEquals(Period.ofYears(3), maybeLeaseLen.get());

    }

}