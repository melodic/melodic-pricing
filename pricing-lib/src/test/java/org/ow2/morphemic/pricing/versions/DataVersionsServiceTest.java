package org.ow2.morphemic.pricing.versions;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class DataVersionsServiceTest {

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private DataVersionsRepository dataVersionsRepository;

    @Before
    public void setUp() {
        dataVersionsRepository.deleteAll();
    }

    @Test
    public void shouldReturnEmptyVersionWhenThereWereNoVersionsCreated() {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId();

        // when
        Optional<VersionNum> maybeVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, "strange");
        Optional<DataVersionsService.VersionMetadata> maybeVersionMetadata = dataVersionsService.lastVersionMetadata(cloudEnvironmentPricingId, new CollectionName("strange"));

        // then
        assertNotNull(maybeVersion);
        assertNotNull(maybeVersionMetadata);
        assertTrue(maybeVersion.isEmpty());
        assertTrue(maybeVersionMetadata.isEmpty());
    }

    @Test
    public void shouldReturnValidLatestVersion() {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId();
        CollectionName coll1Name = new CollectionName("coll1");

        // when
        Optional<VersionNum> maybeVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, coll1Name);
        Optional<DataVersionsService.VersionMetadata> maybeVersionMetadata = dataVersionsService.lastVersionMetadata(cloudEnvironmentPricingId, coll1Name);

        // then
        assertNotNull(maybeVersion);
        assertNotNull(maybeVersionMetadata);
        assertTrue(maybeVersion.isEmpty());
        assertTrue(maybeVersionMetadata.isEmpty());

        // when
        VersionNum versionNum123 = new VersionNum(123L);
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, versionNum123);
        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, coll1Name);
        maybeVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, coll1Name);
        maybeVersionMetadata = dataVersionsService.lastVersionMetadata(cloudEnvironmentPricingId, coll1Name);

        // then
        assertNotNull(maybeVersion);
        assertNotNull(maybeVersionMetadata);
        assertFalse(maybeVersion.isEmpty());
        assertFalse(maybeVersionMetadata.isEmpty());
        assertEquals(versionNum123, maybeVersion.get());
        assertEquals(versionNum123, maybeVersionMetadata.get().getVersionNum());
        assertNotNull(maybeVersionMetadata.get().getCreated());
        assertTrue(dataVersionsService.hasAnyVersion(cloudEnvironmentPricingId));

        VersionNum versionNum124 = new VersionNum(124L);
        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId.newVersion(versionNum124), coll1Name);
        maybeVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, coll1Name);

        // then
        assertNotNull(maybeVersion);
        assertFalse(maybeVersion.isEmpty());
        assertEquals(versionNum124, maybeVersion.get());

    }

    @Test
    public void shouldReportThatThereIsNoVersionForFreshCloudEnvPricing() {
        assertFalse(dataVersionsService.hasAnyVersion(new CloudEnvironmentPricingId("qqq")));
    }


}