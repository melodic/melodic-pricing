package org.ow2.morphemic.pricing.calculators.aws_savings_plans;


import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansTestUtils;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.AWSRawSavingsPlansManager;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption;
import org.ow2.morphemic.pricing.model.aws_savings_plans.AWSSavingsPlansProductIdFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AWSSavingsPlansCalculatorTest {

    @Autowired
    private AWSSavingsPlansProductPricingService productPricingService;

    @Autowired
    private AWSSavingsPlansUpdateService awsSavingsPlansUpdateService;

    @Autowired
    private AWSSavingsPlansCalculator priceCalculator;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private AWSRawSavingsPlansManager awsRawSavingsPlansManager;

    @Value("${pricing.datadump.aws.savingsplans.dir}")
    private String awsDumpDir;

    private File scanDir;

    @Before
    public void setUp() {
        awsRawSavingsPlansManager.deleteAll();
        productPricingService.deleteAll();
        productsMetadataService.removeAllProducts();
        scanDir = new File(awsDumpDir);
        scanDir.mkdirs();
        var files = scanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }
    }

    @Test
    public void mustNotCalculateWhenThereIsNoSuchProduct() {
        ProductId productId = new ProductId("DoesNotExtst");

        // when: we want to buy 3 hours 'OnDemand" of "r5d.2xlarge" - but it does not exist!
        Amount amount = new Amount(3, new Unit("Hrs"));
        Optional<List<Price>> maybeCalculatedPrices = priceCalculator
                .calculatePrice(productId, amount);

        // then
        assertNotNull(maybeCalculatedPrices);
        assertFalse(maybeCalculatedPrices.isPresent());
    }

    @Test
    public void shouldCalculateValueForEc2SavigsPlan() throws IOException {
        AWSSavingsPlansTestUtils.prepareFileToRunLoadPricingFromFiles(scanDir, "src/test/data/aws/savings_plans/v1");

        Optional<VersionedCloudEnvPricingId> versionedCloudEnvPricingId = awsSavingsPlansUpdateService.loadPricingDumpDataFromFiles();

        assertTrue(versionedCloudEnvPricingId.isPresent());
        AWSPurchaseOption expectedPurchaseOption = AWSPurchaseOption.PARTIAL_UPFRONT;
        Period expectedPurchaseTerm = Period.ofYears(1);
        String expectedInstanceType = "x2iezn";

        List<VersionedCloudEnvPricingId> savingsPlansPricingIds = productPricingService.findEc2InstanceSavingsPlansBy(
                expectedPurchaseOption, expectedPurchaseTerm, expectedInstanceType, "us-east-1");
        assertEquals(1, savingsPlansPricingIds.size());

        String skuOfProduct = "22RRGT3PBVP2XF8D";

        ProductId savingsPlansProductIdForCalculate = new AWSSavingsPlansProductIdFactory().createSavingsPlansProductId(savingsPlansPricingIds.get(0), skuOfProduct);
        Amount amount = new Amount(3, new Unit("Hrs"));

        Optional<List<Price>> calculatedPrices = priceCalculator.calculatePrice(savingsPlansProductIdForCalculate, amount);

        assertTrue(calculatedPrices.isPresent());
        assertEquals(1, calculatedPrices.get().size());

        Price expectedPrice = new Price(new BigDecimal("18.090"), "USD");
        assertEquals(expectedPrice, calculatedPrices.get().get(0));
    }

    @Test
    public void shouldCalculateValueForComputeSavigsPlan() throws IOException {
        AWSSavingsPlansTestUtils.prepareFileToRunLoadPricingFromFiles(scanDir, "src/test/data/aws/savings_plans/v1");

        Optional<VersionedCloudEnvPricingId> versionedCloudEnvPricingId = awsSavingsPlansUpdateService.loadPricingDumpDataFromFiles();

        assertTrue(versionedCloudEnvPricingId.isPresent());
        AWSPurchaseOption expectedPurchaseOption = AWSPurchaseOption.NO_UPFRONT;
        Period expectedPurchaseTerm = Period.ofYears(1);

        List<VersionedCloudEnvPricingId> savingsPlansPricingIds = productPricingService.findComputeSavingsPlansBy(
                expectedPurchaseOption, expectedPurchaseTerm);
        assertEquals(1, savingsPlansPricingIds.size());

        String skuOfProduct = "2223RRAP6Z3VBN3N";

        ProductId savingsPlansProductIdForCalculate = new AWSSavingsPlansProductIdFactory().createSavingsPlansProductId(savingsPlansPricingIds.get(0), skuOfProduct);
        Amount amount = new Amount(3, new Unit("Hrs"));

        Optional<List<Price>> calculatedPrices = priceCalculator.calculatePrice(savingsPlansProductIdForCalculate, amount);

        assertTrue(calculatedPrices.isPresent());
        assertEquals(1, calculatedPrices.get().size());

        Price expectedPrice = new Price(new BigDecimal("1.6350"), "USD");
        assertEquals(expectedPrice, calculatedPrices.get().get(0));
    }

}
