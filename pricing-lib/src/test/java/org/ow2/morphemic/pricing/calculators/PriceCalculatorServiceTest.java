package org.ow2.morphemic.pricing.calculators;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansTestUtils;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPStandardTermTypes;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption;
import org.ow2.morphemic.pricing.model.aws_savings_plans.AWSSavingsPlansProductIdFactory;
import org.ow2.morphemic.pricing.model.gcp.GCPAggregatedProductIdFactory;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Period;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AZURE;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class PriceCalculatorServiceTest {

    @Autowired
    private AWSProductPricingService awsProductPricingService;

    @Autowired
    private AWSProductPricingUpdateService awsProductPricingUpdateService;

    @Autowired
    private AWSSavingsPlansProductPricingService productPricingService;

    @Autowired
    private GCPProductPricingService gcpProductPricingService;

    @Autowired
    private GCPProductPricingUpdateService gcpProductPricingUpdateService;

    @Autowired
    private AzureProductPricingService azureProductPricingService;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private PriceCalculatorService priceCalculatorService;

    @Autowired
    private GCPAggregatedProductsService aggregatedProductsService;

    @Autowired
    private AzureProductPricingUpdateService azureProductPricingUpdateService;

    @Autowired
    private AWSSavingsPlansUpdateService awsSavingsPlansUpdateService;

    @Value("${pricing.datadump.gcp.dir}")
    private String gcpDumpImportDir;

    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpDir;

    @Value("${pricing.datadump.aws.savingsplans.dir}")
    private String awsSavingsPlanDumpDir;

    private File gcpScanDir;
    private File awsSavingsPlansScanDir;
    private File azureScanDir;
    private File jsonAggregatedProductsFile;

    @Before
    public void setUp() throws IOException {
        awsProductPricingService.deleteAll();
        gcpProductPricingService.deleteAll();
        aggregatedProductsService.deleteAll();
        azureProductPricingService.deleteAll();

        productsMetadataService.removeAllProducts();

        gcpScanDir = new File(gcpDumpImportDir);
        gcpScanDir.mkdirs();

        Files.walkFileTree(gcpScanDir.toPath(), new SimpleFileVisitor<>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
        gcpScanDir.mkdirs();

        jsonAggregatedProductsFile = new File(aggregatedProductsService.getVmFilePath());
        jsonAggregatedProductsFile.getParentFile().mkdirs();
        jsonAggregatedProductsFile.delete();

        Files.copy(Paths.get("src/test/data/gcp_aggregated_products/test_machines.json"), jsonAggregatedProductsFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

        azureScanDir = new File(azureDumpDir);
        azureScanDir.mkdirs();
        var files = azureScanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }
        awsSavingsPlansScanDir = new File(awsSavingsPlanDumpDir);
        awsSavingsPlansScanDir.mkdirs();
        var awsFiles = awsSavingsPlansScanDir.listFiles();
        if (awsFiles != null) {
            Stream.of(awsFiles).forEach(File::delete);
        }

    }


    @Test
    public void shouldReturnPossiblePricingOptionsForExistingAWSProduct() throws IOException {
        // given
        VersionedCloudEnvPricingId verClId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(123));
        awsProductPricingUpdateService.updateProductsAndPricingData(Collections.singletonList(Files.readString(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), StandardCharsets.UTF_8)), verClId);
        ProductId productId = new ProductId(verClId.getCloudEnvironmentPricingId(), "K3CVNEAWU7BY7JWE");

        // when
        List<PricingOption> pricingOptions = priceCalculatorService.whatArePossiblePricingOptions(productId);

        // then
        assertNotNull(pricingOptions);
        log.debug("Options found {}", pricingOptions);
        assertEquals(5, pricingOptions.size());
        for (PricingOption pricingOption : pricingOptions) {
            assertNotNull(pricingOption.getOfferTermType());
            assertNotNull(pricingOption.getUnit());
            if (pricingOption.getOfferTermType().equals(new OfferTermType("Reserved"))) {
                assertNotNull(pricingOption.getPeriod());
            } else {
                assertNull(pricingOption.getPeriod());
            }
        }

    }

    @Test
    public void shouldReturnPossiblePricingOptionsForExistingGCPProduct() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = loadTestGCPDataToNewCloudEnvironmentPricingId();
        ProductId productId = new ProductId(cloudEnvironmentPricingId, "4022-B36C-109B");


        // when
        List<PricingOption> pricingOptions = priceCalculatorService.whatArePossiblePricingOptions(productId);

        // then
        assertNotNull(pricingOptions);
        log.debug("Options found {}", pricingOptions);
        assertEquals(2, pricingOptions.size());
        PricingOption pricingOption = pricingOptions.get(0);

        assertEquals(new OfferTermType("Commit3Yr"), pricingOption.getOfferTermType());
        assertEquals(new Unit("gibibyte hour"), pricingOption.getUnit());
        assertEquals(Period.ofYears(3), pricingOption.getPeriod());

        assertEquals(Set.of("GiBy.h", "gibibyte hour"), pricingOptions.stream().map(PricingOption::getUnit).map(Unit::getUnitName).collect(Collectors.toSet()));

    }


    @Test
    public void shouldReturnEmptyPricingOptionsForNonExistingProduct() {
        // given
        ProductId productId = new ProductId("DoesNotExist");

        // when
        List<PricingOption> pricingOptions = priceCalculatorService.whatArePossiblePricingOptions(productId);

        // then
        assertNotNull(pricingOptions);
        assertTrue(pricingOptions.isEmpty());
    }

    /**
     * Complex example: shows that it is possible to calculate prices using ALL supported cloud providers.
     * Currently GCP and AWS for simple cases.
     *
     * @throws IOException
     */
    @Test
    public void shouldCalculatePriceUsingPricingOption() throws IOException {
        // given
        VersionedCloudEnvPricingId verClAWSId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(1));
        awsProductPricingUpdateService.updateProductsAndPricingData(Collections.singletonList(Files.readString(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), StandardCharsets.UTF_8)), verClAWSId);

        VersionedCloudEnvPricingId verClGCPId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C2"), new VersionNum(1));
        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), gcpScanDir);
        Files.write(new File(gcpScanDir, GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME).toPath(), verClGCPId.getCloudEnvironmentPricingId().getId().getBytes(StandardCharsets.UTF_8));

        gcpProductPricingUpdateService.loadPricingDumpDataFromFiles();

        ProductId awsProductId = new ProductId(verClAWSId.getCloudEnvironmentPricingId(), "K3CVNEAWU7BY7JWE");

        // when: 1) we want to buy 10 hours of product
        Unit hrsUnit = new Unit("Hrs");
        var maybePriceOnDemand = priceCalculatorService.calculatePrice(awsProductId,
                new Amount(10, hrsUnit),
                new PricingOption(new OfferTermType("OnDemand"), hrsUnit));

        // then
        assertNotNull(maybePriceOnDemand);
        assertTrue(maybePriceOnDemand.isPresent());
        Price price = maybePriceOnDemand.get();
        assertEquals(39.44, price.getValue().doubleValue(), 0.0001);
        assertEquals("USD", price.getCurrencyCode());

        // when: 2) we want to reserve 2 product for 3 years (to be fixed)

        Unit qUnit = new Unit("Quantity");
        var maybePriceReserved = priceCalculatorService.calculatePrice(awsProductId,
                new Amount(2, qUnit),
                new PricingOption(new OfferTermType("Reserved"), qUnit, Period.ofYears(3)));

        // then
        assertNotNull(maybePriceReserved);
        assertTrue(maybePriceReserved.isPresent());
        // FIXME it cannot calculate "reserved' prices
        // assertEquals(2d * 95584d, maybePriceReserved.get().getValue().doubleValue(), 0.0001);

        // GCP
        // given
        ProductId gcpProductId = new ProductId(verClGCPId.getCloudEnvironmentPricingId(), "6BC9-D757-743E");
        Unit gMoUnit = new Unit("GiBy.mo");

        // when
        maybePriceOnDemand = priceCalculatorService.calculatePrice(gcpProductId,
                new Amount(3, gMoUnit),
                new PricingOption(new OfferTermType("OnDemand"), gMoUnit));
        // then
        assertNotNull(maybePriceOnDemand);
        assertTrue(maybePriceOnDemand.isPresent());
        price = maybePriceOnDemand.get();
        assertEquals(new BigDecimal("15.000000000"), price.getValue());
        assertEquals("USD", price.getCurrencyCode());
    }

    @Test
    public void shouldProperlyCalculatePriceOfGCPVirtualMachine() throws IOException {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.GCP);

        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), gcpScanDir);
        Files.write(new File(gcpScanDir, GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        if (gcpProductPricingUpdateService.loadPricingDumpDataFromFiles().isEmpty()) {
            fail("New version has not been created!");
        }

        GCPAggregatedProductIdFactory idFactory = new GCPAggregatedProductIdFactory();

        // when
        // e2-highcpu-2 Commit1Yr: 2CPU * 0.028103700*hours + 2GB*0.003766020*hours
        ProductId e2Pighcpu2Prodid = idFactory.createVMId(cloudEnvironmentPricingId, "e2-highcpu-2", new Place("europe-west3"));
        List<PricingOption> pricingOptions = priceCalculatorService.whatArePossiblePricingOptions(e2Pighcpu2Prodid);
        assertNotNull(pricingOptions);
        assertEquals(4, pricingOptions.size());

        for (String unitName : new String[]{"h", "hour"}) {
            Unit unit = new Unit(unitName);
            String errorMsg = "Unit: " + unitName;

            Optional<Price> maybeCalculatedPrice = priceCalculatorService.calculatePrice(e2Pighcpu2Prodid, new Amount(3, unit),
                    pricingOptions.stream().filter(opt -> GCPStandardTermTypes.COMMIT_1YR.getOfferTermType().equals(opt.getOfferTermType())).findFirst().get());

            // then
            assertNotNull(errorMsg, maybeCalculatedPrice);
            assertTrue(errorMsg, maybeCalculatedPrice.isPresent());
            Price calculatedPrice = maybeCalculatedPrice.get();
            assertEquals(new Price(new BigDecimal("0.120467538"), "USD"), calculatedPrice);

            // and when
            // e2-highcpu-2 OnDemand: 2CPU * 0.028103700*hours + 2GB*0.003766020*hours
            maybeCalculatedPrice = priceCalculatorService.calculatePrice(e2Pighcpu2Prodid, new Amount(3, unit),
                    pricingOptions.stream().filter(opt -> GCPStandardTermTypes.ON_DEMAND.getOfferTermType().equals(opt.getOfferTermType())).findFirst().get());

            // then
            assertNotNull(errorMsg, maybeCalculatedPrice);
            assertTrue(errorMsg, maybeCalculatedPrice.isPresent());
            calculatedPrice = maybeCalculatedPrice.get();
            assertNotNull(errorMsg, calculatedPrice);
            assertEquals(errorMsg, new Price(new BigDecimal("0.191218320"), "USD"), calculatedPrice);
        }
    }


    private CloudEnvironmentPricingId loadTestGCPDataToNewCloudEnvironmentPricingId() throws IOException {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.GCP);
        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), gcpScanDir);
        Files.write(new File(gcpScanDir, GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
        gcpProductPricingUpdateService.loadPricingDumpDataFromFiles();
        return cloudEnvironmentPricingId;
    }

    @Test
    public void shouldProperlyCalculatePriceOfAzureVirtualMachine() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AZURE);
        FileSystemUtils.copyRecursively(new File("src/test/data/azure/raw"), azureScanDir);
        Files.write(new File(azureScanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        azureProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // when
        ProductId productId = new ProductId(cloudEnvironmentPricingId, "0008ac09-705a-431e-b9c7-026746cb920a");
        Unit unit = new Unit("1 Hour");
        Optional<Price> calculatedPrice = priceCalculatorService.calculatePrice(productId, new Amount(3, unit),
                new PricingOption(new OfferTermType(""), unit));

        assertNotNull(calculatedPrice);
        assertTrue(calculatedPrice.isPresent());
        Price price = calculatedPrice.get();
        assertEquals("USD", price.getCurrencyCode());
        assertEquals(3 * 0.112d, price.getValue().doubleValue(), 0.001);

        // and what are possibne pricing options?
        // when
        List<PricingOption> pricingOptions = priceCalculatorService.whatArePossiblePricingOptions(productId);

        // then
        assertNotNull(pricingOptions);

        /// reservations
        productId = new ProductId(cloudEnvironmentPricingId, "005a9d87-d9f1-4140-9529-3aabf427d803");
        unit = new Unit("");

        calculatedPrice = priceCalculatorService.calculatePrice(productId, new Amount(1, unit),
                new PricingOption(new OfferTermType("3 Years"), unit));

        assertNotNull(calculatedPrice);
        assertTrue(calculatedPrice.isPresent());
        price = calculatedPrice.get();
        assertEquals("USD", price.getCurrencyCode());
        assertEquals(43510.0, price.getValue().doubleValue(), 0.001);
    }

    @Test
    public void shouldProperlyCalculatePriceForAwsEc2SavingsPlans() throws IOException {
        // given load data from file
        AWSSavingsPlansTestUtils.prepareFileToRunLoadPricingFromFiles(awsSavingsPlansScanDir, "src/test/data/aws/savings_plans/v1");
        awsSavingsPlansUpdateService.loadPricingDumpDataFromFiles();

        // sku of product
        String awsProductDiscountedSku = "22RRGT3PBVP2XF8D";

        // find pricing id
        Period awsSavingsPlansPeriod = Period.ofYears(1);
        List<VersionedCloudEnvPricingId> pricingIds = productPricingService.findEc2InstanceSavingsPlansBy(AWSPurchaseOption.PARTIAL_UPFRONT, awsSavingsPlansPeriod,
                "x2iezn", "us-east-1");

        assertEquals(1, pricingIds.size());

        ProductId productId = new AWSSavingsPlansProductIdFactory().createSavingsPlansProductId(pricingIds.get(0), awsProductDiscountedSku);

        // calculate price for 10 hrs
        Unit hrsUnit = new Unit("Hrs");
        Amount amount = new Amount(10, hrsUnit);

        // when
        Optional<Price> price = priceCalculatorService.calculatePrice(productId, amount, null);

        assertTrue(price.isPresent());
        assertEquals(60.3, price.get().getValue().doubleValue(), 0.001);
        assertEquals("USD", price.get().getCurrencyCode());

    }

    @Test
    public void shouldProperlyCalculatePriceForAwsComputeSavingsPlans() throws IOException {
        // given load data from file
        AWSSavingsPlansTestUtils.prepareFileToRunLoadPricingFromFiles(awsSavingsPlansScanDir, "src/test/data/aws/savings_plans/v1");
        awsSavingsPlansUpdateService.loadPricingDumpDataFromFiles();

        // sku of product
        String awsProductDiscountedSku = "2223RRAP6Z3VBN3N";

        // find pricing id
        Period awsSavingsPlansPeriod = Period.ofYears(1);
        List<VersionedCloudEnvPricingId> pricingIds = productPricingService.findComputeSavingsPlansBy(AWSPurchaseOption.NO_UPFRONT, awsSavingsPlansPeriod);

        assertEquals(1, pricingIds.size());

        ProductId productId = new AWSSavingsPlansProductIdFactory().createSavingsPlansProductId(pricingIds.get(0), awsProductDiscountedSku);

        // calculate price for 10 hrs
        Unit hrsUnit = new Unit("Hrs");
        Amount amount = new Amount(10, hrsUnit);

        // when
        Optional<Price> price = priceCalculatorService.calculatePrice(productId, amount, null);

        assertTrue(price.isPresent());
        assertEquals(5.45, price.get().getValue().doubleValue(), 0.001);
        assertEquals("USD", price.get().getCurrencyCode());

    }
}
