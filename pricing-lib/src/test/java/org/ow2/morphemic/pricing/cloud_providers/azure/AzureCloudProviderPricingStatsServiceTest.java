package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AzureCloudProviderPricingStatsServiceTest {

    @Autowired
    private AzureProductPricingUpdateService azureProductPricingUpdateService;

    @Autowired
    private AzureTestingSupportService azureTestingSupportService;

    @Autowired
    private AzureProductPricingService azureProductPricingService;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private AzureCloudProviderPricingStatsService azureCloudProviderPricingStatsService;

    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpDir;

    private File azureScanDir;

    @Before
    public void setUp() {
        azureProductPricingService.deleteAll();
        productsMetadataService.removeAllProducts();
    }


    @Test
    public void forEmptyRepositoryItShouldReturnEmptyData() {

        CloudProviderStats cloudProviderStats = azureCloudProviderPricingStatsService.calculateCloudProviderStats();
        assertNotNull(cloudProviderStats);
        assertEquals(CloudProvider.AZURE, cloudProviderStats.getCloudProvider());
        assertEquals(0, cloudProviderStats.getProductsCount());
        assertTrue(cloudProviderStats.getPricings().isEmpty());

        List<PricingStats> pricingStats = azureCloudProviderPricingStatsService.calculatePricingStats();
        assertNotNull(pricingStats);
        assertTrue(pricingStats.isEmpty());

    }

}