package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureOfferTermTypes;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureOfferTypes;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzurePriceOffer;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingRecord;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingUpdateService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AZURE;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AzureProductPricingUpdateServiceTest {

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AzureRawPricingDataRepository azureRawPricingDataRepository;

    @Autowired
    private AzurePublicPricingRawDataLoader azurePublicPricingLoader;

    @Autowired
    private AzureProductPricingUpdateService azureProductPricingUpdateService;

    @Autowired
    private AzureProductPricingService azureProductPricingService;


    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpDir;

    private File scanDir;

    @Before
    public void setUp() {
        azureRawPricingDataRepository.deleteAll();
        dataVersionsService.removeAllVersions();
        scanDir = new File(azureDumpDir);
        scanDir.mkdirs();
        var files = scanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }
    }


    @Test
    public void shouldCheckDataVersionsAndPublicDataForEmptyCache() {

        // when
        boolean pricingShouldBeMissing = azureProductPricingUpdateService.hasPublicPricingLoaded();

        // then
        assertFalse(pricingShouldBeMissing);
        assertFalse(azureProductPricingUpdateService.hasPublicPricingLoaded());
    }

    @Ignore
    public void shouldLoadPricingUsingExternalApi() {

        CloudEnvironmentPricingId publicAzurePricingId = azurePublicPricingLoader.getPublicAzurePricingId();

        // when
        azureProductPricingUpdateService.updateUsingExternalApi(publicAzurePricingId, new PricingDataImportStatisticCollector(AZURE,
                "cloudAccountId", List.of()));

        // then
        Optional<VersionNum> lastVersionNum = dataVersionsService.lastVersionOf(publicAzurePricingId, AzureRawPricingDataRecord.VERSION_COLLECTION_NAME);
        assertTrue(lastVersionNum.isPresent());
        assertTrue(azureProductPricingUpdateService.hasPublicPricingLoaded());
    }

    @Test
    public void shouldGetPublicPricingId() {
        // when
        CloudEnvironmentPricingId publicPricingId = azureProductPricingUpdateService.getPublicPricingId();

        // then
        assertNotNull(publicPricingId);
        assertEquals(publicPricingId, azurePublicPricingLoader.getPublicAzurePricingId());
    }

    @Test
    public void shouldLoadRawPricingDataFromFiles() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AZURE);
        FileSystemUtils.copyRecursively(new File("src/test/data/azure/raw"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        assertTrue(azureProductPricingService.allProductIds(cloudEnvironmentPricingId).isEmpty());

        // when
        Optional<VersionedCloudEnvPricingId> maybeVersionedCloudEnvPricingId = azureProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // then
        assertNotNull(maybeVersionedCloudEnvPricingId);
        assertTrue(maybeVersionedCloudEnvPricingId.isPresent());
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = maybeVersionedCloudEnvPricingId.get();
        assertEquals(cloudEnvironmentPricingId, versionedCloudEnvPricingId.getCloudEnvironmentPricingId());

        Set<ProductId> productIds = azureProductPricingService.allProductIds(cloudEnvironmentPricingId);
        assertEquals(249, productIds.size());

        ProductId sampleProdId = new ProductId(cloudEnvironmentPricingId, "005a9d87-d9f1-4140-9529-3aabf427d803");
        Optional<AzureProductPricingRecord> foundProductPricing = azureProductPricingService.findProductPricing(sampleProdId);
        assertNotNull(foundProductPricing);
        assertTrue(foundProductPricing.isPresent());
        AzureProductPricingRecord prodPricing = foundProductPricing.get();
        assertEquals(sampleProdId, prodPricing.getId());
        assertEquals(versionedCloudEnvPricingId.getCloudEnvironmentPricingId(), prodPricing.getCloudEnvironmentPricingId());
        assertEquals(versionedCloudEnvPricingId.getVersionNum(), prodPricing.getBasedOnRawVersionNum());
        assertEquals(4, prodPricing.getPriceOffers().size());
        for (AzurePriceOffer priceOffer : prodPricing.getPriceOffers()) {
            assertNotNull(priceOffer);
            assertNotNull(priceOffer.getOfferTermType());
            assertEquals("canadaeast", priceOffer.getRegionCode());
            assertNotNull(priceOffer.getDescription());
            assertNotNull(priceOffer.getOfferType());
            assertEquals("1 Hour", priceOffer.getUnitOfMeasure());
            assertEquals(1, priceOffer.getRetailPriceByCurrency().size());
            assertEquals(1, priceOffer.getUnitPriceByCurrency().size());

            if (priceOffer.getOfferTermType().equals(AzureOfferTermTypes.ON_DEMAND.getOfferTermType())
               && priceOffer.getOfferType().equals(AzureOfferTypes.CONSUMPTION.getOfferType())) {
                assertEquals(3.974, priceOffer.getUnitPriceByCurrency().get("USD").doubleValue(), 0.001);
                assertEquals(3.974, priceOffer.getRetailPriceByCurrency().get("USD").doubleValue(), 0.001);
            } else if (priceOffer.getOfferTermType().equals(AzureOfferTermTypes.ON_DEMAND.getOfferTermType())
               && priceOffer.getOfferType().equals(AzureOfferTypes.DEV.getOfferType())) {
                assertEquals(2.974, priceOffer.getUnitPriceByCurrency().get("USD").doubleValue(), 0.001);
                assertEquals(2.974, priceOffer.getRetailPriceByCurrency().get("USD").doubleValue(), 0.001);
            }
        }
    }
}
