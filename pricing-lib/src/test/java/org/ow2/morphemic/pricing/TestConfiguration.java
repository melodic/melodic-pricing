package org.ow2.morphemic.pricing;

import com.github.cloudyrock.spring.v5.EnableMongock;
import org.ow2.morphemic.pricing.cloud_providers.security.CredentialsSupplier;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.resource.CloudCredential;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Optional;

/**
 * Base configuration for tests.
 */
@Configuration
@EnableMongock
class TestConfiguration {

    @Bean
    CredentialsSupplier emptyCredentialsSupplier() {
        return new CredentialsSupplier() {
            @Override
            public Optional<CloudCredential> apiCredentialsOf(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
                return Optional.of(new CloudCredential("test", "test"));
            }
        };
    }

}
