package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.GCP;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class GCPRawDataPricingManagerTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Autowired
    private GCPRawDataPricingManager gcpRawDataPricingManager;

    @Autowired
    private GCPRawServiceSKUDataRecordRepository gcpRawServiceSKUDataRecordRepository;

    @Autowired
    private GCPRawSKURecordRepository gcpRawSKURecordRepository;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Before

    public void setUp() {
        gcpRawServiceSKUDataRecordRepository.deleteAll();
        gcpRawSKURecordRepository.deleteAll();
        gcpRawDataPricingManager.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    @Test
    public void shouldReturnEmptyStreamWhenThereAreNoPricingData() {
        // given
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId(CloudProvider.GCP), new VersionNum());

        // when
        assertEquals(0L, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(versionedCloudEnvPricingId).count());
    }

    @Test
    public void shouldReturnStreamWithPricingData() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId();
        VersionedCloudEnvPricingId oldEnvVersion = createTestDataForVersion(cloudEnvironmentPricingId, 1);
        VersionedCloudEnvPricingId envVersion = createTestDataForVersion(cloudEnvironmentPricingId, 2);

        // when
        List<GCPRawServicePricing> gcpPricingElts = gcpRawDataPricingManager
                .rawPricingDataAsStreamForVersion(envVersion).collect(Collectors.toList());

        // then
        assertEquals(4, gcpPricingElts.size());
        gcpPricingElts.forEach(elt -> {
            assertNotNull(elt);
            assertNotNull(elt.getGcpService());
            assertNotNull(elt.getGcpSkusWithPrices());
        });
    }

    @Test
    public void shouldDeleteRawDataForGivenPricing() throws Exception {
        // given
        CloudEnvironmentPricingId pricing1 = new CloudEnvironmentPricingId(CloudProvider.GCP);
        CloudEnvironmentPricingId pricing2 = new CloudEnvironmentPricingId(CloudProvider.GCP);

        VersionedCloudEnvPricingId pricing1Ver = createTestDataForVersion(pricing1, 2);
        VersionedCloudEnvPricingId pricing2Ver = createTestDataForVersion(pricing2, 3);

        assertEquals(4, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing1Ver).count());
        assertEquals(6, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing2Ver).count());
        assertEquals(80, gcpRawSKURecordRepository.count());

        // when
        gcpRawDataPricingManager.deletePricing(pricing1);

        // then
        assertEquals(0, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing1Ver).count());
        assertEquals(6, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing2Ver).count());
        assertEquals(48, gcpRawSKURecordRepository.count());

        // removing unknown pricing must not change anything
        gcpRawDataPricingManager.deletePricing(new CloudEnvironmentPricingId());

        assertEquals(0, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing1Ver).count());
        assertEquals(6, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing2Ver).count());
        assertEquals(48, gcpRawSKURecordRepository.count());

        gcpRawDataPricingManager.deleteAll();

        assertEquals(0, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing1Ver).count());
        assertEquals(0, gcpRawDataPricingManager.rawPricingDataAsStreamForVersion(pricing2Ver).count());

        assertEquals(0, gcpRawSKURecordRepository.count());

    }

    /**
     * @param cloudEnvironmentPricingId test cloud env id
     * @param xTimes             it will load x times the same test data set
     * @return new version num
     * @throws IOException
     */
    private VersionedCloudEnvPricingId createTestDataForVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, int xTimes) throws IOException {
        List<GCPServicePricingDataStreamSupplier> suppliers = new ArrayList<>();
        // loading X times the same data
        for (int i = 0; i < xTimes; i++) {
            File dir = temporaryFolder.newFolder();
            dir.mkdirs();
            Files.write(new File(dir, FS_LOADER_OK_FILE_NAME).toPath(),
                    cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
            FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), dir);

            GCPFilesServicePricingDataStreamSupplier supplier = new GCPFilesServicePricingDataStreamSupplier(dir);
            suppliers.add(supplier);
        }

        return new GCPPricingRawDataLoader(suppliers, gcpRawServiceSKUDataRecordRepository, gcpRawSKURecordRepository, dataVersionsService)
                .loadRawDataToCache(new PricingDataImportStatisticCollector(GCP, "account_id", List.of())).get();
    }

}