package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansProduct;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AWSSavingsPlansRawFilesDataLoaderTest {

    @Autowired
    private AWSSavingsPlansRawFilesDataLoader loader;

    @Autowired
    private AWSRawSavingsPlansPricingDataRepository repository;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AWSRawSavingsPlansManager awsRawSavingsPlansManager;
    @Autowired
    private AWSSavingsPlansPublicRawDataLoader publicLoader;

    @Value("${pricing.datadump.aws.savingsplans.dir}")
    private String awsDumpDir;

    private File scanDir;

    @Before
    public void setUp() {
        repository.deleteAll();
        dataVersionsService.removeAllVersions();
        scanDir = new File(awsDumpDir);
        scanDir.mkdirs();
        var files = scanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }
    }

    @Test
    public void shouldLoadNoTestDataToRawCacheWhenDirIsEmpty() throws IOException {
        //when
        Optional<VersionedCloudEnvPricingId> maybeClEnvVerPricingId = loader.loadDumpToRawCache();

        // then
        assertNotNull(maybeClEnvVerPricingId);
        assertTrue(maybeClEnvVerPricingId.isEmpty());
        assertTrue(new File(awsDumpDir).isDirectory());
    }

    @Test
    public void shouldLoadRawPricingData() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = publicLoader.getPublicAwsSavingsPlanPricingId();
        FileSystemUtils.copyRecursively(new File("src/test/data/aws/savings_plans/v1"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        Optional<VersionedCloudEnvPricingId> firstClEnvVerPricingId = loader.loadDumpToRawCache();

        assertNotNull(firstClEnvVerPricingId);
        assertTrue(firstClEnvVerPricingId.isPresent());

        assertEquals(62, repository.count());
        var files = scanDir.listFiles();
        assertEquals(0, files.length);

        List<AWSRawSavingsPlansPricingDataRecord> rawPricingRecords = awsRawSavingsPlansManager.rawPricingDataAsFluxForVersion(firstClEnvVerPricingId.get()).collectList().block();
        assertNotNull(rawPricingRecords);
        assertEquals(10, rawPricingRecords.size());
        Set<Long> collectedSortPositionNums = new HashSet<>();
        rawPricingRecords.forEach(r -> {
            assertNotNull(r);
            assertNotNull(r.getId());
            assertNotNull(r.getRawRates());
            assertNotNull(r.getRegionCode());
            assertNotNull(r.getAwsVersion());
            assertNotNull(r.getCloudEnvironmentPricingId());
            assertNotNull(r.getSortPosNum());
            assertFalse(collectedSortPositionNums.contains(r.getSortPosNum()));
            collectedSortPositionNums.add(r.getSortPosNum());
        });

        collectedSortPositionNums.clear();
        List<AWSSavingsPlansProduct> rawProductDataRecords = awsRawSavingsPlansManager.rawProductDataAsFluxForVersion(firstClEnvVerPricingId.get()).collectList().block();
        assertNotNull(rawProductDataRecords);
        assertEquals(52, rawProductDataRecords.size());
        rawProductDataRecords.forEach(r -> {
            assertNotNull(r);
            assertNotNull(r.getOperation());
            assertNotNull(r.getUsageType());
            assertNotNull(r.getAttributes());
            assertNotNull(r.getSkuId());
            assertNotNull(r.getServiceCode());
            assertNotNull(r.getProductFamily());
        });

        FileSystemUtils.copyRecursively(new File("src/test/data/aws/savings_plans/v1"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
        Optional<VersionedCloudEnvPricingId> nextUpdateClEnvVerPricingId = loader.loadDumpToRawCache();

        assertNotNull(nextUpdateClEnvVerPricingId);
        assertTrue(nextUpdateClEnvVerPricingId.isEmpty());

        assertEquals(62, repository.count());
        files = scanDir.listFiles();
        assertEquals(0, files.length);

        assertEquals(Long.valueOf(10), awsRawSavingsPlansManager.rawPricingDataAsFluxForVersion(firstClEnvVerPricingId.get()).count().block());
        assertEquals(Long.valueOf(52), awsRawSavingsPlansManager.rawProductDataAsFluxForVersion(firstClEnvVerPricingId.get()).count().block());

        // Load new Aws savings plans version
        FileSystemUtils.copyRecursively(new File("src/test/data/aws/savings_plans/v2"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
        Optional<VersionedCloudEnvPricingId> updateWithNewAwsSavingsPlansVersion = loader.loadDumpToRawCache();

        assertNotNull(updateWithNewAwsSavingsPlansVersion);
        assertTrue(updateWithNewAwsSavingsPlansVersion.isPresent());

        assertEquals(122, repository.count());
        files = scanDir.listFiles();
        assertEquals(0, files.length);

        assertEquals(Long.valueOf(10), awsRawSavingsPlansManager.rawPricingDataAsFluxForVersion(firstClEnvVerPricingId.get()).count().block());
        assertEquals(Long.valueOf(52), awsRawSavingsPlansManager.rawProductDataAsFluxForVersion(firstClEnvVerPricingId.get()).count().block());

        assertEquals(Long.valueOf(9), awsRawSavingsPlansManager.rawPricingDataAsFluxForVersion(updateWithNewAwsSavingsPlansVersion.get()).count().block());
        assertEquals(Long.valueOf(51), awsRawSavingsPlansManager.rawProductDataAsFluxForVersion(updateWithNewAwsSavingsPlansVersion.get()).count().block());
    }
}
