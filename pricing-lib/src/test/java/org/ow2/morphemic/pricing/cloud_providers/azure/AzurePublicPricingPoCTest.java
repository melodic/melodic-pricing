package org.ow2.morphemic.pricing.cloud_providers.azure;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzureRawPricingResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This is PoC for loading public Azure pricing API: https://prices.azure.com/api/retail/prices
 * This class saves all results to files and it can be run only manually.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AzurePublicPricingPoCTest {

    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpImportDir;

    private File azureScanDir;

    @Before
    public void setUp() throws IOException {
        azureScanDir = new File(azureDumpImportDir);
        azureScanDir.mkdirs();

        Files.walkFileTree(azureScanDir.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
        azureScanDir.mkdirs();
    }

    @Test
    @Ignore
    public void getAzurePublicPricing() {

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            String url = "https://prices.azure.com/api/retail/prices?$filter=serviceName%20eq%20%27Virtual%20Machines%27%20and%20priceType%20eq%20%27Consumption%27%20or%20priceType%20eq%20%27Reservation%27";
            AtomicInteger pageNum = new AtomicInteger();
            AzureRawPricingResponse azureRawPricingPage = loadOnePricingPage(url, httpClient);
            String nextPageLink = azureRawPricingPage.getNextPageLink();
            saveResultsFromOnePageToFile(azureRawPricingPage, pageNum.get());
            while (nextPageLink != null) {
                if (pageNum.get() % 100 == 0) {
                    log.info("=========================== One Azure pricing page loaded. Total items num = {} ======================", pageNum.get());
                }
                azureRawPricingPage = loadOnePricingPage(nextPageLink, httpClient);
                pageNum.incrementAndGet();
                saveResultsFromOnePageToFile(azureRawPricingPage, pageNum.get());
                nextPageLink = azureRawPricingPage.getNextPageLink();
            }
        } catch (IOException e) {
            log.error("Error by getting public Azure pricing: ", e);

        }
    }

    private void saveResultsFromOnePageToFile(AzureRawPricingResponse azureRawPricingPage, int pageNum) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        File file = new File(azureScanDir, String.format("pricing_%05d.json", pageNum));
        mapper.writeValue(file, azureRawPricingPage);

//        FileWriter fileWriter = new FileWriter(file, false);
//
//        SequenceWriter seqWriter = mapper.writer().writeValuesAsArray(fileWriter);
//        for (AzureRawPricingItem item : azureRawPricingPage.getItems()) {
//            seqWriter.write(item);
//        }
//        seqWriter.close();
    }

    private AzureRawPricingResponse loadOnePricingPage(String url, CloseableHttpClient httpClient) throws IOException {
        HttpGet request = new HttpGet(url);
        HttpResponse response = httpClient.execute(request);
        InputStream content = response.getEntity().getContent();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(content, AzureRawPricingResponse.class);
    }
}
