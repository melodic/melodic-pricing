package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;

/**
 * Supports integration tests for AWS
 */
@Slf4j
@Service
public class AWSTestingSupportService {

    @Value("${pricing.datadump.aws.dir}")
    private String dumpImportDir;

    public File prepareScanDir() throws IOException {
        File scanDir = new File(dumpImportDir);
        scanDir.mkdirs();

        Files.walkFileTree(scanDir.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
        scanDir.mkdirs();

        return scanDir;
    }

    public void fillSourceDirectoryWithSamplePricingFiles(CloudEnvironmentPricingId pricingId, int dirsCount, int filesInDir) throws IOException {
        File scanDir = prepareScanDir();
        for (int i = 0; i < dirsCount; i++) {
            File packDir = new File(scanDir, String.format("p%02d", i));
            packDir.mkdirs();
            for (int rec = 0; rec < filesInDir; rec++) {
                Files.copy(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), packDir.toPath().resolve(String.format("p_%06d.json", rec)));
            }
        }
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), pricingId.getId().getBytes(StandardCharsets.UTF_8));

    }

}
