package org.ow2.morphemic.pricing.cloud_providers.gcp;

import org.junit.Test;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.gcp.GCPAggregatedProductIdFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class GCPAggregatedProductIdFactoryTest {

    @Test
    public void shouldCreateId() {
        CloudEnvironmentPricingId pricingId = new CloudEnvironmentPricingId();
        ProductId id1 = new GCPAggregatedProductIdFactory().createVMId(pricingId, "abcd", new Place("r1"));
        ProductId id2 = new GCPAggregatedProductIdFactory().createVMId(pricingId, "abcd", new Place("r1"));
        assertNotNull(id1);
        assertEquals("must not be random", id1, id2);
    }

    @Test
    public void idMustDifferBetweenRegions() {
        CloudEnvironmentPricingId pricingId = new CloudEnvironmentPricingId();
        ProductId id1 = new GCPAggregatedProductIdFactory().createVMId(pricingId, "abcd", new Place("r1"));
        ProductId id2 = new GCPAggregatedProductIdFactory().createVMId(pricingId, "abcd", new Place("r2"));
        assertNotNull(id1);
        assertNotEquals("must not be equal!", id1, id2);
    }


}