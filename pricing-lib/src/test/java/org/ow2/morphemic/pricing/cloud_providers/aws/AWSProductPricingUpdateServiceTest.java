package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSClientPricingRawDataLoaderFactory;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSPricingRawDataLoader;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSRawPricingDataManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService.VERSIONED_AWS_PRICING_COLLECTION_NAME;
import static org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AWS;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AWSProductPricingUpdateServiceTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Autowired
    private AWSProductPricingUpdateService productPricingUpdateService;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AWSProductRecordsRepository awsProductRecordsRepository;

    @Autowired
    private AWSProductPricingRecordsRepository awsProductPricingRecordsRepository;

    @Autowired
    private AWSRawPricingDataManager awsRawPricingDataManager;

    @MockBean
    private AWSClientPricingRawDataLoaderFactory awsClientLoaderFactory;

    @Mock
    private AWSClientPricingRawDataLoaderFactory.CloseableAWSPricingRawDataLoader closeableAWSPricingRawDataLoader;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    @Qualifier("awsPricingFromFiles")
    private AWSPricingRawDataLoader awsPricingFromFiles;

    @Value("${pricing.datadump.aws.dir}")
    private String dumpImportDir;

    private File scanDir;

    @Autowired
    private AWSCloudProviderPricingStatsService awsCloudProviderPricingStatsService;

    @Before
    public void setUp() throws IOException {
        dataVersionsService.removeAllVersions();
        awsRawPricingDataManager.deleteAll();
        awsProductRecordsRepository.deleteAll();
        awsProductPricingRecordsRepository.deleteAll();
        productsMetadataService.removeAllProducts();

        scanDir = new File(dumpImportDir);
        scanDir.mkdirs();

        Files.walkFileTree(scanDir.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
        scanDir.mkdirs();
    }

    @Test
    public void shouldUpdatePricingDataUsingFileLoader() throws Exception {
        // given
        fillSourceDirectoryWithSamplePricingFiles(scanDir, 5, 3);
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AWS);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        // when
        var maybeNewPricingId = productPricingUpdateService.loadPricingDumpDataFromFiles();

        // then
        assertEquals(1L, awsProductRecordsRepository.count()); // one product should be loaded
        assertEquals(1L, awsProductPricingRecordsRepository.count()); // one product should be loaded

        Optional<VersionNum> maybeVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, VERSIONED_AWS_PRICING_COLLECTION_NAME);
        assertNotNull(maybeVersion);
        assertTrue(maybeVersion.isPresent());
        assertEquals(maybeNewPricingId.get().getVersionNum(), maybeVersion.get());

        CloudProviderStats cloudProviderStats = awsCloudProviderPricingStatsService.calculateCloudProviderStats();
        List<PricingStats> pricingStats = awsCloudProviderPricingStatsService.calculatePricingStats();

        assertNotNull(cloudProviderStats);
        assertEquals(AWS, cloudProviderStats.getCloudProvider());
        assertEquals(1, cloudProviderStats.getProductsCount());
        assertEquals(1, cloudProviderStats.getPricings().size());
        assertEquals(Set.of(cloudEnvironmentPricingId), cloudProviderStats.getPricings());

        assertNotNull(pricingStats);
        assertEquals(1, pricingStats.get(0).getProductsCount());
        assertEquals(AWS, pricingStats.get(0).getCloudProvider());
        assertEquals(cloudEnvironmentPricingId, pricingStats.get(0).getCloudEnvironmentPricingId());

        // when
        AtomicInteger pgCounter = new AtomicInteger();
        final PricingDataImportStatisticCollector pricingDataImportStatistic = new PricingDataImportStatisticCollector(AWS,
                "pricingDataImportStatistic", List.of());

        productPricingUpdateService.processLatestFetchedPricingData(maybeNewPricingId.get().getCloudEnvironmentPricingId(),
                List.of(list -> {
                    assertNotNull(list);
                    assertFalse(list.isEmpty());
                    pgCounter.incrementAndGet();
                }),
                pricingDataImportStatistic);
        assertEquals(1, pgCounter.get());

    }

    @Test
    public void shouldUpdateDataFromAWSForGivenCloudProvider() throws IOException {
        // given
        fillSourceDirectoryWithSamplePricingFiles(scanDir, 5, 3);
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AWS);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        final PricingDataImportStatisticCollector pricingDataImportStatistic = new PricingDataImportStatisticCollector(AWS,
                "pricingDataImportStatistic", List.of());
        when(awsClientLoaderFactory.createPricingAWSLoader(eq(cloudEnvironmentPricingId), eq("test"), eq("test"), isA(Set.class)))
                .thenReturn(closeableAWSPricingRawDataLoader);
        when(closeableAWSPricingRawDataLoader.loadRawDataToCache(isA(PricingDataImportStatisticCollector.class)))
                .thenAnswer(ans -> awsPricingFromFiles.loadRawDataToCache(pricingDataImportStatistic));

        // when
        productPricingUpdateService.updateUsingExternalApi(cloudEnvironmentPricingId,
                pricingDataImportStatistic);

        // then
        assertEquals(1L, awsProductRecordsRepository.count()); // one product should be loaded
        assertEquals(1L, awsProductPricingRecordsRepository.count()); // one product should be loaded

        awsProductRecordsRepository.findAll().forEach(prod -> {
            ProductId productId = prod.getId();
            assertTrue(productsMetadataService.isProductMetadataInCache(productId));
            Optional<CloudProvider> maybeFound = productsMetadataService.findCloudProviderOf(productId);
            assertTrue(maybeFound.isPresent());
            assertEquals(AWS, maybeFound.get());

        });
    }

    private void fillSourceDirectoryWithSamplePricingFiles(File rootDir, int dirsCount, int filesInDir) throws IOException {
        for (int i = 0; i < dirsCount; i++) {
            File packDir = new File(rootDir, String.format("p%02d", i));
            packDir.mkdirs();
            for (int rec = 0; rec < filesInDir; rec++) {
                Files.copy(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), packDir.toPath().resolve(String.format("p_%06d.json", rec)));
            }
        }

    }


}