package org.ow2.morphemic.pricing.calculators.azure;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.calculators.PricingOption;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureOfferTypes;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzurePublicPricingRawDataLoader;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.OfferType;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AZURE;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AzurePriceCalculatorTest {
    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AzurePublicPricingRawDataLoader azurePublicPricingLoader;

    @Autowired
    private AzureProductPricingUpdateService azureProductPricingUpdateService;

    @Autowired
    private AzureProductPricingService azureProductPricingService;

    @Autowired
    private AzurePriceCalculator azurePriceCalculator;

    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpDir;

    private File scanDir;

    @Before
    public void setUp() {
        azureProductPricingService.deleteAll();
        dataVersionsService.removeAllVersions();
        scanDir = new File(azureDumpDir);
        scanDir.mkdirs();
        var files = scanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }
    }

    @Test
    public void shouldReturnEmptyPriceListForUnknownProduct() throws Exception {
        ProductId unknownProdId = new ProductId(new CloudEnvironmentPricingId(CloudProvider.AZURE), "unknown1244");
        Optional<List<AzurePriceCalculator.CalculatedPrice>> calculatedPrices = azurePriceCalculator.calculatePrices(unknownProdId, new Amount(1.0, new Unit("1 Hour")), new OfferTermType(""), null);
        assertNotNull(calculatedPrices);
        assertTrue(calculatedPrices.isEmpty());
    }


    @Test
    public void shouldCalculatePriceForRealProductOnDemandAndReservations() throws Exception {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AZURE);
        FileSystemUtils.copyRecursively(new File("src/test/data/azure/raw"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
        assertTrue(azureProductPricingUpdateService.loadPricingDumpDataFromFiles()
                .isPresent());

        azureProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // when
        ProductId productId = new ProductId(cloudEnvironmentPricingId, "0008ac09-705a-431e-b9c7-026746cb920a");
        Unit unit = new Unit("1 Hour");
        Optional<Price> calculatedPrice = azurePriceCalculator.calculatePrice(productId, new Amount(3, unit),
                new OfferTermType(""), AzureOfferTypes.CONSUMPTION.getOfferType());

        assertNotNull(calculatedPrice);
        assertTrue(calculatedPrice.isPresent());
        Price price = calculatedPrice.get();
        assertEquals("USD", price.getCurrencyCode());
        assertEquals(3 * 0.112d, price.getValue().doubleValue(), 0.001);

        /// reservations
        productId = new ProductId(cloudEnvironmentPricingId, "005a9d87-d9f1-4140-9529-3aabf427d803");
        unit = new Unit("");

        calculatedPrice = azurePriceCalculator.calculatePrice(productId, new Amount(1, unit),
                new OfferTermType("3 Years"), AzureOfferTypes.RESERVATION.getOfferType());

        assertNotNull(calculatedPrice);
        assertTrue(calculatedPrice.isPresent());
        price = calculatedPrice.get();
        assertEquals("USD", price.getCurrencyCode());
        assertEquals(43510.0, price.getValue().doubleValue(), 0.001);


        calculatedPrice = azurePriceCalculator.calculatePrice(productId, new Amount(1, unit),
                new OfferTermType("100 Years"), AzureOfferTypes.CONSUMPTION.getOfferType());

        assertNotNull(calculatedPrice);
        assertTrue(calculatedPrice.isEmpty());
    }

    @Test
    public void shouldCalculatePricesForRealProductOnDemandAndReservationsBeforeAddOfferType() throws Exception {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AZURE);
        FileSystemUtils.copyRecursively(new File("src/test/data/azure/raw"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
        assertTrue(azureProductPricingUpdateService.loadPricingDumpDataFromFiles()
                .isPresent());

        var productId = new ProductId(cloudEnvironmentPricingId, "005a9d87-d9f1-4140-9529-3aabf427d803"); // meter i from 1st test file

        // when
        OfferTermType onDemandAzureOffer = new OfferTermType(""); // reservationTerm is null - this is "on demand" Azure offer decoed as empty string/
        Unit unit = new Unit("1 Hour"); // unit must be the same as in raw pricing files
        Optional<List<AzurePriceCalculator.CalculatedPrice>> maybeCalculatedPrices = azurePriceCalculator.calculatePrices(productId, new Amount(3.0, unit), onDemandAzureOffer, null);

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());

        List<AzurePriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(2, calculatedPrices.size());
        assertEquals(new Price(new BigDecimal("11.9220"), "USD"), calculatedPrices.get(0).getPrice());

        // reservations
        // when
        OfferTermType reservedOffer = new OfferTermType("1 Year"); // reservationTerm is null - this is "on demand" Azure offer decoed as empty string/
        maybeCalculatedPrices = azurePriceCalculator.calculatePrices(productId, new Amount(3.0, unit), reservedOffer, null);

        calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        assertEquals(new Price(new BigDecimal("68043.0"), "USD"), calculatedPrices.get(0).getPrice());

        reservedOffer = new OfferTermType("3 Years"); // reservationTerm is null - this is "on demand" Azure offer decoed as empty string/
        maybeCalculatedPrices = azurePriceCalculator.calculatePrices(productId, new Amount(3.0, unit), reservedOffer, null);

        calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        assertEquals(new Price(new BigDecimal("130530.0"), "USD"), calculatedPrices.get(0).getPrice());
    }

    @Test
    public void shouldCalculatePricesForRealProductOnDemandAndReservations() throws Exception {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AZURE);
        FileSystemUtils.copyRecursively(new File("src/test/data/azure/raw"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
        assertTrue(azureProductPricingUpdateService.loadPricingDumpDataFromFiles()
                .isPresent());

        var productId = new ProductId(cloudEnvironmentPricingId, "005a9d87-d9f1-4140-9529-3aabf427d803"); // meter i from 1st test file

        // when
        OfferTermType onDemandAzureOffer = new OfferTermType(""); // reservationTerm is null - this is "on demand" Azure offer decoed as empty string/
        OfferType offerType = new OfferType("Consumption");
        Unit unit = new Unit("1 Hour"); // unit must be the same as in raw pricing files
        Optional<List<AzurePriceCalculator.CalculatedPrice>> maybeCalculatedPrices = azurePriceCalculator.calculatePrices(productId, new Amount(3.0, unit), onDemandAzureOffer, offerType);

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());

        List<AzurePriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        assertEquals(new Price(new BigDecimal("11.9220"), "USD"), calculatedPrices.get(0).getPrice());

        // when
        OfferType devOfferType = new OfferType("DevTestConsumption");
        maybeCalculatedPrices = azurePriceCalculator.calculatePrices(productId, new Amount(3.0, unit), onDemandAzureOffer, devOfferType);

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());

        calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        assertEquals(new Price(new BigDecimal("8.9220"), "USD"), calculatedPrices.get(0).getPrice());

        // reservations
        // when
        OfferType reservedOfferType = new OfferType("Reservation");
        OfferTermType reservedOffer = new OfferTermType("1 Year"); // reservationTerm is null - this is "on demand" Azure offer decoed as empty string/
        maybeCalculatedPrices = azurePriceCalculator.calculatePrices(productId, new Amount(3.0, unit), reservedOffer, reservedOfferType);

        calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        assertEquals(new Price(new BigDecimal("68043.0"), "USD"), calculatedPrices.get(0).getPrice());

        reservedOffer = new OfferTermType("3 Years"); // reservationTerm is null - this is "on demand" Azure offer decoed as empty string/
        maybeCalculatedPrices = azurePriceCalculator.calculatePrices(productId, new Amount(3.0, unit), reservedOffer, reservedOfferType);

        calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        assertEquals(new Price(new BigDecimal("130530.0"), "USD"), calculatedPrices.get(0).getPrice());
    }

}