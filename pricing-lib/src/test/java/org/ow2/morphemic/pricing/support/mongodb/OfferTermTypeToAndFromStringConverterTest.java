package org.ow2.morphemic.pricing.support.mongodb;

import org.junit.Test;
import org.ow2.morphemic.pricing.model.OfferTermType;

import static org.junit.Assert.*;

public class OfferTermTypeToAndFromStringConverterTest {

    @Test
    public void shouldConvertProeprly() {
        OfferTermTypeFromStringConverter fromStr = new OfferTermTypeFromStringConverter();
        OfferTermTypeToStringConverter toStr = new OfferTermTypeToStringConverter();

        assertEquals(new OfferTermType("OnDemand"), fromStr.convert(toStr.convert(new OfferTermType("OnDemand"))));
    }

}