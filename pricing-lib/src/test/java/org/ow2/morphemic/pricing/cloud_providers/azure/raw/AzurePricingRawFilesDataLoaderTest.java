package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AZURE;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AzurePricingRawFilesDataLoaderTest {

    @Rule
    public TemporaryFolder temporaryFolder;

    @Autowired
    private AzurePricingRawFilesDataLoader loader;

    @Autowired
    private AzureRawPricingDataRepository repository;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AzureRawProductPricingManager azureRawProductPricingManager;

    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpDir;

    private File scanDir;

    @Before
    public void setUp() {
        repository.deleteAll();
        dataVersionsService.removeAllVersions();
        scanDir = new File(azureDumpDir);
        scanDir.mkdirs();
        var files = scanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }
    }

    @Test
    public void shouldLoadNoTestDataToRawCacheWhenDirIsEmpty() throws IOException {
        // when
        Optional<VersionedCloudEnvPricingId> maybeClEnvVerPricingId = loader.loadDumpToRawCache(new PricingDataImportStatisticCollector(AZURE,
                "cloudAccountId", List.of()));

        // then
        assertNotNull(maybeClEnvVerPricingId);
        assertTrue(maybeClEnvVerPricingId.isEmpty());
        assertTrue(new File(azureDumpDir).isDirectory());
    }

    @Test
    public void shouldLoadRawPricingData() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AZURE);
        FileSystemUtils.copyRecursively(new File("src/test/data/azure/raw"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        // when
        Optional<VersionedCloudEnvPricingId> maybeClEnvVerPricingId = loader.loadDumpToRawCache(new PricingDataImportStatisticCollector(AZURE,
                "cloudAccountId", List.of()));

        // then
        assertNotNull(maybeClEnvVerPricingId);
        assertTrue(maybeClEnvVerPricingId.isPresent());

        assertEquals(301L, repository.count());
        var files = scanDir.listFiles();
        assertEquals(0, files.length);

        VersionedCloudEnvPricingId versionedCloudEnvPricingId = maybeClEnvVerPricingId.get();
        assertEquals(301L, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(versionedCloudEnvPricingId).count());

        Collection<AzureRawPricingDataRecord> meterIdRecords = azureRawProductPricingManager.rawPricingDataForMeterIdAndVersion(versionedCloudEnvPricingId, "005a9d87-d9f1-4140-9529-3aabf427d803");
        assertNotNull(meterIdRecords);
        assertEquals(4, meterIdRecords.size());
        assertTrue(meterIdRecords.stream().allMatch(raw -> "005a9d87-d9f1-4140-9529-3aabf427d803".equals(raw.getRawContent().getMeterId())));

    }

}