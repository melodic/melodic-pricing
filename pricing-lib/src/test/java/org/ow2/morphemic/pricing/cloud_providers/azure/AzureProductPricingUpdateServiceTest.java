package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AZURE;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AzureProductPricingUpdateServiceTest {

    @Autowired
    private AzureProductPricingUpdateService azureProductPricingUpdateService;

    @Autowired
    private AzureTestingSupportService azureTestingSupportService;

    @Autowired
    private AzureProductPricingService azureProductPricingService;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpDir;

    private File azureScanDir;

    @Before
    public void setUp() {
        azureProductPricingService.deleteAll();
        productsMetadataService.removeAllProducts();
    }

    @Test
    public void shouldUpdatePricingFromFiles() throws Exception {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AZURE);
        azureTestingSupportService.prepareTestPricingToLoad(cloudEnvironmentPricingId);

        // when
        azureProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // then
        markerFileMustNotExist();
        Set<ProductId> productIds = azureProductPricingService.allProductIds(cloudEnvironmentPricingId);
        for (ProductId productId : productIds) {
            assertTrue(productsMetadataService.isProductMetadataInCache(productId));
            Optional<AzureProductPricingRecord> foundPricing = azureProductPricingService.findProductPricing(productId);
            assertNotNull(foundPricing);
            assertTrue(foundPricing.isPresent());
        }

        assertEquals(249, productIds.size());

        // and when again
        azureTestingSupportService.prepareTestPricingToLoad(cloudEnvironmentPricingId);
        azureProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // then
        markerFileMustNotExist();
        Set<ProductId> productIds2 = azureProductPricingService.allProductIds(cloudEnvironmentPricingId);
        assertEquals(productIds, productIds2);

        // and where there is no dump to import
        azureProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // then
        productIds2 = azureProductPricingService.allProductIds(cloudEnvironmentPricingId);
        assertEquals(productIds, productIds2);
        markerFileMustNotExist();
    }

    void markerFileMustNotExist() {
        assertFalse(new File(azureScanDir, FS_LOADER_OK_FILE_NAME).isFile());
    }


}