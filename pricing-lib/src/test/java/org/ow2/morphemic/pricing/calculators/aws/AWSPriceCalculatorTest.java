package org.ow2.morphemic.pricing.calculators.aws;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingUpdateService;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AWSPriceCalculatorTest {

    @Autowired
    private AWSProductPricingService productPricingService;

    @Autowired
    private AWSProductPricingUpdateService productPricingUpdateService;

    @Autowired
    private AWSPriceCalculator awsPriceCalculator;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Before
    public void setUp() {
        productPricingService.deleteAll();
        productsMetadataService.removeAllProducts();
    }

    @Test
    public void mustNotCalculateWhenThereIsNoSuchProduct() {
        ProductId productId = new ProductId("DoesNotExtst");

        // when: we want to buy 3 hours 'OnDemand" of "r5d.2xlarge" - but it does not exist!
        Amount amount = new Amount(3, new Unit("Hrs"));
        Optional<List<AWSPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = awsPriceCalculator
                .calculatePrice(productId, amount, new OfferTermType("OnDemand"));

        // then
        assertNotNull(maybeCalculatedPrices);
        assertFalse(maybeCalculatedPrices.isPresent());
    }

    @Test
    public void shouldCalculateValueEC2() throws IOException {
        // given
        VersionedCloudEnvPricingId verClId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(123));
        productPricingUpdateService.updateProductsAndPricingData(Collections.singletonList(Files.readString(Paths.get("src", "test", "data", "aws", "ec2_p_00082.json"), StandardCharsets.UTF_8)), verClId);
        ProductId productId = new ProductId(verClId.getCloudEnvironmentPricingId(), "K3CVNEAWU7BY7JWE");

        // when: we want to buy 3 hours 'OnDemand" of "r5d.2xlarge"
        Amount amount = new Amount(3, new Unit("Hrs"));
        Optional<List<AWSPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = awsPriceCalculator.calculatePrice(productId, amount,
                new OfferTermType("OnDemand"));

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());
        List<AWSPriceCalculator.CalculatedPrice> prices = maybeCalculatedPrices.get();
        assertEquals(1, prices.size());
        AWSPriceCalculator.CalculatedPrice calculatedPrice = prices.get(0);
        Price price = calculatedPrice.getPrice();
        assertEquals("USD", price.getCurrencyCode());
        assertNotNull(price.getValue());
        assertEquals(new BigDecimal("3.944").multiply(new BigDecimal(3)).doubleValue(), price.getValue().doubleValue(), 0.001);

        log.debug("RESULT 1: Calculated price OnDemand for product: {} price: {}",
                productId.getId(), calculatedPrice);

        // when: we want to buy 2 'Reserved' instances of "r5d.2xlarge"
        amount = new Amount(2, new Unit("Quantity"));
        maybeCalculatedPrices = awsPriceCalculator.calculatePrice(productId, amount, new OfferTermType("Reserved"));

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());
        prices = maybeCalculatedPrices.get();
        assertEquals(8, prices.size());

        log.debug("RESULT 2: Calculated price Reserved for product: {} {}", productId.getId(), prices);
//        calculatedPrice = prices.get(0);
//        assertEquals("USD", calculatedPrice.getCurrencyCode());
//        assertNotNull(calculatedPrice.getValue());
//        assertEquals(new BigDecimal("3.944").multiply(new BigDecimal(3)).doubleValue(), calculatedPrice.getValue().doubleValue(), 0.001);


    }

    @Test
    public void shouldCalculatePricesForS3Transfer() throws IOException {
        VersionedCloudEnvPricingId verClId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(123));
        productPricingUpdateService.updateProductsAndPricingData(Collections.singletonList(Files.readString(
                Paths.get("src", "test", "data", "aws", "s3_p_00077.json"), StandardCharsets.UTF_8)),
                verClId);
        ProductId productId = new ProductId(verClId.getCloudEnvironmentPricingId(), "358YXGGXWV4YQSEA");

        // when: how much will we pay for 511999 Gb stored in S3 per month
        Amount amount = new Amount(511999, new Unit("GB-Mo"));
        Optional<List<AWSPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = awsPriceCalculator.calculatePrice(productId,
                amount, new OfferTermType("OnDemand"));

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());
        List<AWSPriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());

        log.debug("Prices {}", calculatedPrices);
        for (AWSPriceCalculator.CalculatedPrice calculatedPrice : calculatedPrices) {
            assertNotNull(calculatedPrice.getPrice());
            assertNotNull(calculatedPrice.getExternalRateCodeId());
            assertNotNull(calculatedPrice.getRateAttributes());
            assertNotNull(calculatedPrice.getPricingOption());
        }

    }

}