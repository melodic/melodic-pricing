package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzureRawPricingDataRecord;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzureRawProductPricingManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AzureProductPricingServiceTest {

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private AzureRawProductPricingManager azureRawProductPricingManager;

    @Autowired
    private AzureTestingSupportService azureTestingSupportService;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private AzureProductPricingService azureProductPricingService;

    @Autowired
    private AzureProductPricingUpdateService azureProductPricingUpdateService;

    @Before
    public void setUp() {
        azureProductPricingService.deleteAll();
        productsMetadataService.removeAllProducts();
    }

    @Test
    public void shouldReturnEmptyResultForUnknownProductId() {
        Optional<AzureProductPricingRecord> foundProduct = azureProductPricingService.findProductPricing(new ProductId("unknown"));
        assertNotNull(foundProduct);
        assertFalse(foundProduct.isPresent());
    }

    @Test
    public void shouldLoadProductPricingData() throws Exception {
        // given
        CloudEnvironmentPricingId pricingId = new CloudEnvironmentPricingId(CloudProvider.AZURE);
        azureTestingSupportService.prepareTestPricingToLoad(pricingId);
        azureProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // when
        Set<ProductId> productIds = azureProductPricingService.allProductIds(pricingId);
        assertNotNull(productIds);
        assertEquals(249, productIds.size());

        for (ProductId productId : productIds) {
            Optional<AzureProductPricingRecord> foundProdPricing = azureProductPricingService.findProductPricing(productId);
            assertNotNull(foundProdPricing);
            assertTrue(foundProdPricing.isPresent());
            AzureProductPricingRecord pricingRecord = foundProdPricing.get();
            assertEquals(pricingId, pricingRecord.getCloudEnvironmentPricingId());
            assertNotNull(pricingRecord.getBasedOnRawVersionNum());
            assertNotNull(pricingRecord.getPriceOffers());
        }

        // when
        azureProductPricingService.deleteAll();
        assertTrue(azureProductPricingService.allProductIds(pricingId).isEmpty());
    }

    @Test
    public void shouldDeletePricing() throws Exception {
        CloudEnvironmentPricingId pricingId1 = new CloudEnvironmentPricingId(CloudProvider.AZURE);
        CloudEnvironmentPricingId pricingId2 = new CloudEnvironmentPricingId(CloudProvider.AZURE);
        for(CloudEnvironmentPricingId pricingId : new CloudEnvironmentPricingId[] {pricingId1, pricingId2}) {
            for(int i = 0; i < 2; i++) {
                // many loads - many versions
                azureTestingSupportService.prepareTestPricingToLoad(pricingId);
                azureProductPricingUpdateService.loadPricingDumpDataFromFiles();
            }
            Set<ProductId> productIds = azureProductPricingService.allProductIds(pricingId);
            assertNotNull(productIds);
            assertEquals(249, productIds.size());
        }

        Set<ProductId> productIds1 = azureProductPricingService.allProductIds(pricingId1);

        // when
        azureProductPricingService.deletePricing(pricingId1);

        // then
        assertTrue(azureProductPricingService.allProductIds(pricingId1).isEmpty());
        Set<ProductId> productIds = azureProductPricingService.allProductIds(pricingId2);
        assertNotNull(productIds);
        assertEquals(249, productIds.size());

        productIds1.forEach(pId -> {
            assertFalse(productsMetadataService.findCloudProviderOf(pId).isPresent());
        });
        assertFalse(dataVersionsService.lastVersionOf(pricingId1, AzureRawPricingDataRecord.VERSION_COLLECTION_NAME).isPresent());

        // when
        azureProductPricingService.deleteAll();

        // then
        assertTrue(azureProductPricingService.allProductIds(pricingId2).isEmpty());
        assertFalse(dataVersionsService.lastVersionOf(pricingId1, AzureRawPricingDataRecord.VERSION_COLLECTION_NAME).isPresent());
        assertFalse(dataVersionsService.lastVersionOf(pricingId2, AzureRawPricingDataRecord.VERSION_COLLECTION_NAME).isPresent());
    }

}