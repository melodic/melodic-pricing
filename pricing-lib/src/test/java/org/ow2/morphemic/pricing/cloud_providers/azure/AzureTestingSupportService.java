package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.stream.Stream;

import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;

/**
 * Supports integration tests.
 */
@Service
@Slf4j
public class AzureTestingSupportService {

    @Value("${pricing.datadump.azure.dir}")
    private String azureDumpDir;

    private File azureScanDir;


    public void prepareTestPricingToLoad(CloudEnvironmentPricingId cloudEnvironmentPricingId) throws IOException {
        azureScanDir = new File(azureDumpDir);
        azureScanDir.mkdirs();
        var files = azureScanDir.listFiles();
        if (files != null) {
            Stream.of(files).forEach(File::delete);
        }

        FileSystemUtils.copyRecursively(new File("src/test/data/azure/raw"), azureScanDir);
        Files.write(new File(azureScanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
    }

}
