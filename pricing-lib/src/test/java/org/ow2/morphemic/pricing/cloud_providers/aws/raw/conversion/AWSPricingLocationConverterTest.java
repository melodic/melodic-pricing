package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AWSPricingLocationConverterTest {

    @Test
    public void shouldConvertDescriptionOfRegionToItsShortName() {
        AWSPricingLocationConverter converter = new AWSPricingLocationConverter();

        assertEquals("eu-central-1", converter.apply("EU (Frankfurt)"));
        assertEquals("eu-west-1", converter.apply("EU (Ireland)"));

        assertEquals("Strange", converter.apply("Strange"));
    }

}