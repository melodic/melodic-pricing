package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;

public class AWSFilesPricingDataStreamSupplierTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void shouldAcceptEmptyDir() throws IOException {
        assertEquals(0, new AWSFilesPricingDataStreamSupplier(temporaryFolder.newFolder()).createDataStream().count().block());
    }

    @Test
    public void mustNotReadNonJsonFiles() throws IOException {
        // given
        File scanDir = temporaryFolder.newFolder();
        Files.write(scanDir.toPath().resolve("json.txt"), "{}".getBytes(StandardCharsets.UTF_8));
        Files.write(scanDir.toPath().resolve("json"), "{}".getBytes(StandardCharsets.UTF_8));
        Files.write(scanDir.toPath().resolve("fjson"), "{}".getBytes(StandardCharsets.UTF_8));

        // when
        assertEquals(0, new AWSFilesPricingDataStreamSupplier(scanDir).createDataStream().count().block());
    }

    @Test
    public void mustReadAllJsonFilesAndThenDeleteThemAndDirectories() throws IOException {
        // given
        File scanDir = temporaryFolder.newFolder();
        Files.write(scanDir.toPath().resolve("f1.json"), "{'a':'a1'}".getBytes(StandardCharsets.UTF_8));
        File subDir = new File(scanDir, "d2");
        subDir.mkdirs();
        Files.write(subDir.toPath().resolve("f2.json"), "{'b':'b1'}".getBytes(StandardCharsets.UTF_8));
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.AWS);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        // when
        AWSFilesPricingDataStreamSupplier pricingDataStreamSupplier = new AWSFilesPricingDataStreamSupplier(scanDir);
        Optional<CloudEnvironmentPricingId> maybeClEnvId = pricingDataStreamSupplier.getCloudEnvironmentPricingId();
        List<String> jsons = pricingDataStreamSupplier.createDataStream().collectList().block();

        // then
        assertNotNull(maybeClEnvId);
        assertTrue(maybeClEnvId.isPresent());
        assertEquals(cloudEnvironmentPricingId, maybeClEnvId.get());

        assertEquals(2, jsons.size());
        assertEquals(0, scanDir.listFiles().length);
    }

}