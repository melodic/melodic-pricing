package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;

import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;

/**
 * Supports integration testing of GCP pricing
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class GCPTestingSupportService {

    private final GCPAggregatedProductsService gcpAggregatedProductsService;

    @Value("${pricing.datadump.gcp.dir}")
    private String dumpImportDir;


    public File prepareTestPricingToLoad(CloudEnvironmentPricingId cloudEnvironmentPricingId) throws IOException {
        var scanDir = new File(dumpImportDir);
        scanDir.mkdirs();

        Files.walkFileTree(scanDir.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
        scanDir.mkdirs();

        var jsonAggregatedProductsFile = new File(gcpAggregatedProductsService.getVmFilePath());
        jsonAggregatedProductsFile.getParentFile().mkdirs();
        jsonAggregatedProductsFile.delete();

        Files.copy(Paths.get("src/test/data/gcp_aggregated_products/test_machines.json"), jsonAggregatedProductsFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
        return scanDir;
    }

}
