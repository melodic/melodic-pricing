package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import reactor.core.publisher.Flux;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AWSRawPricingDataManagerTest {

    @Autowired
    private AWSRawPricingDataRepository awsRawPricingDataRepository;

    @Autowired
    private AWSRawPricingDataManager awsRawPricingDataManager;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Before
    public void setUp() {
        awsRawPricingDataRepository.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    @Test
    public void shouldProvideEmptyStreamOfDataForNonExistentVersion() throws Exception {
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId(), new VersionNum(777L));

        assertEquals(0L, awsRawPricingDataManager.rawPricingDataAsFluxForVersion(versionedCloudEnvPricingId)
                .flatMap(Flux::fromIterable).count().block());
    }


    @Test
    public void shouldProvideRawDataForAllRecordsForGivenVersion() throws Exception {
        // given
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(123));
        createTestDataForVersion(versionedCloudEnvPricingId, 5);
        versionedCloudEnvPricingId = versionedCloudEnvPricingId.newVersion(new VersionNum(543));
        createTestDataForVersion(versionedCloudEnvPricingId, 10);

        // when
        List<String> rawElts = awsRawPricingDataManager.rawPricingDataAsFluxForVersion(versionedCloudEnvPricingId)
                .flatMap(Flux::fromIterable)
                .collect(Collectors.toList()).block();
        Optional<DataVersionsService.VersionMetadata> maybeVersionMetadata = awsRawPricingDataManager.lastVersionOfRawData(versionedCloudEnvPricingId.getCloudEnvironmentPricingId());

        // then
        assertEquals(10, rawElts.size());
        rawElts.forEach(json -> {
            assertNotNull(json);
            assertTrue(StringUtils.isNotBlank(json));
        });

        assertNotNull(maybeVersionMetadata);
        DataVersionsService.VersionMetadata versionMetadata = maybeVersionMetadata.get();
        assertEquals(versionedCloudEnvPricingId.getVersionNum(), versionMetadata.getVersionNum());
        assertNotNull(versionMetadata.getCreated());
    }

    @Test
    public void shouldRemoveDataOfAllVersionsOfPricing() throws Exception {
        CloudEnvironmentPricingId pricing1 = new CloudEnvironmentPricingId("P1");
        CloudEnvironmentPricingId pricing2 = new CloudEnvironmentPricingId("P2");
        VersionedCloudEnvPricingId versionedCloudEnvPricingId1 = new VersionedCloudEnvPricingId(pricing1, new VersionNum(123));
        createTestDataForVersion(versionedCloudEnvPricingId1, 5);
        versionedCloudEnvPricingId1 = versionedCloudEnvPricingId1.newVersion(new VersionNum(543));
        createTestDataForVersion(versionedCloudEnvPricingId1, 10);

        VersionedCloudEnvPricingId versionedCloudEnvPricingId2 = new VersionedCloudEnvPricingId(pricing2, new VersionNum(321));
        createTestDataForVersion(versionedCloudEnvPricingId2, 15);
        versionedCloudEnvPricingId2 = versionedCloudEnvPricingId1.newVersion(new VersionNum(897));
        createTestDataForVersion(versionedCloudEnvPricingId2, 11);

        // when
        awsRawPricingDataManager.deletePricingData(pricing2);
        awsRawPricingDataManager.deletePricingData(new CloudEnvironmentPricingId("unknown pricing"));
        List<String> rawElts = awsRawPricingDataManager.rawPricingDataAsFluxForVersion(versionedCloudEnvPricingId1)
                .flatMap(Flux::fromIterable)
                .collect(Collectors.toList()).block();

        // then
        assertEquals(10, rawElts.size());
        rawElts.forEach(json -> {
            assertNotNull(json);
            assertTrue(StringUtils.isNotBlank(json));
        });


    }

    @Test
    public void removingRawRecordsOfUnknownPricingIsAcceptable() {
        awsRawPricingDataManager.deletePricingData(new CloudEnvironmentPricingId("unknown pricing"));
    }


    private void createTestDataForVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId, int elements) {
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();
        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();
        for (int i = 0; i < elements; i++) {
            awsRawPricingDataRepository.save(new AWSRawPricingDataRecord(cloudEnvironmentPricingId, versionNum, "{'test':'" + i + "'}",
                    new HashMap<>(), (long) i));
        }
        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, AWSRawPricingDataRecord.VERSION_COLLECTION_NAME);
    }

}