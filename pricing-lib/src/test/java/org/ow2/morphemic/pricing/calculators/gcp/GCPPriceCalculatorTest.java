package org.ow2.morphemic.pricing.calculators.gcp;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPStandardTermTypes;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.model.gcp.GCPAggregatedProductIdFactory;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class GCPPriceCalculatorTest {

    @Autowired
    private GCPPriceCalculator priceCalculator;

    @Autowired
    private GCPProductPricingService productPricingService;

    @Value("${pricing.datadump.gcp.dir}")
    private String dumpImportDir;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    @Qualifier("gcpProductPricingUpdateService")
    private GCPProductPricingUpdateService gcpProductPricingUpdateService;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private GCPAggregatedProductsService aggregatedProductsService;

    private File jsonAggregatedProductsFile;

    private File scanDir;

    @Before
    public void setUp() throws IOException {
        aggregatedProductsService.deleteAll();
        productPricingService.deleteAll();
        productsMetadataService.removeAllProducts();
        dataVersionsService.removeAllVersions();

        scanDir = new File(dumpImportDir);
        scanDir.mkdirs();

        Files.walkFileTree(scanDir.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        });
        scanDir.mkdirs();

        jsonAggregatedProductsFile = new File(aggregatedProductsService.getVmFilePath());
        jsonAggregatedProductsFile.getParentFile().mkdirs();
        jsonAggregatedProductsFile.delete();

        Files.copy(Paths.get("src/test/data/gcp_aggregated_products/test_machines.json"), jsonAggregatedProductsFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

    }


    @Test
    public void mustNotCalculatePriceWhenTherIsNoSuchProduct() {

        Optional<List<GCPPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = priceCalculator.calculatePrice(
                new ProductId("ProdWhichDoesNotExist"), new Amount(3,
                        new Unit("b.sek")),
                GCPStandardTermTypes.ON_DEMAND.getOfferTermType());

        assertNotNull(maybeCalculatedPrices);
        assertFalse(maybeCalculatedPrices.isPresent());

    }

    @Test
    public void mustCalculatePriceForProductOnDemand() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.GCP);

        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), scanDir);
        Files.write(new File(scanDir, GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        if (gcpProductPricingUpdateService.loadPricingDumpDataFromFiles().isEmpty()) {
            fail("New version has not been created!");
        }

        // when

        Optional<List<GCPPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = priceCalculator
                .calculatePrice(new ProductId(cloudEnvironmentPricingId, "6BC9-D757-743E"),
                        new Amount(3, new Unit("GiBy.mo")),
                        GCPStandardTermTypes.ON_DEMAND.getOfferTermType());

        // then

        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());
        List<GCPPriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        GCPPriceCalculator.CalculatedPrice calculatedPrice = calculatedPrices.get(0);
        assertNotNull(calculatedPrice);
        Price price = calculatedPrice.getPrice();
        assertNotNull(price);
        assertEquals("USD", price.getCurrencyCode());
        assertEquals(new BigDecimal("15.000000000"), price.getValue());
        assertNotNull(calculatedPrice.getExternalRateCodeId());
    }

    @Test
    public void shouldProperlyCalculatePriceOfVirtualMachine() throws IOException {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.GCP);

        FileSystemUtils.copyRecursively(new File("src/test/data/gcp"), scanDir);
        Files.write(new File(scanDir, GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));

        if (gcpProductPricingUpdateService.loadPricingDumpDataFromFiles().isEmpty()) {
            fail("New version has not been created!");
        }

        GCPAggregatedProductIdFactory idFactory = new GCPAggregatedProductIdFactory();

        // when
        // e2-highcpu-2 Commit1Yr: 2CPU * 0.028103700*hours + 2GB*0.003766020*hours
        ProductId e2Pighcpu2Prodid = idFactory.createVMId(cloudEnvironmentPricingId, "e2-highcpu-2", new Place("europe-west3"));
        Optional<List<GCPPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = priceCalculator.calculatePrice(e2Pighcpu2Prodid, new Amount(3, new Unit("h")), GCPStandardTermTypes.COMMIT_1YR.getOfferTermType());

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());
        List<GCPPriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        GCPPriceCalculator.CalculatedPrice calculatedPrice = calculatedPrices.get(0);
        assertNotNull(calculatedPrice);
        assertEquals(new Price(new BigDecimal("0.120467538"), "USD"), calculatedPrice.getPrice());

        // and when
        // e2-highcpu-2 OnDemand: 2CPU * 0.028103700*hours + 2GB*0.003766020*hours
        maybeCalculatedPrices = priceCalculator.calculatePrice(e2Pighcpu2Prodid, new Amount(3, new Unit("h")), GCPStandardTermTypes.ON_DEMAND.getOfferTermType());

        // then
        assertNotNull(maybeCalculatedPrices);
        assertTrue(maybeCalculatedPrices.isPresent());
        calculatedPrices = maybeCalculatedPrices.get();
        assertEquals(1, calculatedPrices.size());
        calculatedPrice = calculatedPrices.get(0);
        assertNotNull(calculatedPrice);
        assertEquals(new Price(new BigDecimal("0.191218320"), "USD"), calculatedPrice.getPrice());

    }

}
