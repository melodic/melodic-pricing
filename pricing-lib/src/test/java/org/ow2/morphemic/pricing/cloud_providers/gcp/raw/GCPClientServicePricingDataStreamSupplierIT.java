package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.services.cloudbilling.model.Service;
import com.google.api.services.cloudbilling.model.Sku;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.gcp.config.GCPProductPricingServicesConfig;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.ow2.morphemic.pricing.model.CloudProvider.GCP;


/**
 * Can be used to dump raw data and test whle process of fetching data.
 * To run you have to define property: -Dtest.gcp=true and create file application-testgcp.yml with api key in src/test/resources.
 * <p>
 * If you want to fetch SKUs only for one service e.g. 6F81-5844-456A - compute engine,
 * instead of empty set which is provided to the constructor
 * you can provide set with the service id e.g. Set.of("6F81-5844-456A")
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@ActiveProfiles({"localtestgcp", "test"})
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class GCPClientServicePricingDataStreamSupplierIT {

    @Value("${test.gcp.key}")
    private String apiKey;

    @Autowired
    private GCPProductPricingServicesConfig config;

    @Test
    @IfProfileValue(name = "test.gcp", value = "true")
    public void fetchAllPricingDataAndSaveToJsons() throws IOException {
        File destDir = new File("target/GCPClientServicePricingDataStreamSupplierTest/services");
        destDir.mkdirs();
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(GCP);

        log.info("It will save service and its prices to {}", destDir.getAbsolutePath());
        new GCPClientServicePricingDataStreamSupplier(apiKey, config.getServicesIdsToFetch(), config.getServicesFetchingDelayMs(),
                cloudEnvironmentPricingId, "test-pricing-lib", new PricingDataImportStatisticCollector(GCP, "cloud_account", List.of()))
                .createDataStream()
                .doOnNext(serviceWithSkusPrices -> {
            Service serviceDef = serviceWithSkusPrices.getServiceDef();
            log.debug("Service: {}", serviceDef.getServiceId());
            File serviceDir = new File(destDir, "service_" + serviceDef.getServiceId());
            serviceDir.mkdirs();

            try {
                Path serviceDataDirPath = serviceDir.toPath();
                Files.write(serviceDataDirPath.resolve("service.json"), serviceDef.toPrettyString().getBytes(UTF_8));
                for (Sku sku : serviceWithSkusPrices.getSkus()) {
                    Files.write(serviceDataDirPath.resolve("sku_" + sku.getSkuId() + ".json"), sku.toPrettyString().getBytes(UTF_8));
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }).count().block();
    }
}