package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.fileloader;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class GCPVmProductsLoaderTest {
    @Test
    public void shouldLoadVmDataFromFile() throws Exception {

        // when
        List<GCPVmProductDef> gcpVmProductDefs = new GCPVmProductsLoader().loadVMsFrom(Files.newInputStream(Paths.get("src/test/data/gcp_aggregated_products/test_machines.json")));

        // then
        assertNotNull(gcpVmProductDefs);
        assertEquals(5, gcpVmProductDefs.size());
        Set<String> names = gcpVmProductDefs.stream().map(GCPVmProductDef::getName).collect(Collectors.toSet());
        assertEquals(Set.of("e2-highcpu-16", "e2-highcpu-2", "e2-highcpu-32", "n1-ultramem-80"), names);

    }
}