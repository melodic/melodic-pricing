package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.pricing.AWSPricing;
import com.amazonaws.services.pricing.AWSPricingClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@ActiveProfiles({"localtestaws", "test"})
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class AWSClientPricingDataStreamSupplierTest {

    @Value("${test.aws.user}")
    private String awsUser;

    @Value("${test.aws.password}")
    private String awsPassword;

    /**
     * Dumps each of pricing records to separate file. Each file is placed in separate directory created for each "page".
     * Used to fetch raw data from AWS for AmazonEC2 service.
     *
     * @throws IOException
     */
    @Test
    @IfProfileValue(name = "test.aws", value = "true")
    public void dumpAwsPricingToFile() throws IOException {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId();
        AWSPricing client = createAWSPricingClient();

        String awsServiceCode = "AmazonRDS";
        File destDir = new File("target/AWSClientPricingDataStreamSupplierTest/" + awsServiceCode);
        destDir.mkdirs();

        AtomicInteger fileNum = new AtomicInteger();
        new AWSClientPricingDataStreamSupplier(cloudEnvironmentPricingId, client, awsServiceCode).createDataStream().doOnNext(json -> {
            try {
                Files.write(new File(destDir, String.format("f_%06d.json", fileNum.getAndIncrement())).toPath(), json.getBytes(StandardCharsets.UTF_8));
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }).count().block();

        client.shutdown();

    }


    private AWSPricing createAWSPricingClient() {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(awsUser, awsPassword);
        AWSCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        return AWSPricingClientBuilder.standard().withRegion(Regions.US_EAST_1).withCredentials(credentialsProvider).build();
    }

}