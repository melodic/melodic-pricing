package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class GCPCloudProviderPricingStatsServiceTest {

    @Autowired
    private GCPProductPricingService pricingService;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private GCPProductPricingRecordsRepository pricingRecordsRepository;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    @Qualifier("gcpProductPricingUpdateService")
    private GCPProductPricingUpdateService gcpProductPricingUpdateService;

    @Autowired
    private GCPTestingSupportService gcpTestingSupportService;

    @Autowired
    private GCPCloudProviderPricingStatsService gcpCloudProviderPricingStatsService;

    @Before
    public void setUp() {
        dataVersionsService.removeAllVersions();
        pricingService.deleteAll();
        productsMetadataService.removeAllProducts();
    }

    @Test
    public void forEmptyRepositoryItShouldReturnEmptyData() {

        CloudProviderStats cloudProviderStats = gcpCloudProviderPricingStatsService.calculateCloudProviderStats();
        assertNotNull(cloudProviderStats);
        assertEquals(CloudProvider.GCP, cloudProviderStats.getCloudProvider());
        assertEquals(0, cloudProviderStats.getProductsCount());
        assertTrue(cloudProviderStats.getPricings().isEmpty());

        List<PricingStats> pricingStats = gcpCloudProviderPricingStatsService.calculatePricingStats();
        assertNotNull(pricingStats);
        assertTrue(pricingStats.isEmpty());

    }

}