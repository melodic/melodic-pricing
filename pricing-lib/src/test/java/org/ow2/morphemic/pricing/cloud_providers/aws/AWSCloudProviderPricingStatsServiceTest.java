package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class AWSCloudProviderPricingStatsServiceTest {

    @Autowired
    private AWSCloudProviderPricingStatsService awsCloudProviderPricingStatsService;

    @Autowired
    private AWSProductPricingService awsProductPricingService;

    @Before
    public void setUp() {
        awsProductPricingService.deleteAll();
    }

    @Test
    public void forEmptyRepositoryItShouldReturnEmptyData() {

        CloudProviderStats cloudProviderStats = awsCloudProviderPricingStatsService.calculateCloudProviderStats();
        assertNotNull(cloudProviderStats);
        assertEquals(CloudProvider.AWS, cloudProviderStats.getCloudProvider());
        assertEquals(0, cloudProviderStats.getProductsCount());
        assertTrue(cloudProviderStats.getPricings().isEmpty());

        List<PricingStats> pricingStats = awsCloudProviderPricingStatsService.calculatePricingStats();
        assertNotNull(pricingStats);
        assertTrue(pricingStats.isEmpty());

    }



}