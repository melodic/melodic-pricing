package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.ow2.morphemic.pricing.model.CloudProvider.AWS;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@ActiveProfiles({"localtestaws", "test"})
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
public class AWSClientPricingRawDataLoaderFactoryTest {

    @Value("${test.aws.user}")
    private String awsUser;

    @Value("${test.aws.password}")
    private String awsPassword;

    @Autowired
    private AWSClientPricingRawDataLoaderFactory awsLoaderFactory;


    @Value("${pricing.cloudproviders.aws.serviceCodes:AmazonEC2,AmazonRDS}")
    private Set<String> serviceCodes;

    /**
     * Very simple test: it loads RDS pricing to MongoDB.
     *
     */
    @Test
    @IfProfileValue(name = "test.aws", value = "true")
    public void dumpAwsPricingToFile() {
        Set<String> serviceCodesToFetch = Sets.newHashSet("AmazonRDS");
        AWSPricingRawDataLoader pricingAWSLoader = awsLoaderFactory.createPricingAWSLoader(
                new CloudEnvironmentPricingId(CloudProvider.AWS), awsUser, awsPassword, serviceCodesToFetch);

        final PricingDataImportStatisticCollector dataImportStatisticCollector = new PricingDataImportStatisticCollector(AWS, "account_id", List.of());
        Optional<VersionedCloudEnvPricingId> maybeCloudEnvironmentId = pricingAWSLoader.loadRawDataToCache(
                dataImportStatisticCollector);
        dataImportStatisticCollector.stop();
        log.info(dataImportStatisticCollector.toString());

        assertNotNull(maybeCloudEnvironmentId);
        assertTrue(maybeCloudEnvironmentId.isPresent());
        log.debug("imported to cloud env: {}", maybeCloudEnvironmentId.get());

    }

}