package org.ow2.morphemic.pricing.cloud_providers.gcp;

import com.google.api.services.cloudbilling.model.Sku;
import org.junit.Test;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;

import static org.junit.Assert.*;

public class GcpProductIdFactoryTest {

    @Test
    public void shouldCreateIdFromSkuCode() {
        // given
        GcpProductIdFactory idFactory = new GcpProductIdFactory();
        CloudEnvironmentPricingId pricing = new CloudEnvironmentPricingId("pr1");

        // when
        ProductId id1 = idFactory.createFromSku(pricing, "p1");

        // then
        assertNotNull(id1);

    }

    @Test
    public void shouldCreateIdFromSkuObj() {
        // given
        GcpProductIdFactory idFactory = new GcpProductIdFactory();
        CloudEnvironmentPricingId pricing = new CloudEnvironmentPricingId("pr1");

        Sku sku = new Sku();
        sku.setSkuId("testId");
        // when
        ProductId id1 = idFactory.createFromSku(pricing, sku);

        // then
        assertNotNull(id1);

    }

}