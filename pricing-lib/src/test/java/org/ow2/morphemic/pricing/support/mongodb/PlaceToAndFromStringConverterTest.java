package org.ow2.morphemic.pricing.support.mongodb;

import org.junit.Test;
import org.ow2.morphemic.pricing.model.Place;

import static org.junit.Assert.*;

public class PlaceToAndFromStringConverterTest {

    @Test
    public void shouldProperlyEncodeAndDecodeValues() {
        PlaceToStringConverter toStr = new PlaceToStringConverter();
        PlaceFromStringConverter fromStr = new PlaceFromStringConverter();

        Place poland = new Place("Poland");
        assertEquals(poland, fromStr.convert(toStr.convert(poland)));

        Place polandWarsaw = new Place("Poland/Warsaw");
        assertEquals(polandWarsaw, fromStr.convert(toStr.convert(polandWarsaw)));

        assertEquals(Place.ANY_PLACE, fromStr.convert(toStr.convert(Place.ANY_PLACE)));
        assertEquals(Place.GLOBAL_PLACE, fromStr.convert(toStr.convert(Place.GLOBAL_PLACE)));

    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldDetectWrongType() {
        PlaceFromStringConverter fromStr = new PlaceFromStringConverter();

        fromStr.convert("unknown type/");

    }
}