package org.ow2.morphemic.pricing.cloud_providers.azure.raw;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AzureRawProductPricingManagerTest {

    @Autowired
    private AzureRawPricingDataRepository azureRawPricingDataRepository;

    @Autowired
    private AzureRawProductPricingManager azureRawProductPricingManager;

    @Autowired
    private DataVersionsService dataVersionsService;

    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() {
        azureRawPricingDataRepository.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    @Test
    public void shouldProvideEmptyStreamOfDataForNonExistentVersion() {
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId(), new VersionNum(777L));

        assertEquals(0L, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(versionedCloudEnvPricingId).count());
    }

    @Test
    public void shouldProvideRawDataForAllRecordsForGivenVersion() {
        // given
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(new CloudEnvironmentPricingId("C1"), new VersionNum(123));
        createTestDataForVersion(versionedCloudEnvPricingId, 5);
        versionedCloudEnvPricingId = versionedCloudEnvPricingId.newVersion(new VersionNum(543));
        createTestDataForVersion(versionedCloudEnvPricingId, 10);

        // when
        List<AzureRawPricingItem> rawElts = azureRawProductPricingManager.rawPricingDataAsStreamForVersion(versionedCloudEnvPricingId).collect(Collectors.toList());

        // then
        assertEquals(10, rawElts.size());
        for (AzureRawPricingItem azureRawPricingItem : rawElts) {
            assertNotNull(azureRawPricingItem);
        }
    }

    @Test
    public void shouldDeleteOnlyDataForGivenPricing() {
        CloudEnvironmentPricingId pricing1 = new CloudEnvironmentPricingId("C1");
        CloudEnvironmentPricingId pricing2 = new CloudEnvironmentPricingId("C2");
        for(CloudEnvironmentPricingId pricingId : new CloudEnvironmentPricingId[]{pricing1, pricing2}) {
            VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(pricingId, new VersionNum(123));
            createTestDataForVersion(versionedCloudEnvPricingId, 5);
            versionedCloudEnvPricingId = versionedCloudEnvPricingId.newVersion(new VersionNum(543));
            createTestDataForVersion(versionedCloudEnvPricingId, 10);
        }

        assertEquals(5, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing1, new VersionNum(123))).count());
        assertEquals(10, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing1, new VersionNum(543))).count());
        assertEquals(5, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing2, new VersionNum(123))).count());
        assertEquals(10, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing2, new VersionNum(543))).count());

        // when
        azureRawProductPricingManager.deletePricing(pricing1);

        // then
        assertEquals(0, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing1, new VersionNum(123))).count());
        assertEquals(0, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing1, new VersionNum(543))).count());
        assertEquals(5, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing2, new VersionNum(123))).count());
        assertEquals(10, azureRawProductPricingManager.rawPricingDataAsStreamForVersion(new VersionedCloudEnvPricingId(pricing2, new VersionNum(543))).count());

    }


    @Test
    public void shouldFindProductPricingItems() throws IOException {
        // given
        List<AzureRawPricingDataRecord> azureRawPricingDataRecords = Arrays.asList(mapper.readValue(Files.newBufferedReader(
                Path.of("src/test/data/azure/azure_pricing_products.json"), StandardCharsets.UTF_8),
                AzureRawPricingDataRecord[].class));
        azureRawPricingDataRepository.saveAll(azureRawPricingDataRecords);

        // when
        Set<AzureProductPricingItem> productPricingItems = azureRawProductPricingManager.findProductPricingItems(AzurePublicPricingRawDataLoader.PUBLIC_AZURE_PRICING_ID, "Standard_A3", "uknorth");
        // then
        assertNotNull(productPricingItems);
        assertThat(productPricingItems).hasSize(4);

        for (AzureProductPricingItem productPricingItem : productPricingItems) {
            assertNotNull(productPricingItem.getMeterId());
        }

        // check if each pricing item has different meter id (pricing Id)
        Set<String> pricingIds = productPricingItems.stream()
                .map(AzureProductPricingItem::getMeterId)
                .collect(Collectors.toSet());
        assertThat(pricingIds).hasSize(4);


        //when - one result
        productPricingItems = azureRawProductPricingManager.findProductPricingItems(AzurePublicPricingRawDataLoader.PUBLIC_AZURE_PRICING_ID, "Standard_E20_v3", "southafricawest");
        // then
        assertThat(productPricingItems).isNotNull();
        assertThat(productPricingItems).hasSize(1);


        // when - empty result (only one Spot exists)
        productPricingItems = azureRawProductPricingManager.findProductPricingItems(AzurePublicPricingRawDataLoader.PUBLIC_AZURE_PRICING_ID, "Standard_DS4_v2", "westindia");
        // then
        assertThat(productPricingItems).isNotNull();
        assertThat(productPricingItems).isEmpty();


        // when - empty result
        productPricingItems = azureRawProductPricingManager.findProductPricingItems(AzurePublicPricingRawDataLoader.PUBLIC_AZURE_PRICING_ID, "xyz", "xxx");
        // then
        assertThat(productPricingItems).isNotNull();
        assertThat(productPricingItems).isEmpty();
    }

    private void createTestDataForVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId, int elements) {
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();
        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();
        for (int i = 0; i < elements; i++) {
            AzureRawPricingItem azurePricingRec = AzureRawPricingItem.builder()
                    .currencyCode("USD")
                    .armRegionName("r")
                    .productId("pr-" + i)
                    .meterId("m-" + i)
                    .build();
            azureRawPricingDataRepository.save(new AzureRawPricingDataRecord(cloudEnvironmentPricingId, versionNum, azurePricingRec));
        }
        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, AzureRawPricingDataRecord.VERSION_COLLECTION_NAME);
    }
}
