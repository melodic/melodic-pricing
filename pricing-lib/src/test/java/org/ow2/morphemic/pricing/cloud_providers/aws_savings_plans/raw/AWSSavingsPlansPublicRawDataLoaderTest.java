package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansProductPricingService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@Slf4j
@ActiveProfiles("test")
public class AWSSavingsPlansPublicRawDataLoaderTest {

    @Autowired
    private AWSRawSavingsPlansPricingDataRepository rawSavingsPlansRepository;
    @Autowired
    private DataVersionsService dataVersionsService;
    @Autowired
    private AWSSavingsPlansPublicRawDataLoader publicLoader;

    @Before
    public void setUp() {
        rawSavingsPlansRepository.deleteAll();
        dataVersionsService.removeAllVersions();
    }

    // Really simple test only for checking if public pricing id is available
    @Test
    public void shouldReturnIdOfAwsPublicPricing() {

        // when
        CloudEnvironmentPricingId publicAwsSavingsPlansPricingId = publicLoader.getPublicAwsSavingsPlanPricingId();

        // then
        assertNotNull(publicAwsSavingsPlansPricingId);
    }

    @Test
    public void shouldLoadPublicAwsPricingToDBCache() {
        // when - 1st loading of aws savings plans pricing
        Set<VersionedCloudEnvPricingId> versionedCloudEnvPricingIds = publicLoader.loadRawDataToCache();

        // then
        assertNotNull(versionedCloudEnvPricingIds);
        assertFalse(versionedCloudEnvPricingIds.isEmpty());
        assertEquals(1, versionedCloudEnvPricingIds.size());
        assertEquals(publicLoader.getPublicAwsSavingsPlanPricingId(), new ArrayList<>(versionedCloudEnvPricingIds).get(0).getCloudEnvironmentPricingId());

        // check if load any data and save in db
        long rawDataNumberAfterFirstRun = rawSavingsPlansRepository.count();
        assertTrue(rawDataNumberAfterFirstRun > 0);

        assertTrue(dataVersionsService.hasAnyVersion(publicLoader.getPublicAwsSavingsPlanPricingId()));

        Optional<VersionNum> versionAfterFirstRun = dataVersionsService.lastVersionOf(publicLoader.getPublicAwsSavingsPlanPricingId(), AWSSavingsPlansProductPricingService.VERSIONED_AWS_SAVINGS_PLANS_PRICING_COLLECTION_NAME);

        assertTrue(versionAfterFirstRun.isPresent());
        // check if all items have all data set
//        for (AWSRawSavingsPlansPricingDataRecord rawData : allRawData) {
//            assertTrue(rawData.getRawRates() != null || rawData.getRawProduct() != null);
//            assertEquals(publicLoader.getPublicAwsSavingsPlanPricingId(), rawData.getCloudEnvironmentPricingId());
//            assertEquals(rawData.getVersionNum(), versionAfterFirstRun.get());
//            assertNotNull(rawData.getId());
//            assertNotNull(rawData.getAwsVersion());
//        }

        VersionNum lastVersionNum = new ArrayList<>(versionedCloudEnvPricingIds).get(0).getVersionNum();
        assertNotNull(lastVersionNum);
        assertEquals(lastVersionNum, versionAfterFirstRun.get());

        // when - 2nd loading of aws savings plans pricing
        Set<VersionedCloudEnvPricingId> nextVersionCloudEnvPricingIds = publicLoader.loadRawDataToCache();

        assertNotNull(nextVersionCloudEnvPricingIds);
        assertTrue(nextVersionCloudEnvPricingIds.isEmpty());

        long rawDataNumberAfterSecondRun = rawSavingsPlansRepository.count();
        assertEquals(rawDataNumberAfterFirstRun, rawDataNumberAfterSecondRun);

        assertTrue(dataVersionsService.hasAnyVersion(publicLoader.getPublicAwsSavingsPlanPricingId()));

        Optional<VersionNum> lastVersionNumberFromDb = dataVersionsService.lastVersionOf(publicLoader.getPublicAwsSavingsPlanPricingId(), AWSSavingsPlansProductPricingService.VERSIONED_AWS_SAVINGS_PLANS_PRICING_COLLECTION_NAME);
        assertTrue(lastVersionNumberFromDb.isPresent());
        assertEquals(lastVersionNumberFromDb.get(), versionAfterFirstRun.get());
    }
}
