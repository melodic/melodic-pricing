package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import static org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;
import static org.ow2.morphemic.pricing.model.CloudProvider.AWS;

public class AWSSavingsPlansTestUtils {

    public static void prepareFileToRunLoadPricingFromFiles(File scanDir, String pathname) throws IOException {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(AWS);
        FileSystemUtils.copyRecursively(new File(pathname), scanDir);
        Files.write(new File(scanDir, FS_LOADER_OK_FILE_NAME).toPath(), cloudEnvironmentPricingId.getId().getBytes(StandardCharsets.UTF_8));
    }
}
