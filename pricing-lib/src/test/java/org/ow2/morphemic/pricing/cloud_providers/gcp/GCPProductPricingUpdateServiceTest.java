package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ow2.morphemic.pricing.PricingConfiguration;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.raw.GCPPricingRawDataLoaderFactory;
import org.ow2.morphemic.pricing.cloud_providers.security.CredentialsSupplier;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.ow2.morphemic.pricing.model.CloudProvider.GCP;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = PricingConfiguration.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@Slf4j
public class GCPProductPricingUpdateServiceTest {

    @Autowired
    @Qualifier("gcpProductPricingUpdateService")
    private GCPProductPricingUpdateService gcpProductPricingUpdateService;

    @Autowired
    private GCPProductPricingService gcpProductPricingService;

    @Autowired
    private DataVersionsService dataVersionsService;

    @Autowired
    private GCPProductPricingRecordsRepository productPricingRecordsRepository;

    @Autowired
    private ProductsMetadataService productsMetadataService;

    @Autowired
    private GCPAggregatedProductsService aggregatedProductsService;

    @Autowired
    private GCPTestingSupportService gcpTestingSupportService;

    @MockBean
    private CredentialsSupplier credentialsSupplier;

    @MockBean
    private GCPPricingRawDataLoaderFactory loaderFactory;


    @Before
    public void setUp() throws IOException {
        aggregatedProductsService.deleteAll();
        dataVersionsService.removeAllVersions();
        productPricingRecordsRepository.deleteAll();
        productsMetadataService.removeAllProducts();
    }

    @Test
    public void shouldUpdatePricingDataUsingFileLoader() throws IOException {
        // given
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(GCP);
        File scanDir = gcpTestingSupportService.prepareTestPricingToLoad(cloudEnvironmentPricingId);

        // when
        gcpProductPricingUpdateService.loadPricingDumpDataFromFiles();

        // then
        assertEquals(0, scanDir.listFiles().length);
        assertEquals(16, productPricingRecordsRepository.count());

        productPricingRecordsRepository.findAll().forEach(gcpProd -> {
            ProductId productId = gcpProd.getId();
            assertTrue(productsMetadataService.isProductMetadataInCache(productId));
            Optional<CloudProvider> maybeFound = productsMetadataService.findCloudProviderOf(productId);
            assertTrue(maybeFound.isPresent());
            assertEquals(GCP, maybeFound.get());
        });

        assertEquals(5, aggregatedProductsService.allProductIds(cloudEnvironmentPricingId).size());
        assertEquals(16, gcpProductPricingService.allProductIds(cloudEnvironmentPricingId).size());
    }

    @Test
    public void shouldDoNothingWhenPricingIdNotFound() {
        // given
        CloudEnvironmentPricingId testPricingId = new CloudEnvironmentPricingId();
        when(credentialsSupplier.apiCredentialsOf(testPricingId)).thenReturn(Optional.empty());
        // when
        gcpProductPricingUpdateService.updateUsingExternalApi(testPricingId, new PricingDataImportStatisticCollector(GCP, "account_id", List.of()));


        // then
        verify(loaderFactory, never()).createPricingGCPApiLoader(any(String.class), any(HashSet.class), any(Long.class),
                any(CloudEnvironmentPricingId.class), any(PricingDataImportStatisticCollector.class));

        assertTrue(aggregatedProductsService.allProductIds(testPricingId).isEmpty());

    }
}