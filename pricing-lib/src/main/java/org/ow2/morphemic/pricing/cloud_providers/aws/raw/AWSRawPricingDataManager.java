package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.SynchronousSink;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Provides advanced access operation for raw data stored in cache.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class AWSRawPricingDataManager {

    private final AWSRawPricingDataRepository awsRawPricingDataRepository;

    private final DataVersionsService dataVersionsService;

    /**
     * Stream of raw data stored in cache for given version of given cloud environment.
     *
     * @param versionedCloudEnvPricingId versioned cloud environment id (version num and id)
     * @return created stream
     */
    public Flux<List<String>> rawPricingDataAsFluxForVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId)
            throws AWSInvalidRawRecordException {
        CloudEnvironmentPricingId pricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();
        VersionNum pricingVersion = versionedCloudEnvPricingId.getVersionNum();
        long invalidRecordsCount = awsRawPricingDataRepository.countAllByCloudEnvironmentPricingIdAndVersionAndSortPosNumIsNull(pricingId, pricingVersion);
        if (invalidRecordsCount > 0L) {
            throw new AWSInvalidRawRecordException("There are " + invalidRecordsCount + " RAW records without field sortPosNum set in pricing "
                    + pricingId.getId() + ", version: " + pricingVersion.getNum());
        }

        PageRequest page = PageRequest.of(0, 1000, Sort.by(Sort.Direction.ASC, "sortPosNum"));
        return Flux.generate(() -> Long.MIN_VALUE, (Long lastSortPosNum, SynchronousSink<List<String>> sink) -> {
            List<AWSRawPricingContentsOnlyProjection> rawPricingData = awsRawPricingDataRepository
                    .findByCloudEnvironmentPricingIdAndVersionAndSortPosNumGreaterThanOrderBySortPosNum(pricingId, pricingVersion, lastSortPosNum, page);

            if (rawPricingData.isEmpty()) {
                sink.complete();
                return Long.MAX_VALUE;
            } else {
                AWSRawPricingContentsOnlyProjection last = rawPricingData.get(rawPricingData.size() - 1);
                sink.next(rawPricingData.stream().map(AWSRawPricingContentsOnlyProjection::getRawContents).collect(Collectors.toList()));
                return last.getSortPosNum();
            }

        });
    }

    /**
     * Provides info about last version of raw data for pricing.
     *
     * @param cloudEnvironmentPricingId pricing id
     * @return maybe data
     */
    public Optional<DataVersionsService.VersionMetadata> lastVersionOfRawData(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        return dataVersionsService.lastVersionMetadata(cloudEnvironmentPricingId, AWSRawPricingDataRecord.VERSION_COLLECTION_NAME);
    }

    @Transactional
    public void deleteAll() {
        awsRawPricingDataRepository.deleteAll();
        log.info("Removed all data from db cache.");
    }

    /**
     * Removes all records with RAW data for given pricing.
     *
     * @param cloudEnvironmentPricingId pricing id
     */
    @Transactional
    public void deletePricingData(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        long removed = awsRawPricingDataRepository.deleteAllByCloudEnvironmentPricingId(cloudEnvironmentPricingId);
        log.info("Removed all RAW data pricing records for pricing {}. Removed records {}.", cloudEnvironmentPricingId.getId(), removed);
    }

}


