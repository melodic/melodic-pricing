package org.ow2.morphemic.pricing.support.mongodb.migrations;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import lombok.extern.slf4j.Slf4j;

/**
 * Changes for version 001. Executed by mongock.
 */
@ChangeLog(order = "002")
@Slf4j
public class MongoMigrationChangelog002 {

    /**
     * Old pricing data for GCP must be erased. Document format was changed.
     *
     * @param mongockTemplate
     */
    @ChangeSet(order = "002", id = "002.002.remove_GCPAggregatedProduct", author = "pricing-lib", systemVersion = "0.10")
    public void removeAllGCPPRicings(MongockTemplate mongockTemplate) {
        mongockTemplate.dropCollection("gCPAggregatedProduct");
        log.info("GCP gCPAggregatedProduct data removed.");
    }

}
