package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Manages SKU pricing data records.
 */
@Repository
interface GCPRawSKURecordRepository extends MongoRepository<GCPRawSKURecord, String> {

    long deleteAllByIdIn(Set<String> ids);

}
