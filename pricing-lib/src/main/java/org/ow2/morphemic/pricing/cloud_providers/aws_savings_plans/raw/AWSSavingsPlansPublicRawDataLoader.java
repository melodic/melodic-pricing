package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.primitives.Longs;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansOfferResponse;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansRatesPage;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansRegionsUrl;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * It loads public pricing for AWS savings plans. This Api is publicly available, so any suppliers with credentials are not required.
 */
@SuppressFBWarnings("HTTP_PARAMETER_POLLUTION")
@Service
@Slf4j
public class AWSSavingsPlansPublicRawDataLoader extends AbstractAWSSavingsPlansRawDataLoader {

    private static final CloudEnvironmentPricingId PUBLIC_AWS_SAVINGS_PLANS_PRICING_ID = new CloudEnvironmentPricingId("aws_savings_plans_public_pricing");

    /**
     * Aws savings plans url prefix.
     */
    private static final String PUBLIC_AWS_SAVINGS_PLANS_URL = "https://pricing.us-east-1.amazonaws.com";

    /**
     * Aws savings plans url suffix for obtain first page with all regions and links for offers.
     */
    private static final String PUBLIC_AWS_COMPUTE_SAVINGS_PLANS_URL = PUBLIC_AWS_SAVINGS_PLANS_URL + "/savingsPlan/v1.0/aws/AWSComputeSavingsPlan/current/region_index.json";

    public AWSSavingsPlansPublicRawDataLoader(AWSRawSavingsPlansPricingDataRepository rawSavingsPlansRepository, DataVersionsService dataVersionsService) {
        super(rawSavingsPlansRepository, dataVersionsService);
    }

    /**
     * It loads raw data from public AWS savings plans page to cache.
     *
     * @return new VersionedCloudEnvPricingId if everything's go correct
     */
    @Transactional
    public Set<VersionedCloudEnvPricingId> loadRawDataToCache() {
        Map<VersionNum, MutableInt> createdNewSpVersions = new HashMap<>();

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            List<AWSSavingsPlansRegionsUrl> spAddrs = obtainPublicSavingsPlansUrls(httpClient);

            AtomicLong sortPositionNumberProvider = new AtomicLong();
            for (AWSSavingsPlansRegionsUrl spDataAddr : spAddrs) {
                log.info("Loading savings plans for region: {} url: {}", spDataAddr.getRegionCode(), spDataAddr.getVersionUrl());
                if (StringUtils.isBlank(spDataAddr.getVersionUrl())) {
                    log.warn("One of urls is blank. Skipping and continuing");
                    continue;
                }

                AWSSavingsPlansRatesPage rawSavingsPlansPage = loadOffers(PUBLIC_AWS_SAVINGS_PLANS_URL + spDataAddr.getVersionUrl(), httpClient);
                Long externalSavingsPlansVersion = Longs.tryParse(rawSavingsPlansPage.getVersion());
                if (externalSavingsPlansVersion == null) {
                    log.error("Cannot determine version of Savings Plan: {}!", rawSavingsPlansPage.getVersion());
                    continue;
                }

                VersionNum versionNum = new VersionNum(externalSavingsPlansVersion);

                if (createdNewSpVersions.containsKey(versionNum)
                        || checkIfThisVersionNonExist(PUBLIC_AWS_SAVINGS_PLANS_PRICING_ID, versionNum)) {
                    MutableInt counter = createdNewSpVersions.computeIfAbsent(versionNum, v -> new MutableInt(0));

                    counter.addAndGet(saveItemsFromOnePage(rawSavingsPlansPage, versionNum,
                            PUBLIC_AWS_SAVINGS_PLANS_PRICING_ID, spDataAddr.getRegionCode(), sortPositionNumberProvider));
                } else {
                    log.info("This {} version of savings plans for {} CloudEnvironmentPricingId exist in DB", externalSavingsPlansVersion, PUBLIC_AWS_SAVINGS_PLANS_PRICING_ID);
                }
            }

            return createdNewSpVersions.entrySet().stream()
                    .filter(e -> e.getValue().intValue() > 0)
                    .map(e -> {
                        log.info("{} raw records of aws savings plans for {}", e.getValue(), PUBLIC_AWS_SAVINGS_PLANS_PRICING_ID.getId());
                        return registerNewVersionAndUpdateLast(e.getKey(), PUBLIC_AWS_SAVINGS_PLANS_PRICING_ID);
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toSet());

        } catch (IOException | URISyntaxException e) {
            log.error("error" + e);
        } catch (NumberFormatException e) {
            log.error("Cannot parse version format for aws savings plans " + e.getMessage(), e);
        }

        return Collections.emptySet();
    }

    /**
     * Method for obtain link for default region from public aws savings plans page.
     *
     * @param httpClient http client
     * @return link with offer for default region
     */
    private List<AWSSavingsPlansRegionsUrl> obtainPublicSavingsPlansUrls(CloseableHttpClient httpClient) {
        try {
            AWSSavingsPlansOfferResponse awsSavingsPlansOfferResponse = loadVersionPage(httpClient);
            return awsSavingsPlansOfferResponse.getRegions();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    private AWSSavingsPlansOfferResponse loadVersionPage(CloseableHttpClient httpClient) throws IOException {
        HttpGet request = new HttpGet(PUBLIC_AWS_COMPUTE_SAVINGS_PLANS_URL);
        HttpResponse response = httpClient.execute(request);
        InputStream content = response.getEntity().getContent();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(content, AWSSavingsPlansOfferResponse.class);
    }

    private AWSSavingsPlansRatesPage loadOffers(String url, CloseableHttpClient httpClient) throws IOException, URISyntaxException {
        HttpGet request = new HttpGet(url);
        HttpResponse response = httpClient.execute(request);
        InputStream content = response.getEntity().getContent();
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(content, AWSSavingsPlansRatesPage.class);
    }

    /**
     * Returns id of AWS savings plans public pricing
     */
    public CloudEnvironmentPricingId getPublicAwsSavingsPlanPricingId() {
        return PUBLIC_AWS_SAVINGS_PLANS_PRICING_ID;
    }
}
