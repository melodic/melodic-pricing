package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import com.google.common.annotations.VisibleForTesting;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Period;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides consistent access to products and pricing data for aws savings plans.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AWSSavingsPlansProductPricingService {

    public static final CollectionName VERSIONED_AWS_SAVINGS_PLANS_PRICING_COLLECTION_NAME = new CollectionName("aws_savings_plans:pricings:" + CollectionName.TEMPLATE);

    private final AWSSavingsPlansPricingRepository savingsPlansPricingRepository;
    private final AWSSavingsPlansRepository savingsPlansRepository;

    private final DataVersionsService dataVersionsService;

    private final ProductsMetadataService productsMetadataService;

    /**
     * All EC2 instance Savings Plans IDs matching given parameters
     *
     * @param purchaseOption purchase option
     * @param purchaseTerm   purchase term (how long)
     * @param instanceType   instance type code
     * @return all pricing IDs for found savings plans, sorted by version descending.
     */
    public List<VersionedCloudEnvPricingId> findEc2InstanceSavingsPlansBy(AWSPurchaseOption purchaseOption,
                                                                          Period purchaseTerm,
                                                                          String instanceType,
                                                                          String regionCode) {
        return savingsPlansRepository.findAllByPurchaseOptionAndPurchaseTermAndInstanceTypeAndRegionCodeOrderByVersionDesc(
                        purchaseOption, purchaseTerm, instanceType, regionCode)
                .stream()
                .map(sp -> new VersionedCloudEnvPricingId(sp.getCloudEnvironmentPricingId(), sp.getVersion()))
                .collect(Collectors.toList());
    }

    /**
     * All EC2 instance Savings Plans IDs matching given parameters
     *
     * @param purchaseOption purchase option
     * @param purchaseTerm   purchase term (how long)
     * @return all pricing IDs for found savings plans, sorted by version descending.
     */
    public List<VersionedCloudEnvPricingId> findComputeSavingsPlansBy(AWSPurchaseOption purchaseOption,
                                                                      Period purchaseTerm) {
        return savingsPlansRepository.findAllByPurchaseOptionAndPurchaseTermAndSavingsPlanTypeOrderByVersionDesc(purchaseOption, purchaseTerm, AWSSavingsPlanType.COMPUTE_SP)
                .stream()
                .map(sp -> new VersionedCloudEnvPricingId(sp.getCloudEnvironmentPricingId(), sp.getVersion()))
                .collect(Collectors.toList());
    }


    /**
     * Discounted product in the versioned Savings Plan.
     */
    @Data
    public static class DiscountedProductInSavingsPlan {
        /**
         * Product id int context of Savings Plan's pricing id.
         */
        private final ProductId spProductId;

        /**
         * Savings Plan's pricing id.
         */
        private final VersionedCloudEnvPricingId versionedSavingsPlanPricingId;

        private final List<Price> pricesPerUnit;
    }

    /**
     * Loads aws savings plans rates for given product Ids
     *
     * @param productId unique aws product id for some cloud env
     * @return empty when is unavailable.
     */
    @Transactional
    public Optional<AWSSavingsPlansProductPricingService.ProductWithPricing> findProduct(ProductId productId) {
        return savingsPlansPricingRepository.findById(productId).map(maybePricing -> new ProductWithPricing(productId, maybePricing));
    }

    /**
     * To be used only by tests.
     */
    @VisibleForTesting
    @Transactional
    public void deleteAll() {
        removeAllOldVersionsSavingsPlansPricing(0);
        log.info("Removed all AWS products and pricing data.");
    }

    /**
     * Remove all old AWS Savings Plans.
     *
     * @param deleteIfMoreThan delete older versions count than N.
     */
    @Transactional
    public void removeAllOldVersionsSavingsPlansPricing(int deleteIfMoreThan) {
        Map<VersionNum, List<CloudEnvironmentPricingId>> allSavingsPlansByVersion = savingsPlansRepository.findAll().stream()
                .collect(Collectors.groupingBy(AWSSavingsPlansRecord::getVersion,
                        Collectors.mapping(AWSSavingsPlansRecord::getCloudEnvironmentPricingId, Collectors.toList())));

        List<VersionNum> versions = allSavingsPlansByVersion.keySet()
                .stream()
                .sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        if (versions.size() > deleteIfMoreThan) {
            List<VersionNum> versionsToBeDeleted = versions.subList(Math.max(0, deleteIfMoreThan), versions.size());

            for (VersionNum versionNum : versionsToBeDeleted) {
                log.info("Removing AWS Savings Plan pricing fata for version {}", versionNum.getNum());

                List<CloudEnvironmentPricingId> awsSavingsPlansToRemove = allSavingsPlansByVersion.get(versionNum);
                if (awsSavingsPlansToRemove == null || awsSavingsPlansToRemove.isEmpty()) {
                    continue;
                }

                for (CloudEnvironmentPricingId pricingId : awsSavingsPlansToRemove) {
                    log.info("Pricing {} {}", pricingId, versionNum);
                    List<AWSSavingsPlansPricingRecord> pricingEntriesToRemove = savingsPlansPricingRepository
                            .findAllByCloudEnvironmentPricingIdAndVersion(pricingId, versionNum);
                    Set<CloudEnvironmentPricingId> spPricingsToRemove = pricingEntriesToRemove.stream().map(AWSSavingsPlansPricingRecord::getCloudEnvironmentPricingId).collect(Collectors.toSet());
                    List<ProductId> discountedProductsToDelete = pricingEntriesToRemove.stream()
                            .map(AWSSavingsPlansPricingRecord::getId)
                            .collect(Collectors.toList());
                    productsMetadataService.removeProducts(discountedProductsToDelete);
                    savingsPlansPricingRepository.deleteByCloudEnvironmentPricingIdAndVersion(pricingId, versionNum);
                    spPricingsToRemove.forEach(spPricingId -> dataVersionsService.removeVersion(pricingId, versionNum));
                    savingsPlansRepository.deleteByCloudEnvironmentPricingIdAndVersion(pricingId, versionNum);
                    log.info("Removed all discounted products {} and Savings Plans ({}) metadata for AWS version {}",
                            discountedProductsToDelete.size(), spPricingsToRemove.size(), versionNum.getNum());
                }
            }
        }
    }

    @Data
    public static class ProductWithPricing {
        private final ProductId id;
        private final AWSSavingsPlansPricingRecord pricingRecord;
    }
}
