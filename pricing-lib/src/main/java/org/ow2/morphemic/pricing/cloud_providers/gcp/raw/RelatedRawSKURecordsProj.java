package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import java.util.List;

/**
 * Query result projection which allows to fetch only related raw records.
 */
interface RelatedRawSKURecordsProj {

    List<GCPRawSKURecord> getRawSKURecords();

}
