package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

/**
 * Helps to read {@link org.ow2.morphemic.pricing.versions.VersionNum} saved as long.
 */
@ReadingConverter
class VersionNumFromLongConverter implements Converter<Long, VersionNum> {

    @Override
    public VersionNum convert(Long source) {
        return new VersionNum(source);
    }
}
