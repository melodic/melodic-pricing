package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.beans.Transient;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Describes product and its pricing data. In GCP products is described as "service". Used to calculate product price in calculator.
 */
@Document(collection = GCPProductPricingRecord.MONGO_COLLECTION_NAME)
@TypeAlias("TYPE_GCPProductRecord")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class GCPProductPricingRecord {

    static final String MONGO_COLLECTION_NAME = "gcp_products";

    public static final String GLOBAL_REGION_CODE = "global";

    private static final Map<OfferTermType, Period> CONTRACT_LENGHTS = Map.of(new OfferTermType("Commit1Yr"), Period.ofYears(1), new OfferTermType("Commit3Yr"), Period.ofYears(3));

    @Id
    @Field(targetType = FieldType.STRING)
    private ProductId id;

    /**
     * SKU ID
     */
    @Indexed
    private String productCode;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum basedOnRawVersionNum;

    private List<GCPProductPriceOfferRecord> offers;

    /**
     * It is taken from category/usageType. Currently there are 4 possible values:
     * "Commit1Yr",
     * "Commit3Yr",
     * "OnDemand",
     * "Preemptible".
     */
    @Field(targetType = FieldType.STRING)
    @Indexed
    private OfferTermType offerTermType;

    /**
     * Is this product available in all regions?
     */
    private boolean global;

    private Set<String> availableInRegions;

    private GCPTaxonomy taxonomy;

    public void addOffer(GCPProductPriceOfferRecord offer) {
        if (offers == null) {
            offers = new ArrayList<>();
        }
        offers.add(offer);
    }

    /**
     * Postprocessing: updates {@link #global} flag value.
     *
     * @return this
     */
    public GCPProductPricingRecord updateGlobalAvailFlag() {
        if (availableInRegions == null) {
            availableInRegions = new HashSet<>();
        }

        global = availableInRegions.contains(GLOBAL_REGION_CODE);
        return this;
    }

    /**
     * Tries to extract lease contract length.
     *
     * @return
     */
    @Transient
    public Optional<Period> getLeaseContractLength() {
        return Optional.ofNullable(CONTRACT_LENGHTS.get(offerTermType));
    }

}
