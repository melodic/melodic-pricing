package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingIdOnlyDbRowProjection;
import org.ow2.morphemic.pricing.cloud_providers.ProductIdOnlyDbRowProjection;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.raw.GCPRawDataPricingManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides pricing data for GCP products.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class GCPProductPricingService {
    static final CollectionName VERSIONED_GCP_PRICING_COLLECTION_NAME = new CollectionName("gcp:pricings:" + CollectionName.TEMPLATE);

    private final GCPProductPricingRecordsRepository pricingRecordsRepository;

    private final DataVersionsService dataVersionsService;

    private final GCPRawDataPricingManager gcpRawDataPricingManager;

    private final ProductsMetadataService productsMetadataService;

    /**
     * Allows to fetch all IDs of LATEST product pricing records for given Cloud Environment.
     * Each cloud environment may have different pricing.
     *
     * @param cloudEnvironmentPricingId cloud env
     * @return set of product ids
     */
    @Transactional
    public Set<ProductId> allProductIds(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        Optional<VersionNum> maybeLastVersionNum = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, VERSIONED_GCP_PRICING_COLLECTION_NAME);

        if (maybeLastVersionNum.isPresent()) {
            return pricingRecordsRepository.findAllByCloudEnvironmentPricingIdAndBasedOnRawVersionNum(cloudEnvironmentPricingId,
                    maybeLastVersionNum.get(), ProductIdOnlyDbRowProjection.class)
                    .map(ProductIdOnlyDbRowProjection::getId)
                    .collect(Collectors.toSet());

        } else {
            return Collections.emptySet();
        }

    }

    /**
     * Tries to find product pricing record for given Cloud Provider's client and product ID for that client.
     *
     * @param prodId unique product id
     * @return maybe found record
     */
    @Transactional
    public Optional<GCPProductPricingRecord> findProduct(ProductId prodId) {
        return pricingRecordsRepository.findById(prodId);
    }


    /**
     * To be used only by tests.
     */
    @Transactional
    public void deleteAll() {
        gcpRawDataPricingManager.deleteAll();
        pricingRecordsRepository.findDistinctBy(PricingIdOnlyDbRowProjection.class)
                .map(PricingIdOnlyDbRowProjection::getCloudEnvironmentPricingId)
                .distinct().forEach(dataVersionsService::removeAllVersions);
        pricingRecordsRepository.findAllBy(ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId)
                .forEach(productsMetadataService::removeProduct);
        pricingRecordsRepository.deleteAll();
        log.info("Removed all GCP products and pricing data.");
    }

    /**
     * Remove GCP pricing data for given pricing.
     * @param pricingId pricing id
     */
    @Transactional
    public void deletePricing(CloudEnvironmentPricingId pricingId) {
        log.debug("GCP pricing data will be deleted {}", pricingId);
        gcpRawDataPricingManager.deletePricing(pricingId);
        dataVersionsService.removeAllVersions(pricingId);
        pricingRecordsRepository.findAllByCloudEnvironmentPricingId(pricingId, ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId)
                .forEach(productsMetadataService::removeProduct);

        long removed = pricingRecordsRepository.deleteAllByCloudEnvironmentPricingId(pricingId);
        log.info("Removed GCP pricing data for {}. Removed records {}", pricingId, removed);
    }

}
