package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import org.ow2.morphemic.pricing.versions.VersionNum;

/**
 * projection: only version number.
 */
public interface VersionMetadataProjection {

    VersionNum getVersion();
}
