package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.Map;

/**
 * Contains single record fetched from AWS Pricing API. Contains raw data (usually in json format) and some metadata.
 */
@Document(collection = "aws_raw_pricings")
@TypeAlias("AWSRawPricingDataRecord")
@NoArgsConstructor
@Data
@CompoundIndexes(@CompoundIndex(def = "{cloudEnvironmentId: 1, version: 1}"))
class AWSRawPricingDataRecord {
    /**
     * it should differ from Document collection, because it must not be changed during refactorings
     */
    static final CollectionName VERSION_COLLECTION_NAME = new CollectionName("aws:raw_pricings:" + CollectionName.TEMPLATE);

    /**
     * Managed by spring
     */
    @Id
    private String id;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum version;

    /**
     * what was loaded from AWS Pricing Api
     */
    private String rawContents;

    /**
     * maybe {@link #rawContents} should be parsed in future? - hmmmmmm
     */
    private Map<Object, Object> pricingForProduct;

    @Indexed
    private Long sortPosNum;

    AWSRawPricingDataRecord(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                            VersionNum versionNum, String rawContents,
                            Map<Object, Object> pricingForProduct, Long sortPosNum) {
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.version = versionNum;
        this.rawContents = rawContents;
        this.pricingForProduct = pricingForProduct;
        this.sortPosNum = sortPosNum;
    }
}
