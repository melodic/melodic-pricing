package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.Place;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * Helps to save {@link org.ow2.morphemic.pricing.model.Place} as encoded text.
 */
@WritingConverter
class PlaceToStringConverter implements Converter<Place, String> {

    static final String SEPARATOR = "/";

    @Override
    public String convert(Place p) {
        return p.getType().name() + SEPARATOR + p.getCode();
    }
}
