package org.ow2.morphemic.pricing.cloud_providers;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.PriceListType;

/**
 * Generic product Dto
 */
@RequiredArgsConstructor
@Getter
public class GenericProductDto {
    private final CloudProvider cloudProvider;
    private final PriceListType priceListType;
}
