package org.ow2.morphemic.pricing.versions;

import lombok.RequiredArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;

/**
 * Versioned, logical collection of data.
 */
@RequiredArgsConstructor
public class CollectionName {
    public static final String TEMPLATE = "{}";

    private final String name;

    public String asEnvCollectionName(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        if (name.contains(TEMPLATE)) {
            return name.replace(TEMPLATE, cloudEnvironmentPricingId.getId());
        } else {
            return name + ":" + cloudEnvironmentPricingId.getId();
        }
    }

}
