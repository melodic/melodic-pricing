package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.Unit;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

/**
 * Contains single rate (range) used to generate price.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GCPPriceOfferRate {

    private GCPUnit baseUnit;

    private Double baseUnitConversionFactor;

    private GCPUnit usageUnit;

    private Double displayQuantity;

    private double startUsageAmount;

    private Double endUsageAmount;

    private Double currencyConversionRate;

    private Map<String, BigDecimal> pricePerUnitByCurrency;

    /**
     * If usage amount is in accepted range of amounts, this rate probably can be applied.
     *
     * @param usageAmount
     * @return
     */
    public boolean isUsageAmountInRange(double usageAmount) {
        return usageAmount >= startUsageAmount && (endUsageAmount == null || startUsageAmount < endUsageAmount);
    }

    public Optional<BigDecimal> pricePerUnit(String currencyCode) {
        return Optional.ofNullable(pricePerUnitByCurrency.get(currencyCode));
    }

    /**
     * Unit metadata.
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static final class GCPUnit {
        /**
         * unit abbreviate
         */
        private String abbr;
        /**
         * long name / description
         */
        private String description;

        /**
         * Converts itself to {@link Unit}.
         *
         * @return unit
         */
        public Unit asUnit() {
            return new Unit(abbr);
        }
    }

    /**
     * Make shallow copy.
     *
     * @return shallow copy
     */
    public GCPPriceOfferRate shallowClone() {
        return new GCPPriceOfferRate(baseUnit, baseUnitConversionFactor,
                usageUnit, displayQuantity,
                startUsageAmount, endUsageAmount,
                currencyConversionRate, pricePerUnitByCurrency);
    }
}
