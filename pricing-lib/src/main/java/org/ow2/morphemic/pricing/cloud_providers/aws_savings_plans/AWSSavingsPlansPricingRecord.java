package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.time.Period;
import java.util.Date;
import java.util.Map;

/**
 * Contains Aws savings plans rates details.
 */
@Document(collection = AWSSavingsPlansPricingRecord.MONGO_COLLECTION_NAME)
@TypeAlias("AWSSavingsPlansOffersRecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AWSSavingsPlansPricingRecord {

    static final String MONGO_COLLECTION_NAME = "aws_savings_plans_pricing";

    /**
     * Product id created.
     */
    @Id
    @Field(targetType = FieldType.STRING)
    private ProductId id;

    /**
     * External product id (SKU) identifier from AWS.
     */
    @Indexed
    private String productSkuId;

    /**
     * External savings plans rateCode it's look like savingsPlansSku.productSku
     */
    @Indexed
    private String rateCode;

    /**
     * It is connected with this pricing.
     */
    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    /**
     * actual version number
     */
    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum version;

    private Date effectiveDate;

    /**
     * Unit of prices (Hrs).
     */
    @Field(targetType = FieldType.STRING)
    private Unit unit;

    /**
     * Price per unit by currency (Usually USD).
     */
    private Map<String, BigDecimal> pricePerUnitByCurrency;

    @Indexed
    private String regionCode;

    /**
     * Declared length. Usually: 1y or 3yrs.
     */
    @Indexed
    private Period purchaseTerm;
}
