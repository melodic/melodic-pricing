package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Describes virtual machine in GCP. VM consists of CPU and RAM - and they can be billed.
 */
@Document(collection = GCPAggregatedProduct.COLLECTION_NAME)
@TypeAlias("TYPE_GCPVmAggregatedProduct")
@NoArgsConstructor
@Data
public class GCPVmAggregatedProduct extends GCPAggregatedProduct {

    private Amount cpuCount;

    private Amount ramAmountGB;

    private GCPAggregatedUnits cpuUnits = new GCPAggregatedUnits();

    private GCPAggregatedUnits ramUnits = new GCPAggregatedUnits();

    @Builder
    @SuppressWarnings("checkstyle:parameternumber")
    public GCPVmAggregatedProduct(ProductId productId, CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                  String description, String productFamily, GCPAggregatedProductFamily gcpFamily,
                                  Set<Place> regions, List<GCPBasicProduct> components, Set<OfferTermType> offerTermTypes,
                                  Amount cpuCount, Amount ramAmountGB, GCPAggregatedUnits cpuUnits, GCPAggregatedUnits ramUnits) {
        super(productId, cloudEnvironmentPricingId, description, productFamily, gcpFamily, regions, components, offerTermTypes);
        this.cpuCount = cpuCount;
        this.ramAmountGB = ramAmountGB;
        this.cpuUnits = cpuUnits;
        this.ramUnits = ramUnits;
    }

    @Override
    public Map<String, Object> rawAttributes() {
        return new HashMap<>();
    }

    public Optional<Unit> suggestedCpuUnit(Unit aggregateUnit) {
        return cpuUnits != null ? cpuUnits.suggestedUnit(aggregateUnit) : Optional.empty();
    }

    public Optional<Unit> suggestedRamUnit(Unit aggregateUnit) {
        return ramUnits != null ? ramUnits.suggestedUnit(aggregateUnit) : Optional.empty();
    }
}
