
package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.fileloader;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "description",
        "guestCpus",
        "isSharedCpu",
        "memoryGb",
        "regions",
        "family",
        "offers"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GCPVmProductDef {

    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("guestCpus")
    private Integer guestCpus;
    @JsonProperty("isSharedCpu")
    private String isSharedCpu;
    @JsonProperty("memoryGb")
    private Double memoryGb;
    @JsonProperty("regions")
    private String regions;
    @JsonProperty("family")
    private String family;
    @JsonProperty("offers")
    private List<Offer> offers = new ArrayList<Offer>();
}
