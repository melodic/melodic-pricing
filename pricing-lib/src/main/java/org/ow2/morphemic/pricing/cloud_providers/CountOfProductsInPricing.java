package org.ow2.morphemic.pricing.cloud_providers;

import lombok.Data;

/**
 * Used in calculating db aggregate data. Count of products in pricing.
 */
@Data
public class CountOfProductsInPricing {

    private String id;

    private long productsCount;

}
