package org.ow2.morphemic.pricing.cloud_providers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.PriceListType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Generic products metadata service. Used to help build generic pricing service which can use non-uniform pricing datasets.
 * Experimental.
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ProductsMetadataService {

    private final GenericProductRecordsRepository repository;

    @Transactional
    public boolean isProductMetadataInCache(ProductId productId) {
        return repository.existsById(productId);
    }

    @Transactional
    public void createProductMetadata(ProductId productId, CloudProvider cloudProvider) {
        if (!isProductMetadataInCache(productId)) {
            repository.save(new GenericProductRecord(productId, cloudProvider));
        }
    }

    @Transactional
    public void createProductMetadata(ProductId productId, CloudProvider cloudProvider, PriceListType priceListType) {
        if (!isProductMetadataInCache(productId)) {
            repository.save(new GenericProductRecord(productId, cloudProvider, priceListType));
        }
    }

    public void createProductsMetadata(Set<ProductId> productIds, CloudProvider cloudProvider, PriceListType priceListType) {
        List<GenericProductRecord> productsToRegister = productIds.stream()
                .filter(id -> !isProductMetadataInCache(id))
                .map(productId -> new GenericProductRecord(productId, cloudProvider, priceListType))
                .collect(Collectors.toList());
        repository.saveAll(productsToRegister);
    }


    @Transactional
    public Optional<CloudProvider> findCloudProviderOf(ProductId productId) {
        if (productId.getId().contains("custom")) { // custom VMs are not present in metadata repository
            return Optional.of(CloudProvider.GCP);
        }
        return repository.findById(productId).map(GenericProductRecord::getCloudProvider);
    }

    @Transactional
    public Optional<GenericProductDto> findGenericProductDto(ProductId productId) {
        if (productId.getId().contains("custom")) { // custom VMs are not present in metadata repository
            return Optional.of(new GenericProductDto(CloudProvider.GCP, null));
        }
        return repository.findById(productId, GenericProductDto.class);
    }

    /**
     * Remove product from metadata cache.
     *
     * @param productId
     */
    public void removeProduct(ProductId productId) {
        repository.deleteById(productId);
    }

    /**
     * Remove all these products
     *
     * @param productIds
     */
    public void removeProducts(Collection<ProductId> productIds) {
        if (!productIds.isEmpty()) {
            // TODO not optimal but Spring Mongo repository does not support delete when Id is In set of productIds
            // (conversion problems).
            // this is sloow but works properly
            productIds.forEach(repository::deleteById);
            // TODO when you resolve this problem remember to split ids into chunks
            // because MingoDB cannot process large sets of IDs (we have > 600 000 ids in one query!)
        }
    }

    /**
     * Use only in tests.
     */
    public void removeAllProducts() {
        repository.deleteAll();
        log.info("Removed all products.");
    }

}
