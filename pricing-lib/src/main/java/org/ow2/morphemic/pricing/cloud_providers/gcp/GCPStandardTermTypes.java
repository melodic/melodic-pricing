package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.Getter;
import org.ow2.morphemic.pricing.model.OfferTermType;

import java.time.Period;

/**
 * There is no need to hardcode GCP Offer tem types (usage types).
 */
public enum GCPStandardTermTypes {

    ON_DEMAND(new OfferTermType("OnDemand")),
    COMMIT_1YR(new OfferTermType("Commit1Yr"), Period.ofYears(1)),
    COMMIT_3YR(new OfferTermType("Commit3Yr"), Period.ofYears(3)),
    PREEMTIBLE(new OfferTermType("Preemptible"));

    @Getter
    private final OfferTermType offerTermType;

    @Getter
    private final Period contractLength;

    GCPStandardTermTypes(OfferTermType offerTermType, Period contractLength) {
        this.offerTermType = offerTermType;
        this.contractLength = contractLength;
    }

    GCPStandardTermTypes(OfferTermType offerTermType) {
        this(offerTermType, null);
    }

}
