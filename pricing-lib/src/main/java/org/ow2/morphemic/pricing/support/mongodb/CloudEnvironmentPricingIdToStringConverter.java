package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * Used to convert value to mongo text field.
 */
@WritingConverter
class CloudEnvironmentPricingIdToStringConverter implements Converter<CloudEnvironmentPricingId, String> {

    @Override
    public String convert(CloudEnvironmentPricingId source) {
        return source.getId();
    }
}
