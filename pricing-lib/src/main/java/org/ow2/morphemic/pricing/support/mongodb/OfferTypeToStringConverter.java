package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.OfferType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * Helps to save {@link OfferType} as text.
 */
@WritingConverter
public class OfferTypeToStringConverter implements Converter<OfferType, String> {

    @Override
    public String convert(OfferType source) {
        return source.getName();
    }
}
