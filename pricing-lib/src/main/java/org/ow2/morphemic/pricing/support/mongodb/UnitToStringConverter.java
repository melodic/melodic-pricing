package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * Helps to save {@link Unit} as text.
 */
@WritingConverter
class UnitToStringConverter implements Converter<Unit, String> {

    @Override
    public String convert(Unit source) {
        return source.getUnitName();
    }
}
