package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

/**
 * DB projection: fetches only raw contents.
 */
interface AWSRawPricingContentsOnlyProjection {

    /**
     * Used to provide fast "page fetch" operation.
     * @return last number of sort field.
     */
    Long getSortPosNum();

    /**
     *
     * @return raw contents.
     */
    String getRawContents();

}
