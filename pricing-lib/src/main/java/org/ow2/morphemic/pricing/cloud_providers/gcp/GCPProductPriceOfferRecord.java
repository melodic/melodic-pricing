package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.TypeAlias;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Contains all price offers. Based on "pricingInfo' from raw pricing data.
 */
@TypeAlias("GCPProductPriceOfferRecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GCPProductPriceOfferRecord {


    private String description;

    private Date effectiveDate;

    private List<GCPPriceOfferRate> rates;

    private GCPAggregationInfo aggregationInfo;

    public void addOfferRate(GCPPriceOfferRate offerRate) {
        if (rates == null) {
            rates = new ArrayList<>();
        }
        rates.add(offerRate);
    }

    /**
     * Postprocessing: Rates should be organized in asc order.
     */
    public GCPProductPriceOfferRecord sortAndAssignProperRateRanges() {
        if (rates == null) {
            rates = new ArrayList<>();
        }

        rates.sort(new GCPPriceOfferRateUsageUnitAbbrComparator()
                .thenComparing(new GCPPriceOfferValueRangeComparator()));
        if (!rates.isEmpty()) {
            Double prevStartRange = null;
            // for each unit we recalculate ranges independently
            GCPPriceOfferRate.GCPUnit group = rates.get(rates.size() - 1).getUsageUnit();
            for (int i = rates.size() - 1; i >= 0; i--) {
                GCPPriceOfferRate rate = rates.get(i);
                if (!group.equals(rate.getUsageUnit())) {
                    prevStartRange = null;
                    group = rate.getUsageUnit();
                }
                rate.setEndUsageAmount(prevStartRange);
                prevStartRange = rate.getStartUsageAmount();
            }
        }

        return this;
    }

    /**
     * Aggregation info - purpose is still unknown.
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static final class GCPAggregationInfo {
        private Integer aggregationCount;
        /**
         * Enum: DAILY, MONTHLY?
         */
        private String aggregationInterval;
        /**
         * Enum: PROJECT ???
         */
        private String aggregationLevel;
    }

    private static class GCPPriceOfferRateUsageUnitAbbrComparator implements Comparator<GCPPriceOfferRate> {

        @Override
        public int compare(GCPPriceOfferRate rate1, GCPPriceOfferRate rate2) {
            return rate1.getUsageUnit().getAbbr().compareTo(rate2.getUsageUnit().getAbbr());
        }
    }

    private static class GCPPriceOfferValueRangeComparator implements Comparator<GCPPriceOfferRate> {

        @Override
        public int compare(GCPPriceOfferRate rate1, GCPPriceOfferRate rate2) {
            return (int) Math.round(Math.signum(rate1.getStartUsageAmount() - rate2.getStartUsageAmount()));
        }
    }


}
