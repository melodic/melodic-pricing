package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.data.annotation.TypeAlias;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Helper object which allows to determine units used to calculate basic products' prices in context of unit for aggregate product.
 */
@TypeAlias("TYPE_AggregatedUnits")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GCPAggregatedUnits {

    @Builder.Default
    private Map<Unit, Unit> aggregateUnitToBaseUnit = new HashMap<>();

    public Optional<Unit> suggestedUnit(Unit aggregateUnit) {
        return Optional.ofNullable(aggregateUnitToBaseUnit).map(a -> a.get(aggregateUnit));
    }

    public GCPAggregatedUnits addMapping(Map<String, String> m) {
        aggregateUnitToBaseUnit.putAll(m.entrySet().stream().collect(Collectors.toMap(e -> new Unit(e.getKey()), e -> new Unit(e.getValue()), (v1, v2) -> v2)));
        return this;
    }
}
