package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Represents raw data fetched from Azure pricing API
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@Builder
public class AzureRawPricingItem {

    /**
     * Curency code such as: "USD", "EUR".
     */
    private String currencyCode;
    private double tierMinimumUnits;
    private String reservationTerm;
    private BigDecimal retailPrice;
    private BigDecimal unitPrice;
    private String armRegionName;
    private String location;
    private Date effectiveStartDate;
    /**
     * for us it is a ProductId
     */
    @Indexed(name = "idx_azure_meter_id", direction = IndexDirection.ASCENDING)
    private String meterId;
    private String meterName;
    /**
     * WARNING: it is not a ProcuctId!!!
     */
    private String productId;
    private String skuId;
    private String productName;
    private String skuName;
    private String serviceName;
    private String serviceId;
    private String serviceFamily;
    private String unitOfMeasure;
    private String type;
    private boolean isPrimaryMeterRegion;
    private String armSkuName;
}
