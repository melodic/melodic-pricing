package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;

import java.util.Optional;

/**
 * Base class for creating RAW pricing data loaders for AZURE.
 */
@RequiredArgsConstructor
@Slf4j
public abstract class AbstractAzurePricingRawDataLoader {
    private final AzureRawPricingDataRepository azureRawPricingDataRepository;
    private final DataVersionsService dataVersionsService;

    @Getter
    @Setter
    private boolean removeOldVersions = true;

    protected Optional<VersionedCloudEnvPricingId> registerNewVersionAndClean(VersionNum newVersionNum, CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                                              PricingDataImportStatisticCollector pricingDataImportStatisticCollector) {
        VersionedCloudEnvPricingId versionedEnvId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, newVersionNum);
        dataVersionsService.registerNewVersion(versionedEnvId, AzureRawPricingDataRecord.VERSION_COLLECTION_NAME);
        if (removeOldVersions) {
            pricingDataImportStatisticCollector.resumeMeasureTimeOfDeletePricingData();
            long removedCount = azureRawPricingDataRepository.deleteAllByCloudEnvironmentPricingIdAndVersionLessThan(cloudEnvironmentPricingId, newVersionNum);
            pricingDataImportStatisticCollector.suspendMeasureTimeOfDeletePricingData();
            log.debug("Removed {} old records from raw Azure data cache for {}.", removedCount, cloudEnvironmentPricingId);
        } else {
            log.debug("Old records from raw Azure data cache were not removed for {}.", cloudEnvironmentPricingId);
        }
        return Optional.of(versionedEnvId);
    }

    /**
     * Saves raw data in Mongo for given version of pricing.
     *
     * @param azureRawPricingPage       fetched from web (or from other place)
     * @param versionNum                version
     * @param cloudEnvironmentPricingId pricing id
     * @return number of items.
     */
    protected int saveItemsFromOnePage(AzureRawPricingResponse azureRawPricingPage, VersionNum versionNum, CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        azureRawPricingPage.getItems().stream()
                .map(azureRawPricingItem -> mapToAzureRawPricingDataRecord(azureRawPricingItem, versionNum, cloudEnvironmentPricingId))
                .forEach(azureRawPricingDataRepository::save);
        return azureRawPricingPage.getItems().size();
    }

    protected AzureRawPricingDataRecord mapToAzureRawPricingDataRecord(AzureRawPricingItem azureRawPricingItem, VersionNum versionNum, CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        return new AzureRawPricingDataRecord(cloudEnvironmentPricingId, versionNum, azureRawPricingItem);
    }

}
