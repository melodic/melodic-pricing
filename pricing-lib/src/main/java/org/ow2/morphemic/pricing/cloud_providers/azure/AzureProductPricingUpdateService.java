package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzurePricingRawFilesDataLoader;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzurePublicPricingRawDataLoader;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzureRawPricingDataRecord;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzureRawProductPricingManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.OfferType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.azure.AzureProductIdFactory;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService.VERSIONED_AZURE_PRICING_COLLECTION_NAME;

/**
 * Responsible for update of AZURE pricing data. Update process consists of following steps:
 * <ol>
 * <li>Downloading data from Azure API </li>
 * <li>Saving raw data in Mongo DB (data repository)</li>
 * <li>Transforming raw data to domain data (products, pricing offers)</li>
 * </ol>
 */
@Service("azureProductPricingUpdateService")
@Slf4j
@RequiredArgsConstructor
public class AzureProductPricingUpdateService implements ProductPricingUpdateService {

    @Qualifier("azurePublicPricingLoader")
    private final AzurePublicPricingRawDataLoader azurePublicPricingLoader;

    private final DataVersionsService dataVersionsService;

    private final AzurePricingRawFilesDataLoader filesDataLoader;

    private final AzureRawProductPricingManager azureRawProductPricingManager;

    private final ProductsMetadataService productsMetadataService;

    private final AzurePricingRecordsRepository productPricingRecordsRepository;

    private final AzureProductIdFactory productIdFactory = new AzureProductIdFactory();

    @Override
    @Transactional
    public synchronized Optional<VersionedCloudEnvPricingId> loadPricingDumpDataFromFiles() throws IOException {
        PricingDataImportStatisticCollector pricingDataImportStatisticCollector = new PricingDataImportStatisticCollector(
                CloudProvider.AZURE, "pricing_dump", List.of()); //for collecting import statistics
        Optional<VersionedCloudEnvPricingId> maybeVersionedCloudEnvPricingId = filesDataLoader.loadDumpToRawCache(pricingDataImportStatisticCollector);
        if (maybeVersionedCloudEnvPricingId.isPresent()) {
            VersionedCloudEnvPricingId versionedCloudEnvPricingId = maybeVersionedCloudEnvPricingId.get();
            updatePricingFromRaw(versionedCloudEnvPricingId, pricingDataImportStatisticCollector);
            log.debug("Pricing dump load statistics: \n{}", pricingDataImportStatisticCollector.printStatistics());
        }
        return maybeVersionedCloudEnvPricingId;
    }

    /**
     * Converts all raw pricing records for given versioned pricing to records describing pricing data.
     *
     * @param versionedCloudEnvPricingId
     */
    void updatePricingFromRaw(VersionedCloudEnvPricingId versionedCloudEnvPricingId, PricingDataImportStatisticCollector pricingDataImportStatistic) {
        pricingDataImportStatistic.startMeasureTimeOfTransformationOfSourceDataToBusinessModel();

        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();

        AtomicInteger counter = new AtomicInteger();
        long savedCount = azureRawProductPricingManager.rawPricingDataAsStreamForVersion(versionedCloudEnvPricingId).peek(rawItem -> {
            ProductId productId = productIdFactory.createProductId(cloudEnvironmentPricingId, rawItem.getMeterId());

            AzureProductPricingRecord azureProductPricingRecord = productPricingRecordsRepository.findById(productId)
                    .orElseGet(() -> new AzureProductPricingRecord(productId, cloudEnvironmentPricingId, versionNum, new ArrayList<>()));

            if (!versionNum.equals(azureProductPricingRecord.getBasedOnRawVersionNum())) {
                azureProductPricingRecord.setPriceOffers(new ArrayList<>());
                azureProductPricingRecord.setBasedOnRawVersionNum(versionNum);
            }

            OfferTermType offerTermType = rawItem.getReservationTerm() == null ? AzureOfferTermTypes.ON_DEMAND.getOfferTermType()
                    : new OfferTermType(rawItem.getReservationTerm());

            azureProductPricingRecord.addPriceOffer(new AzurePriceOffer(offerTermType, new OfferType(rawItem.getType()),
                    rawItem.getProductName(), rawItem.getArmRegionName(),
                    Map.of(rawItem.getCurrencyCode(), rawItem.getUnitPrice()),
                    Map.of(rawItem.getCurrencyCode(), rawItem.getRetailPrice()), rawItem.getUnitOfMeasure()));

            productPricingRecordsRepository.save(azureProductPricingRecord);
            if (!productsMetadataService.isProductMetadataInCache(productId)) {
                productsMetadataService.createProductMetadata(productId, CloudProvider.AZURE);
            }

            var i = counter.incrementAndGet();
            if (i % 1000 == 0) {
                log.debug("Loaded {} products for {}", i, versionedCloudEnvPricingId);
            }

        }).count();

        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, VERSIONED_AZURE_PRICING_COLLECTION_NAME);
        pricingDataImportStatistic.incrementTotalNumberOfTransformationRowsOfSourceDataToBusinessModel(savedCount);
        pricingDataImportStatistic.stopMeasureTimeOfTransformationOfSourceDataToBusinessModel();
        log.info("Updated products: {} for version {}", savedCount, versionedCloudEnvPricingId);
    }

    /**
     * Now only public pricing is loaded. It will be probably final solution for Azure.
     *
     * @param cloudEnvironmentPricingId pricing of cloud env.
     */
    @Override
    @Transactional
    public void updateUsingExternalApi(CloudEnvironmentPricingId cloudEnvironmentPricingId, PricingDataImportStatisticCollector pricingDataImportStatisticCollector) {
        Optional<VersionedCloudEnvPricingId> maybeVersionedCloudEnvPricingId = azurePublicPricingLoader.loadRawDataToCache(cloudEnvironmentPricingId,
                pricingDataImportStatisticCollector);
        maybeVersionedCloudEnvPricingId.ifPresent(versionedCloudEnvPricingId -> {
            updatePricingFromRaw(versionedCloudEnvPricingId, pricingDataImportStatisticCollector);
        });
    }

    /**
     * Returns public pricing id. It can be used by each of Azure accounts.
     */
    public CloudEnvironmentPricingId getPublicPricingId() {
        return azurePublicPricingLoader.getPublicAzurePricingId();
    }

    /**
     * Checks if public pricing is available.
     */
    public boolean hasPublicPricingLoaded() {
        return dataVersionsService.lastVersionOf(azurePublicPricingLoader.getPublicAzurePricingId(), AzureRawPricingDataRecord.VERSION_COLLECTION_NAME)
                .isPresent();
    }
}
