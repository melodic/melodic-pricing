package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.services.cloudbilling.model.AggregationInfo;
import com.google.api.services.cloudbilling.model.Money;
import com.google.api.services.cloudbilling.model.PricingExpression;
import com.google.api.services.cloudbilling.model.PricingInfo;
import com.google.api.services.cloudbilling.model.Sku;
import com.google.api.services.cloudbilling.model.TierRate;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.CustomConverter;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.converter.builtin.DateToStringConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.Type;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPPriceOfferRate;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPriceOfferRecord;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingRecord;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPTaxonomy;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GcpProductIdFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Converts raw data into domain pricing model for GCP.
 */
@Slf4j
@Component
public class GCPProductsAndOffersFactory {

    private final MapperFactory mapperFactory;

    private final GcpProductIdFactory gcpProductIdFactory;

    /**
     * In product usage reports GCP provides unit name as unit description. So we have to provide possibility to calculate
     * prices using abbreviations (eg: "h") or full unit name ("hours"). This flag controls it.
     */
    @Value("${pricing.gcp.shouldAlsoUseDescriptionUnitsAsAbbr:true}")
    private boolean shouldAlsoUseDescriptionUnitsAsAbbr = true;

    GCPProductsAndOffersFactory(GcpProductIdFactory gcpProductIdFactory) {
        this.gcpProductIdFactory = gcpProductIdFactory;

        this.mapperFactory = new DefaultMapperFactory.Builder().build();
        ConverterFactory converterFactory = mapperFactory.getConverterFactory();
        converterFactory.registerConverter(new DateToStringConverter("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
        final String moneyToMapConvId = "moneyToMap";
        converterFactory.registerConverter(moneyToMapConvId, new MoneyToBigDecimalConverter());

        mapperFactory.classMap(com.google.api.services.cloudbilling.model.Service.class, GCPTaxonomy.class)
                .field("name", "serviceName")
                .byDefault()
                .register();

        mapperFactory.classMap(GCPRawServicePricing.class, GCPProductPricingRecord.class)
                .field("versionedCloudEnvPricingId.versionNum", "basedOnRawVersionNum")
                .field("versionedCloudEnvPricingId.cloudEnvironmentPricingId", "cloudEnvironmentPricingId")
                .field("gcpService", "taxonomy")
                .register();

        mapperFactory.classMap(PricingInfo.class, GCPProductPriceOfferRecord.class)
                .field("aggregationInfo", "aggregationInfo")
                .field("effectiveTime", "effectiveDate")
                .field("summary", "description")
                .register();

        mapperFactory.classMap(Sku.class, GCPProductPricingRecord.class)
                .field("category.usageType", "offerTermType")
                .field("skuId", "productCode")
                .field("name", "taxonomy.productSkuName")
                .field("serviceRegions", "availableInRegions")
                .register();

        mapperFactory.classMap(AggregationInfo.class, GCPProductPriceOfferRecord.GCPAggregationInfo.class)
                .byDefault().register();

        mapperFactory.classMap(TierRate.class, GCPPriceOfferRate.class)
                .field("startUsageAmount", "startUsageAmount")
                .fieldMap("unitPrice", "pricePerUnitByCurrency").converter(moneyToMapConvId).add()
                .register();

        mapperFactory.classMap(PricingExpression.class, GCPPriceOfferRate.class)
                .field("baseUnit", "baseUnit.abbr")
                .field("baseUnitDescription", "baseUnit.description")
                .field("usageUnit", "usageUnit.abbr")
                .field("usageUnitDescription", "usageUnit.description")
                .field("baseUnitConversionFactor", "baseUnitConversionFactor")
                .field("displayQuantity", "displayQuantity")
                .register();
    }

    /**
     * Converts raw GCP pricing record to domain list of product pricing offer.
     * Mapping.
     *
     * @param gcpRawServicePricing raw data from cache or from loader
     * @return never null
     */
    public List<GCPProductPricingRecord> fromRaw(GCPRawServicePricing gcpRawServicePricing) {
        MapperFacade conv = mapperFactory.getMapperFacade();
        List<GCPProductPricingRecord> products = new ArrayList<>();

        for (Sku gcpServiceSku : gcpRawServicePricing.getGcpSkusWithPrices()) {
            GCPProductPricingRecord product = conv.map(gcpRawServicePricing, GCPProductPricingRecord.class);
            conv.map(gcpServiceSku, product);
            product.setId(gcpProductIdFactory.createFromSku(gcpRawServicePricing.getCloudEnvironmentPricingId(), gcpServiceSku));
            product.updateGlobalAvailFlag();

            for (PricingInfo pricingInfo : gcpServiceSku.getPricingInfo()) {
                GCPProductPriceOfferRecord offer = conv.map(pricingInfo, GCPProductPriceOfferRecord.class);

                PricingExpression pricingExpression = pricingInfo.getPricingExpression();
                for (TierRate tieredRate : pricingExpression.getTieredRates()) {
                    GCPPriceOfferRate offerRate = conv.map(tieredRate, GCPPriceOfferRate.class);
                    conv.map(pricingExpression, offerRate);
                    offer.addOfferRate(offerRate);
                }

                if (shouldAlsoUseDescriptionUnitsAsAbbr) {
                    if (offer.getRates() != null) {
                        List<GCPPriceOfferRate> newRates = offer.getRates().stream()
                                .map(this::makeCopyRateWithChangedUsageUnit)
                                .collect(Collectors.toList());
                        newRates.forEach(offer::addOfferRate);
                    }
                }

                offer.sortAndAssignProperRateRanges();
                product.addOffer(offer);
            }

            products.add(product);
        }

        return products;
    }

    private GCPPriceOfferRate makeCopyRateWithChangedUsageUnit(GCPPriceOfferRate rate) {
        GCPPriceOfferRate newRate = rate.shallowClone();
        GCPPriceOfferRate.GCPUnit u = rate.getUsageUnit();
        newRate.setUsageUnit(new GCPPriceOfferRate.GCPUnit(u.getDescription(), u.getDescription()));
        return newRate;
    }


    /**
     * Converts Google's {@link Money} (currency and unit amount + nanos of unit) to {@link BigDecimal}s with currency code (as map).
     */
    public static class MoneyToBigDecimalConverter extends CustomConverter<Money, Map> {
        private static final MathContext MATH_CTX = new MathContext(9);
        public static final BigDecimal DIVISOR = new BigDecimal("0.000000001", MATH_CTX);

        @Override
        public Map<String, BigDecimal> convert(Money money, Type<? extends Map> destinationType, MappingContext mappingContext) {
            Map<String, BigDecimal> pricePerCurr = new HashMap<>(1, 1f);

            pricePerCurr.put(money.getCurrencyCode(), new BigDecimal(money.getUnits(), MATH_CTX).add(new BigDecimal(money.getNanos(), MATH_CTX).multiply(DIVISOR)));
            return pricePerCurr;
        }
    }

}
