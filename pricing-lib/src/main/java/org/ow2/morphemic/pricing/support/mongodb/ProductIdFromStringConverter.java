package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

/**
 * Helps to read {@link ProductId} saved as text.
 */
@ReadingConverter
class ProductIdFromStringConverter implements Converter<String, ProductId> {

    @Override
    public ProductId convert(String source) {
        return new ProductId(source);
    }
}
