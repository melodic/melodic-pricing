package org.ow2.morphemic.pricing.cloud_providers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.PriceListType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

/**
 * Simple, generic record for common product metadata.
 * Currently product and its pricing data is in different format for each of CloudProviders.
 * This class allow to match product id to specific collection of data.
 */
@Document("generic_products")
@TypeAlias("TYPE_GenericProductRecord")
@Data
@AllArgsConstructor
@NoArgsConstructor
class GenericProductRecord {

    /**
     * We assume that product ID is unique across all {@link CloudProvider}s and {@link org.ow2.morphemic.pricing.model.CloudEnvironment}s.
     */
    @Id
    @Field(targetType = FieldType.STRING)
    private ProductId id;

    private CloudProvider cloudProvider;

    private PriceListType priceListType;

    GenericProductRecord(ProductId id, CloudProvider cloudProvider) {
        this.id = id;
        this.cloudProvider = cloudProvider;
    }
}
