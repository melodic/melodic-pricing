package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Provides access to Azure products from public pricing.
 * Enables to get ids required to connect Azure offers and pricing.
 * Must not be used outside pricing lib.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AzureRawProductPricingManager {

    private final AzureRawPricingDataRepository azureRawPricingDataRepository;

    private final AzurePublicPricingRawDataLoader azurePublicPricingRawDataLoader;

    private final DataVersionsService dataVersionsService;

    private final ObjectMapper objectMapper = new ObjectMapper();


    public Set<AzureProductPricingItem> findProductPricingItems(CloudEnvironmentPricingId publicAzurePricingId, String name, String location) {
        Collection<AzureRawPricingDataRecord> pricingDataRecords = azureRawPricingDataRepository.findByCloudEnvironmentPricingIdAndRawContentArmSkuNameAndRawContentArmRegionName(publicAzurePricingId, name, location);
        return pricingDataRecords.stream()
                .filter(azureRecord -> !azureRecord.getRawContent().getSkuName().endsWith("Spot"))
                .map(this::mapToAzureProductPricingItem)
                .collect(Collectors.toSet());
    }

    private AzureProductPricingItem mapToAzureProductPricingItem(AzureRawPricingDataRecord azureRawPricingDataRecord) {
        return objectMapper.convertValue(azureRawPricingDataRecord.getRawContent(), AzureProductPricingItem.class);
    }

    /**
     * Stream of records describing raw pricing data.
     *
     * @param versionedCloudEnvPricingId for this versioned cloud env. pricing
     * @return stream of non-null elements.
     */
    public Stream<AzureRawPricingItem> rawPricingDataAsStreamForVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();
        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();

        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(azureRawPricingDataRepository
                        .findByCloudEnvironmentPricingIdAndVersion(cloudEnvironmentPricingId,
                                versionNum).iterator(), 0), false)
                .map(AzureRawPricingDataRecord::getRawContent)
                .filter(Objects::nonNull);
    }


    /**
     * Fetcjes all entries from given version with meterId (the same product).
     *
     * @param versionedCloudEnvPricingId versioned pricing id
     * @param azureMeterId               meterId (azure field)
     * @return found elements
     */
    public Collection<AzureRawPricingDataRecord> rawPricingDataForMeterIdAndVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId, String azureMeterId) {
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();
        CloudEnvironmentPricingId pricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();

        return azureRawPricingDataRepository.findByCloudEnvironmentPricingIdAndVersionAndRawContentMeterId(pricingId, versionNum, azureMeterId);
    }

    /**
     * Use only in tests.
     */
    @Transactional
    public void deleteAll() {
        azureRawPricingDataRepository.deleteAll();
        log.info("Deleted all AZURE raw pricing data.");
    }

    @Transactional
    public void deletePricing(CloudEnvironmentPricingId pricingId) {
        long removed = azureRawPricingDataRepository.deleteAllByCloudEnvironmentPricingId(pricingId);
        log.info("Removed AZURE RAW pricing data for pricing {}. Removed records count {}.", pricingId.getId(), removed);
    }

    /**
     * Provides info about last version of raw data for pricing.
     *
     * @param cloudEnvironmentPricingId pricing id
     * @return maybe data
     */
    public Optional<DataVersionsService.VersionMetadata> lastVersionOfRawData(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        return dataVersionsService.lastVersionMetadata(cloudEnvironmentPricingId, AzureRawPricingDataRecord.VERSION_COLLECTION_NAME);
    }
}
