package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderPricingStatsService;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.CountOfProductsInPricing;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSRawPricingDataManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Statistics for AWS pricing data.
 */
@Slf4j
@Service
@RequiredArgsConstructor
class AWSCloudProviderPricingStatsService implements CloudProviderPricingStatsService {

    private final MongoTemplate mongoTemplate;

    private final AWSProductRecordsRepository productRecordsRepository;

    private final AWSRawPricingDataManager awsRawPricingDataManager;

    @Override
    @Transactional
    public List<PricingStats> calculatePricingStats() {

        Aggregation aggregation = Aggregation.newAggregation(Aggregation.project("cloudEnvironmentPricingId"),
                Aggregation.group("cloudEnvironmentPricingId").count().as("productsCount"));
        AggregationResults<CountOfProductsInPricing> result = mongoTemplate.aggregate(aggregation,
                AWSProductRecord.MONGO_COLLECTION_NAME, CountOfProductsInPricing.class);
        List<PricingStats> stats = result.getMappedResults().stream()
                .map(res -> {
                    CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(res.getId());
                    Optional<DataVersionsService.VersionMetadata> maybeVersionMetadata = awsRawPricingDataManager.lastVersionOfRawData(cloudEnvironmentPricingId);

                    Date lastVersionDate = maybeVersionMetadata.map(DataVersionsService.VersionMetadata::getCreated).orElse(null);
                    VersionNum lastVersionNum = maybeVersionMetadata.map(DataVersionsService.VersionMetadata::getVersionNum).orElse(null);

                    return new PricingStats(cloudEnvironmentPricingId, CloudProvider.AWS, res.getProductsCount(), lastVersionDate, lastVersionNum, new HashMap<>());
                })
                .collect(Collectors.toList());
        log.trace("Stats: {}", stats);
        return stats;
    }

    @Override
    @Transactional
    public CloudProviderStats calculateCloudProviderStats() {
        Aggregation aggregation = Aggregation.newAggregation(Aggregation.project("cloudEnvironmentPricingId"),
                Aggregation.group("cloudEnvironmentPricingId"));
        AggregationResults<CountOfProductsInPricing> result = mongoTemplate.aggregate(aggregation, AWSProductRecord.MONGO_COLLECTION_NAME, CountOfProductsInPricing.class);
        Set<CloudEnvironmentPricingId> pricingIds = result.getMappedResults().stream()
                .map(res -> new CloudEnvironmentPricingId(res.getId()))
                .collect(Collectors.toSet());

        return new CloudProviderStats(CloudProvider.AWS, pricingIds, productRecordsRepository.count());
    }

    @Override
    public CloudProvider getCloudProvider() {
        return CloudProvider.AWS;
    }
}
