package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * It loads public pricing for Azure. This Api is publicly available, so any suppliers with credentials are not required.
 */
@SuppressFBWarnings("HTTP_PARAMETER_POLLUTION")
@Service
@Slf4j
public class AzurePublicPricingRawDataLoader extends AbstractAzurePricingRawDataLoader {

    public static final CloudEnvironmentPricingId PUBLIC_AZURE_PRICING_ID = new CloudEnvironmentPricingId("azure_public_pricing");
    private static final String PUBLIC_AZURE_PRICING_URL = "https://prices.azure.com/api/retail/prices?$filter=serviceName%20eq%20%27Virtual%20Machines%27%20and%20priceType%20eq%20%27Consumption%27%20or%20priceType%20eq%20%27Reservation%27";

    @Value("${pricing.datasources.azure.publicPricingUrl:}")
    private String publicAzurePricingUrl;

    public AzurePublicPricingRawDataLoader(AzureRawPricingDataRepository azureRawPricingDataRepository, DataVersionsService dataVersionsService) {
        super(azureRawPricingDataRepository, dataVersionsService);
    }

    /**
     * Returns id of Azure public pricing
     */
    public CloudEnvironmentPricingId getPublicAzurePricingId() {
        return PUBLIC_AZURE_PRICING_ID;
    }

    /**
     * It loads raw data from public Azure pricing to cache for particular cloudEnvironmentPricingId
     *
     * @param cloudEnvironmentPricingId
     * @param pricingDataImportStatisticCollector - collector of data import statistics
     * @return
     */
    @Transactional
    public Optional<VersionedCloudEnvPricingId> loadRawDataToCache(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                                   PricingDataImportStatisticCollector pricingDataImportStatisticCollector) {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            MutableInt counter = new MutableInt();
            String url = StringUtils.isNotBlank(publicAzurePricingUrl) ? publicAzurePricingUrl : PUBLIC_AZURE_PRICING_URL;
            log.info("Url used to load azure pricing: {}", url);

            AzureRawPricingResponse azureRawPricingPage = loadOnePricingPage(url, httpClient, pricingDataImportStatisticCollector);
            VersionNum newVersionNum = new VersionNum();
            counter.add(saveItemsFromOnePage(azureRawPricingPage, newVersionNum, cloudEnvironmentPricingId));
            String nextPageLink = azureRawPricingPage.getNextPageLink();

            while (nextPageLink != null) {
                azureRawPricingPage = loadOnePricingPage(nextPageLink, httpClient, pricingDataImportStatisticCollector);
                pricingDataImportStatisticCollector
                        .resumeMeasureTimeOfSaveDataIntoDatabase(); //resume measure time of db save
                counter.add(saveItemsFromOnePage(azureRawPricingPage, newVersionNum, cloudEnvironmentPricingId));
                pricingDataImportStatisticCollector.suspendMeasureTimeOfSaveDataIntoDatabase(); //suspend measure time of db save to collect data for all pages
                nextPageLink = azureRawPricingPage.getNextPageLink();
                int i = counter.getValue();
                if (i % 1000 == 0) {
                    log.debug("Fetched {} pricing items from public Azure pricing for: {}", i, cloudEnvironmentPricingId.getId());
                }
            }
            pricingDataImportStatisticCollector
                    .incrementTotalNumberOfSavedRowsIntoDatabase(counter.getValue());

            if (counter.getValue() > 0) {
                log.info("{} raw records of Azure pricing loaded for {}", counter.getValue(), cloudEnvironmentPricingId.getId());
                return registerNewVersionAndClean(newVersionNum, cloudEnvironmentPricingId, pricingDataImportStatisticCollector);
            }
        } catch (IOException e) {
            log.error("Error by getting public Azure pricing for: {}", cloudEnvironmentPricingId.getId(), e);
        }
        return Optional.empty();
    }

    private AzureRawPricingResponse loadOnePricingPage(String url, CloseableHttpClient httpClient, PricingDataImportStatisticCollector pricingDataImportStatisticCollector) throws IOException {
        pricingDataImportStatisticCollector.resumeMeasureTimeOfDataReadFromProvider();
        HttpGet request = new HttpGet(url);
        HttpResponse response = httpClient.execute(request);
        InputStream content = response.getEntity().getContent();
        ObjectMapper mapper = new ObjectMapper();
        final AzureRawPricingResponse azureRawPricingResponse = mapper.readValue(content, AzureRawPricingResponse.class);
        pricingDataImportStatisticCollector
                .suspendMeasureTimeOfDataReadFromProvider();
        return azureRawPricingResponse;
    }
}
