package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Repository used to store and query raw AWS pricing data.
 */
@Repository
interface AWSRawPricingDataRepository extends MongoRepository<AWSRawPricingDataRecord, Long> {

    /**
     * Removes all records with version number less than given one (current).
     *
     * @param cloudEnvironmentPricingId cloud environment id
     * @param currentVersion            version of cloud environment pricing id
     * @return removed records count.
     */
    long deleteAllByCloudEnvironmentPricingIdAndVersionLessThan(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum currentVersion);

    /**
     * Removes all records associated with given pricing.
     *
     * @param cloudEnvironmentPricingId pricing id
     * @return removed records count.
     */
    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId);

    /**
     * Provides raw contents of pricing data for given version in given cloud environment.
     *
     * @param cloudEnvironmentPricingId cloud environment pricing id
     * @param versionNum                version of cloud environment pricing id
     * @return collection of json raw data
     */
    Collection<AWSRawPricingContentsOnlyProjection> findByCloudEnvironmentPricingIdAndVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum);

    /**
     * Provides raw contents of pricing data for given version in given cloud environment.
     *
     * @param cloudEnvironmentPricingId cloud environment pricing id
     * @param versionNum                version of cloud environment pricing id
     * @return collection of json raw data
     */
    List<AWSRawPricingContentsOnlyProjection> findByCloudEnvironmentPricingIdAndVersionAndSortPosNumGreaterThanOrderBySortPosNum(
            CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum, long lastSortPosNum, Pageable page);


    /**
     * Checks how many RAW records have NULL value of {@link AWSRawPricingDataRecord#getSortPosNum()} field.
     * @param cloudEnvironmentPricingId in this AWS pricing
     * @param versionNum in this version.
     * @return
     */
    long countAllByCloudEnvironmentPricingIdAndVersionAndSortPosNumIsNull(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum);
}
