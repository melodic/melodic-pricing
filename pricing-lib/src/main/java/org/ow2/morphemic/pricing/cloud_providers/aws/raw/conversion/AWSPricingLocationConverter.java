package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import com.amazonaws.regions.Regions;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * Location converter: descriptive human readable name to location id.
 */
public class AWSPricingLocationConverter implements Function<String, String> {

    private final Map<String, String> regionDescrToRegionName;

    public AWSPricingLocationConverter() {
        regionDescrToRegionName = new HashMap<>();

        for (Regions region : Regions.values()) {
            regionDescrToRegionName.put(region.getDescription(), region.getName());
            regionDescrToRegionName.put(region.getDescription().toLowerCase(Locale.US), region.getName());
        }
    }

    @Nullable
    @Override
    public String apply(@Nullable String awsPricingLocationName) {
        if (awsPricingLocationName == null) {
            return null;
        }

        return Optional.ofNullable(regionDescrToRegionName.get(awsPricingLocationName))
                .orElseGet(() ->
                        Optional.ofNullable(regionDescrToRegionName
                                .get(awsPricingLocationName.trim().toLowerCase(Locale.US))).orElse(awsPricingLocationName)
                );
    }
}
