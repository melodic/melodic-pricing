package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AWSIntermediateProduct {
    private String sku;
    private String productFamily;
    private Map<String, String> attributes;
}
