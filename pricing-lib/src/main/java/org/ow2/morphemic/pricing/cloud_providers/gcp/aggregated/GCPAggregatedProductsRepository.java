package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

/**
 * Stores aggregated products data.
 */
@Repository
interface GCPAggregatedProductsRepository extends MongoRepository<GCPAggregatedProduct, ProductId> {

    /**
     * Universal query with projection.
     *
     * @param cloudEnvironmentPricingId cloud env pricing id
     * @param projectionType            interface which describes projection (mapping fields from source to result)
     * @param <P>                       projection interface
     * @return stream of values
     */
    @Query
    <P> Stream<P> findAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                     Class<P> projectionType);

    /**
     * @param projectionType interface which describes projection (mapping fields from source to result)
     * @param <P>            projection  interface
     * @return all product ids
     */
    @Query
    <P> Stream<P> findAllBy(Class<P> projectionType);

    /**
     * @param projectionType interface which describes projection (mapping fields from source to result)
     * @param <P>            projection  interface
     * @return all product ids
     */
    @Query
    <P> Stream<P> findDistinctBy(Class<P> projectionType);

    /**
     * Deletes all records associated with given pricing.
     *
     * @param pricingId pricing id
     * @return removed records count
     */
    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId pricingId);

    /**
     * It finds all GCPAggregatedProduct for specific @param cloudEnvironmentPricingId, @param productFamily and @param region
     */
    @Query(value = "{'cloudEnvironmentPricingId' : ?0, 'productFamily' : ?1, 'regions': ?2}")
    Stream<GCPAggregatedProduct> findAllByCloudEnvPricingIdAndProductFamilyAndRegion(CloudEnvironmentPricingId cloudEnvironmentPricingId, String productFamily, Place region);
}
