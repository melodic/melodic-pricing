package org.ow2.morphemic.pricing.calculators.azure;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingRecord;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.OfferType;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Calculates prices for AZURE.
 *
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AzurePriceCalculator {

    private final AzureProductPricingService productPricingService;

    /**
     * Tries to calculate azure price for product.
     *
     * @param productId     for this product
     * @param amount        we want to buy this 'amount' of product
     * @param offerTermType calculation params: Offer term type
     * @param offerType     calculation params: offer type for example: Consumption, Reservation, DevTestConsumption
     * @return only price without metadata, if can be calculated
     */
    public Optional<Price> calculatePrice(ProductId productId, Amount amount, OfferTermType offerTermType, OfferType offerType) {
        Optional<List<AzurePriceCalculator.CalculatedPrice>> maybeCalculatedPrices;

        if (amount.getUnit().equals(new Unit(""))
                && offerTermType.getName().toLowerCase(Locale.ROOT).contains("year")) {
            // reservations
            maybeCalculatedPrices = calculatePrices(productId, amount, offerTermType, offerType);
        } else {
            OfferTermType onDemandAzureOffer = new OfferTermType(""); // reservationTerm is null - this is "on demand" Azure offer decoded as empty string/
            Unit unit = new Unit("1 Hour"); // unit must be the same as in raw pricing files
            maybeCalculatedPrices = calculatePrices(productId, new Amount(amount.getQuantity(), unit), onDemandAzureOffer, offerType);
        }

        if (maybeCalculatedPrices.isEmpty()) {
            log.warn("It has not been possible to calculate price for AZURE product {}.", productId);
            return Optional.empty();
        } else {
            List<AzurePriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
            if (calculatedPrices.size() > 1) {
                log.warn("Returning only the first price from {} calculated prices for {}.", calculatedPrices.size(), productId);
            }
            return calculatedPrices.isEmpty() ? Optional.empty() : Optional.of(calculatedPrices.get(0).getPrice());
        }
    }

    /**
    * Tries to calculate all prices of given amount of "product" ,offer term type and offer type.
    *
    * @param productId     product
    * @param amount        amount of that product
    * @param offerTermType calculation params: offer
    * @param offerType calculation params: offer type for example: Consumption, Reservation, DevTestConsumption
    * @return empty when product cannot be found. List of calculated prices can be empty when there is no matching position in pricing tables for given amount and offer type.
    */
    public Optional<List<CalculatedPrice>> calculatePrices(ProductId productId, Amount amount, OfferTermType offerTermType, OfferType offerType) {
        Optional<AzureProductPricingRecord> foundProductPricing = productPricingService.findProductPricing(productId);
        if (foundProductPricing.isPresent()) {
            AzureProductPricingRecord productPricing = foundProductPricing.get();
            // FIXME it cannot properly calculate "reserved" prices.
            List<CalculatedPrice> calculatedPrices = tryCalculatePriceWithOfferType(amount, offerTermType, offerType, productPricing);
            if (calculatedPrices.isEmpty()) {
                calculatedPrices = tryCalculatePriceAndOrderValueFromHigh(amount, offerTermType, productPricing);
            }
            return Optional.of(calculatedPrices);
        } else {
            return Optional.empty();
        }
    }

    private List<CalculatedPrice> tryCalculatePriceAndOrderValueFromHigh(Amount amount, OfferTermType offerTermType, AzureProductPricingRecord productPricing) {
        Comparator<CalculatedPrice> priceComparator = Comparator.comparing(p -> p.getPrice().getValue());

        return productPricing.getPriceOffers().stream()
                .filter(priceOffer -> offerTermType.equals(priceOffer.getOfferTermType()))
                .flatMap(priceOffer -> priceOffer.getUnitPriceByCurrency().entrySet().stream()
                        .map(e -> new Price(e.getValue().multiply(BigDecimal.valueOf(amount.getQuantity())), e.getKey()))
                        .map(price -> new CalculatedPrice(price, priceOffer.getRegionCode())))
                        .sorted(priceComparator.reversed())
                .collect(Collectors.toList());
    }

    private List<CalculatedPrice> tryCalculatePriceWithOfferType(Amount amount, OfferTermType offerTermType, OfferType offerType, AzureProductPricingRecord productPricing) {
        return productPricing.getPriceOffers().stream()
                .filter(priceOffer -> offerTermType.equals(priceOffer.getOfferTermType())
                        && Objects.equals(priceOffer.getOfferType(), offerType))
                .flatMap(priceOffer -> priceOffer.getUnitPriceByCurrency().entrySet().stream()
                        .map(e -> new Price(e.getValue().multiply(BigDecimal.valueOf(amount.getQuantity())), e.getKey()))
                        .map(price -> new CalculatedPrice(price, priceOffer.getRegionCode())))
                .collect(Collectors.toList());
    }

    /**
     * Result: calculated price. May be extended.
     */
    @Data
    public static class CalculatedPrice {
        private final Price price;
        private final String regionCode;
    }
}
