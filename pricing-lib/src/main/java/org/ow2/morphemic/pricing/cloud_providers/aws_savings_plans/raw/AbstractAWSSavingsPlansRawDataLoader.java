package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlans;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansProduct;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansRatesPage;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import reactor.core.publisher.Flux;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Base class for creating RAW pricing data loaders for Aws savings plans.
 */
@RequiredArgsConstructor
@Slf4j
public abstract class AbstractAWSSavingsPlansRawDataLoader {
    private final AWSRawSavingsPlansPricingDataRepository rawSavingsPlansRepository;
    private final DataVersionsService dataVersionsService;

    @Getter
    @Setter
    private boolean removeOldVersions = true;

    /**
     * records to save at once.
     */
    @Getter
    @Setter
    private int bufferSize = 100;

    /**
     * Method responsible register new version in data versions service and remove old.
     */
    protected Optional<VersionedCloudEnvPricingId> registerNewVersionAndUpdateLast(VersionNum newVersionNum, CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        dataVersionsService.setEndOfValidityForLastVersion(cloudEnvironmentPricingId, AWSSavingsPlansProductPricingService.VERSIONED_AWS_SAVINGS_PLANS_PRICING_COLLECTION_NAME);
        VersionedCloudEnvPricingId versionedEnvId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, newVersionNum);
        dataVersionsService.registerNewVersion(versionedEnvId, AWSSavingsPlansProductPricingService.VERSIONED_AWS_SAVINGS_PLANS_PRICING_COLLECTION_NAME);

        return Optional.of(versionedEnvId);
    }

    protected boolean checkIfThisVersionNonExist(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum) {
        return dataVersionsService.checkIfThisVersionNonExist(cloudEnvironmentPricingId, versionNum, AWSSavingsPlansProductPricingService.VERSIONED_AWS_SAVINGS_PLANS_PRICING_COLLECTION_NAME);
    }

    /**
     * Saves raw data in Mongo for given version of pricing.
     *
     * @param rawSavingsPlansPage        fetched from web (or from other place)
     * @param newVersionNum              version
     * @param clEnvPricingId             pricing id
     * @param sortPositionNumberProvider
     * @return number of items.
     */
    protected int saveItemsFromOnePage(AWSSavingsPlansRatesPage rawSavingsPlansPage, VersionNum newVersionNum, CloudEnvironmentPricingId clEnvPricingId,
                                       String regionCode,
                                       AtomicLong sortPositionNumberProvider) {
        AtomicInteger counter = new AtomicInteger();

        saveRawSavingsPlans(rawSavingsPlansPage, newVersionNum, clEnvPricingId, counter, regionCode, sortPositionNumberProvider);

        saveRawSavingsPlansRates(rawSavingsPlansPage, newVersionNum, clEnvPricingId, counter, regionCode, sortPositionNumberProvider);

        log.debug("Total savings plans raw records from 1 page for pricing {}: {}", clEnvPricingId, counter.get());

        return counter.get();
    }

    private void saveRawSavingsPlansRates(AWSSavingsPlansRatesPage rawSavingsPlansPage, VersionNum newVersionNum, CloudEnvironmentPricingId clEnvPricingId,
                                          AtomicInteger counter, String regionCode, AtomicLong sortPositionNumberProvider) {
        Flux.fromIterable(rawSavingsPlansPage.getProducts())
                .map(rawProduct -> newAWSSavingsPlansRawRatesRecord(rawProduct, newVersionNum, rawSavingsPlansPage.getVersion(), clEnvPricingId,
                        regionCode, sortPositionNumberProvider))
                .buffer(bufferSize)
                .doOnNext(records -> {
                    counter.addAndGet(records.size());
                    log.debug("Fetched and saved {} savings plans records for {}.", counter.get(), clEnvPricingId);
                    rawSavingsPlansRepository.saveAll(records);
                }).count().block();
    }

    private void saveRawSavingsPlans(AWSSavingsPlansRatesPage rawSavingsPlansPage, VersionNum newVersionNum, CloudEnvironmentPricingId clEnvPricingId,
                                     AtomicInteger counter, String regionCode, AtomicLong sortPositionNumberProvider) {
        Flux.fromIterable(rawSavingsPlansPage.getTerms().getSavingsPlan())
                .map(rawRates -> newAWSSavingsPlansRawRatesRecord(rawRates, newVersionNum, rawSavingsPlansPage.getVersion(), clEnvPricingId,
                        regionCode, sortPositionNumberProvider))
                .buffer(bufferSize)
                .doOnNext(records -> {
                    counter.addAndGet(records.size());
                    log.debug("Fetched and saved {} savings plans records for {}.", counter.get(), clEnvPricingId);
                    rawSavingsPlansRepository.saveAll(records);
                }).count().block();
    }

    private AWSRawSavingsPlansPricingDataRecord newAWSSavingsPlansRawRatesRecord(AWSSavingsPlans rawRates, VersionNum newVersionNum,
                                                                                 String awsVersionNum,
                                                                                 CloudEnvironmentPricingId clEnvPricingId,
                                                                                 String regionCode,
                                                                                 AtomicLong sortPositionNumberProvider) {
        return new AWSRawSavingsPlansPricingDataRecord(rawRates, newVersionNum, awsVersionNum, clEnvPricingId,
                regionCode, sortPositionNumberProvider.incrementAndGet());
    }

    private AWSRawSavingsPlansPricingDataRecord newAWSSavingsPlansRawRatesRecord(AWSSavingsPlansProduct rawProduct, VersionNum newVersionNum,
                                                                                 String awsVersionNum,
                                                                                 CloudEnvironmentPricingId clEnvPricingId,
                                                                                 String regionCode,
                                                                                 AtomicLong sortPositionNumberProvider) {
        return new AWSRawSavingsPlansPricingDataRecord(rawProduct, newVersionNum, awsVersionNum, clEnvPricingId,
                regionCode, sortPositionNumberProvider.incrementAndGet());
    }
}
