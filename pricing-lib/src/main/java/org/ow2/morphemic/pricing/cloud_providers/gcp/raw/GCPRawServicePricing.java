package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.services.cloudbilling.model.Sku;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Transient;

import java.util.List;

/**
 * Value which represents GCP service (which is NOT a product!) and its pricing data as GCP SKU values.
 * SKU is interpreted as a product: something which can be sold and has price.
 */
@RequiredArgsConstructor
@Data
public final class GCPRawServicePricing {
    private final com.google.api.services.cloudbilling.model.Service gcpService;
    private final List<Sku> gcpSkusWithPrices;
    private final VersionedCloudEnvPricingId versionedCloudEnvPricingId;

    @Transient
    @java.beans.Transient
    public CloudEnvironmentPricingId getCloudEnvironmentPricingId() {
        return versionedCloudEnvPricingId.getCloudEnvironmentPricingId();
    }

    @Transient
    @java.beans.Transient
    public VersionNum getVersionNum() {
        return versionedCloudEnvPricingId.getVersionNum();
    }
}
