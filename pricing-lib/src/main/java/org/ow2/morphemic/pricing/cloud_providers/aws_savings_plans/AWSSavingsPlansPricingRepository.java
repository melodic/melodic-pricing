package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Responsible for preserving Aws savings plans pricing data. This is pricing prepared for pricing lib (transformed RAW pricing to internal structure).
 */
@Repository
interface AWSSavingsPlansPricingRepository extends MongoRepository<AWSSavingsPlansPricingRecord, ProductId> {

    /**
     * Find by productId and Version.
     *
     * @param productId product id
     * @param version   version of cloud env pricing id
     * @return List of records
     */
    Optional<AWSSavingsPlansPricingRecord> findByIdAndVersion(ProductId productId, VersionNum version);


    /**
     * Loads all records for version.
     *
     * @param version version of cloud env pricing id
     * @return List of records
     */
    List<AWSSavingsPlansPricingRecord> findAllByVersion(VersionNum version);

    /**
     * Loads all records for priding and version.
     *
     * @param pricingId pricing id
     * @param version   version of cloud env pricing id
     * @return List of records
     */
    List<AWSSavingsPlansPricingRecord> findAllByCloudEnvironmentPricingIdAndVersion(CloudEnvironmentPricingId pricingId, VersionNum version);


    void deleteByCloudEnvironmentPricingIdAndVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum version);
}
