package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSIntermediateDimensionRates extends HashMap<String, AWSItermediateBaseTerm> {
    private static final long serialVersionUID = -1L;

}
