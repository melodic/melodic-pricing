package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderPricingStatsService;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.CountOfProductsInPricing;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzureRawProductPricingManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Statistics for Azure pricing data.
 */
@Slf4j
@Service
@RequiredArgsConstructor
class AzureCloudProviderPricingStatsService implements CloudProviderPricingStatsService {

    private final MongoTemplate mongoTemplate;

    private final AzurePricingRecordsRepository azurePricingRecordsRepository;

    private final AzureRawProductPricingManager azureRawProductPricingManager;

    @Override
    @Transactional
    public List<PricingStats> calculatePricingStats() {

        Aggregation aggregation = Aggregation.newAggregation(Aggregation.project("cloudEnvironmentPricingId"),
                Aggregation.group("cloudEnvironmentPricingId").count().as("productsCount"));
        AggregationResults<CountOfProductsInPricing> result = mongoTemplate.aggregate(aggregation,
                AzureProductPricingRecord.MONGO_COLLECTION_NAME, CountOfProductsInPricing.class);
        List<PricingStats> stats = result.getMappedResults().stream()
                .map(res -> {
                    CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(res.getId());
                    Optional<DataVersionsService.VersionMetadata> maybeVersionMetadata = azureRawProductPricingManager.lastVersionOfRawData(cloudEnvironmentPricingId);

                    Date lastVersionDate = maybeVersionMetadata.map(DataVersionsService.VersionMetadata::getCreated).orElse(null);
                    VersionNum lastVersionNum = maybeVersionMetadata.map(DataVersionsService.VersionMetadata::getVersionNum).orElse(null);

                    return new PricingStats(cloudEnvironmentPricingId, getCloudProvider(), res.getProductsCount(), lastVersionDate, lastVersionNum, new HashMap<>());
                })
                .collect(Collectors.toList());
        log.trace("Stats: {}", stats);
        return stats;
    }

    @Override
    @Transactional
    public CloudProviderStats calculateCloudProviderStats() {
        Aggregation aggregation = Aggregation.newAggregation(Aggregation.project("cloudEnvironmentPricingId"),
                Aggregation.group("cloudEnvironmentPricingId"));
        AggregationResults<CountOfProductsInPricing> result = mongoTemplate.aggregate(aggregation, AzureProductPricingRecord.MONGO_COLLECTION_NAME, CountOfProductsInPricing.class);
        Set<CloudEnvironmentPricingId> pricingIds = result.getMappedResults().stream()
                .map(res -> new CloudEnvironmentPricingId(res.getId()))
                .collect(Collectors.toSet());

        return new CloudProviderStats(getCloudProvider(), pricingIds, azurePricingRecordsRepository.count());
    }

    @Override
    public CloudProvider getCloudProvider() {
        return CloudProvider.AZURE;
    }
}
