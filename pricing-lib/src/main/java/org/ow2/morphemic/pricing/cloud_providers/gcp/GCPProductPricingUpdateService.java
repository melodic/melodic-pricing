package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.mutable.MutableInt;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.config.GCPProductPricingServicesConfig;
import org.ow2.morphemic.pricing.cloud_providers.gcp.raw.GCPPricingRawDataLoader;
import org.ow2.morphemic.pricing.cloud_providers.gcp.raw.GCPPricingRawDataLoaderFactory;
import org.ow2.morphemic.pricing.cloud_providers.gcp.raw.GCPProductsAndOffersFactory;
import org.ow2.morphemic.pricing.cloud_providers.gcp.raw.GCPRawDataPricingManager;
import org.ow2.morphemic.pricing.cloud_providers.security.CredentialsSupplier;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.resource.CloudCredential;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Updates GCP pricing data. Each pricing loaded from files or from Google API
 * must be extended by additional data describing "aggregated products", which is currently loaded from json file.
 */
@Service("gcpProductPricingUpdateService")
@Slf4j
@RequiredArgsConstructor
public class GCPProductPricingUpdateService implements ProductPricingUpdateService {

    private final GCPProductPricingRecordsRepository productPricingRecordsRepository;

    @Qualifier("gcpPricingFromFiles")
    private final GCPPricingRawDataLoader gcpPricingFromFiles;

    private final GCPRawDataPricingManager rawDataPricingManager;

    private final GCPProductsAndOffersFactory productsAndOffersFactory;

    private final ProductsMetadataService productsMetadataService;

    private final CredentialsSupplier credentialsSupplier;

    private final GCPPricingRawDataLoaderFactory gcpPricingRawDataLoaderFactory;

    private final GCPAggregatedProductsService aggregatedProductsService;

    private final DataVersionsService dataVersionsService;

    private final GCPProductPricingServicesConfig productPricingServicesConfig;

    /**
     * Scans all dirs and imports found datasets.
     *
     * @return version when created
     * @throws IOException when IO error.
     */
    @Override
    @Transactional
    public synchronized Optional<VersionedCloudEnvPricingId> loadPricingDumpDataFromFiles() throws IOException {
        PricingDataImportStatisticCollector pricingDataImportStatisticCollector = new PricingDataImportStatisticCollector(
                CloudProvider.GCP, "pricing_dump", List.of()); //for collecting import statistics
        Optional<VersionedCloudEnvPricingId> maybeNewEnvVersion = gcpPricingFromFiles.loadRawDataToCache(pricingDataImportStatisticCollector);
        maybeNewEnvVersion.ifPresent(versionedCloudEnvPricingId -> {
            updateProductsAndPricings(versionedCloudEnvPricingId, pricingDataImportStatisticCollector);
            log.debug("Pricing dump load statistics: \n{}", pricingDataImportStatisticCollector.printStatistics());
        });
        return maybeNewEnvVersion;
    }


    /**
     * It uses GCP REST API to download and update current pricing data.
     * Credentials supplier must provide valid credentials for given cloud environment (identified by pricing).
     *
     * @param cloudEnvironmentPricingId pricing of cloud env.
     */
    @Override
    @Transactional
    public void updateUsingExternalApi(CloudEnvironmentPricingId cloudEnvironmentPricingId, PricingDataImportStatisticCollector pricingDataImportStatistic) {
        Optional<CloudCredential> maybeCredentials = credentialsSupplier.apiCredentialsOf(cloudEnvironmentPricingId);

        if (maybeCredentials.isPresent()) {
            log.info("GCP Pricing update: found credentials for pricing {}", cloudEnvironmentPricingId);
            CloudCredential cloudCredential = maybeCredentials.get();
            try {
                GCPPricingRawDataLoader loader = createPricingGCPApiLoader(cloudEnvironmentPricingId, cloudCredential, pricingDataImportStatistic);
                loader.loadRawDataToCache(pricingDataImportStatistic)
                        .ifPresent(versionedCloudEnvPricingId -> updateProductsAndPricings(versionedCloudEnvPricingId, pricingDataImportStatistic));
                log.info("Pricing updated for {}", cloudEnvironmentPricingId);
            } catch (IOException e) {
                log.error("Cannot update pricing for {}. Cause: {}", cloudEnvironmentPricingId, e.getMessage());
                log.error("Details: " + e.getMessage(), e);
            }

        } else {
            log.error("Cannot find credentials for {}", cloudEnvironmentPricingId);
        }

    }

    /**
     * Loads data from raw cche and updates given pricing. For internal use only. Do not use directly.
     *
     * @param versionedCloudEnvPricingId
     * @param pricingDataImportStatistic
     */
    @Transactional
    public void updateProductsAndPricings(VersionedCloudEnvPricingId versionedCloudEnvPricingId, PricingDataImportStatisticCollector pricingDataImportStatistic) {
        MutableInt counter = new MutableInt();
        pricingDataImportStatistic.startMeasureTimeOfTransformationOfSourceDataToBusinessModel();
        long saved = rawDataPricingManager.rawPricingDataAsStreamForVersion(versionedCloudEnvPricingId)
                .flatMap(gcpRawServiceGroupPricing -> productsAndOffersFactory.fromRaw(gcpRawServiceGroupPricing).stream())
                .peek(product -> {
                    productPricingRecordsRepository.save(product);
                    ProductId productId = product.getId();
                    if (!productsMetadataService.isProductMetadataInCache(productId)) {
                        productsMetadataService.createProductMetadata(productId, CloudProvider.GCP);
                    }

                    int i = counter.incrementAndGet();
                    if (i % 1000 == 0) {
                        log.debug("Loaded {} products for {}", i, versionedCloudEnvPricingId);
                    }

                }).count();

        pricingDataImportStatistic.incrementTotalNumberOfTransformationRowsOfSourceDataToBusinessModel(saved);
        log.debug("cloud_environment_pricing_id {} | version {} | Loaded {} products",
                versionedCloudEnvPricingId.getCloudEnvironmentPricingId(), versionedCloudEnvPricingId.getVersionNum(), saved);
        try {
            log.debug("Starting aggregation of products for cloud_environment_pricing_id {} | version {}",
                    versionedCloudEnvPricingId.getCloudEnvironmentPricingId(), versionedCloudEnvPricingId.getVersionNum());
            aggregatedProductsService.updateAggregatedProducts(versionedCloudEnvPricingId.getCloudEnvironmentPricingId());
            log.debug("Finished aggregation of products for cloud_environment_pricing_id {} | version {}",
                    versionedCloudEnvPricingId.getCloudEnvironmentPricingId(), versionedCloudEnvPricingId.getVersionNum());
        } catch (IOException e) {
            log.error("There was an error during aggregated GCP products update: " + e.getMessage(), e);
        }

        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, GCPProductPricingService.VERSIONED_GCP_PRICING_COLLECTION_NAME);

        pricingDataImportStatistic.stopMeasureTimeOfTransformationOfSourceDataToBusinessModel();
        log.info("Updated products: {} for version {}", saved, versionedCloudEnvPricingId);
    }

    private GCPPricingRawDataLoader createPricingGCPApiLoader(CloudEnvironmentPricingId cloudEnvironmentPricingId, CloudCredential cloudCredential,
                                                              PricingDataImportStatisticCollector pricingDataImportStatistic) {
        return gcpPricingRawDataLoaderFactory.createPricingGCPApiLoader(cloudCredential.id(), productPricingServicesConfig.getServicesIdsToFetch(),
                productPricingServicesConfig.getServicesFetchingDelayMs(), cloudEnvironmentPricingId, pricingDataImportStatistic);
    }
}
