package org.ow2.morphemic.pricing.support.mongodb.migrations;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingRecord;

/**
 * Changes for version 001. Executed by mongock.
 */
@ChangeLog(order = "001")
@Slf4j
public class MongoMigrationChangelog001 {

    /**
     * Old pricing data for GCP must be erased. Document format was changed.
     *
     * @param mongockTemplate
     */
    @ChangeSet(order = "001", id = "001.001.removeAllGCPPRicings", author = "pricing-lib", systemVersion = "0.7")
    public void removeAllGCPPRicings(MongockTemplate mongockTemplate) {
        mongockTemplate.remove(GCPProductPricingRecord.class);
        log.info("GCP pricing data removed.");
    }

}
