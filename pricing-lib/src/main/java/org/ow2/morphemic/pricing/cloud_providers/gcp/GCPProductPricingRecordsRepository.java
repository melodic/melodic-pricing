package org.ow2.morphemic.pricing.cloud_providers.gcp;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

/**
 * Responsible for preserving GCP pricing data for products. This is pricing prepared for pricing lib (transformed RAW pricing to internal structure).
 */
@Repository
interface GCPProductPricingRecordsRepository extends MongoRepository<GCPProductPricingRecord, ProductId> {

    /**
     * Universal query with projection.
     *
     * @param cloudEnvironmentPricingId cloud env pricing id
     * @param basedOnRawVersionNum      version of cloud env pricing id
     * @param projectionType            interface which describes projection (mapping fields from source to result)
     * @param <P>                       projection interface
     * @return stream of values
     */
    @Query
    <P> Stream<P> findAllByCloudEnvironmentPricingIdAndBasedOnRawVersionNum(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                                            VersionNum basedOnRawVersionNum,
                                                                            Class<P> projectionType);

    /**
     * Universal query with projection.
     *
     * @param cloudEnvironmentPricingId cloud env pricing id
     * @param projectionType            interface which describes projection (mapping fields from source to result)
     * @param <P>                       projection interface
     * @return stream of values
     */
    @Query
    <P> Stream<P> findAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                     Class<P> projectionType);

    /**
     * Universal query with projection.
     *
     * @param projectionType            interface which describes projection (mapping fields from source to result)
     * @param <P>                       projection interface
     * @return stream of values
     */
    @Query
    <P> Stream<P> findDistinctBy(Class<P> projectionType);

    /**
     * Universal query with projection.
     *
     * @param projectionType            interface which describes projection (mapping fields from source to result)
     * @param <P>                       projection interface
     * @return stream of values
     */
    @Query
    <P> Stream<P> findAllBy(Class<P> projectionType);


    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId);
}
