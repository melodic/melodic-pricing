package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Repository used to store and query raw Azure pricing data.
 */
@Repository
interface AzureRawPricingDataRepository extends MongoRepository<AzureRawPricingDataRecord, String> {

    /**
     * Remove all records with version number less than given one (current).
     *
     * @param cloudEnvironmentPricingId cloud environment id
     * @param currentVersion            version of cloud environment pricing id
     * @return removed count
     */
    long deleteAllByCloudEnvironmentPricingIdAndVersionLessThan(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum currentVersion);

    /**
     * Remove all records with assigned given pricing id
     *
     * @param cloudEnvironmentPricingId pricing id
     * @return removed count
     */
    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId);

    @Query
    Collection<AzureRawPricingDataRecord> findByCloudEnvironmentPricingIdAndRawContentArmSkuNameAndRawContentArmRegionName(
            CloudEnvironmentPricingId cloudEnvironmentPricingId, String armSkuName, String armRegionName);

    Collection<AzureRawPricingDataRecord> findByCloudEnvironmentPricingIdAndVersion(
            CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum version);

    /**
     * Fetcjes all entries from given version with meterId (the same product).
     *
     * @param cloudEnvironmentPricingId pricing id
     * @param version                   version
     * @param meterId                   meterId (azure field)
     * @return found elements
     */
    Collection<AzureRawPricingDataRecord> findByCloudEnvironmentPricingIdAndVersionAndRawContentMeterId(
            CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum version, String meterId);
}
