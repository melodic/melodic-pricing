package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.OfferType;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Parametrized price offer.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AzurePriceOffer {

    @Field(targetType = FieldType.STRING)
    @Indexed
    private OfferTermType offerTermType;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private OfferType offerType;

    private String description;

    private String regionCode;

    private Map<String, BigDecimal> unitPriceByCurrency;
    private Map<String, BigDecimal> retailPriceByCurrency;

    /**
     * Typically "1 Hour"
     */
    private String unitOfMeasure;
}
