package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

/**
 * Contains single record fetched from  public Azure pricing API. Contains raw data in json format.
 */
@Document(collection = "azure_raw_pricings")
@TypeAlias("AzureRawPricingDataRecord")
@NoArgsConstructor
@Data
@CompoundIndexes({@CompoundIndex(def = "{cloudEnvironmentId: 1, version: 1}"),
        @CompoundIndex(name = "idx_azure_pricing_version_meter",
            def = "{cloudEnvironmentPricingId: 1, version: 1, \"rawContent.meterId\": 1}")})
public class AzureRawPricingDataRecord {

    /**
     * it should differ from Document collection, because it must not be changed during refactorings
     */
    public static final CollectionName VERSION_COLLECTION_NAME = new CollectionName("azure:raw_pricings:" + CollectionName.TEMPLATE);

    /**
     * Managed by spring
     */
    @Id
    private String id;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum version;

    /**
     * data from Azure Api
     */
    private AzureRawPricingItem rawContent;

    public AzureRawPricingDataRecord(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum version, AzureRawPricingItem rawContent) {
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.version = version;
        this.rawContent = rawContent;
    }
}
