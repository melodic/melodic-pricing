package org.ow2.morphemic.pricing.calculators;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.calculators.aws.AWSPriceCalculator;
import org.ow2.morphemic.pricing.calculators.aws_savings_plans.AWSSavingsPlansCalculator;
import org.ow2.morphemic.pricing.calculators.azure.AzurePriceCalculator;
import org.ow2.morphemic.pricing.calculators.gcp.GCPPriceCalculator;
import org.ow2.morphemic.pricing.cloud_providers.GenericProductDto;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPStandardTermTypes;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.PriceListType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Simplified, universal price calculator.
 * <p>
 * TODO Currently it cannot properly calculate reserved (contract) prices.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class PriceCalculatorService {

    private final AWSPriceCalculator awsPriceCalculator;

    private final AWSSavingsPlansCalculator awsSavingsPlansCalculator;

    private final AWSProductPricingService awsProductPricingService;

    private final GCPPriceCalculator gcpPriceCalculator;

    private final GCPProductPricingService gcpProductPricingService;

    private final AzurePriceCalculator azurePriceCalculator;

    private final AzureProductPricingService azureProductPricingService;

    private final ProductsMetadataService productsMetadataService;

    private final GCPAggregatedProductsService gcpAggregatedProductsService;

    private final Supplier<List<PricingOption>> defaultEmptyList = Collections::emptyList;

    /**
     * Index.
     */
    private final Map<OfferTermType, GCPStandardTermTypes> gcpStandardTermTypesByOfferType = Collections.unmodifiableMap(Stream.of(
            GCPStandardTermTypes.values()).collect(Collectors.toMap(GCPStandardTermTypes::getOfferTermType,
            enumVal -> enumVal, (v1, v2) -> v2)));

    /**
     * Tries to calculate price for product.
     *
     * @param productId     for this product
     * @param amount        we want to buy this 'amount' of product
     * @param pricingOption which pricing option should be used to calculate price?
     * @return price, if can be calculated
     */
    public Optional<Price> calculatePrice(ProductId productId, Amount amount, PricingOption pricingOption) {
        log.trace("Trying to calculate price for product {}, amount {} and pricing option {}.", productId, amount, pricingOption);
        Optional<GenericProductDto> maybeGenericProductDto = productsMetadataService.findGenericProductDto(productId);

        if (maybeGenericProductDto.isEmpty() || maybeGenericProductDto.get().getCloudProvider() == null) {
            log.warn("Unknown cloud provider for product {}. It is not possible to calculate price!", productId);
            return Optional.empty();
        }

        GenericProductDto genericProductDto = maybeGenericProductDto.get();
        switch (genericProductDto.getCloudProvider()) {
            case AWS:
                return calculateAWSPriceByPriceList(productId, amount, pricingOption, genericProductDto.getPriceListType());

            case GCP:
                return calculateGCPPrice(productId, amount, pricingOption);

            case AZURE:
                return calculateAzurePrice(productId, amount, pricingOption);

            default:
                log.warn("Not implemented for cloud provider {} (product {})!", genericProductDto.getCloudProvider(), productId);
                return Optional.empty();
        }
    }

    private Optional<Price> calculateAWSPriceByPriceList(ProductId productId, Amount amount, PricingOption pricingOption, PriceListType priceListType) {
        if (priceListType == PriceListType.AWS_SAVINGS_PLANS) {
            return calculateAWSSavingsPlansPrice(productId, amount);
        }
        return calculateAWSPrice(productId, amount, pricingOption);
    }

    /**
     * Calculates AZURE prices.
     *
     * @param productId
     * @param amount
     * @param pricingOption
     * @return
     */
    private Optional<Price> calculateAzurePrice(ProductId productId, Amount amount, PricingOption pricingOption) {
        log.trace("Trying to calculate price for AZURE product {}, amount: {} and options: {}.", productId, amount, pricingOption);

        return azurePriceCalculator.calculatePrice(productId, amount, pricingOption.getOfferTermType(), pricingOption.getOfferType());
    }

    /**
     * FIXME it cannot calculate "reserved' prices
     *
     * @param productId
     * @param amount
     * @param pricingOption
     * @return
     */
    private Optional<Price> calculateAWSPrice(ProductId productId, Amount amount, PricingOption pricingOption) {
        log.trace("Trying to calculate price for AWS product {}, amount: {} and options: {}.", productId, amount, pricingOption);
        Optional<List<AWSPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = awsPriceCalculator.calculatePrice(productId, amount, pricingOption.getOfferTermType());
        if (maybeCalculatedPrices.isEmpty()) {
            log.warn("It has not been possible to calculate price for AWS product {}.", productId);
            return Optional.empty();
        } else {
            List<AWSPriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
            log.debug("Calculated {} price(s) for AWS product {}. Returning first one.", calculatedPrices.size(), productId);
            if (calculatedPrices.size() > 1) {
                log.warn("Returning only the first price from {} calculated prices for {}.", calculatedPrices.size(), productId);
            }
            return calculatedPrices.stream().filter(calcPrice -> pricingOption.equals(calcPrice.getPricingOption()))
                    .findFirst()
                    .map(AWSPriceCalculator.CalculatedPrice::getPrice);
        }
    }


    private Optional<Price> calculateAWSSavingsPlansPrice(ProductId productId, Amount amount) {
        log.trace("Trying to calculate price for AWS savings plans product {}, amount: {}.", productId, amount);
        Optional<List<Price>> maybeCalculatedPrices = awsSavingsPlansCalculator.calculatePrice(productId, amount);
        if (maybeCalculatedPrices.isEmpty()) {
            log.warn("It has not been possible to calculate price for AWS savings plans with product id {}.", productId);
            return Optional.empty();
        } else {
            List<Price> calculatedPrices = maybeCalculatedPrices.get();
            log.debug("Calculated {} price(s) for AWS savings plans with product id {}. Returning first one.", calculatedPrices.size(), productId);
            if (calculatedPrices.size() > 1) {
                log.warn("Returning only the first price from {} calculated prices for {}.", calculatedPrices.size(), productId);
            }
            return calculatedPrices.stream().findFirst();
        }
    }

    /**
     * FIXME it cannot calculate "reserved' prices
     *
     * @param productId
     * @param amount
     * @param pricingOption
     * @return
     */
    private Optional<Price> calculateGCPPrice(ProductId productId, Amount amount, PricingOption pricingOption) {
        log.trace("Trying to calculate price for GCP product {}, amount: {} and options: {}.", productId, amount, pricingOption);
        Optional<List<GCPPriceCalculator.CalculatedPrice>> maybeCalculatedPrices = gcpPriceCalculator.calculatePrice(productId, amount, pricingOption.getOfferTermType());
        if (maybeCalculatedPrices.isEmpty()) {
            log.warn("It has not been possible to calculate price for GCP product {}.", productId);
            return Optional.empty();
        } else {
            List<GCPPriceCalculator.CalculatedPrice> calculatedPrices = maybeCalculatedPrices.get();
            log.debug("Calculated {} price(s) for GCP product {}. Returning first one.", calculatedPrices.size(), productId);
            if (calculatedPrices.size() > 1) {
                log.warn("Returning only the first price from {} calculated prices for {}.", calculatedPrices.size(), productId);
            }
            return calculatedPrices.stream().findFirst().map(GCPPriceCalculator.CalculatedPrice::getPrice);
        }
    }

    /**
     * What are possible pricing options for that cloud provider and product?
     *
     * @param productId for that product
     * @return possible pricing options (may be empty, but never null).
     */
    public List<PricingOption> whatArePossiblePricingOptions(ProductId productId) {
        Optional<CloudProvider> maybeCloudProviderForProduct = productsMetadataService.findCloudProviderOf(productId);
        if (maybeCloudProviderForProduct.isEmpty()) {
            log.warn("Unknown cloud provider for product {}. Cannot return any pricing options for that product!", productId);
            return Collections.emptyList();
        }

        switch (maybeCloudProviderForProduct.get()) {
            case AWS:
                return extractAWSProductPricingOptions(productId);
            case GCP:
                return extractGCPProductPricingOptions(productId);
            case AZURE:
                return extractAzureProductPricingOptions(productId);
            default:
                log.error("Cloud provider {} not supported!", maybeCloudProviderForProduct.get());
        }

        return Collections.emptyList();
    }

    /**
     * TODO Move to AZURE pricing service.
     *
     * @param productId
     * @return
     */

    private List<PricingOption> extractAzureProductPricingOptions(ProductId productId) {
        Optional<List<PricingOption>> maybePricingOptions = azureProductPricingService.findProductPricing(productId)
                .map(azureProductPricingRecord ->
                        azureProductPricingRecord.getPriceOffers().stream().map(offer ->
                                new PricingOption(offer.getOfferTermType(), new Unit(offer.getUnitOfMeasure()))
                        ).distinct().collect(Collectors.toList()));

        if (maybePricingOptions.isPresent()) {
            List<PricingOption> pricingOptions = maybePricingOptions.get();
            log.trace("Pricing options for AZURE product {}: {}.", productId, pricingOptions);
            return pricingOptions;
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * TODO Move to GCP pricing service.
     *
     * @param productId
     * @return
     */
    private List<PricingOption> extractGCPProductPricingOptions(ProductId productId) {
        Optional<List<PricingOption>> maybePricingOptions = gcpProductPricingService.findProduct(productId)
                .map(gcpProductPricingRecord ->
                        gcpProductPricingRecord.getOffers().stream()
                                .map(offer ->
                                        offer.getRates().stream().map(offerRate ->
                                                        new PricingOption(gcpProductPricingRecord.getOfferTermType(),
                                                                offerRate.getUsageUnit().asUnit(),
                                                                gcpProductPricingRecord.getLeaseContractLength().orElse(null)))
                                                .collect(Collectors.toSet())
                                ).flatMap(Collection::stream).distinct().collect(Collectors.toList())
                );

        if (maybePricingOptions.isPresent()) {
            List<PricingOption> pricingOptions = maybePricingOptions.get();
            log.trace("Pricing options for basic product {}: {}.", productId, pricingOptions);
            return pricingOptions;
        }

        log.debug("Pricing options for aggregated product {}.", productId);
        return gcpAggregatedProductsService.findProduct(productId)
                .map(aggrProduct -> aggrProduct.getOfferTermTypes().stream()
                        .map(gcpStandardTermTypesByOfferType::get).filter(Objects::nonNull)
                        .map(offer -> new PricingOption(offer.getOfferTermType(), new Unit("h"), offer.getContractLength()))
                        .collect(Collectors.toList())).orElseGet(defaultEmptyList);

    }

    private List<PricingOption> extractAWSProductPricingOptions(ProductId productId) {
        Optional<List<PricingOption>> maybePricingOptions = awsProductPricingService.findProduct(productId)
                .map(productWithPricing ->
                        productWithPricing.getProductPricingRecord().getOffers().stream()
                                .map(offer ->
                                        offer.getRates().stream().map(offerRate ->
                                                        new PricingOption(offer.getOfferTermType(),
                                                                offerRate.getUnit(),
                                                                offer.getLeaseContractLength().orElse(null)))
                                                .collect(Collectors.toSet())

                                ).flatMap(Collection::stream).distinct().collect(Collectors.toList())

                );

        if (maybePricingOptions.isEmpty()) {
            log.warn("There are no pricing options for AWS product {}.", productId);
        }

        return maybePricingOptions.orElseGet(defaultEmptyList);
    }


}
