
package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.fileloader;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "resourceFamily",
        "resourceGroup",
        "usageType",
        "description",
        "skuId",
        "serviceRegions",
        "pricingInfo"
})
@Data
public class Offer {

    @JsonProperty("resourceFamily")
    private String resourceFamily;
    @JsonProperty("resourceGroup")
    private String resourceGroup;
    @JsonProperty("usageType")
    private String usageType;
    @JsonProperty("description")
    private String description;
    @JsonProperty("skuId")
    private String skuId;
    @JsonProperty("serviceRegions")
    private List<String> serviceRegions = new ArrayList<String>();

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

}
