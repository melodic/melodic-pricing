package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.List;

/**
 * Contains AWS product pricing details.
 */
@Document(collection = "aws_product_pricing")
@TypeAlias("AWSProductPricingRecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AWSProductPricingRecord {

    @Id
    @Field(targetType = FieldType.STRING)
    private ProductId id;

    /**
     * External product id (SKU) identifier from AWS.
     */
    @Indexed
    private String productSkuId;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum basedOnRawVersionNum;

    private List<AWSOffersTermsRecord> offers;

}
