package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.services.cloudbilling.model.Service;
import com.google.api.services.cloudbilling.model.Sku;
import lombok.Data;
import org.ow2.morphemic.pricing.cloud_providers.WithCloudEnvironmentPricingId;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.util.List;

/**
 * Allows to load GCP pricing data for some service as stream of raw jsons.
 */
public interface GCPServicePricingDataStreamSupplier extends WithCloudEnvironmentPricingId {

    /**
     * Creates stream with data records which contains definitions of GCP pricing.
     *
     * @return stream of raw json data describing service pricing.
     */
    Flux<ServiceWithSkusPrices> createDataStream() throws IOException;

    /**
     * GCP Service definition and its SKUs and prices
     */
    @Data
    class ServiceWithSkusPrices {
        private final Service serviceDef;
        private final List<Sku> skus;
    }
}
