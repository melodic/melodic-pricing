package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

/**
 * Stores aggregated products data.
 */
@Repository
interface GCPVmAggregatedProductsRepository extends MongoRepository<GCPVmAggregatedProduct, ProductId> {

    /**
     * Universal query with projection.
     *
     * @param cloudEnvironmentPricingId cloud env pricing id
     * @param projectionType            interface which describes projection (mapping fields from source to result)
     * @param <P>                       projection interface
     * @return stream of values
     */
    @Query
    <P> Stream<P> findAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                     Class<P> projectionType);

    /**
     * @param projectionType interface which describes projection (mapping fields from source to result)
     * @param <P>            projection  interface
     * @return all product ids
     */
    @Query
    <P> Stream<P> findAllBy(Class<P> projectionType);

}
