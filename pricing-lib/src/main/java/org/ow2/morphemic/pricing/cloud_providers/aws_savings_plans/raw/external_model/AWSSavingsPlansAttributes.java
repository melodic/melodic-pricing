package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

import java.time.Period;

/**
 * Represents object with all savings plans attributes data.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSSavingsPlansAttributes {

    /**
     * Purchase option for savings plans.
     * For now is -> No Upfront , Partial Upfront, All Upfront
     * {@link org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption}
     */
    @JsonProperty("purchaseOption")
    private String purchaseOption;

    /**
     * Granularity of savings plans.
     * Now it's always "hourly".
     */
    @JsonProperty("granularity")
    private String granularity;

    /**
     * Location type of savings plans.
     * Now it's always "AWS Region".
     */
    @JsonProperty("locationType")
    private String locationType;

    /**
     * Purchase term of savings plans.
     * Now it's 1 or 3 years.
     */
    @JsonProperty("purchaseTerm")
    private String purchaseTerm;

    /**
     * Location of savings plans.
     */
    @JsonProperty("location")
    private String location;

    /**
     * Instance type of savings plans.
     */
    @JsonProperty("instanceType")
    private String instanceType;

    /**
     * Instance type of savings plans.
     */
    @JsonProperty("regionCode")
    private String regionCode;

    @JsonIgnore
    @Transient
    public Period getPurchaseTermAsPeriod() {
        return purchaseTerm != null ? Period.ofYears(Integer.parseInt(purchaseTerm.replaceAll("[^0-9]", ""))) : null;
    }
}
