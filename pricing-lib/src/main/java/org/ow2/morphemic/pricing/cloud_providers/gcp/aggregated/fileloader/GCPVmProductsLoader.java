package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.fileloader;

import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Loads raw GCP products descriptions from various data streams.
 */
@Slf4j
public class GCPVmProductsLoader {

    private final ObjectMapper objectMapper;

    public GCPVmProductsLoader() {
        objectMapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .enable(JsonReadFeature.ALLOW_NON_NUMERIC_NUMBERS.mappedFeature());
    }

    /**
     * Loads VMs descriptions from machines json file.
     *
     * @param dataSrc
     * @return
     * @throws IOException
     */
    public List<GCPVmProductDef> loadVMsFrom(InputStream dataSrc) throws IOException {
        return Arrays.asList(objectMapper.readValue(dataSrc, GCPVmProductDef[].class));
    }

}
