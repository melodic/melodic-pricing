package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.Period;
import java.util.List;
import java.util.Optional;

@Repository
interface AWSSavingsPlansRepository extends MongoRepository<AWSSavingsPlansRecord, String> {

    Optional<AWSSavingsPlansRecord> findByCloudEnvironmentPricingIdAndVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum);

    List<AWSSavingsPlansRecord> findAllByPurchaseOptionAndPurchaseTermAndInstanceTypeAndRegionCodeOrderByVersionDesc(
            AWSPurchaseOption purchaseOption, Period purchaseTerm, String instanceType, String regionCode);

    List<AWSSavingsPlansRecord> findAllByPurchaseOptionAndPurchaseTermAndSavingsPlanTypeOrderByVersionDesc(
            AWSPurchaseOption purchaseOption, Period purchaseTerm,
            AWSSavingsPlanType savingsPlanType);

    void deleteByCloudEnvironmentPricingIdAndVersion(CloudEnvironmentPricingId pricingId, VersionNum version);
}
