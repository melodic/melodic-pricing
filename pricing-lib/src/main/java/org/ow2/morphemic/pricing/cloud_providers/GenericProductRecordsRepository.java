package org.ow2.morphemic.pricing.cloud_providers;

import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Used to store records in Mongo. For internal use only.
 */
@Repository
interface GenericProductRecordsRepository extends MongoRepository<GenericProductRecord, ProductId> {

    /**
     * Loads record by id with given projection.
     *
     * @param projectionType projection
     * @param <P>type
     * @return Optional of record
     */
    <P> Optional<P> findById(ProductId productId, Class<P> projectionType);

}
