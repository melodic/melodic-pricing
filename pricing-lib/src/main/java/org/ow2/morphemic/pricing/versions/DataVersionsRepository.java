package org.ow2.morphemic.pricing.versions;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Preserves versions for "virtual collections" of data.
 */
@Repository
interface DataVersionsRepository extends MongoRepository<VersionRecord, String> {

    /**
     * Tries to find lastest version number for data collection in cloud environment.
     *
     * @param cloudEnvironmentPricingId cloud environment pricing id
     * @param collectionName            collection name in cloud environment
     * @return found record (optional)
     */
    Optional<VersionRecord> findFirstByCloudEnvironmentPricingIdAndCollectionNameOrderByVersionDesc(CloudEnvironmentPricingId cloudEnvironmentPricingId, String collectionName);

    /**
     * Check if exist version record for cloudEnvironmentPricingId, versionNum and collection name.
     *
     * @param cloudEnvironmentPricingId cloud environment pricing id
     * @param versionNum                version number
     * @param collectionName            collection name in cloud environment
     * @return found record (optional)
     */
    boolean existsByCloudEnvironmentPricingIdAndVersionAndCollectionName(CloudEnvironmentPricingId cloudEnvironmentPricingId, Long versionNum, String collectionName);

    /**
     * How many versions is associated with this pricing id?
     *
     * @param cloudEnvironmentPricingId cloud environment pricing id
     * @return number of versions (in all data collections).
     */
    long countByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId);

    /**
     * Removes all records (versions) for given pricing.
     *
     * @param cloudEnvironmentPricingId pricing to be deregistered
     */
    void deleteByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId);

    /**
     * Removes vrsion of given pricing.
     *
     * @param cloudEnvironmentPricingId pricing
     * @param version                   version
     */
    void deleteByCloudEnvironmentPricingIdAndVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum version);
}
