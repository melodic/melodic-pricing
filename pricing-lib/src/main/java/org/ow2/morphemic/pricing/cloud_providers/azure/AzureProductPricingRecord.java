package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains AZURE product pricing details.
 */
@Document(collection = AzureProductPricingRecord.MONGO_COLLECTION_NAME)
@TypeAlias("AzureProductPricingRecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AzureProductPricingRecord {

    static final String MONGO_COLLECTION_NAME =  "azure_product_pricing";

    @Id
    @Field(targetType = FieldType.STRING)
    private ProductId id;

    /**
     * It is connected with this pricing.
     */
    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    /**
     * It is based on this version number of raw data.
     */
    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum basedOnRawVersionNum;

    private List<AzurePriceOffer> priceOffers;

    AzureProductPricingRecord addPriceOffer(AzurePriceOffer offer) {
        if (priceOffers == null) {
            priceOffers = new ArrayList<>();
        }
        priceOffers.add(offer);
        return this;
    }
}
