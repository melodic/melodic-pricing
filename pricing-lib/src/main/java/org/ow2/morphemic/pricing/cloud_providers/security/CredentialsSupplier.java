package org.ow2.morphemic.pricing.cloud_providers.security;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.resource.CloudCredential;

import java.util.Optional;

/**
 * Provides credentials needed to connect to remote REST API provided by various Cloud Providers.
 * Very generic.
 */
public interface CredentialsSupplier {

    /**
     * Provides api credentials of given cloud environment id.
     *
     * @param cloudEnvironmentPricingId cloud provider
     * @return credentials container
     */
    Optional<CloudCredential> apiCredentialsOf(CloudEnvironmentPricingId cloudEnvironmentPricingId);

}
