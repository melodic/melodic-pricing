package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents object with all savings plans price list data.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSSavingsPlansProduct {

    /**
     * Sku of savings plans.
     */
    @JsonProperty("sku")
    private String skuId;

    /**
     * Product family for savings plans.
     */
    @JsonProperty("productFamily")
    private String productFamily;

    /**
     * Service code for savings plans.
     */
    @JsonProperty("serviceCode")
    private String serviceCode;

    /**
     * Usage type for savings plans.
     */
    @JsonProperty("usageType")
    private String usageType;

    /**
     * Operation for savings plans.
     */
    @JsonProperty("operation")
    private String operation;

    /**
     * Attributes for savings plans.
     */
    @JsonProperty("attributes")
    private AWSSavingsPlansAttributes attributes;
}
