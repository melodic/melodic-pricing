package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.OfferTermType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * Helps to save {@link OfferTermType} as text.
 */
@WritingConverter
class OfferTermTypeToStringConverter implements Converter<OfferTermType, String> {

    @Override
    public String convert(OfferTermType source) {
        return source.getName();
    }
}

