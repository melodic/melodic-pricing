package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents response from Aws savings plans pages.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSSavingsPlansRates {

    /**
     * Sku for discounted product.
     */
    @JsonProperty("discountedSku")
    private String discountedSku;

    /**
     * Discounted usage type.
     * For example "EU-Processing:ml.m4.2xlarge"
     */
    @JsonProperty("discountedUsageType")
    private String discountedUsageType;

    /**
     * Discounted operation.
     * For example "RunInstance"
     */
    @JsonProperty("discountedOperation")
    private String discountedOperation;

    /**
     * Discounted service code.
     */
    @JsonProperty("discountedServiceCode")
    private String discountedServiceCode;

    /**
     * Rate code.
     * It's SavingsPlanSKU . ProductSKU
     * For example "P4CY32WJTWG6YMVJ.2BBE9RBA2J8XTZZW"
     */
    @JsonProperty("rateCode")
    private String rateCode;

    /**
     * Unit of price.
     * It's always Hrs.
     */
    @JsonProperty("unit")
    private String unit;

    /**
     * Discounted rate for this product.
     */
    @JsonProperty("discountedRate")
    private AWSSavingsPlansDiscountedRate discountedRate;
}
