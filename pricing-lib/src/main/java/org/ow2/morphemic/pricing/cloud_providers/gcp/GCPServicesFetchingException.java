package org.ow2.morphemic.pricing.cloud_providers.gcp;

public class GCPServicesFetchingException extends RuntimeException {

    public GCPServicesFetchingException(String message, Throwable cause) {
        super(message, cause);
    }
}
