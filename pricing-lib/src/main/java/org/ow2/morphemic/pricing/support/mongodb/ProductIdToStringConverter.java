package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * Helps to save {@link ProductId} as text.
 */
@WritingConverter
class ProductIdToStringConverter implements Converter<ProductId, String> {

    @Override
    public String convert(ProductId source) {
        return source.getId();
    }
}
