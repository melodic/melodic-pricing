package org.ow2.morphemic.pricing.cloud_providers.aws;

import com.google.common.primitives.Longs;
import lombok.Builder;
import lombok.Data;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.springframework.data.annotation.TypeAlias;

import java.beans.Transient;
import java.time.Period;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * AWS pricing offer and terms for single product. It is used to load/store data in db repo.
 */
@Builder
@Data
@TypeAlias("AWSOffersTermsRecord")
public class AWSOffersTermsRecord {

    private String productSkuId;

    private String externalOfferTermId;

    /**
     * term type or  "usage type". Typical values: "OnDemand", "Reserved".
     */
    private OfferTermType offerTermType;

    private Date effectiveDate;

    private Map<String, String> termAttributesRaw;

    private Map<String, String> termAttributesNormalized;

    /**
     * parameters used to calculate prices: value ranges and costs.
     */
    private List<AWSOfferRate> rates;

    /**
     * Tries to extract lease contract length.
     *
     * @return
     */
    @Transient
    public Optional<Period> getLeaseContractLength() {
        String leaseContractLength = (termAttributesRaw != null) ? termAttributesRaw.get("LeaseContractLength") : null;
        if (leaseContractLength != null) {
            if (leaseContractLength.endsWith("yr")) {
                String years = leaseContractLength.substring(0, leaseContractLength.length() - 2);
                return Optional.ofNullable(Longs.tryParse(years)).map(yrs -> Period.ofYears(yrs.intValue()));
            }
        }
        return Optional.empty();
    }
}
