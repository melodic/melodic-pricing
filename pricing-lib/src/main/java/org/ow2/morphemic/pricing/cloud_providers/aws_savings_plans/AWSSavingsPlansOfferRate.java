package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Describes AWS savings plans rates.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TypeAlias("AWSSavingsPlansOfferRate")
public class AWSSavingsPlansOfferRate {

    @Field(targetType = FieldType.STRING)
    private Unit unit;

    private Map<String, BigDecimal> pricePerUnitByCurrency;

    private String externalRateCodeId;

    private String discountedSku;

    private String discountedUsageType;

    private String discountedOperation;
}
