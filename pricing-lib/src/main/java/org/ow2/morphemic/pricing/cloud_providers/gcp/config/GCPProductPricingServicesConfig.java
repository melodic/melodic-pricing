package org.ow2.morphemic.pricing.cloud_providers.gcp.config;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Configuration
@ConfigurationProperties(prefix = "pricing.gcp")
@Setter
public class GCPProductPricingServicesConfig {

    private static final long DEFAULT_SERVICES_FETCHING_DELAY_MS = 1000L;

    private long servicesFetchingDelayMs;

    private Set<String> servicesIdsToFetch = new HashSet<>();

    public Set<String> getServicesIdsToFetch() {
        return this.servicesIdsToFetch;
    }

    public long getServicesFetchingDelayMs() {
        return this.servicesFetchingDelayMs != 0L ? this.servicesFetchingDelayMs : DEFAULT_SERVICES_FETCHING_DELAY_MS;
    }
}
