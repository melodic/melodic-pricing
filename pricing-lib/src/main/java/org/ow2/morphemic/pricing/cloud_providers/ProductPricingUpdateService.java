package org.ow2.morphemic.pricing.cloud_providers;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;

import java.io.IOException;
import java.util.Optional;

/**
 * Basic functions which must be supported by pricing data updaters.
 */
public interface ProductPricingUpdateService {

    /**
     * Marker file. Its existence means that data is complete and ready to be imported. It must contain pricing cloud env id.
     */
    String FS_LOADER_OK_FILE_NAME = "pricingId.ok";

    /**
     * It scans some directory for new files containing import data.
     *
     * @return new pricing version if loaded properly
     * @throws io exception when cannot read files.
     */
    Optional<VersionedCloudEnvPricingId> loadPricingDumpDataFromFiles() throws IOException;

    /**
     * Updates pricing data for given cloud environment pricing. It somehow must load credentuals needed to connect to that environment.
     *
     * @param cloudEnvironmentPricingId pricing of cloud env.
     * @param pricingDataImportStatistic {@link PricingDataImportStatisticCollector} helper object for collection some execution metrics
     */
    void updateUsingExternalApi(CloudEnvironmentPricingId cloudEnvironmentPricingId, PricingDataImportStatisticCollector pricingDataImportStatistic);
}
