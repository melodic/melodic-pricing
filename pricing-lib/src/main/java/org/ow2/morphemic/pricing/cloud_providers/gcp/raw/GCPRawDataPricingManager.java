package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * manages raw data loaded from GCP API services.
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class GCPRawDataPricingManager {

    private final GCPRawServiceSKUDataRecordRepository recordRepository;

    private final GCPRawSKURecordRepository gcpRawSKURecordRepository;

    private final DataVersionsService dataVersionsService;

    private final JsonFactory jsonFactory = new JacksonFactory();

    /**
     * For given version it returns stream of pricing data records.
     *
     * @param versionedCloudEnvPricingId versioned cloud env
     * @return raw data for given version of cloud environment id.
     */
    @Transactional
    public Stream<GCPRawServicePricing> rawPricingDataAsStreamForVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();
        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(recordRepository
                        .findByCloudEnvironmentPricingIdAndVersion(cloudEnvironmentPricingId, versionNum).iterator(), 0), false)
                .map(record -> {
                    try {
                        record.afterLoad(jsonFactory);
                        return new GCPRawServicePricing(record.getGcpService(), record.getGcpServiceSkus(),
                                versionedCloudEnvPricingId);
                    } catch (IOException e) {
                        log.error("Cannot deserialize record: " + record.getId() + ". Cause: " + e.getMessage(), e);
                        return null;
                    }
                }).filter(Objects::nonNull);
    }

    /**
     * Cleans whole collection of fetched data.
     */
    @Transactional
    public void deleteAll() {
        gcpRawSKURecordRepository.deleteAll();
        recordRepository.deleteAll();
        log.info("Removed all data from db GCP pricing cache.");
    }

    @Transactional
    public void deletePricing(CloudEnvironmentPricingId pricingId) {
        Set<String> relatedSkuRecordsToDelete = recordRepository.findAllByCloudEnvironmentPricingId(pricingId, RelatedRawSKURecordsProj.class).stream()
                .filter(projection -> projection.getRawSKURecords() != null && !projection.getRawSKURecords().isEmpty())
                .flatMap(projection -> projection.getRawSKURecords().stream())
                .map(GCPRawSKURecord::getId)
                .collect(Collectors.toSet());
        if (!relatedSkuRecordsToDelete.isEmpty()) {
            gcpRawSKURecordRepository.deleteAllByIdIn(relatedSkuRecordsToDelete);
        }

        var removed = recordRepository.deleteAllByCloudEnvironmentPricingId(pricingId);
        log.info("Deleted all RAW GCP pricing {} records. Removed records: {}", pricingId, removed);
    }

    /**
     * Provides info about last version of raw data for pricing.
     *
     * @param cloudEnvironmentPricingId pricing id
     * @return maybe data
     */
    public Optional<DataVersionsService.VersionMetadata> lastVersionOfRawData(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        return dataVersionsService.lastVersionMetadata(cloudEnvironmentPricingId, GCPRawServiceSKUDataRecord.VERSION_COLLECTION_NAME);
    }
}
