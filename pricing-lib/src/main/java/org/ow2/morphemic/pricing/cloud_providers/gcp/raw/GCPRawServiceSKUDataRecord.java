package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.client.json.JsonFactory;
import com.google.api.services.cloudbilling.model.Service;
import com.google.api.services.cloudbilling.model.Sku;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Can store single data describing "service" and its "SKUs" (pricing data, service description etc).
 * Note that SKU is a separate product, but here we group data as it comes from GCP REST API.
 */
@Document(collection = "gcp_raw_pricings")
@TypeAlias("GCPRawServiceSKUDataRecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
class GCPRawServiceSKUDataRecord {
    /**
     * it should differ from Document collection, because it must not be changed during refactorings
     */
    static final CollectionName VERSION_COLLECTION_NAME = new CollectionName("gcp:raw_pricings:" + CollectionName.TEMPLATE);

    /**
     * managed by Spring
     */
    @Id
    private String id;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum version;

    @Transient
    private transient Service gcpService;

    @Transient
    private transient List<Sku> gcpServiceSkus;

    /**
     * serialized raw data
     */
    private String serializedGcpServiceAsJson;

    /**
     * serialized raw data
     */
    @Deprecated
    private String serializedGcpServiceSkusAsJson;

    @DBRef
    private List<GCPRawSKURecord> rawSKURecords;

    GCPRawServiceSKUDataRecord(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum, Service gcpService,
                               List<GCPRawSKURecord> rawSKURecords) throws IOException {
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.version = versionNum;
        this.gcpService = gcpService;
        this.serializedGcpServiceAsJson = gcpService.toPrettyString();
        this.rawSKURecords = rawSKURecords;
        this.gcpServiceSkus = rawSKURecords.stream().map(GCPRawSKURecord::getGcpSku).collect(Collectors.toList());
    }

    /**
     * Needed for testing purposes.
     *
     * @param versionNum
     * @param serializedGcpServiceAsJson
     * @param serializedSkus
     */
    GCPRawServiceSKUDataRecord(VersionNum versionNum, String serializedGcpServiceAsJson, String... serializedSkus) {
        this.version = versionNum;
        this.serializedGcpServiceAsJson = serializedGcpServiceAsJson;
        this.serializedGcpServiceSkusAsJson = (serializedSkus == null) ? "[]" : "[" + String.join(", ", serializedSkus) + "]";
    }

    /**
     * Must be called after loading from db in order to properly restore internal data.
     *
     * @param jsonFactory
     * @return this
     * @throws IOException
     */
    GCPRawServiceSKUDataRecord afterLoad(JsonFactory jsonFactory) throws IOException {
        this.gcpService = jsonFactory.fromString(serializedGcpServiceAsJson, Service.class);
        this.gcpService.setFactory(jsonFactory);

        if (StringUtils.isNotBlank(serializedGcpServiceSkusAsJson)) {
            // deprecated storage stategy: SKUs stored as field th "service" record.
            this.gcpServiceSkus = Arrays.asList(jsonFactory.fromString(serializedGcpServiceSkusAsJson, Sku[].class));
            this.gcpServiceSkus.forEach(sku -> sku.setFactory(jsonFactory));
        } else {
            this.gcpServiceSkus = rawSKURecords.stream()
                    .map(gcpRawSKURecord -> {
                        try {
                            return gcpRawSKURecord.afterLoad(jsonFactory).getGcpSku();
                        } catch (IOException e) {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        return this;
    }

    /**
     * Conversion.
     *
     * @return
     */
    GCPRawServicePricing toGCPRawServicePricing() {
        return new GCPRawServicePricing(gcpService, gcpServiceSkus,
                new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, version));
    }


}
