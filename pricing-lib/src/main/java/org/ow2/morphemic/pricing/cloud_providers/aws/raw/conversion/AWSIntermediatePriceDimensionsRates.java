package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AWSIntermediatePriceDimensionsRates {
    private static final long serialVersionUID = -1L;

    private String sku;
    private String effectiveDate;
    private String offerTermCode;
    private Map<String, String> termAttributes;
    @JsonIgnore
    private Map<String, AWSIntermediateDimensionRates> awsDimensionRates = new HashMap<>();

    @JsonAnyGetter
    public Map<String, AWSIntermediateDimensionRates> getAwsDimensionRates() {
        return this.awsDimensionRates;
    }

    @JsonAnySetter
    public void setAwsDimensionRates(String name, AWSIntermediateDimensionRates value) {
        this.awsDimensionRates.put(name, value);
    }
}

