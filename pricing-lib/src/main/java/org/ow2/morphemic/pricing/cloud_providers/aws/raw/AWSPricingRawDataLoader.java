package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.mutable.MutableInt;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.WithCloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSRawPricingDataRecord.VERSION_COLLECTION_NAME;

/**
 * Provides pricing data as raw text data for given list of AWS products for single {@link CloudEnvironmentPricingId}.
 * Loader uses data suppliers to load data to db.
 */
@Slf4j
@RequiredArgsConstructor
@AllArgsConstructor
public class AWSPricingRawDataLoader {

    /**
     * Data suppliers must share the same {@link CloudEnvironmentPricingId}!
     */
    private final List<AWSServicePricingDataStreamSupplier> dataSuppliers;

    private final AWSRawPricingDataRepository awsRawPricingDataRepository;

    private final DataVersionsService dataVersionsService;

    private boolean removeOldVersions = true;

    /**
     * records to save at once.
     */
    @Getter
    @Setter
    private int bufferSize = 500;

    public AWSPricingRawDataLoader(List<AWSServicePricingDataStreamSupplier> dataSuppliers,
                                   AWSRawPricingDataRepository awsRawPricingDataRepository,
                                   DataVersionsService dataVersionsService, boolean removeOldVersions) {
        this.dataSuppliers = dataSuppliers;
        this.awsRawPricingDataRepository = awsRawPricingDataRepository;
        this.dataVersionsService = dataVersionsService;
        this.removeOldVersions = removeOldVersions;
    }

    /**
     * Loads AWS pricing raw data to DB cache using defined collection of data suppliers {@link #dataSuppliers}.
     *
     * @return new version id if new data was stored in cache.
     * @param pricingDataImportStatisticCollector
     */
    @Transactional
    public Optional<VersionedCloudEnvPricingId> loadRawDataToCache(PricingDataImportStatisticCollector pricingDataImportStatisticCollector) {
        if (dataSuppliers != null) {

            Set<CloudEnvironmentPricingId> cloudEnvironmentPricingIds = dataSuppliers.stream()
                    .map(WithCloudEnvironmentPricingId::getCloudEnvironmentPricingId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toSet());
            if (cloudEnvironmentPricingIds.size() > 1) {
                throw new IllegalArgumentException("Data suppliers must fetch data for one cloud environment! "
                        + "But they are connected with multiple environments: " + cloudEnvironmentPricingIds);
            } else if (cloudEnvironmentPricingIds.size() == 1) {
                final CloudEnvironmentPricingId cloudEnvironmentPricingId = cloudEnvironmentPricingIds.iterator().next();
                log.info("Loading raw pricing data to cache for {}", cloudEnvironmentPricingId);
                final VersionNum newVersion = new VersionNum();
                long countRecordsAdded = 0L;

                pricingDataImportStatisticCollector.resumeMeasureTimeOfDataReadFromProvider(); //start measure of data read
                for (AWSServicePricingDataStreamSupplier dataSupplier : dataSuppliers) {
                    pricingDataImportStatisticCollector.suspendMeasureTimeOfDataReadFromProvider(); //suspend measure of data read from provider
                    if (dataSupplier.getCloudEnvironmentPricingId().isPresent()) {
                        MutableInt counter = new MutableInt();
                        pricingDataImportStatisticCollector.resumeMeasureTimeOfDataReadFromProvider();
                        AtomicLong sortPositionNum = new AtomicLong();
                        Long countedPages = dataSupplier.createDataStream()
                                .map(json -> new AWSRawPricingDataRecord(cloudEnvironmentPricingId,
                                    newVersion, json, Collections.emptyMap(), sortPositionNum.incrementAndGet()))
                                .buffer(bufferSize)
                                .doOnNext(records -> {
                                    pricingDataImportStatisticCollector.suspendMeasureTimeOfDataReadFromProvider();
                                    counter.add(records.size());
                                    int i = counter.getValue();
                                    log.debug("Fetched and saved {} records for {}.", i, cloudEnvironmentPricingId);
                                    pricingDataImportStatisticCollector.resumeMeasureTimeOfSaveDataIntoDatabase();
                                    awsRawPricingDataRepository.saveAll(records);
                                    pricingDataImportStatisticCollector.suspendMeasureTimeOfSaveDataIntoDatabase();
                                    pricingDataImportStatisticCollector.resumeMeasureTimeOfDataReadFromProvider();
                                }).count().block();

                        log.debug("Total records for pricing {}: {} / {}", cloudEnvironmentPricingId, counter.getValue(), countedPages);
                        countRecordsAdded += counter.getValue();
                    }
                }
                pricingDataImportStatisticCollector.suspendMeasureTimeOfDataReadFromProvider()
                        .incrementTotalNumberOfSavedRowsIntoDatabase(countRecordsAdded);

                if (countRecordsAdded > 0L) {
                    log.info("{} raw records have been added for pricing {}", countRecordsAdded, cloudEnvironmentPricingId);

                    VersionedCloudEnvPricingId versionedEnvId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, newVersion);
                    dataVersionsService.registerNewVersion(versionedEnvId, VERSION_COLLECTION_NAME);

                    if (removeOldVersions) {
                        pricingDataImportStatisticCollector.resumeMeasureTimeOfDeletePricingData();
                        long removedCount = awsRawPricingDataRepository.deleteAllByCloudEnvironmentPricingIdAndVersionLessThan(cloudEnvironmentPricingId, newVersion);
                        pricingDataImportStatisticCollector.suspendMeasureTimeOfDeletePricingData();
                        log.debug("Removed {} old records from raw data cache for {}.", removedCount, cloudEnvironmentPricingId);
                    } else {
                        log.debug("Old records from raw data cache were not removed for {}.", cloudEnvironmentPricingId);
                    }

                    return Optional.of(versionedEnvId);
                }

            }


        }

        return Optional.empty();
    }

}



