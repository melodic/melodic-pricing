package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Represents response from one page from Azure pricing API
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AzureRawPricingResponse {

    @JsonProperty("BillingCurrency")
    private String billingCurrency;

    @JsonProperty("CustomerEntityId")
    private String customerEntityId;

    @JsonProperty("Items")
    private List<AzureRawPricingItem> items;

    @JsonProperty("NextPageLink")
    private String nextPageLink;
}
