package org.ow2.morphemic.pricing.cloud_providers.aws;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSClientPricingRawDataLoaderFactory;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSInvalidRawRecordException;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSPricingRawDataLoader;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSRawPricingDataManager;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion.AWSProductAndOffersFactory;
import org.ow2.morphemic.pricing.cloud_providers.security.CredentialsSupplier;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.resource.CloudCredential;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService.VERSIONED_AWS_PRICING_COLLECTION_NAME;

/**
 * Responsible for update of product and its pricing data. Update process consists of following steps:
 * <ol>
 *     <li>Downloading data from AWS Servers or from disk catalog</li>
 *     <li>Saving raw data in Mongo DB (data repository)</li>
 *     <li>Transforming raw data to domain data (products, pricing offers)</li>
 * </ol>
 */
@Service("awsProductPricingUpdateService")
@Slf4j
@RequiredArgsConstructor
public class AWSProductPricingUpdateService implements ProductPricingUpdateService {

    @Qualifier("awsPricingFromFiles")
    private final AWSPricingRawDataLoader awsPricingFromFiles;

    private final AWSClientPricingRawDataLoaderFactory awsClientLoaderFactory;

    private final AWSRawPricingDataManager awsRawPricingDataManager;

    private final AWSProductAndOffersFactory productAndOffersFactory;

    private final AWSProductRecordsRepository awsProductRecordsRepository;

    private final AWSProductPricingRecordsRepository awsProductPricingRecordsRepository;

    private final CredentialsSupplier credentialsSupplier;

    private final DataVersionsService dataVersionsService;

    private final ProductsMetadataService productsMetadataService;

    @Value("${pricing.cloudproviders.aws.serviceCodes:AmazonEC2,AmazonS3,AmazonRDS}")
    private Set<String> serviceCodes;

    @Value("${pricing.cloudproviders.aws.productUpdate.pageSize:1000}")
    private int productLoadingPageSize = 1000;

    private final List<Consumer<List<AWSProductAndOffersFactory.ProductAndOffers>>> additionalProductConsumers = new LinkedList<>();

    /**
     * It scans directory for new files containing import data.
     */
    @Override
    @Transactional
    public synchronized Optional<VersionedCloudEnvPricingId> loadPricingDumpDataFromFiles() throws IOException {
        PricingDataImportStatisticCollector pricingDataImportStatisticCollector = new PricingDataImportStatisticCollector(
                CloudProvider.AWS, "pricing_dump", List.of()); //for collecting import statistics
        Optional<VersionedCloudEnvPricingId> maybeNewVersion = awsPricingFromFiles.loadRawDataToCache(pricingDataImportStatisticCollector);
        if (maybeNewVersion.isPresent()) {
            try {
                updateProductsAndPricings(maybeNewVersion.get(), pricingDataImportStatisticCollector);
            } catch (AWSInvalidRawRecordException e) {
                throw new IOException(e);
            }
            log.debug("Pricing dump load statistics: \n{}", pricingDataImportStatisticCollector.printStatistics());
        }
        return maybeNewVersion;
    }


    /**
     * Updates AWS pricing data for given cloud environment pricing. Uses credentialsSupplier, whoch should give credentials needed to connect to cloud env.
     *
     * @param cloudEnvironmentPricingId pricing of cloud env which should be updated.
     */
    @Override
    @Transactional
    @SuppressFBWarnings("RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE")
    public void updateUsingExternalApi(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                       PricingDataImportStatisticCollector pricingDataImportStatisticCollector) {
        Optional<CloudCredential> maybeCredentials = credentialsSupplier.apiCredentialsOf(cloudEnvironmentPricingId);

        if (maybeCredentials.isPresent()) {
            log.debug("Found credentials for {}", cloudEnvironmentPricingId);
            CloudCredential cloudCredential = maybeCredentials.get();
            try (AWSClientPricingRawDataLoaderFactory.CloseableAWSPricingRawDataLoader pricingAWSLoader = awsClientLoaderFactory.createPricingAWSLoader(
                    cloudEnvironmentPricingId, cloudCredential.user(), cloudCredential.password(), serviceCodes)) {
                Optional<VersionedCloudEnvPricingId> maybeNewVersion = pricingAWSLoader.loadRawDataToCache(pricingDataImportStatisticCollector);
                if (maybeNewVersion.isPresent()) {
                    updateProductsAndPricings(maybeNewVersion.get(), pricingDataImportStatisticCollector);
                }
            } catch (Exception e) {
                log.error("Cannot update pricing for " + cloudEnvironmentPricingId + "! Cause: " + e.getMessage(), e);
            }
        } else {
            log.error("Cannot find credentials for {}", cloudEnvironmentPricingId);
        }
    }

    /**
     * It can perform additional custom operations for all fetched records from latest pricing version.
     *
     * @param cloudEnvironmentPricingId pricing
     * @param consumers                 consumers which will consume data.
     * @return
     */
    @Transactional
    public Optional<VersionedCloudEnvPricingId> processLatestFetchedPricingData(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                                                List<Consumer<List<AWSProductAndOffersFactory.ProductAndOffers>>> consumers,
                                                                                PricingDataImportStatisticCollector pricingDataImportStatisticCollector)
            throws AWSInvalidRawRecordException {
        pricingDataImportStatisticCollector.resumeMeasureTimeOfSaveDataIntoDatabase();
        Optional<DataVersionsService.VersionMetadata> lastVersion = awsRawPricingDataManager.lastVersionOfRawData(cloudEnvironmentPricingId);
        pricingDataImportStatisticCollector.suspendMeasureTimeOfSaveDataIntoDatabase();
        if (lastVersion.isPresent()) {
            VersionNum versionNum = lastVersion.get().getVersionNum();
            VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, versionNum);
            log.debug("Found last version of pricing {} {} {}", cloudEnvironmentPricingId, versionNum, versionedCloudEnvPricingId);

            if (!consumers.isEmpty()) {
                log.debug("{} consumers", consumers.size());
                Flux<List<AWSProductAndOffersFactory.ProductAndOffers>> productDataFlow = awsRawPricingDataManager.rawPricingDataAsFluxForVersion(versionedCloudEnvPricingId)
                        .flatMap(Flux::fromIterable)
                        .map(json -> productAndOffersFactory.fromJson(json, versionedCloudEnvPricingId))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .buffer(productLoadingPageSize);
                for (Consumer<List<AWSProductAndOffersFactory.ProductAndOffers>> consumer : consumers) {
                    productDataFlow = productDataFlow.doOnNext(productAndOffers -> {
                        pricingDataImportStatisticCollector.resumeMeasureTimeOfSaveDataIntoDatabase();
                        consumer.accept(productAndOffers);
                        pricingDataImportStatisticCollector.suspendMeasureTimeOfSaveDataIntoDatabase();
                    });
                }
                productDataFlow.count().block();
            }

            return Optional.of(versionedCloudEnvPricingId);
        } else {
            log.error("Cannot update because there is no fetched pricing data for {}", cloudEnvironmentPricingId);
            return Optional.empty();
        }
    }

    /**
     * Does whole update process for data marked by given version.
     *
     * @param versionedCloudEnvPricingId          update products data for cloud environment and given version.
     * @param pricingDataImportStatisticCollector {@link PricingDataImportStatisticCollector} for collecting import statistics
     */
    void updateProductsAndPricings(VersionedCloudEnvPricingId versionedCloudEnvPricingId, PricingDataImportStatisticCollector pricingDataImportStatisticCollector)
            throws AWSInvalidRawRecordException {
        pricingDataImportStatisticCollector.startMeasureTimeOfTransformationOfSourceDataToBusinessModel();
        log.info("Update pricing for {}", versionedCloudEnvPricingId);
        Flux<String> streamOfJsons = awsRawPricingDataManager.rawPricingDataAsFluxForVersion(versionedCloudEnvPricingId)
                .flatMap(Flux::fromIterable);
        updateUsingDataStream(streamOfJsons, versionedCloudEnvPricingId, pricingDataImportStatisticCollector);
        pricingDataImportStatisticCollector.stopMeasureTimeOfTransformationOfSourceDataToBusinessModel();
    }

    private void updateUsingDataStream(Flux<String> streamOfJsons, VersionedCloudEnvPricingId versionedCloudEnvPricingId,
                                       PricingDataImportStatisticCollector pricingDataImportStatisticCollector) {
        AtomicInteger updatedProductsCounter = new AtomicInteger();
        AtomicInteger updatedProductPricingsCounter = new AtomicInteger();

        Flux<AWSProductAndOffersFactory.ProductAndOffers> flow = streamOfJsons.map(json -> productAndOffersFactory.fromJson(json, versionedCloudEnvPricingId))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .doOnNext(productAndOffers -> {
                    updateProduct(productAndOffers.getAwsProductRecord());
                    int productsCount = updatedProductsCounter.incrementAndGet();

                    updateProductPricingOffers(productAndOffers.getAwsProductPricing());
                    updatedProductPricingsCounter.incrementAndGet();

                    if (productsCount % 1000 == 0) {
                        log.debug("Updated {} products and their pricing offers for {}.", productsCount, versionedCloudEnvPricingId);
                    }
                });

        if (hasAdditionalProductConsumers()) {
            flow.buffer(1000).doOnNext(this::sendFetchedDataToAdditionalConsumers).count().block();
        } else {
            flow.count().block();
        }
        pricingDataImportStatisticCollector.incrementTotalNumberOfTransformationRowsOfSourceDataToBusinessModel(updatedProductPricingsCounter.get());
        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, VERSIONED_AWS_PRICING_COLLECTION_NAME);
        log.info("Updated products: {} and their pricing data: {}", updatedProductsCounter.get(), updatedProductPricingsCounter.get());
    }

    private void sendFetchedDataToAdditionalConsumers(List<AWSProductAndOffersFactory.ProductAndOffers> packOfProductAndOffers) {
        for (Consumer<List<AWSProductAndOffersFactory.ProductAndOffers>> productConsumer : additionalProductConsumers) {
            try {
                productConsumer.accept(packOfProductAndOffers);
            } catch (Exception e) {
                log.error("Consumer threw an error. But we ignore it! Details: " + e.getMessage(), e);
            }
        }

    }

    /**
     * To be used only in tests.
     *
     * @param jsons                      test jsons
     * @param versionedCloudEnvPricingId version of given cloud env.
     */
    @Transactional
    public void updateProductsAndPricingData(Collection<String> jsons, VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        updateUsingDataStream(Flux.fromIterable(jsons), versionedCloudEnvPricingId,
                new PricingDataImportStatisticCollector(CloudProvider.AWS, "cloud_account_id", List.of()));
    }

    private void updateProductPricingOffers(AWSProductPricingRecord awsProductPricing) {
        awsProductPricingRecordsRepository.save(awsProductPricing);
    }

    private void updateProduct(AWSProductRecord awsProductRecord) {
        awsProductRecordsRepository.save(awsProductRecord);
        ProductId productId = awsProductRecord.getId();
        if (!productsMetadataService.isProductMetadataInCache(productId)) {
            productsMetadataService.createProductMetadata(productId, CloudProvider.AWS);
        }
    }

    /**
     * Registers additional data consumer. It allows to provide additional data processing steps
     * for all product records which were fetched from AWS.
     *
     * @param dataConsumer consumer.
     */
    public void addProductDataConsumer(Consumer<List<AWSProductAndOffersFactory.ProductAndOffers>> dataConsumer) {
        synchronized (this.additionalProductConsumers) {
            this.additionalProductConsumers.add(dataConsumer);
        }
    }

    public boolean hasAdditionalProductConsumers() {
        synchronized (this.additionalProductConsumers) {
            return !this.additionalProductConsumers.isEmpty();
        }
    }

}
