package org.ow2.morphemic.pricing.cloud_providers;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;

import java.util.Optional;

/**
 * Something with cloud environment ID info.
 */
public interface WithCloudEnvironmentPricingId {

    /**
     * @return cloud environment id for that data stream (if known).
     */
    Optional<CloudEnvironmentPricingId> getCloudEnvironmentPricingId();
}
