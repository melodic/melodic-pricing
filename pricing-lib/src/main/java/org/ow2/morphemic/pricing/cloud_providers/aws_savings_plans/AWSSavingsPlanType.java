package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

/**
 * AWS Savings Plan type: one of:
 * <ul>
 *     <li>compute</li>
 *     <li>ec2</li>
 * </ul>
 */
public enum AWSSavingsPlanType {

    /**
     * Unknown Savings Plan. Technical value.
     */
    UNKNOWN_SP,

    /**
     * Compute Instance Savings Plan. The most elastic one.
     */
    COMPUTE_SP,

    /**
     * EC2 instance Savings Plan
     */
    EC2_INSTANCE_SP


}
