package org.ow2.morphemic.pricing.calculators.aws;

import com.google.common.base.Ascii;
import com.google.common.primitives.Doubles;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.calculators.PricingOption;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSOfferRate;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSOffersTermsRecord;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Calculates product price according to given parameters.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class AWSPriceCalculator {

    private final AWSProductPricingService productPricingService;


    /**
     * Calculates price of given product for specified amount and units.
     *
     * @param productId     product id
     * @param amount        how many / how much is needed (amount of product)
     * @param offerTermType what offer type?
     * @return calculated prices, when empty then it means that calculation was not possible
     */
    public Optional<List<CalculatedPrice>> calculatePrice(ProductId productId, Amount amount, OfferTermType offerTermType) {

        Optional<AWSProductPricingService.ProductWithPricing> maybeProductWithPricing = productPricingService.findProduct(productId);
        return maybeProductWithPricing.map(productWithPricing -> calculatePrice(productWithPricing, amount, offerTermType));
    }

    /**
     * Scans all offers and offer rates for matching records and calculate prices according to given parameters.
     *
     * @param product
     * @param amount
     * @param offerTermType
     * @return
     */
    List<CalculatedPrice> calculatePrice(AWSProductPricingService.ProductWithPricing product, Amount amount, OfferTermType offerTermType) {
        return product.getProductPricingRecord().getOffers().stream()
                .filter(offer -> offerTermType.equals(offer.getOfferTermType()))
                .flatMap(offer -> offer.getRates().stream().filter(rate -> amount.getUnit().equals(rate.getUnit())).map(rate -> calculatePriceForRate(offer, rate, amount)))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

    }

    /**
     * @param offer
     * @param rate
     * @param amount
     * @return calculated prices for each of currency codes defined in rate. Empty when value is not in range specified in rate.
     */
    private List<CalculatedPrice> calculatePriceForRate(AWSOffersTermsRecord offer, AWSOfferRate rate, Amount amount) {
        if (isAmountValueInsideRateRange(rate, amount) && amount.getUnit().equals(rate.getUnit())) {
            return rate.getPricePerUnitByCurrency().entrySet().stream().map(currencyValuePrice ->
                    new CalculatedPrice(calculatePrice(amount, currencyValuePrice),
                            rate.getExternalRateCodeId(), offer.getTermAttributesRaw(),
                            new PricingOption(offer.getOfferTermType(), rate.getUnit(), offer.getLeaseContractLength().orElse(null)))
            ).collect(Collectors.toList());
        } else {
            return Collections.emptyList();
        }
    }

    private Price calculatePrice(Amount amount, Map.Entry<String, BigDecimal> currencyValuePrice) {
        return new Price(currencyValuePrice.getValue().multiply(new BigDecimal(amount.getQuantity())), currencyValuePrice.getKey());
    }

    private boolean isAmountValueInsideRateRange(AWSOfferRate rate, Amount amount) {
        if (rate.getBeginRange() == null) {
            return true;
        }

        Double beginRange = Doubles.tryParse(rate.getBeginRange());
        Double endRange = (rate.getEndRange() != null && !Ascii.equalsIgnoreCase(rate.getEndRange(), "Inf"))
                ? Doubles.tryParse(rate.getEndRange()) : null;
        return (beginRange == null || (beginRange <= amount.getQuantity()))
                && (endRange == null || (endRange > amount.getQuantity()));
    }

    /**
     * Calculated price and its context.
     */
    @Data
    public static class CalculatedPrice {

        private final Price price;

        private final String externalRateCodeId;

        private final Map<String, String> rateAttributes;

        private final PricingOption pricingOption;


    }

}
