package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents item which connects offers and pricing
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AzureProductPricingItem {

    /**
     * id from pricing
     */
    private String meterId;

    /**
     * This field can contain 'Low Priority' string or not (e.g. "A3 Low Priority" or "A3")
     */
    private String meterName;

    /**
     * This field can contain 'Windows' string or not (e.g. "Virtual Machines A Series Windows"
     * or "Virtual Machines A Series")
     */
    private String productName;

    private String productId;
    /**
     * Type of product
     */
    private String type;
}
