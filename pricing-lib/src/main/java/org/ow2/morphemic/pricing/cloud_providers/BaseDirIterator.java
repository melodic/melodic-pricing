package org.ow2.morphemic.pricing.cloud_providers;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Base class for all importers which traverse directory structure.
 */
@NoArgsConstructor
@Slf4j
public abstract class BaseDirIterator {

    /**
     * Queue which contains all files and directories to process.
     */
    @Getter(AccessLevel.PROTECTED)
    private final Queue<File> filesToProcess = new LinkedList<>();

    /**
     * Directories which must be deleted at the end of import.
     */
    @Getter(AccessLevel.PROTECTED)
    private final List<File> dirsToDelete = new ArrayList<>();

    @Getter(AccessLevel.PROTECTED)
    private final List<File> additionalFilesToDelete = new ArrayList<>();


    /**
     * By default all processed files must be removed.
     */
    @Getter
    @Setter
    private boolean deleteProcessedFiles = true;

    public BaseDirIterator(File scanDirRoot, boolean deleteProcessedFiles) {
        File[] files = scanDirRoot.listFiles();
        if (files != null) {
            filesToProcess.addAll(acceptOnlyDirsAndJsonFiles(files));
        }
        this.deleteProcessedFiles = deleteProcessedFiles;
    }

    /**
     * Filtering files and dirs to process.
     *
     * @param files
     * @return
     */
    protected List<File> acceptOnlyDirsAndJsonFiles(File[] files) {
        return Stream.of(files).filter(f -> f.isDirectory() || (f.isFile() && f.getName().endsWith(".json"))).collect(Collectors.toList());
    }

    protected void addFileToDelete(File f) {
        additionalFilesToDelete.add(f);
    }

    /**
     * Is queue empty? If empty, then it will also remove processed files (if allowed).
     *
     * @return true; queue is not empty
     */
    public boolean hasMoreElementsToProcess() {
        boolean hasMore = !filesToProcess.isEmpty();
        if (!hasMore) {
            if (deleteProcessedFiles) {
                log.info("Processed all files: processed ones will be removed.");
                for (File f : additionalFilesToDelete) {
                    log.info("Deleted file {} result: {}", f.getAbsolutePath(), f.delete());
                }
                for (File d : dirsToDelete) {
                    log.info("Deleting dir {}", d.getAbsolutePath());
                    if (!d.delete()) {
                        log.warn("Cannot delete {}", d.getAbsolutePath());
                    }
                }
            } else {
                log.info("Processed all files: processed ones will NOT be removed.");
            }
        }
        return hasMore;
    }

    protected void processDirectory(File dir) {
        dirsToDelete.add(dir);
        File[] newFiles = dir.listFiles();
        if (newFiles != null) {
            filesToProcess.addAll(acceptOnlyDirsAndJsonFiles(newFiles));
        }
    }
}
