package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Repository used to store and query raw Aws savings plans pricing data.
 */
interface AWSRawSavingsPlansPricingDataRepository extends MongoRepository<AWSRawSavingsPlansPricingDataRecord, String> {

    /**
     * Removes all records associated with given pricing.
     *
     * @param cloudEnvironmentPricingId pricing id
     * @return removed records count.
     */
    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId);

    List<AWSRawSavingsPlansPricingDataRecord> findByCloudEnvironmentPricingIdAndVersionNum(
            CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum);

    List<AWSRawSavingsPlansPricingDataRecord> findByCloudEnvironmentPricingIdAndVersionNumAndSortPosNumGreaterThanOrderBySortPosNum(
            CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum, Long lastSortPosNum, Pageable page);

    List<AWSRawSavingsPlansPricingDataRecord> findByCloudEnvironmentPricingIdAndVersionNumOrderById(
            CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum, Pageable page);
}
