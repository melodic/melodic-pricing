package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Pricing rate for single product pricing offer.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TypeAlias("AWSOfferRate")
public class AWSOfferRate {

    @Field(targetType = FieldType.STRING)
    private Unit unit;

    private String beginRange;

    private String endRange;

    private Map<String, BigDecimal> pricePerUnitByCurrency;

    private String externalRateCodeId;

    private String description;

    @Builder.Default
    private Map<String, String> additionalProperties = new HashMap<>();


}
