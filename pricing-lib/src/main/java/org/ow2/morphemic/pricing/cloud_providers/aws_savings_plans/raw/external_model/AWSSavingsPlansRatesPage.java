package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Represents response from Aws savings plans pages.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSSavingsPlansRatesPage {

    /**
     * Region for savings plans
     */
    @JsonProperty("regionCode")
    private String regionCode;

    /**
     * Current version.
     */
    @JsonProperty("version")
    private String version;

    /**
     * Date of publication.
     */
    @JsonProperty("publicationDate")
    private String publicationDate;

    /**
     * All savings plans.
     */
    @JsonProperty("products")
    private List<AWSSavingsPlansProduct> products;

    /**
     * All savings plans rates.
     */
    @JsonProperty("terms")
    private AWSSavingsPlansTerms terms;
}
