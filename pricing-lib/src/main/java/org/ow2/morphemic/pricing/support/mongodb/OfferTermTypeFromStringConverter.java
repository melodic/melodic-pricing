package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.OfferTermType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

/**
 * Used to read value from mongo string field.
 * Helps to read {@link OfferTermType} saved as text.
 */
@ReadingConverter
class OfferTermTypeFromStringConverter implements Converter<String, OfferTermType> {

    @Override
    public OfferTermType convert(String source) {
        return new OfferTermType(source);
    }
}
