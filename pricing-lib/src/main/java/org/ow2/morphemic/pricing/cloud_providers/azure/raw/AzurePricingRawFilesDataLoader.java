package org.ow2.morphemic.pricing.cloud_providers.azure.raw;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.ow2.morphemic.pricing.cloud_providers.BaseDirIterator;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Loads RAW pricing AZURE data from given dump dirctory.
 */
@Slf4j
@Service
public class AzurePricingRawFilesDataLoader extends AbstractAzurePricingRawDataLoader {

    @Value("${pricing.datadump.azure.dir:/tmp/pricing/azure}")
    private String azureDumpDir;

    public AzurePricingRawFilesDataLoader(AzureRawPricingDataRepository azureRawPricingDataRepository,
                                          DataVersionsService dataVersionsService) {
        super(azureRawPricingDataRepository, dataVersionsService);
    }

    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    @Transactional
    public Optional<VersionedCloudEnvPricingId> loadDumpToRawCache(PricingDataImportStatisticCollector pricingDataImportStatistic) throws IOException {
        File azureDumpDir = new File(this.azureDumpDir);
        boolean created = azureDumpDir.mkdirs();
        if (created) {
            log.info("Dump dir {} has been created.", azureDumpDir);
        }

        File okFile = new File(azureDumpDir, ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME);
        if (okFile.isFile()) {
            ObjectMapper objMapper = new ObjectMapper();

            log.debug("Found ok file.");
            String clPricingIdAsText = Files.readString(okFile.toPath(), StandardCharsets.UTF_8).trim();
            if (StringUtils.isNotBlank(clPricingIdAsText)) {
                CloudEnvironmentPricingId clEnvPricingId = new CloudEnvironmentPricingId(clPricingIdAsText);
                log.info("Loading raw data for cloud env pricing: {}", clEnvPricingId);
                VersionNum newVersionNum = new VersionNum();
                AzureDumpDirIterator azureDumpDirIterator = new AzureDumpDirIterator(azureDumpDir, true, okFile);

                AtomicInteger savedRawPages = new AtomicInteger();
                AtomicInteger errors = new AtomicInteger();
                Flux.fromIterable(() -> azureDumpDirIterator)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .doOnNext(json -> {
                            try {
                                AzureRawPricingResponse rawPricingData = objMapper.readValue(json, AzureRawPricingResponse.class);
                                pricingDataImportStatistic.resumeMeasureTimeOfSaveDataIntoDatabase();
                                saveItemsFromOnePage(rawPricingData, newVersionNum, clEnvPricingId);
                                pricingDataImportStatistic.suspendMeasureTimeOfSaveDataIntoDatabase();
                                int savedCount = savedRawPages.incrementAndGet();
                                if (savedCount % 100 == 0) {
                                    log.debug("Saved {} pages with raw AZURE pricing data.", savedCount);
                                }
                            } catch (JsonProcessingException e) {
                                log.error("Cannot deserialize! " + e.getMessage(), e);
                                log.trace("Contents: {}", json);
                                errors.incrementAndGet();
                            }
                        }).count().block();

                log.info("Saved {} pages. Errors detected {}.", savedRawPages.get(), errors.get());

                if (savedRawPages.get() > 0) {
                    registerNewVersionAndClean(newVersionNum, clEnvPricingId, pricingDataImportStatistic);
                }

                return Optional.of(new VersionedCloudEnvPricingId(clEnvPricingId, newVersionNum));

            } else {
                log.error("Cloud environment pricing id must not be blank in '.ok' file!");
            }


        }
        return Optional.empty();
    }


    private static final class AzureDumpDirIterator extends BaseDirIterator implements Iterator<Optional<String>> {
        private AzureDumpDirIterator(File scanDirRoot, boolean deleteProcessedFiles, File okFile) {
            super(scanDirRoot, deleteProcessedFiles);
            addFileToDelete(okFile);
        }


        @Override
        public boolean hasNext() {
            return hasMoreElementsToProcess();
        }

        /**
         * Reads next file fetched from queue or reads contents of directory.
         *
         * @return null for "file" which is not a typical file or file contents. Null values must be filtered out.
         */
        @Override
        public Optional<String> next() {
            File f = getFilesToProcess().poll();
            if (f != null) {
                if (f.isDirectory()) {
                    processDirectory(f);
                } else if (f.isFile()) {
                    if (f.getName().endsWith(".json")) {
                        try {
                            String json = Files.readString(f.toPath(), StandardCharsets.UTF_8);
                            if (!f.delete()) {
                                log.warn("Cannot delete file {}", f.getAbsolutePath());
                            }
                            return Optional.ofNullable(json);
                        } catch (IOException e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                }
            } else {
                throw new NoSuchElementException("No more files to process!");
            }

            return Optional.empty();
        }

    }
}
