package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableInt;
import org.ow2.morphemic.pricing.cloud_providers.PricingIdOnlyDbRowProjection;
import org.ow2.morphemic.pricing.cloud_providers.ProductIdOnlyDbRowProjection;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws.raw.AWSRawPricingDataManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides consistent access to products and pricing data.
 * May be used to transfer data to other systems or to provide data for price calculator.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AWSProductPricingService {

    static final CollectionName VERSIONED_AWS_PRICING_COLLECTION_NAME = new CollectionName("aws:pricings:" + CollectionName.TEMPLATE);

    private final AWSProductRecordsRepository awsProductRecordsRepository;

    private final AWSProductPricingRecordsRepository awsProductPricingRecordsRepository;

    private final DataVersionsService dataVersionsService;

    private final AWSRawPricingDataManager awsRawPricingDataManager;

    private final ProductsMetadataService productsMetadataService;

    /**
     * Returns all product ids for newest data version.
     *
     * @param cloudEnvironmentPricingId all products for that cloud env
     * @return non null set of product ids
     */
    @Transactional
    public Set<ProductId> allProductIds(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        Optional<VersionNum> maybeVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId,
                VERSIONED_AWS_PRICING_COLLECTION_NAME);

        if (maybeVersion.isPresent()) {
            return awsProductRecordsRepository.findAllByCloudEnvironmentPricingIdAndBasedOnRawVersionNum(cloudEnvironmentPricingId,
                    maybeVersion.get(), ProductIdOnlyDbRowProjection.class)
                    .map(ProductIdOnlyDbRowProjection::getId)
                    .collect(Collectors.toSet());
        } else {
            return Collections.emptySet();
        }
    }

    /**
     * Return latest version of product in given family name. Simple product search.
     *
     * @param cloudEnvironmentPricingId environment
     * @param productFamilyName         product family name (AWS value).
     * @return set of found product ids; never null.
     */
    public Set<ProductId> findProductsByFamilyName(CloudEnvironmentPricingId cloudEnvironmentPricingId, String productFamilyName) {
        Optional<VersionNum> maybeVersion = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId,
                VERSIONED_AWS_PRICING_COLLECTION_NAME);

        if (StringUtils.isBlank(productFamilyName)) {
            throw new IllegalArgumentException("Product family name must not be blank! Current value is: " + productFamilyName);
        }

        if (maybeVersion.isPresent()) {
            return awsProductRecordsRepository.findAllByCloudEnvironmentPricingIdAndBasedOnRawVersionNumAndProductFamily(cloudEnvironmentPricingId,
                    maybeVersion.get(),
                    productFamilyName,
                    ProductIdOnlyDbRowProjection.class)
                    .map(ProductIdOnlyDbRowProjection::getId)
                    .collect(Collectors.toSet());

        } else {
            return Collections.emptySet();
        }
    }

    /**
     * Loads product and its pricing data.
     *
     * @param productId unique aws product id for some cloud env
     * @return Aggregate object with product and its all prices params. empty when one of igredients is unavailable.
     */
    @Transactional
    public Optional<ProductWithPricing> findProduct(ProductId productId) {
        Optional<AWSProductRecord> maybeProduct = awsProductRecordsRepository.findById(productId);
        Optional<AWSProductPricingRecord> maybePricing = awsProductPricingRecordsRepository.findById(productId);

        if (maybePricing.isPresent() && maybeProduct.isPresent()) {
            return Optional.of(new ProductWithPricing(productId, maybeProduct.get(), maybePricing.get()));
        } else {
            return Optional.empty();
        }
    }

    /**
     * To be used only by tests.
     */
    @Transactional
    public void deleteAll() {
        awsRawPricingDataManager.deleteAll();
        awsProductRecordsRepository.findDistinctBy(PricingIdOnlyDbRowProjection.class)
                .map(PricingIdOnlyDbRowProjection::getCloudEnvironmentPricingId).distinct()
                .forEach(dataVersionsService::removeAllVersions);
        awsProductRecordsRepository.findAllBy(ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId).forEach(productsMetadataService::removeProduct);
        awsProductPricingRecordsRepository.deleteAll();
        awsProductRecordsRepository.deleteAll();
        log.info("Removed all AWS products and pricing data.");
    }

    /**
     * Removes ALL data associated with given AWS pricing including RAW pricing data.
     *
     * @param pricingId pricing id.
     */
    @Transactional
    public void deletePricing(CloudEnvironmentPricingId pricingId) {
        log.debug("AWS pricing data will be deleted {}", pricingId);
        dataVersionsService.removeAllVersions(pricingId);
        awsRawPricingDataManager.deletePricingData(pricingId);

        MutableInt deletedProductsMetadataCount = new MutableInt();
        awsProductRecordsRepository.findAllByCloudEnvironmentPricingId(pricingId, ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId).distinct().peek(prodId -> {
            deletedProductsMetadataCount.increment();
        }).forEach(productsMetadataService::removeProduct);

        long deletedPricingRecordsCount = awsProductPricingRecordsRepository.deleteAllByCloudEnvironmentPricingId(pricingId);
        long deletedProductsRecordsCount = awsProductRecordsRepository.deleteAllByCloudEnvironmentPricingId(pricingId);

        log.info("Deleted all AWS pricing and product data for pricing {}. Deleted products: {} and pricing records {}.", pricingId, deletedProductsRecordsCount, deletedPricingRecordsCount);
    }

    @Transactional
    public Set<CloudEnvironmentPricingId> getAllPricingIds() {
        return null; // TODO
    }

    @Data
    public static class ProductWithPricing {
        private final ProductId productId;
        private final AWSProductRecord productRecord;
        private final AWSProductPricingRecord productPricingRecord;
    }

}
