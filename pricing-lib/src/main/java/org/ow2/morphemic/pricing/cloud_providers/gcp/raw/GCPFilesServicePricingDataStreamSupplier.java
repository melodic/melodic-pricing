package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.cloudbilling.model.Service;
import com.google.api.services.cloudbilling.model.Sku;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.ow2.morphemic.pricing.cloud_providers.BaseDirIterator;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import reactor.core.publisher.Flux;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;

/**
 * Loads data from file cache. After import all processed files and dirs are removed.
 */
@Slf4j
@RequiredArgsConstructor
public class GCPFilesServicePricingDataStreamSupplier implements GCPServicePricingDataStreamSupplier {
    static final String SKU_FILE_NAME_PREFIX = "sku_";
    static final String SERVICE_JSON_FILE_NAME = "service.json";

    private final File scanDirRoot;

    @Getter
    @Setter
    private String cloudEnvIdFileName = FS_LOADER_OK_FILE_NAME;

    @Setter
    @Getter
    private boolean deleteProcessedFiles = true;

    GCPFilesServicePricingDataStreamSupplier(File scanDirRoot, boolean deleteProcessedFiles) {
        this.scanDirRoot = scanDirRoot;
        this.deleteProcessedFiles = deleteProcessedFiles;
        log.info("OK file: {}", cloudEnvIdFileName);
    }

    /**
     * @return deserialized structure describing GCP service and its SKUs.
     * @throws IOException
     */
    @Override
    public Flux<ServiceWithSkusPrices> createDataStream() throws IOException {
        Optional<CloudEnvironmentPricingId> maybeCloudEnvId = getCloudEnvironmentPricingId();
        if (maybeCloudEnvId.isPresent()) {
            log.debug("ok file found. Cloud environment ID: {}. W will load stream of data...", maybeCloudEnvId.get());
            return Flux.fromIterable(() -> new DirIterator(deleteProcessedFiles))
                    .filter(Optional::isPresent).map(Optional::get);
        } else {
            return Flux.empty();
        }
    }

    /**
     * Tries to determine id of cloud environment loading contents of {@link #cloudEnvIdFileName}.
     *
     * @return id when file found
     */
    @Override
    public Optional<CloudEnvironmentPricingId> getCloudEnvironmentPricingId() {
        try {
            File okFile = okFile();
            if (okFile.isFile()) {
                String id = Files.readString(okFile.toPath(), StandardCharsets.UTF_8).trim();
                if (!id.isBlank()) {
                    return Optional.of(new CloudEnvironmentPricingId(id));
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    /**
     * Ok file contains id of cloud environment AND means that data is ready to import.
     *
     * @return 'ok" marker file
     */
    @SuppressFBWarnings("WEAK_FILENAMEUTILS")
    private File okFile() {
        return new File(scanDirRoot, FilenameUtils.getName(cloudEnvIdFileName));
    }

    /**
     * Iterates through all files and sub directories. Removes processed files and directories.
     */
    private final class DirIterator extends BaseDirIterator implements Iterator<Optional<ServiceWithSkusPrices>> {

        private final JsonFactory jsonFactory;

        private DirIterator(boolean deleteProcessedFiles) {
            super(scanDirRoot, deleteProcessedFiles);
            this.jsonFactory = new JacksonFactory();
            addFileToDelete(okFile());
        }

        @Override
        public boolean hasNext() {
            return hasMoreElementsToProcess();
        }

        /**
         * Reads next file fetched from queue or reads contents of directory.
         *
         * @return null for "file" which is not a typical file or file contents. Null values must be filtered out.
         */
        @Override
        public Optional<ServiceWithSkusPrices> next() {
            File f = getFilesToProcess().poll();
            if (f != null) {
                if (f.isDirectory()) {
                    return readServiceAndItsSkusDataFromDirectory(f);
                } else if (f.isFile()) {
                    if (!f.delete()) {
                        log.warn("Cannot delete file {}", f.getAbsolutePath());
                    }
                    return Optional.empty();
                }
            } else {
                throw new NoSuchElementException("No more files to process!");
            }

            return Optional.empty();
        }

        private Optional<ServiceWithSkusPrices> readServiceAndItsSkusDataFromDirectory(File dir) {
            processDirectory(dir);

            try {
                Service gcpService = null;
                List<Sku> gcpServiceSkus = new ArrayList<>();

                for (File f : acceptOnlyDirsAndJsonFiles(dir.listFiles())) {
                    if (f.isFile()) {
                        String json = Files.readString(f.toPath(), StandardCharsets.UTF_8);
                        if (SERVICE_JSON_FILE_NAME.equals(f.getName())) {
                            gcpService = jsonFactory.fromString(json, Service.class);
                            gcpService.setFactory(jsonFactory);
                        } else if (f.getName().startsWith(SKU_FILE_NAME_PREFIX)) {
                            Sku sku = jsonFactory.fromString(json, Sku.class);
                            sku.setFactory(jsonFactory);
                            gcpServiceSkus.add(sku);
                        }
                    }
                }

                return gcpService != null ? Optional.of(new ServiceWithSkusPrices(gcpService, gcpServiceSkus)) : Optional.empty();
            } catch (IOException e) {
                log.error("Cannot read: " + e.getMessage(), e);
                return Optional.empty();
            }


        }
    }

}
