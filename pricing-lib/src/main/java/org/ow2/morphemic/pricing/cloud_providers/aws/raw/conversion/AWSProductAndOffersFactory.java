package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.DateToStringConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSOfferRate;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSOffersTermsRecord;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingRecord;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductRecord;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Can convert raw data to domain objects describing product and its prices.
 * Conversion is split to many steps, because raw data needs to be properly structured and normalized.
 */
@Component
@Slf4j
public class AWSProductAndOffersFactory {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MapperFactory mapperFactory;

    AWSProductAndOffersFactory() {
        mapperFactory = new DefaultMapperFactory.Builder().build();
        mapperFactory.getConverterFactory().registerConverter(new DateToStringConverter("yyyy-MM-dd'T'HH:mm:ss'Z'"));

        mapperFactory.classMap(AWSIntermediateProductAndTerms.class, AWSProductRecord.class)
                .field("product.sku", "skuId")
                .field("product.attributes", "attrsRaw")
                .field("serviceCode", "serviceCode")
                .field("product.productFamily", "productFamily")
                .register();

        mapperFactory.classMap(AWSIntermediatePriceDimensionsRates.class, AWSOffersTermsRecord.class)
                .field("sku", "productSkuId")
                .field("offerTermCode", "externalOfferTermId")
                .field("termAttributes", "termAttributesRaw")
                .field("effectiveDate", "effectiveDate")
                .register();

        mapperFactory.classMap(AWSItermediateBaseTerm.class, AWSOfferRate.class)
                .field("pricePerUnit", "pricePerUnitByCurrency")
                .byDefault()
                .register();
    }

    /**
     * Creates aggregate describing product and its procing offer using JSON data from AWS Pricing API.
     *
     * @param json                       raw json fetched from AWS
     * @param versionedCloudEnvPricingId version of source data for given cloud environment
     * @return
     */
    public Optional<ProductAndOffers> fromJson(String json, VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        try {
            AWSIntermediateProductAndTerms awsIntermediateProductAndTerms = objectMapper.readValue(json, AWSIntermediateProductAndTerms.class);

            MapperFacade conv = mapperFactory.getMapperFacade();
            AWSProductRecord awsProductRecord = createProductRecord(versionedCloudEnvPricingId, conv,
                    awsIntermediateProductAndTerms);
            normalizeProduct(awsProductRecord);

            List<AWSOffersTermsRecord> offers = new ArrayList<>();
            for (Map.Entry<String, AWSIntermediateTermRates> termCodeAndValue : awsIntermediateProductAndTerms.getTerms().entrySet()) {
                final String offerType = termCodeAndValue.getKey();
                AWSIntermediateTermRates termValue = termCodeAndValue.getValue();

                for (Map.Entry<String, AWSIntermediatePriceDimensionsRates> termValueCodeAndValue : termValue.entrySet()) {
                    AWSOffersTermsRecord singleOffer = AWSOffersTermsRecord.builder()
                            .externalOfferTermId(termValueCodeAndValue.getKey())
                            .offerTermType(new OfferTermType(offerType))
                            .productSkuId(awsProductRecord.getSkuId()).build();
                    AWSIntermediatePriceDimensionsRates intermediatePriceRates = termValueCodeAndValue.getValue();
                    conv.map(intermediatePriceRates, singleOffer);

                    normalizeOffer(awsProductRecord, singleOffer);

                    singleOffer.setRates(intermediatePriceRates.getAwsDimensionRates().values().stream()
                            .flatMap(d -> d.entrySet().stream())
                            .map(e -> {
                                AWSOfferRate offerRate = AWSOfferRate.builder().externalRateCodeId(e.getKey()).build();
                                conv.map(e.getValue(), offerRate);
                                return offerRate;
                            })
                            .peek(offerRate -> normalizeOfferRate(awsProductRecord, singleOffer, offerRate))
                            .collect(Collectors.toList()));

                    offers.add(singleOffer);
                }
            }

            AWSProductPricingRecord awsProductPricing = AWSProductPricingRecord.builder()
                    .id(awsProductRecord.getId())
                    .productSkuId(awsProductRecord.getSkuId())
                    .cloudEnvironmentPricingId(versionedCloudEnvPricingId.getCloudEnvironmentPricingId())
                    .basedOnRawVersionNum(versionedCloudEnvPricingId.getVersionNum())
                    .offers(offers)
                    .build();

            return Optional.of(new ProductAndOffers(awsProductRecord, awsProductPricing));
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            return Optional.empty();
        }

    }

    /**
     * Normalization and postprocessing of product data.
     *
     * @param awsProductRecord
     */
    private void normalizeProduct(AWSProductRecord awsProductRecord) {
        // TODO normalize product here
    }

    /**
     * Normalize offer: postprocessing values, attributes etc.
     *
     * @param awsProductRecord context: product
     * @param singleOffer      this offer which should be normalized
     */
    private void normalizeOffer(AWSProductRecord awsProductRecord, AWSOffersTermsRecord singleOffer) {
        // TODO normalzie offer here
    }

    /**
     * It is used to normalize and postprocess offer rate values.
     *
     * @param awsProductRecord context: product data
     * @param singleOffer      context: whole offer
     * @param offerRate        this offer rate (to be modified).
     */
    private void normalizeOfferRate(AWSProductRecord awsProductRecord, AWSOffersTermsRecord singleOffer, AWSOfferRate offerRate) {
        // TODO add normalizers here

    }

    private AWSProductRecord createProductRecord(VersionedCloudEnvPricingId versionedCloudEnvPricingId, MapperFacade conv,
                                                 AWSIntermediateProductAndTerms awsIntermediateProductAndTerms) {
        AWSProductRecord awsProductRecord = conv.map(awsIntermediateProductAndTerms, AWSProductRecord.class);
        awsProductRecord.setBasedOnRawVersionNum(versionedCloudEnvPricingId.getVersionNum());
        awsProductRecord.setCloudEnvironmentPricingId(versionedCloudEnvPricingId.getCloudEnvironmentPricingId());
        awsProductRecord.setId(new ProductId(versionedCloudEnvPricingId.getCloudEnvironmentPricingId(),
                awsIntermediateProductAndTerms.getProduct().getSku()));
        return awsProductRecord;
    }


    /**
     * Contains product data and price offers as a result of parsing JSON with AWS pricing data.
     */
    @RequiredArgsConstructor
    @Data
    public static class ProductAndOffers {
        private final AWSProductRecord awsProductRecord;

        private final AWSProductPricingRecord awsProductPricing;
    }
}
