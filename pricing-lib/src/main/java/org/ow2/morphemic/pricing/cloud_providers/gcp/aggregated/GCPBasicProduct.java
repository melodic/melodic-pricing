package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@Data
@TypeAlias("TYPE_GCPBasicProduct")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GCPBasicProduct {
    @JsonProperty("resourceFamily")
    private String resourceFamily;

    @JsonProperty("resourceGroup")
    private String resourceGroup;

    @JsonProperty("description")
    private String description;

    @Field(targetType = FieldType.STRING)
    private ProductId productId;

    @JsonProperty("skuId")
    private String skuId;

    @JsonProperty("regions")
    private Set<Place> regions = new HashSet<>();

    @JsonProperty("offerTermType")
    private OfferTermType offerTermType;
}
