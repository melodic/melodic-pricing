package org.ow2.morphemic.pricing;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderPricingStatsService;
import org.ow2.morphemic.pricing.cloud_providers.CloudProviderStats;
import org.ow2.morphemic.pricing.cloud_providers.PricingStats;
import org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.azure.AzureProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Global management operations for registered pricings.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class PricingManagementService {

    private final AWSProductPricingService awsProductPricingService;
    private final AzureProductPricingService azureProductPricingService;

    private final GCPProductPricingService gcpProductPricingService;
    private final GCPAggregatedProductsService gcpAggregatedProductsService;

    private final List<CloudProviderPricingStatsService> pricingStatsServices;

    /**
     * Removes given pricing: its versions, raw data, cached data. Everything.
     *
     * @param pricingId pricing id
     */
    @Transactional
    public void removePricing(CloudEnvironmentPricingId pricingId) {
        awsProductPricingService.deletePricing(pricingId);
        azureProductPricingService.deletePricing(pricingId);

        gcpAggregatedProductsService.deletePricing(pricingId);
        gcpProductPricingService.deletePricing(pricingId);

    }

    /**
     * Calculates and fetches pricing statistics for all cloud providers.
     *
     * @return pricing statistics.
     */
    @Transactional
    public List<PricingStats> fetchPricingStats() {
        return pricingStatsServices.parallelStream().map(CloudProviderPricingStatsService::calculatePricingStats)
                .flatMap(Collection::stream).collect(Collectors.toList());
    }

    /**
     * Calculates cloud providers stats.
     *
     * @return stats.
     */
    @Transactional
    public List<CloudProviderStats> fetchCloudProviderStats() {
        return pricingStatsServices.parallelStream().map(CloudProviderPricingStatsService::calculateCloudProviderStats)
                .collect(Collectors.toList());
    }
}
