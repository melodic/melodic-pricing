package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import com.google.common.annotations.VisibleForTesting;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansProduct;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.SynchronousSink;

import java.util.List;

/**
 * Provides access to aws savings plans products from public pricing.
 * Enables to get ids required to connect aws savings plans offers.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AWSRawSavingsPlansManager {

    private final AWSRawSavingsPlansPricingDataRepository awsRawSavingsPlansPricingDataRepository;

    /**
     * Flux stream of records describing raw pricing Savings Plans data.
     *
     * @param versionedCloudEnvPricingId for this versioned cloud env. pricing
     * @return stream of non-null elements.
     */
    public Flux<AWSRawSavingsPlansPricingDataRecord> rawPricingDataAsFluxForVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();
        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();

        // trick: always 1st page, because we will always return values > than prefious last record id.
        PageRequest page = PageRequest.of(0, 100, Sort.by(Sort.Direction.ASC, "sortPosNum"));
        return fluxOfRawSavingsPlansPricingDataRecords(versionNum, cloudEnvironmentPricingId, page)
                .flatMap(Flux::fromIterable)
                .filter(rawProdingRecord -> rawProdingRecord.getRawRates() != null);
    }

    /**
     * Flux stream of records describing raw product data in Savings Plans data.
     *
     * @param versionedCloudEnvPricingId for this versioned cloud env. pricing
     * @return stream of non-null elements.
     */
    public Flux<AWSSavingsPlansProduct> rawProductDataAsFluxForVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        VersionNum versionNum = versionedCloudEnvPricingId.getVersionNum();
        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();

        // trick: always 1st page, because we will always return values > than prefious last record id.
        PageRequest page = PageRequest.of(0, 100, Sort.by(Sort.Direction.ASC, "sortPosNum"));
        return fluxOfRawSavingsPlansPricingDataRecords(versionNum, cloudEnvironmentPricingId, page)
                .flatMap(Flux::fromIterable)
                .filter(r -> r.getRawProduct() != null)
                .map(AWSRawSavingsPlansPricingDataRecord::getRawProduct);
    }

    private Flux<List<AWSRawSavingsPlansPricingDataRecord>> fluxOfRawSavingsPlansPricingDataRecords(VersionNum versionNum,
                                                                                                    CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                                                                    PageRequest page) {
        return Flux.generate(() -> Long.MIN_VALUE, (Long lastSortPosNum, SynchronousSink<List<AWSRawSavingsPlansPricingDataRecord>> sink) -> {
            List<AWSRawSavingsPlansPricingDataRecord> rawSavingsPlans = awsRawSavingsPlansPricingDataRepository
                    .findByCloudEnvironmentPricingIdAndVersionNumAndSortPosNumGreaterThanOrderBySortPosNum(cloudEnvironmentPricingId, versionNum,
                            lastSortPosNum, page);
            if (rawSavingsPlans.isEmpty()) {
                sink.complete();
                return Long.MAX_VALUE;
            } else {
                AWSRawSavingsPlansPricingDataRecord last = rawSavingsPlans.get(rawSavingsPlans.size() - 1);
                sink.next(rawSavingsPlans);
                return last.getSortPosNum();
            }
        });
    }

    @Transactional
    @VisibleForTesting
    public void deleteAll() {
        awsRawSavingsPlansPricingDataRepository.deleteAll();
        log.info("Removed all savings plans data from db cache.");
    }
}
