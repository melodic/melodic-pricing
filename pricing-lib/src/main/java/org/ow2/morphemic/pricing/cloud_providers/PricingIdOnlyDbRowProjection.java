package org.ow2.morphemic.pricing.cloud_providers;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;

/**
 * DB row projection: fetches only pricing id.
 */
public interface PricingIdOnlyDbRowProjection {

    CloudEnvironmentPricingId getCloudEnvironmentPricingId();

}
