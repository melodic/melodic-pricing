package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.services.cloudbilling.model.Sku;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.WithCloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides pricing data as raw text data for given list of GCP services (products).
 */
@Slf4j
@RequiredArgsConstructor
public class GCPPricingRawDataLoader {

    private final List<GCPServicePricingDataStreamSupplier> dataSuppliers;

    private final GCPRawServiceSKUDataRecordRepository rawServiceRecordsRepository;

    private final GCPRawSKURecordRepository gcpRawSKURecordRepository;

    private final DataVersionsService dataVersionsService;

    private boolean removeOldVersions = true;

    public GCPPricingRawDataLoader(List<GCPServicePricingDataStreamSupplier> dataSuppliers,
                                   GCPRawServiceSKUDataRecordRepository rawServiceRecordsRepository,
                                   GCPRawSKURecordRepository gcpRawSKURecordRepository,
                                   DataVersionsService dataVersionsService,
                                   boolean removeOldVersions) {
        this.dataSuppliers = dataSuppliers;
        this.rawServiceRecordsRepository = rawServiceRecordsRepository;
        this.gcpRawSKURecordRepository = gcpRawSKURecordRepository;
        this.dataVersionsService = dataVersionsService;
        this.removeOldVersions = removeOldVersions;
    }

    /**
     * Loads GCP pricing raw data to DB cache using defined collection of data suppliers {@link #dataSuppliers}.
     *
     * @return new version id if new data was stored in cache.
     */
    @Transactional
    public Optional<VersionedCloudEnvPricingId> loadRawDataToCache(PricingDataImportStatisticCollector pricingDataImportStatistic) throws IOException {
        if (dataSuppliers != null) {
            Set<CloudEnvironmentPricingId> cloudEnvironmentPricingIds = dataSuppliers.stream()
                    .map(WithCloudEnvironmentPricingId::getCloudEnvironmentPricingId)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toSet());
            if (cloudEnvironmentPricingIds.size() > 1) {
                throw new IllegalArgumentException("Data suppliers must fetch data for one cloud environment! "
                        + "But they are connected with multiple environments: " + cloudEnvironmentPricingIds);
            } else if (cloudEnvironmentPricingIds.size() == 1) {

                @AllArgsConstructor
                class GCPServiceWithSkuListToPersist {
                    private final GCPRawServiceSKUDataRecord rawService;
                    private final List<GCPRawSKURecord> rawSkuRecords;
                }

                final CloudEnvironmentPricingId cloudEnvironmentPricingId = cloudEnvironmentPricingIds.iterator().next();
                VersionNum newVersion = new VersionNum();

                long countRecordsAdded = 0L;
                for (GCPServicePricingDataStreamSupplier dataSupplier : dataSuppliers) {
                    Long countedRecords = dataSupplier.createDataStream()
                            .map(serviceWithSkus -> {
                                try {
                                    List<Sku> skuList = serviceWithSkus.getSkus();
                                    List<GCPRawSKURecord> skuRecordsToPersist = skuList.stream().map(sku -> {
                                        try {
                                            return new GCPRawSKURecord(sku);
                                        } catch (IOException e) {
                                            log.error("Cannot process SKU: " + e.getMessage(), e);
                                            log.info("Cannot process SKU: {}", sku.getSkuId());
                                            return null;
                                        }
                                    }).filter(Objects::nonNull).collect(Collectors.toList());
                                    GCPRawServiceSKUDataRecord rawService = new GCPRawServiceSKUDataRecord(cloudEnvironmentPricingId, newVersion, serviceWithSkus.getServiceDef(),
                                            skuRecordsToPersist);
                                    return rawService;
                                } catch (IOException e) {
                                    log.error("Error under pricing raw data load", e);
                                    return null;
                                }
                            }).filter(Objects::nonNull)
                            .doOnNext((rawServiceToPersist) -> {
                                pricingDataImportStatistic.resumeMeasureTimeOfSaveDataIntoDatabase(); //resume measure it as I want to measure total time for all records

                                gcpRawSKURecordRepository.saveAll(rawServiceToPersist.getRawSKURecords());
                                rawServiceRecordsRepository.save(rawServiceToPersist);

                                pricingDataImportStatistic.suspendMeasureTimeOfSaveDataIntoDatabase();
                                pricingDataImportStatistic.incrementTotalNumberOfSavedRowsIntoDatabase(rawServiceToPersist.getRawSKURecords().size()); //count skus as row is grouping element only
                            })
                            .count()
                            .block();
                    countRecordsAdded += countedRecords != null ? countedRecords : 0;
                }

                if (!pricingDataImportStatistic.isSuspendedMeasureTimeOfDataReadFromProvider()) {
                    pricingDataImportStatistic.suspendMeasureTimeOfDataReadFromProvider(); // stop measure of gcp data read
                }
                pricingDataImportStatistic.incrementTotalNumberOfSavedRowsIntoDatabase(countRecordsAdded);

                if (countRecordsAdded > 0L) {
                    VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, newVersion);
                    pricingDataImportStatistic.resumeMeasureTimeOfSaveDataIntoDatabase();
                    dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, GCPRawServiceSKUDataRecord.VERSION_COLLECTION_NAME);
                    pricingDataImportStatistic.suspendMeasureTimeOfSaveDataIntoDatabase();
                    if (removeOldVersions) {
                        pricingDataImportStatistic.resumeMeasureTimeOfDeletePricingData();

                        // at first remove ralated SKU records from saparate collection
                        Set<String> relatedSkuRecordsToDelete = rawServiceRecordsRepository.findAllByCloudEnvironmentPricingIdAndVersionLessThan(cloudEnvironmentPricingId, newVersion, RelatedRawSKURecordsProj.class).stream()
                                .filter(projection -> projection.getRawSKURecords() != null && !projection.getRawSKURecords().isEmpty())
                                .flatMap(projection -> projection.getRawSKURecords().stream())
                                .map(GCPRawSKURecord::getId)
                                .collect(Collectors.toSet());
                        if (!relatedSkuRecordsToDelete.isEmpty()) {
                            gcpRawSKURecordRepository.deleteAllByIdIn(relatedSkuRecordsToDelete);
                        }

                        // then remove "service" records.
                        long removedCount = rawServiceRecordsRepository.deleteAllByCloudEnvironmentPricingIdAndVersionLessThan(cloudEnvironmentPricingId, newVersion);

                        pricingDataImportStatistic.suspendMeasureTimeOfDeletePricingData();
                        log.debug("Removed {} old records from raw data cache", removedCount);
                    }

                    return Optional.of(versionedCloudEnvPricingId);
                }
            }
        } else {
            log.warn("No data suppliers for GCP raw data loader!");
        }

        return Optional.empty();
    }


}

