package org.ow2.morphemic.pricing.versions;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Describes "version" of some data. It is usually used to distinguish many datasets downloaded from the same datasource.
 */
@Document(collection = "versions")
@NoArgsConstructor
@Data
@TypeAlias("Version")
class VersionRecord {

    @Id
    private String id;

    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    private String collectionName;

    private long version;

    private Date created;

    private Date endOfValidity;

    VersionRecord(CloudEnvironmentPricingId cloudEnvironmentPricingId, String collectionName, VersionNum versionNum, Date created) {
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.collectionName = collectionName;
        this.version = versionNum.getNum();
        this.created = created;
        this.id = cloudEnvironmentPricingId + ":" + collectionName + ":" + version;
    }
}
