package org.ow2.morphemic.pricing.cloud_providers.aws;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * AWS pricing data repository
 */
@Repository
interface AWSProductPricingRecordsRepository extends MongoRepository<AWSProductPricingRecord, ProductId> {

    /**
     *
     * @param pricingId pricing id.
     * @return removed records count.
     */
    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId pricingId);



}
