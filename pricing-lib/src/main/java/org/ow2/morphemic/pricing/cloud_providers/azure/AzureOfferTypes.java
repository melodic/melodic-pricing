package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.Getter;
import org.ow2.morphemic.pricing.model.OfferType;

/**
 * Standard offer types for Azure.
 */
public enum AzureOfferTypes {
    CONSUMPTION(new OfferType("Consumption")),
    RESERVATION(new OfferType("Reservation")),
    DEV(new OfferType("DevTestConsumption"));

    AzureOfferTypes(OfferType o) {
        this.offerType = o;
    }

    @Getter
    private final OfferType offerType;
}
