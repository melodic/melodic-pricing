package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.cloudbilling.Cloudbilling;
import com.google.api.services.cloudbilling.model.ListServicesResponse;
import com.google.api.services.cloudbilling.model.ListSkusResponse;
import com.google.api.services.cloudbilling.model.Service;
import com.google.api.services.cloudbilling.model.Sku;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPServicesFetchingException;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import reactor.core.Exceptions;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Loads pricing data using GCP API directly form Google servers.
 */
@Slf4j
@RequiredArgsConstructor
public class GCPClientServicePricingDataStreamSupplier implements GCPServicePricingDataStreamSupplier {

    /**
     * Used to authorize in GCP API.
     */
    private final String apiKey;

    /**
     * Services ids for which SKUs pricing should be downloaded
     */
    private final Set<String> servicesIds;

    /**
     * Delay for services fetching in milliseconds
     */
    private final long servicesFetchingDelayMs;

    private final CloudEnvironmentPricingId cloudEnvironmentPricingId;

    private final String applicationName;

    private final PricingDataImportStatisticCollector pricingDataImportStatisticCollector;

    @Override
    public Flux<ServiceWithSkusPrices> createDataStream() throws IOException {
        return Flux.fromIterable(() -> {
            try {
                return new GCPIterator();
            } catch (IOException e) {
                throw Exceptions.bubble(e);
            }
        });
    }

    @Override
    public Optional<CloudEnvironmentPricingId> getCloudEnvironmentPricingId() {
        return Optional.of(cloudEnvironmentPricingId);
    }

    /**
     * For each service (product) reported by GCP API it returns stream of SKUs  and pricing data.
     */
    private final class GCPIterator implements Iterator<GCPServicePricingDataStreamSupplier.ServiceWithSkusPrices> {

        private static final String SERVICE_NAME_PREFIX = "services/";
        private final Iterator<Service> itGcpServices;
        private final Cloudbilling cloudBilling;
        private final ScheduledExecutorService delayExecutor;

        private GCPIterator() throws IOException {
            try {
                delayExecutor = Executors.newScheduledThreadPool(1);
                cloudBilling = createCloudBilling();
                if (servicesIds.isEmpty()) {
                    itGcpServices = fetchAllServices().getServices().iterator();
                } else {
                    itGcpServices = servicesIds.stream()
                            .map(this::createService)
                            .collect(Collectors.toList())
                            .iterator();
                }
            } catch (GeneralSecurityException e) {
                shutdownExecutor();
                throw new IOException(e);
            }
        }

        @Override
        public boolean hasNext() {
            boolean hasNext = itGcpServices.hasNext();
            if (!hasNext) {
                shutdownExecutor();
            }
            return hasNext;
        }

        @SneakyThrows
        @Override
        public GCPServicePricingDataStreamSupplier.ServiceWithSkusPrices next() {
            Service gcpService = itGcpServices.next();
            try {
                pricingDataImportStatisticCollector.resumeMeasureTimeOfDataReadFromProvider();
                AllSkusForService allSkusForService = fetchAllSkusForService(gcpService);
                pricingDataImportStatisticCollector.suspendMeasureTimeOfDataReadFromProvider();
                return new ServiceWithSkusPrices(gcpService, allSkusForService.getSkus());
            } catch (RuntimeException e) {
                log.error("Exception when fetching from GCP skus for service " + gcpService.getServiceId(), e);
                shutdownExecutor();
                throw e;
            } catch (Exception e) {
                log.error("Exception when fetching from GCP skus for service " + gcpService.getServiceId(), e);
                shutdownExecutor();
                throw e;
            }
        }

        /**
         * Creates cloudbilling responsible for fetching data from Google API.
         *
         * @return
         * @throws GeneralSecurityException
         * @throws IOException
         */
        private Cloudbilling createCloudBilling() throws GeneralSecurityException, IOException {
            JacksonFactory jsonFactory = new JacksonFactory();
            return new Cloudbilling.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, null).setApplicationName(applicationName).build();
        }

        /**
         * Fetch all skus for given service
         *
         * @param service
         * @return metadata describing all skus for given service
         * @throws IOException,RuntimeException
         */
        private AllSkusForService fetchAllSkusForService(Service service) throws IOException {
            final AtomicReference<List<Sku>> allSkus = new AtomicReference<>(new ArrayList<>());
            final AtomicReference<List<String>> allSkusAsJsons = new AtomicReference<>(new ArrayList<>());
            StopWatch stopWatch = StopWatch.createStarted();
            log.debug("cloud_environment_pricing_id: {} | service_name: {} | Fetching skus...", cloudEnvironmentPricingId.getId(), service.getName());
            Cloudbilling.Services.Skus.List skusListForService = cloudBilling.services().skus().list(service.getName()).setKey(apiKey).setPrettyPrint(true);
            fetchSkusRecursively(service.getName(), skusListForService, allSkus, allSkusAsJsons);
            log.debug("cloud_environment_pricing_id: {} | service_name: {} | Fetching skus finished. Fetched skus {} rows. It took {} seconds",
                    cloudEnvironmentPricingId.getId(), service.getName(), allSkus.get().size(), stopWatch.getTime(TimeUnit.SECONDS));
            return new AllSkusForService(allSkusAsJsons.get(), allSkus.get());
        }

        private void fetchSkusRecursively(
                String serviceName,
                Cloudbilling.Services.Skus.List skusListForService,
                AtomicReference<List<Sku>> allSkus,
                AtomicReference<List<String>> allSkusAsJsons) {

            ScheduledFuture<String> stringScheduledFuture = invokeWithDelay(() -> fetchSkus(skusListForService, allSkus, allSkusAsJsons));
            try {
                String newToken = stringScheduledFuture.get();
                if (StringUtils.isNotBlank(newToken)) {
                    skusListForService.setPageToken(newToken);
                    fetchSkusRecursively(serviceName, skusListForService, allSkus, allSkusAsJsons);
                }
            } catch (Exception e) {
                log.error("cloud_environment_pricing_id: {} | service_name: {} | Fetching skus failed", cloudEnvironmentPricingId.getId(), serviceName);
                throw new GCPServicesFetchingException(
                        String.format("cloud_environment_pricing_id: %s | service_name: %s | Fetching skus failed", cloudEnvironmentPricingId.getId(), serviceName),
                        Optional.ofNullable(e.getCause()).orElse(e));
            }
        }

        private String fetchSkus(Cloudbilling.Services.Skus.List skusListForService,
                                 AtomicReference<List<Sku>> allSkus,
                                 AtomicReference<List<String>> allSkusAsJsons) throws IOException {
            ListSkusResponse response = skusListForService.execute();
            allSkus.get().addAll(response.getSkus());
            allSkusAsJsons.get().add(response.toPrettyString());
            return response.getNextPageToken();
        }

        /**
         * Loads all GCP services metadata.
         *
         * @return metadata describing all GCP services (for that client)
         * @throws IOException,RuntimeException
         */
        private AllServices fetchAllServices() throws IOException, RuntimeException {
            final AtomicReference<List<Service>> atomicServices = new AtomicReference<>(new ArrayList<>());
            final AtomicReference<List<String>> atomicServicesAsJson = new AtomicReference<>(new ArrayList<>());
            log.debug("cloud_environment_pricing_id: {}  | Fetching services list started", cloudEnvironmentPricingId.getId());
            Cloudbilling.Services.List listOfServices = cloudBilling.services().list().setKey(apiKey).setPrettyPrint(true);
            fetchServicesRecursively(listOfServices, atomicServices, atomicServicesAsJson);
            log.debug("cloud_environment_pricing_id: {}  | Fetching services list finished", cloudEnvironmentPricingId.getId());
            return new AllServices(atomicServicesAsJson.get(), atomicServices.get());
        }

        private void fetchServicesRecursively(
                Cloudbilling.Services.List listOfServices,
                AtomicReference<List<Service>> allServices,
                AtomicReference<List<String>> allServicesAsJsons) {
            ScheduledFuture<String> stringScheduledFuture = invokeWithDelay(() -> fetchServices(listOfServices, allServices, allServicesAsJsons));

            try {
                String newToken = stringScheduledFuture.get();
                if (StringUtils.isNotBlank(newToken)) {
                    listOfServices.setPageToken(newToken);
                    fetchServicesRecursively(listOfServices, allServices, allServicesAsJsons);
                }
            } catch (Exception e) {
                log.error("cloud_environment_pricing_id: {} | Fetching services list failed", cloudEnvironmentPricingId.getId());
                throw new GCPServicesFetchingException(
                        String.format("cloud_environment_pricing_id: %s | Fetching services list failed", cloudEnvironmentPricingId.getId()),
                        Optional.ofNullable(e.getCause()).orElse(e));
            }
        }

        private String fetchServices(Cloudbilling.Services.List listOfServices,
                                     AtomicReference<List<Service>> allServices,
                                     AtomicReference<List<String>> allServicesAsJsons) throws IOException {
            ListServicesResponse response = listOfServices.execute();
            allServices.get().addAll(response.getServices());
            allServicesAsJsons.get().add(response.toPrettyString());
            return response.getNextPageToken();
        }

        private <T> ScheduledFuture<T> invokeWithDelay(Callable<T> callable) {
            return delayExecutor.schedule(callable, servicesFetchingDelayMs, TimeUnit.MILLISECONDS);
        }

        private void shutdownExecutor() {
            try {
                if (!delayExecutor.isShutdown()) {
                    delayExecutor.shutdown();
                }
            } catch (Exception e) {
                log.debug("Can not shutdown executor. Skip, go further", e);
            }
        }

        private Service createService(String id) {
            Service service = new Service();
            service.setName(SERVICE_NAME_PREFIX + id);
            service.setFactory(new JacksonFactory());
            return service;
        }

        @Data
        private final class AllServices {
            private final List<String> servicesAsJsons;
            private final List<Service> services;
        }

        @Data
        private final class AllSkusForService {
            private final List<String> skusAsJsons;
            private final List<Sku> skus;
        }
    }
}
