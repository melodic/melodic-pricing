package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Describes aggregated product.
 */
@Data
@NoArgsConstructor
@Document(collection = GCPAggregatedProduct.COLLECTION_NAME)
@TypeAlias("TYPE_GCPAggregatedProduct")
public class GCPAggregatedProduct {
    protected static final String COLLECTION_NAME = "gcp_aggregated_products";

    @Id
    @Field(targetType = FieldType.STRING)
    private ProductId id;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    private String description;

    private String productFamily; // e.g. 'N2'

    private GCPAggregatedProductFamily gcpFamily;

    private Set<Place> regions; // one region e.g. CONCRETE/asia-northeast1

    private List<GCPBasicProduct> components; // cores and ram, e.g. "N2 Custom Instance Ram running in APAC"

    /**
     * duplicated: extracted from components
     */
    private Set<OfferTermType> offerTermTypes;

    public Map<String, Object> rawAttributes() {
        return new HashMap<>();
    }


    public GCPAggregatedProduct(ProductId productId, CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                String description, String productFamily,
                                GCPAggregatedProductFamily gcpFamily, Set<Place> regions, List<GCPBasicProduct> components,
                                Set<OfferTermType> offerTermTypes) {
        this.id = productId;
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.description = description;
        this.productFamily = productFamily;
        this.gcpFamily = gcpFamily;
        this.regions = regions;
        this.components = components;
        this.offerTermTypes = offerTermTypes;
    }

    /**
     * Merge two entries of the same product: it "glues" regions and offers.
     *
     * @param p2
     * @throws IllegalArgumentException products must have the same product id
     */
    public void merge(GCPAggregatedProduct p2) {
        if (!id.equals(p2.getId())) {
            throw new IllegalArgumentException("Products are not the same: p1=" + id + " p2=" + p2.getId());
        }

        if (regions == null) {
            regions = new HashSet<>();
        }
        if (p2.getRegions() != null) {
            try {
                regions.addAll(p2.regions);
            } catch (UnsupportedOperationException e) {
                regions = new HashSet<>(regions);
                regions.addAll(p2.regions);
            }
        }

        if (offerTermTypes == null) {
            offerTermTypes = new HashSet<>();
        }
        if (p2.getOfferTermTypes() != null) {
            try {
                offerTermTypes.addAll(p2.offerTermTypes);
            } catch (UnsupportedOperationException e) {
                offerTermTypes = new HashSet<>(offerTermTypes);
                offerTermTypes.addAll(p2.offerTermTypes);
            }
        }

        if (components == null) {
            components = new ArrayList<>();
        }

        if (p2.getComponents() != null) {
            components = Stream.concat(components.stream(), p2.components.stream())
                    .collect(Collectors.groupingBy(GCPBasicProduct::getProductId, LinkedHashMap::new,
                            Collectors.reducing((comp1, comp2) -> {
                                try {
                                    comp1.getRegions().addAll(comp2.getRegions());
                                } catch (UnsupportedOperationException e) {
                                    comp1.setRegions(new HashSet<>(comp1.getRegions()));
                                    comp1.getRegions().addAll(comp2.getRegions());
                                }
                                return comp1;
                            }))).values().stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());
        }
    }
}
