package org.ow2.morphemic.pricing.cloud_providers.gcp;

import com.google.api.services.cloudbilling.model.Sku;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * One place which defines how to create GCP ProductId from various sources.
 */
@Component
public class GcpProductIdFactory {

    public ProductId createFromSku(CloudEnvironmentPricingId pricingId, String skuCode) {
        Objects.requireNonNull(pricingId);
        Objects.requireNonNull(skuCode);
        return new ProductId(pricingId, skuCode);
    }

    public ProductId createFromSku(CloudEnvironmentPricingId pricingId, Sku sku) {
        Objects.requireNonNull(pricingId);
        Objects.requireNonNull(sku);
        return createFromSku(pricingId, sku.getSkuId());
    }

}
