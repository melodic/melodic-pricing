package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.time.Period;
import java.util.Map;

/**
 * Describes AWS savings plans.
 * Only - without pricing data. Just attribute of savings plans.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Document(collection = AWSSavingsPlansRecord.MONGO_COLLECTION_NAME)
@TypeAlias("TYPE_AWSSavingsPlansRecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AWSSavingsPlansRecord {

    static final String MONGO_COLLECTION_NAME = "aws_savings_plans";

    @Id
    private String id;

    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum version;

    @Indexed
    private AWSPurchaseOption purchaseOption;

    @Indexed
    private String instanceType;

    /**
     * Declared length. Usually: 1y or 3yrs.
     */
    @Indexed
    private Period purchaseTerm;

    private Map<String, String> attrsRaw;

    /**
     * Each SP is active in one region.
     */
    @Indexed
    private String regionCode;

    @Indexed
    private AWSSavingsPlanType savingsPlanType;

    /**
     * Creates compute instance Savings Plan.
     *
     * @param cloudEnvironmentPricingId
     * @param version
     * @param purchaseOption
     * @param instanceType
     * @param purchaseTerm
     */
    public AWSSavingsPlansRecord(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                 VersionNum version, AWSPurchaseOption purchaseOption,
                                 String instanceType,
                                 Period purchaseTerm) {
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.version = version;
        this.purchaseOption = purchaseOption;
        this.instanceType = instanceType;
        this.purchaseTerm = purchaseTerm;
        this.attrsRaw = null;
        this.savingsPlanType = AWSSavingsPlanType.COMPUTE_SP;
    }

    /**
     * Creates EC2 instance Savings Plan record.
     *
     * @param cloudEnvironmentPricingId
     * @param version
     * @param purchaseOption
     * @param instanceType
     * @param purchaseTerm
     * @param regionCode
     */
    public AWSSavingsPlansRecord(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                 VersionNum version, AWSPurchaseOption purchaseOption,
                                 String instanceType,
                                 Period purchaseTerm, String regionCode) {
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.version = version;
        this.purchaseOption = purchaseOption;
        this.instanceType = instanceType;
        this.purchaseTerm = purchaseTerm;
        this.attrsRaw = null;
        this.regionCode = regionCode;
        this.savingsPlanType = AWSSavingsPlanType.EC2_INSTANCE_SP;
    }


}
