package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import com.google.api.client.json.JsonFactory;
import com.google.api.services.cloudbilling.model.Sku;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.IOException;

/**
 * Single record representing pricing data for a single SKU.
 */
@Document(collection = "gcp_raw_pricing_skus")
@TypeAlias("GCPRawSKURecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
class GCPRawSKURecord {

    /**
     * managed by Spring
     */
    @Id
    private String id;

    @Transient
    private transient Sku gcpSku;

    /**
     * serialized raw data
     */
    private String serializedGcpSkuAsJson;

    GCPRawSKURecord(Sku gcpSku) throws IOException {
        this.gcpSku = gcpSku;
        this.serializedGcpSkuAsJson = gcpSku.toPrettyString();
    }

    /**
     * Must be called after loading from db in order to properly restore internal data.
     *
     * @param jsonFactory
     * @return this
     * @throws IOException
     */
    GCPRawSKURecord afterLoad(JsonFactory jsonFactory) throws IOException {
        this.gcpSku = jsonFactory.fromString(serializedGcpSkuAsJson, Sku.class);
        this.gcpSku.setFactory(jsonFactory);
        return this;
    }
}
