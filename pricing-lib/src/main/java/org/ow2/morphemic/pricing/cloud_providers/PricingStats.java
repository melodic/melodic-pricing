package org.ow2.morphemic.pricing.cloud_providers;

import lombok.Data;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.versions.VersionNum;

import java.util.Date;
import java.util.Map;


/**
 * Statistics for a pricing.
 */
@Data
public class PricingStats {
    private final CloudEnvironmentPricingId cloudEnvironmentPricingId;
    private final CloudProvider cloudProvider;
    /**
     * all products count
     */
    private final long productsCount;
    /**
     * when last version was created
     */
    private final Date lastVersionDate;
    /**
     * last version num
     */
    private final VersionNum lastVersionNum;
    private final Map<String, String> additionalInfo;
}

