package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Represents response from Aws savings plans pages.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSSavingsPlans {

    /**
     * Sku of savings plans.
     */
    @JsonProperty("sku")
    private String sku;

    /**
     * Description of savings plans.
     */
    @JsonProperty("description")
    private String description;

    /**
     * Effective date of savings plans.
     */
    @JsonProperty("effectiveDate")
    private String effectiveDate;

    /**
     * Lease contract length of savings plans.
     */
    @JsonProperty("leaseContractLength")
    private AWSSavingsPlansContract leaseContractLength;

    /**
     * All rates for savings plans.
     */
    @JsonProperty("rates")
    private List<AWSSavingsPlansRates> rates;
}
