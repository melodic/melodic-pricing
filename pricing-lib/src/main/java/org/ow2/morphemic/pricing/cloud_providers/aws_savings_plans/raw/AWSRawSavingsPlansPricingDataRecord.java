package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlans;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansProduct;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

/**
 * Contains single record fetched from  public aws savings plans pricing API. Contains raw data in json format.
 */
@Document(collection = "aws_savings_plans_raw")
@TypeAlias("AwsSavingsPlansRawPricingDataRecord")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class AWSRawSavingsPlansPricingDataRecord {

    /**
     * Managed by spring
     */
    @Id
    private String id;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum versionNum;

    private String awsVersion;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    /**
     * data from Aws Api
     */
    private AWSSavingsPlansProduct rawProduct;

    /**
     * data from Aws Api
     */
    private AWSSavingsPlans rawRates;

    @Indexed
    private String regionCode;

    @Indexed
    private Long sortPosNum;

    public AWSRawSavingsPlansPricingDataRecord(AWSSavingsPlans rawRates, VersionNum versionNum, String awsVersion,
                                               CloudEnvironmentPricingId clEnvPricingId,
                                               String regionCode,
                                               long sortPosNum) {
        this.versionNum = versionNum;
        this.rawRates = rawRates;
        this.awsVersion = awsVersion;
        this.cloudEnvironmentPricingId = clEnvPricingId;
        this.regionCode = regionCode;
        this.sortPosNum = sortPosNum;
    }

    public AWSRawSavingsPlansPricingDataRecord(AWSSavingsPlansProduct rawProduct, VersionNum versionNum, String awsVersion,
                                               CloudEnvironmentPricingId clEnvPricingId,
                                               String regionCode,
                                               long sortPosNum) {
        this.versionNum = versionNum;
        this.rawProduct = rawProduct;
        this.awsVersion = awsVersion;
        this.cloudEnvironmentPricingId = clEnvPricingId;
        this.regionCode = regionCode;
        this.sortPosNum = sortPosNum;
    }
}
