package org.ow2.morphemic.pricing.cloud_providers.aws;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.util.Map;

/**
 * Describes AWS products. Only - without pricing data. Just its attributes.
 */
@Document(collection = AWSProductRecord.MONGO_COLLECTION_NAME)
@TypeAlias("TYPE_AWSProductRecord")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AWSProductRecord {

    static final String MONGO_COLLECTION_NAME = "aws_products";

    @Id
    @Field(targetType = FieldType.STRING)
    private ProductId id;

    /**
     * External id from AWS API
     */
    private String skuId;

    @Indexed
    private String serviceCode;

    @Indexed
    private String productFamily;

    @Field(targetType = FieldType.STRING)
    @Indexed
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    @Field(targetType = FieldType.INT64)
    @Indexed
    private VersionNum basedOnRawVersionNum;

    private Map<String, String> attrsRaw;

    private Map<String, String> attrsNormalized;

}
