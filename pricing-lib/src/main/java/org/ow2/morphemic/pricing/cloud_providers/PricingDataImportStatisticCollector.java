package org.ow2.morphemic.pricing.cloud_providers;

import lombok.NonNull;
import org.apache.commons.lang3.time.StopWatch;
import org.ow2.morphemic.pricing.model.CloudEnvironmentId;
import org.ow2.morphemic.pricing.model.CloudProvider;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.unmodifiableList;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Placeholder for some pricing data import statistics
 *
 * You can use all stopwatch's only via resume/suspend methods.
 */

public class PricingDataImportStatisticCollector {

    public static final double SECONDS_IN_MINUTE = 60D;

    @NonNull
    private final String cloudAccountId;

    @NonNull
    private final List<CloudEnvironmentId> cloudEnvironmentIds;

    @NonNull
    private final CloudProvider cloudProvider;

    /**
     * Time of start in milliseconds
     */
    private final long startedAt;

    /**
     * Time of stop in milliseconds
     */
    private long stoppedAt;

    /**
     * Stopwatch to measure duration of delete existing pricing data
     */
    private final StopWatch timerToMeasureDurationOfTotalDeletionOfPricingData = new StopWatch();

    /**
     * Stopwatch to measure total duration of (read from provider + process data + save to database) pricing data import from provider
     */
    private final StopWatch timerToMeasureDurationOfFullProcess = new StopWatch();

    /**
     * Stopwatch to measure total duration of total time of read only (download and response from provider) all pricing data from provider
     */
    private final StopWatch timerToMeasureDurationOfDataReadFromProvider = new StopWatch();

    /**
     * Stopwatch to measure total duration of save into local database all pricing data
     */
    private final StopWatch timerToMeasureDurationOfSaveDataIntoDatabase = new StopWatch();

    /**
     * Stopwatch to measure total duration of transforming source data from provider to business model and save into local database.
     */
    private final StopWatch timerToMeasureDurationTransformationOfSourceDataToBusinessModel = new StopWatch();

    /**
     * Total number of saved pricing data rows to database
     */
    private long totalNumberOfSavedRowsIntoDatabase = 0;

    /**
     * Total number of transformed data rows to business model
     */
    private long numberOfTransformationRowsOfSourceDataToBusinessModel = 0;

    /**
     * Create statistic collector. Setting all timers to SUSPENDED state. To start measure time please use resume* method.
     * To stop it use suspend*.
     * @param cloudProvider
     * @param cloudAccountId
     * @param cloudEnvironmentIds
     */
    public PricingDataImportStatisticCollector(@NonNull CloudProvider cloudProvider, @NonNull String cloudAccountId, @NonNull List<CloudEnvironmentId> cloudEnvironmentIds) {
        this.cloudAccountId = cloudAccountId;
        this.cloudEnvironmentIds = unmodifiableList(cloudEnvironmentIds);
        this.cloudProvider = cloudProvider;
        this.startedAt = System.currentTimeMillis();
        this.timerToMeasureDurationOfFullProcess.start();
        this.startMeasureTimeOfDeletePricingData()
                .suspendMeasureTimeOfDeletePricingData(); //stop watch can only be resumed or suspended
        this.startMeasureTimeOfSaveDataIntoDatabase()
                .suspendMeasureTimeOfSaveDataIntoDatabase(); //stopwatch can only be resumed or suspended
        this.startMeasureTimeOfDataReadFromProvider()
                .suspendMeasureTimeOfDataReadFromProvider(); //stop watch can only be resumed or suspended
    }

    /**
     * Stopping collecting of metrics. Stopping all stopwatches.
     * @return
     */
    public PricingDataImportStatisticCollector stop() {
        if (!timerToMeasureDurationOfFullProcess.isStopped()) {
            timerToMeasureDurationOfFullProcess.stop();
        }
        stoppedAt = System.currentTimeMillis();
        if (!timerToMeasureDurationOfDataReadFromProvider.isStopped()) {
            timerToMeasureDurationOfDataReadFromProvider.stop();
        }
        if (!timerToMeasureDurationOfSaveDataIntoDatabase.isStopped()) {
            timerToMeasureDurationOfSaveDataIntoDatabase.stop();
        }
        return this;
    }

    @Override
    public String toString() {
        return printStatistics();
    }

    /**
     * Create statistic information based on current stopwatches states.
     * @return Information about statistics as string
     */
    public String printStatistics() {
        return cloudProvider.name() + " PricingDataImportStatistic{"
                + "cloudAccountId=" + cloudAccountId
                + ", cloudEnvironmentIds=" + Arrays.toString(cloudEnvironmentIds.stream().map(CloudEnvironmentId::getId).toArray())
                + ", startedAt=" + toLocalDateTime(startedAt)
                + ", stoppedAt=" + toLocalDateTime(stoppedAt)
                + ", totalDurationInSecondsOfDeleteData=" + timerToMeasureDurationOfTotalDeletionOfPricingData.getTime(SECONDS)
                + ", totalDurationInMinutesOfDeleteData=" + toMinutes(timerToMeasureDurationOfTotalDeletionOfPricingData.getTime(SECONDS))
                + ", totalDurationInSecondsOfPricingDataProcess=" + timerToMeasureDurationOfFullProcess.getTime(SECONDS)
                + ", totalDurationInMinutesOfPricingDataProcess=" + toMinutes(timerToMeasureDurationOfFullProcess.getTime(SECONDS))
                + ", totalDurationInSecondsOfDataReadFromProvider=" + timerToMeasureDurationOfDataReadFromProvider.getTime(SECONDS)
                + ", totalDurationInMinutesOfDataReadFromProvider=" + toMinutes(timerToMeasureDurationOfDataReadFromProvider.getTime(SECONDS))
                + ", totalDurationInSecondsOfSaveDataIntoDatabase=" + timerToMeasureDurationOfSaveDataIntoDatabase.getTime(SECONDS)
                + ", totalDurationInMinutesOfSaveDataIntoDatabase=" + toMinutes(timerToMeasureDurationOfSaveDataIntoDatabase.getTime(SECONDS))
                + ", totalNumberOfSaveRowsIntoDatabase=" + totalNumberOfSavedRowsIntoDatabase
                + ", totalDurationInSecondsOfTransformationOfSourceDataToBusinessModel="
                + timerToMeasureDurationTransformationOfSourceDataToBusinessModel.getTime(SECONDS)
                + ", totalDurationInMinutesOfTransformationOfSourceDataToBusinessModel="
                + toMinutes(timerToMeasureDurationTransformationOfSourceDataToBusinessModel.getTime(SECONDS))
                + ", numberOfTransformationRowsOfSourceDataToBusinessModel=" + numberOfTransformationRowsOfSourceDataToBusinessModel
                + '}';
    }

    private LocalDateTime toLocalDateTime(long startedAt) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(startedAt), ZoneId.systemDefault());
    }

    private double toMinutes(long totalExecutionDurationInSeconds) {
        return (double) totalExecutionDurationInSeconds / SECONDS_IN_MINUTE;
    }

    private PricingDataImportStatisticCollector startMeasureTimeOfDataReadFromProvider() {
        timerToMeasureDurationOfDataReadFromProvider.start();
        return this;
    }

    public boolean isSuspendedMeasureTimeOfDataReadFromProvider() {
        return timerToMeasureDurationOfDataReadFromProvider.isSuspended();
    }

    /**
     * Use it to suspend measure time of reading data from provider eg. AWS, Azure etc.
     * To resume measure use {@link this#resumeMeasureTimeOfDataReadFromProvider()}
     * @return
     */
    public PricingDataImportStatisticCollector suspendMeasureTimeOfDataReadFromProvider() {
        timerToMeasureDurationOfDataReadFromProvider.suspend();
        return this;
    }

    /**
     * Use it to resume measure time of reading data from provider eg. AWS, Azure etc.
     * To suspend measure use {@link this#suspendMeasureTimeOfDataReadFromProvider()}
     * @return
     */
    public PricingDataImportStatisticCollector resumeMeasureTimeOfDataReadFromProvider() {
        timerToMeasureDurationOfDataReadFromProvider.resume();
        return this;
    }

    private PricingDataImportStatisticCollector startMeasureTimeOfSaveDataIntoDatabase() {
        timerToMeasureDurationOfSaveDataIntoDatabase.start();
        return this;
    }

    public boolean isSuspendedMeasureTimeOfSaveDataIntoDatabase() {
        return timerToMeasureDurationOfSaveDataIntoDatabase.isSuspended();
    }

    /**
     * Use it to suspend measure time of save data into your application database.
     * To resume measure use {@link this#resumeMeasureTimeOfSaveDataIntoDatabase()}
     * @return
     */
    public PricingDataImportStatisticCollector suspendMeasureTimeOfSaveDataIntoDatabase() {
        timerToMeasureDurationOfSaveDataIntoDatabase.suspend();
        return this;
    }

    /**
     * Use it to resume measure time of save data into your application database.
     * To suspend measure use {@link this#suspendMeasureTimeOfSaveDataIntoDatabase()}
     * @return
     */
    public PricingDataImportStatisticCollector resumeMeasureTimeOfSaveDataIntoDatabase() {
        timerToMeasureDurationOfSaveDataIntoDatabase.resume();
        return this;
    }

    private PricingDataImportStatisticCollector startMeasureTimeOfDeletePricingData() {
        timerToMeasureDurationOfTotalDeletionOfPricingData.start();
        return this;
    }

    public boolean isMeasureTimeOfDeletePricingData() {
        return timerToMeasureDurationOfTotalDeletionOfPricingData.isSuspended();
    }

    /**
     * Use it to suspend measure time of delete data from your application database.
     * To resume measure use {@link this#resumeMeasureTimeOfDeletePricingData()}
     * @return
     */
    public PricingDataImportStatisticCollector suspendMeasureTimeOfDeletePricingData() {
        timerToMeasureDurationOfTotalDeletionOfPricingData.suspend();
        return this;
    }

    /**
     * Use it to measure time of delete data from your application database.
     * To suspend measure use {@link this#suspendMeasureTimeOfDeletePricingData()}
     * @return
     */
    public PricingDataImportStatisticCollector resumeMeasureTimeOfDeletePricingData() {
        timerToMeasureDurationOfTotalDeletionOfPricingData.resume();
        return this;
    }

    public PricingDataImportStatisticCollector incrementTotalNumberOfSavedRowsIntoDatabase(long numberOfRows) {
        totalNumberOfSavedRowsIntoDatabase += numberOfRows;
        return this;
    }

    public PricingDataImportStatisticCollector startMeasureTimeOfTransformationOfSourceDataToBusinessModel() {
        timerToMeasureDurationTransformationOfSourceDataToBusinessModel.start();
        return this;
    }

    public PricingDataImportStatisticCollector stopMeasureTimeOfTransformationOfSourceDataToBusinessModel() {
        timerToMeasureDurationTransformationOfSourceDataToBusinessModel.stop();
        return this;
    }

    public PricingDataImportStatisticCollector incrementTotalNumberOfTransformationRowsOfSourceDataToBusinessModel(long numberOfRows) {
        numberOfTransformationRowsOfSourceDataToBusinessModel += numberOfRows;
        return this;
    }
}
