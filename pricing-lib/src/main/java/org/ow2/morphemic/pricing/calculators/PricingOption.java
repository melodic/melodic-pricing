package org.ow2.morphemic.pricing.calculators;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.OfferType;
import org.ow2.morphemic.pricing.model.Unit;

import java.time.Period;

/**
 * Describes possible pricing parameters, becacuse at first user (caller) has to know
 * parameters: what unit should be used and for what offer.
 * We assume that caller has to understand these parameters.
 */
@Data
@RequiredArgsConstructor
public class PricingOption {

    /**
     * what offer can be given
     */
    private final OfferTermType offerTermType;

    /**
     * it is unit used to calculate price for given offer term type. It must not be null:
     */
    private final Unit unit;

    /**
     * Reservation period or how long should be used. Can be null (on demand)
     */
    private final Period period;

    /**
     * what type of offer can be given
     */
    private final OfferType offerType;

    public PricingOption(OfferTermType offerTermType, Unit unit) {
        this.offerTermType = offerTermType;
        this.unit = unit;
        this.period = null;
        this.offerType = null;
    }

    public PricingOption(OfferTermType offerTermType, Unit unit, Period period) {
        this.offerTermType = offerTermType;
        this.unit = unit;
        this.period = period;
        this.offerType = null;
    }
}
