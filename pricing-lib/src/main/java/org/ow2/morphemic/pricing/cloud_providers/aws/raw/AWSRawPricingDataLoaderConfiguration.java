package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;

import java.io.File;
import java.util.Collections;

/**
 * Spring configuration class: properly assembles 2 different data loaders.
 * <p>
 * Probably it will be removed in the future because each account can have different set of prices and products.
 */
@Configuration
@Slf4j
@RequiredArgsConstructor
class AWSRawPricingDataLoaderConfiguration {

    private final MongoTemplate mongoTemplate;

    private final MongoConverter mongoConverter;

    /**
     * This bean allows to load AWS pricing dumps from given directory. Dumps must be saved as JSON files.
     *
     * @param awsDataDumpDir
     * @param awsRawPricingDataRepository
     * @param dataVersionsService
     * @return
     */
    @Bean("awsPricingFromFiles")
    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    AWSPricingRawDataLoader awsPricingFromFiles(@Value("${pricing.datadump.aws.dir:/tmp/pricing/aws}") String awsDataDumpDir,
                                                AWSRawPricingDataRepository awsRawPricingDataRepository,
                                                DataVersionsService dataVersionsService) {
        File scanDir = new File(awsDataDumpDir);
        if (!scanDir.mkdirs()) {
            log.info("Created scan dir for AWS dump loader {}", scanDir.getAbsolutePath());
        }

        return new AWSPricingRawDataLoader(Collections.singletonList(new AWSFilesPricingDataStreamSupplier(scanDir)),
                awsRawPricingDataRepository, dataVersionsService);
    }

}
