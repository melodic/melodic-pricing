package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.Getter;
import org.ow2.morphemic.pricing.model.OfferTermType;

/**
 * Standard offer term types with contracts.
 */
public enum AzureOfferTermTypes {
    ON_DEMAND(new OfferTermType("")),
    CONTRACT_1YR(new OfferTermType("1 Year")),
    CONTRACT_3YR(new OfferTermType("3 Years"));

    AzureOfferTermTypes(OfferTermType o) {
        this.offerTermType = o;
    }

    @Getter
    private OfferTermType offerTermType;

}
