package org.ow2.morphemic.pricing.cloud_providers;

import lombok.Data;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;

import java.util.Set;

/**
 * Basic statistics for one cloud provider.
 */
@Data
public class CloudProviderStats {

    private final CloudProvider cloudProvider;
    private final Set<CloudEnvironmentPricingId> pricings;
    private final long productsCount;
}
