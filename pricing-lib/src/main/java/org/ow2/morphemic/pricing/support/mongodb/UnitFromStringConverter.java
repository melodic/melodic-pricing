package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

/**
 * Used to read value from mongo string field.
 * Helps to read {@link Unit} saved as text.
 */
@ReadingConverter
class UnitFromStringConverter implements Converter<String, Unit> {

    @Override
    public Unit convert(String source) {
        return new Unit(source);
    }
}
