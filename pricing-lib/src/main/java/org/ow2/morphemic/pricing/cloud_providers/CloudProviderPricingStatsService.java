package org.ow2.morphemic.pricing.cloud_providers;

import org.ow2.morphemic.pricing.model.CloudProvider;

import java.util.List;

/**
 * Provides statistics for pricing data from one of {@link org.ow2.morphemic.pricing.model.CloudProvider}s.
 */
public interface CloudProviderPricingStatsService {

    /**
     * Re-calculates statistics of all pricings in this {@link CloudProvider}.
     *
     * @return stats - one per pricing
     */
    List<PricingStats> calculatePricingStats();

    /**
     * Re-calculates stats for this {@link CloudProvider}.
     *
     * @return stats
     */
    CloudProviderStats calculateCloudProviderStats();


    /**
     * Cloud provider supported by this service.
     *
     * @return
     */
    CloudProvider getCloudProvider();

}
