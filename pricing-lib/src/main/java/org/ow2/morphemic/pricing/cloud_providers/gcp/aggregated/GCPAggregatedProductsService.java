package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import com.google.common.base.Ascii;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.ProductIdOnlyDbRowProjection;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.fileloader.GCPVmProductDef;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.fileloader.GCPVmProductsLoader;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.model.gcp.GCPAggregatedProductIdFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;
import static java.util.stream.Collectors.toList;


/**
 * Provides all functionality needed to manage "aggregated" GCP products (VMs etc),
 * which consist of many "basic" products (for which one can calculate price).
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class GCPAggregatedProductsService {

    private final ProductsMetadataService productsMetadataService;

    private final GCPAggregatedProductsRepository gcpAggregatedProductsRepository;

    private Set<String> keyWordsToRejectComponent = Set.of("Reserved", "Custom");


    @Value("${pricing.gcp.aggregated.default.cpuUnitName:}")
    private String cpuUnitName = "";

    @Value("${pricing.gcp.aggregated.default.ramUnitName:GiBy}")
    private String ramUnitName = "GiBy";

    /**
     * Path to file with product data.
     */
    @Value("${pricing.datasources.gcp.vmProductsJson}")
    @Getter
    @Setter
    private String vmFilePath;

    @Value("${pricing.datasources.gcp.vmProductsJsonRequired:true}")
    @Getter
    @Setter
    private boolean vmFilePathRequired = true;

    private GCPAggregatedProductIdFactory productIdFactory = new GCPAggregatedProductIdFactory();

    /**
     * Various post construct checks.
     *
     * @return
     * @throws Exception
     */
    @PostConstruct
    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    Object initialize() throws Exception {
        if (vmFilePathRequired && !new File(vmFilePath).isFile()) {
            throw new FileNotFoundException("File with aggegated products was not found! Expected file must be located at: " + vmFilePath);
        }
        return true;
    }

    /**
     * Loads or updates GCP aggregated products in context of given pricing.
     *
     * @param pricingId pricing
     * @throws IOException when file with pricing cannot be found or other similar error.
     */
    @Transactional
    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    public void updateAggregatedProducts(CloudEnvironmentPricingId pricingId) throws IOException {
        if (vmFilePath == null) {
            throw new FileNotFoundException("File with product data was not defined!");
        }

        File vmFile = new File(this.vmFilePath);
        if (!vmFile.isFile()) {
            throw new FileNotFoundException("Cannot access file with product data: " + vmFilePath);
        }

        try (InputStream inpData = new BufferedInputStream(new FileInputStream(vmFile))) {
            updateAggregatedProductsPrices(pricingId, inpData);
        }
    }

    /**
     * It loads aggregated products definitions from given input stream and then it creates or updates pricing in context of given pricing.
     * Easch pricing has its own set of aggregated GCP products.
     *
     * @param pricingId pricing
     * @param inpData   input stream with json
     * @throws IOException
     */
    @Transactional
    void updateAggregatedProductsPrices(CloudEnvironmentPricingId pricingId, InputStream inpData) throws IOException {
        // complicated: it fetches and glues (merges) records describing the same products but in different regions.

        List<GCPVmAggregatedProduct> vms = new GCPVmProductsLoader().loadVMsFrom(inpData).stream()
                .map(gcpProdDef -> toGcpVmAggregatedProduct(pricingId, gcpProdDef))
                .collect(groupingBy(GCPAggregatedProduct::getId,
                        HashMap::new,
                        reducing((p1, p2) -> {
                            p1.merge(p2);
                            return p1;
                        })))
                .values().stream().filter(Optional::isPresent).map(Optional::get).collect(toList());

        log.debug("Fetched and \"glued\" {} GCP VMs definitions for pricing {}.", vms.size(), pricingId);

        gcpAggregatedProductsRepository.saveAll(vms);
        vms.stream().map(GCPVmAggregatedProduct::getId).forEach(prodId -> {
            productsMetadataService.createProductMetadata(prodId, CloudProvider.GCP);
        });

        log.debug("Saved {} products", vms.size());
    }

    private GCPVmAggregatedProduct toGcpVmAggregatedProduct(CloudEnvironmentPricingId pricingId, GCPVmProductDef gcpVmProductDef) {
        Set<OfferTermType> collectedTermTypes = new HashSet<>();
        List<GCPBasicProduct> collectedComponents = gcpVmProductDef.getOffers().stream()
                .peek(offer -> {
                    if (offer.getUsageType() != null) {
                        collectedTermTypes.add(new OfferTermType(offer.getUsageType()));
                    }

                    // this fixes problems with inconsistent taxonomy in GCP data.
                    if (offer.getDescription() != null) {
                        String descr = offer.getDescription().toLowerCase(Locale.US);
                        if (descr.contains("instance core") || descr.contains("cpu")) {
                            offer.setResourceGroup("CPU");
                        } else if (descr.contains("instance ram")) {
                            offer.setResourceGroup("RAM");
                        }
                    }
                })
                .map(offer -> GCPBasicProduct.builder()
                        .productId(new ProductId(pricingId, offer.getSkuId()))
                        .description(offer.getDescription())
                        .offerTermType(new OfferTermType(offer.getUsageType()))
                        .resourceFamily(offer.getResourceFamily())
                        .resourceGroup(offer.getResourceGroup())
                        .skuId(offer.getSkuId())
                        .regions(toPlaces(offer.getServiceRegions()))
                        .build())
                .collect(toList());

        Place place = (Ascii.equalsIgnoreCase(gcpVmProductDef.getRegions(), "global"))
                ? Place.GLOBAL_PLACE : new Place(gcpVmProductDef.getRegions());
        ProductId aggregatedProductId = productIdFactory.createVMId(pricingId, gcpVmProductDef.getName(), place);

        if (collectedTermTypes.isEmpty()) {
            log.warn("There are no term types for GCP aggregated product {} generated from VM product def {}! Check input file!", aggregatedProductId, gcpVmProductDef.getName());
        }

        GCPVmAggregatedProduct aggregatedProduct = GCPVmAggregatedProduct.builder()
                .productId(aggregatedProductId)
                .cloudEnvironmentPricingId(pricingId)
                .cpuCount(new Amount(gcpVmProductDef.getGuestCpus(), new Unit(cpuUnitName)))
                .ramAmountGB(new Amount(gcpVmProductDef.getMemoryGb(), new Unit(ramUnitName)))
                .description(gcpVmProductDef.getDescription())
                .productFamily(gcpVmProductDef.getFamily())
                .regions(toPlaces(gcpVmProductDef.getRegions()))
                .offerTermTypes(collectedTermTypes)
                .components(collectedComponents)
                .build();
        setUnits(aggregatedProduct);
        return aggregatedProduct;
    }

    private Set<Place> toPlaces(String regions) {
        return Set.of(regions != null ? regions : "")
                .stream().filter(Objects::nonNull).map(Place::new).collect(Collectors.toSet());
    }

    private Set<Place> toPlaces(Collection<String> regions) {
        return regions != null ? regions.stream().filter(Objects::nonNull).map(Place::new).collect(Collectors.toSet()) : new HashSet<>();
    }

    /**
     * Removes all aggregated products.
     */
    @Transactional
    public void deleteAll() {
        gcpAggregatedProductsRepository.findAllBy(ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId)
                .forEach(productsMetadataService::removeProduct);
        gcpAggregatedProductsRepository.deleteAll();
        log.info("Removed all aggregated products data.");
    }

    /**
     * Deletes all data describing aggregated product.
     *
     * @param productId product id
     */
    @Transactional
    public void delete(ProductId productId) {
        productsMetadataService.removeProduct(productId);
        gcpAggregatedProductsRepository.deleteById(productId);
    }

    /**
     * Deletes all data for given pricing.
     *
     * @param pricingId pricing id
     */
    @Transactional
    public void deletePricing(CloudEnvironmentPricingId pricingId) {
        gcpAggregatedProductsRepository.findAllByCloudEnvironmentPricingId(pricingId, ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId).forEach(productsMetadataService::removeProduct);
        long removed = gcpAggregatedProductsRepository.deleteAllByCloudEnvironmentPricingId(pricingId);
        log.info("Deleted all GCP aggregated products for pricing {}. Removed records count: {}.", pricingId, removed);
    }

    /**
     * All product IDs which belong to given pricing.
     *
     * @param pricingId pridcing id. We do not use verion num here (yes, simplification).
     * @return set of found product ids.
     */
    @Transactional
    public Set<ProductId> allProductIds(CloudEnvironmentPricingId pricingId) {
        return gcpAggregatedProductsRepository.findAllByCloudEnvironmentPricingId(pricingId, ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId).collect(Collectors.toSet());
    }

    @Transactional
    public Optional<? extends GCPAggregatedProduct> findProduct(ProductId productId) {
        return gcpAggregatedProductsRepository.findById(productId);
    }

    /**
     * Finds GCPAggregatedProduct based on @param aggregatedProduct. From @param aggregatedProduct it takes product family,
     * pricing id and region and it returns the first element as all are equivalent from calculating custom VM pricing point of view.
     */
    @Transactional
    public Optional<GCPAggregatedProduct> findAggregatedPricingForCustomVm(GCPVmAggregatedProduct aggregatedProduct) {
        Optional<Place> maybeVmRegion = aggregatedProduct.getRegions().stream().findFirst();
        if (maybeVmRegion.isPresent()) {
            return gcpAggregatedProductsRepository.findAllByCloudEnvPricingIdAndProductFamilyAndRegion(aggregatedProduct.getCloudEnvironmentPricingId(),
                    aggregatedProduct.getProductFamily(), maybeVmRegion.get()).findFirst();
        } else {
            log.warn("Can not find region of aggregated vm: {}", aggregatedProduct.getId());
            return Optional.empty();
        }
    }

    /**
     * Based on @param productId (e.g. p:clpr:8eff8329-74ee-41be-82a8-8a290a12e55d/extId:vm:n2-custom-72-262144/r:CONCRETE/europe-west1
     * or p:clpr:8eff8329-74ee-41be-82a8-8a290a12e55d/extId:vm:custom-12-30720/r:CONCRETE/europe-west1)
     * it creates definition of GCPAggregatedProduct.
     */
    @SuppressFBWarnings(value = "IMPROPER_UNICODE")
    public Optional<GCPVmAggregatedProduct> mapToAggregatedCustomVm(ProductId productId) {
        String[] splitProductId = productId.getId().split("/");
        if (splitProductId.length == 4 && splitProductId[0].startsWith("p:")) {
            String region = splitProductId[3];
            String cloudEnvPricingId = splitProductId[0].replace("p:", "");
            String[] splitVmName = splitProductId[1].split("-");
            if ((splitVmName.length == 4 || splitVmName.length == 3) && splitVmName[0].startsWith("extId:vm:")) {
                double cpu, ramGB;
                String productFamily;
                if (splitVmName.length == 4) {
                    productFamily = splitVmName[0].replace("extId:vm:", "");
                    productFamily = productFamily.toUpperCase(Locale.US);
                    cpu = Double.valueOf(splitVmName[2]);
                    ramGB = Double.valueOf(splitVmName[3]) / 1024; // custom vm has RAM in MB in its name, so we need to convert to GB
                } else {
                    productFamily = "N1"; // products without product family belongs to N1
                    cpu = Double.valueOf(splitVmName[1]);
                    ramGB = Double.valueOf(splitVmName[2]) / 1024; // custom vm has RAM in MB in its name, so we need to convert to GB
                }
                GCPVmAggregatedProduct gcpAggregatedProduct = new GCPVmAggregatedProduct();
                gcpAggregatedProduct.setId(productId);
                gcpAggregatedProduct.setCloudEnvironmentPricingId(new CloudEnvironmentPricingId(cloudEnvPricingId));
                gcpAggregatedProduct.setProductFamily(productFamily);
                gcpAggregatedProduct.setCpuCount(new Amount(cpu, new Unit(cpuUnitName)));
                gcpAggregatedProduct.setRamAmountGB(new Amount(ramGB, new Unit(ramUnitName)));
                gcpAggregatedProduct.setRegions(toPlaces(region));
                setUnits(gcpAggregatedProduct);
                return Optional.of(gcpAggregatedProduct);
            } else {
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    /**
     * Adds custom components based on @param gcpAggregatedProductPricing and  @param offerTermType to @param aggregatedCustomVm
     */
    public void addDataToCustomVm(GCPVmAggregatedProduct aggregatedCustomVm, GCPAggregatedProduct gcpAggregatedProductPricing, OfferTermType offerTermType) {
        List<GCPBasicProduct> customComponents = filterCustomComponents(gcpAggregatedProductPricing, aggregatedCustomVm, offerTermType);
        aggregatedCustomVm.setComponents(customComponents);
        aggregatedCustomVm.setOfferTermTypes(createOfferTermTypes(customComponents));
    }

    private Set<OfferTermType> createOfferTermTypes(List<GCPBasicProduct> customComponents) {
        return customComponents.stream().map(GCPBasicProduct::getOfferTermType)
                .collect(Collectors.toSet());
    }

    private List<GCPBasicProduct> filterCustomComponents(GCPAggregatedProduct gcpAggregatedProductPricing, GCPVmAggregatedProduct aggregatedCustomVm, OfferTermType offerTermType) {
        return gcpAggregatedProductPricing.getComponents().stream()
                .filter(gcpBasicProduct -> offerTermType.equals(gcpBasicProduct.getOfferTermType())
                        && isCustomCoreRamComponent(gcpBasicProduct.getDescription(), aggregatedCustomVm.getProductFamily()))
                .collect(toList());
    }

    private boolean isCustomCoreRamComponent(String description, String productFamily) {
        String coreRamPrefix;
        if ("N1".equals(productFamily)) { // Custom N1 components do not have product family in description
            coreRamPrefix = "Custom";
        } else if ("E2".equals(productFamily)) { // Custom E2 components are equal to E2 standard components and does not have "Custom" in description
            coreRamPrefix = "E2";
        } else {
            coreRamPrefix = "N2D".equals(productFamily) ? "N2D AMD Custom" : String.format("%s ", productFamily + " Custom");
        }
        return description.startsWith(String.format("%s Instance Ram", coreRamPrefix))
                || description.startsWith(String.format("%s Instance Core", coreRamPrefix));
    }

    // TODO move these definitions to external config file
    private void setUnits(GCPVmAggregatedProduct gcpAggregatedProduct) {
        gcpAggregatedProduct.setRamUnits(new GCPAggregatedUnits().addMapping(Map.of("h", "GiBy.h", "hour", "gibibyte hour")));
        gcpAggregatedProduct.setCpuUnits(new GCPAggregatedUnits().addMapping(Map.of("h", "h", "hour", "hour")));
    }

    public List<GCPBasicProduct> filterNotCustomComponents(GCPAggregatedProduct gcpAggregatedProduct, OfferTermType offerTermType) {
        return gcpAggregatedProduct.getComponents().stream()
                .filter(gcpBasicProduct -> offerTermType.equals(gcpBasicProduct.getOfferTermType())
                        && isNotCustomCoreRamComponent(gcpBasicProduct.getDescription()))
                .collect(toList());
    }

    private boolean isNotCustomCoreRamComponent(String description) {
        for (String keyWord : keyWordsToRejectComponent) {
            if (description.contains(keyWord)) {
                return false;
            }
        }
        return true;
    }
}
