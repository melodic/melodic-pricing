package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

/**
 * Raw record is/raw records are invalid. Cannot process RAW data.
 */
public class AWSInvalidRawRecordException extends Exception {

    public AWSInvalidRawRecordException(String message) {
        super(message);
    }
}
