package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Represents response from Aws savings plans pages.
 * First page if used public api link.In response obtain page with all regions and links for version page.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSSavingsPlansOfferResponse {


    /**
     * Publication date of savings plans page.
     */
    @JsonProperty("publicationDate")
    private String publicationDate;

    /**
     * Format version date of savings plans page.
     */
    @JsonProperty("formatVersion")
    private String formatVersion;

    /**
     * All available regions.
     */
    @JsonProperty("regions")
    private List<AWSSavingsPlansRegionsUrl> regions;
}
