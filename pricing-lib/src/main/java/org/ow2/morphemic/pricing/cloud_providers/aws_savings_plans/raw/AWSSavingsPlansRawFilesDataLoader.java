package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.ow2.morphemic.pricing.cloud_providers.BaseDirIterator;
import org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansRatesPage;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Loads RAW aws savings plans  data from given dump dirctory.
 */
@Slf4j
@Service
public class AWSSavingsPlansRawFilesDataLoader extends AbstractAWSSavingsPlansRawDataLoader {

    @Value("${pricing.datadump.aws.savingsplans.dir:/tmp/pricing/aws/savings_plans}")
    private String awsSavingsPlansDumpDir;

    public AWSSavingsPlansRawFilesDataLoader(AWSRawSavingsPlansPricingDataRepository savingsPlansPricingDataRepository, DataVersionsService dataVersionsService) {
        super(savingsPlansPricingDataRepository, dataVersionsService);
    }

    /**
     * Loads AWS pricing raw data to DB cache.
     *
     * @return new version id if new data was stored in cache.
     */
    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    @Transactional
    public Optional<VersionedCloudEnvPricingId> loadDumpToRawCache() throws IOException {
        File awsDumpDir = new File(this.awsSavingsPlansDumpDir);
        boolean created = awsDumpDir.mkdirs();
        if (created) {
            log.info("Dump dir {} has been created.", awsDumpDir);
        }
        File okFile = new File(awsDumpDir, ProductPricingUpdateService.FS_LOADER_OK_FILE_NAME);
        if (okFile.isFile()) {
            ObjectMapper objMapper = new ObjectMapper();

            log.debug("Found ok file.");
            String clPricingIdAsText = Files.readString(okFile.toPath(), StandardCharsets.UTF_8).trim();
            if (StringUtils.isNotBlank(clPricingIdAsText)) {
                CloudEnvironmentPricingId clEnvPricingId = new CloudEnvironmentPricingId(clPricingIdAsText);
                log.info("Loading raw data for cloud env pricing: {}", clEnvPricingId);

                AwsDumpDirIterator awsDumpDirIterator = new AwsDumpDirIterator(awsDumpDir, true, okFile);
                AtomicInteger scannedRawPage = new AtomicInteger();
                AtomicInteger errors = new AtomicInteger();
                AtomicReference<VersionNum> newVersionNum = new AtomicReference<>();
                AtomicLong sortPositionNumberProvider = new AtomicLong();

                Flux.fromIterable(() -> awsDumpDirIterator)
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .doOnNext(json -> {
                            try {
                                AWSSavingsPlansRatesPage rawSavingsPlansPage = objMapper.readValue(json, AWSSavingsPlansRatesPage.class);
                                newVersionNum.setRelease(new VersionNum(Long.parseLong(rawSavingsPlansPage.getVersion())));
                                if (checkIfThisVersionNonExist(clEnvPricingId, newVersionNum.get())) {
                                    saveItemsFromOnePage(rawSavingsPlansPage, newVersionNum.get(), clEnvPricingId, rawSavingsPlansPage.getRegionCode(), sortPositionNumberProvider);
                                    int savedCount = scannedRawPage.incrementAndGet();
                                    if (savedCount % 100 == 0) {
                                        log.debug("Saved {} pages with raw AWS savings plans data.", savedCount);
                                    }
                                } else {
                                    log.info("This {} version of savings plans for {} CloudEnvironmentPricingId exist in DB", newVersionNum, clEnvPricingId);
                                }
                            } catch (JsonProcessingException e) {
                                log.error("Cannot deserialize! " + e.getMessage(), e);
                                log.trace("Contents: {}", json);
                                errors.incrementAndGet();
                            } catch (NumberFormatException e) {
                                log.error("Cannot parse version format for aws savings plans " + e.getMessage(), e);
                            }
                        }).count().block();
                log.info("Scanned {} savings plans pages. Errors detected {}.", scannedRawPage.get(), errors.get());

                if (scannedRawPage.get() > 0) {
                    registerNewVersionAndUpdateLast(newVersionNum.get(), clEnvPricingId);
                    return Optional.of(new VersionedCloudEnvPricingId(clEnvPricingId, newVersionNum.get()));
                }
            } else {
                log.error("Cloud environment pricing id must not be blank in '.ok' file!");
            }
        }
        return Optional.empty();
    }

    private static final class AwsDumpDirIterator extends BaseDirIterator implements Iterator<Optional<String>> {
        private AwsDumpDirIterator(File scanDirRoot, boolean deleteProcessedFiles, File okFile) {
            super(scanDirRoot, deleteProcessedFiles);
            addFileToDelete(okFile);
        }

        @Override
        public boolean hasNext() {
            return hasMoreElementsToProcess();
        }

        @Override
        public Optional<String> next() {
            File f = getFilesToProcess().poll();
            if (f != null) {
                if (f.isDirectory()) {
                    processDirectory(f);
                } else if (f.isFile()) {
                    if (f.getName().endsWith(".json")) {
                        try {
                            String json = Files.readString(f.toPath(), StandardCharsets.UTF_8);
                            if (!f.delete()) {
                                log.warn("Cannot delete file {}", f.getAbsolutePath());
                            }
                            return Optional.ofNullable(json);
                        } catch (IOException e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                }
            } else {
                throw new NoSuchElementException("No more files to process!");
            }

            return Optional.empty();
        }
    }
}
