package org.ow2.morphemic.pricing.calculators.gcp;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPPriceOfferRate;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPriceOfferRecord;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingRecord;
import org.ow2.morphemic.pricing.cloud_providers.gcp.GCPProductPricingService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProduct;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPAggregatedProductsService;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPBasicProduct;
import org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated.GCPVmAggregatedProduct;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.OfferTermType;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Calculates prices of GCP products. It allows to calculate prices of basic products (identified as SKU records) and
 * - which more useful - "aggregated products" (virtual machines etc).
 */
@Component
@RequiredArgsConstructor
@Slf4j
// FIXME it must be refactored!!!
public class GCPPriceCalculator {

    private final GCPProductPricingService productPricingService;

    private final GCPAggregatedProductsService aggregatedProductsService;

    enum ResourceGroup {
        RAM("RAM"), CPU("CPU");
        @Getter
        private final String code;

        ResourceGroup(String code) {
            this.code = code;
        }

        boolean is(String groupCode) {
            return code.equals(groupCode);
        }
    }

    /**
     * Tries to calculate price of given amount of "product" and offer type.
     *
     * @param productId     product
     * @param amount        amount of that product
     * @param offerTermType calculation params: offer
     * @return empty when product cannot be found. List of calculated prices can be empty when there is no matching position in pricing tables for given amount and offer type.
     */
    // FIXME it must be refactored!!!
    public Optional<List<CalculatedPrice>> calculatePrice(ProductId productId, Amount amount, OfferTermType offerTermType) {
        log.trace("Try to calculate price of: {} amount: {} with offer: {}", productId, amount, offerTermType);
        if (productId.getId().contains("custom")) {
            log.trace("Calculating price for custom VM: {}", productId);
            // 1. create custom vm based on productId
            Optional<GCPVmAggregatedProduct> maybeAggregatedCustomVm = aggregatedProductsService.mapToAggregatedCustomVm(productId);
            if (maybeAggregatedCustomVm.isPresent()) {
                GCPVmAggregatedProduct aggregatedCustomVm = maybeAggregatedCustomVm.get();
                // 2. find aggregated vm which has components with pricing for this custom vm
                Optional<GCPAggregatedProduct> maybeAggregatedPricing = aggregatedProductsService.findAggregatedPricingForCustomVm(aggregatedCustomVm);
                if (maybeAggregatedPricing.isPresent()) {
                    GCPAggregatedProduct gcpAggregatedProductPricing = maybeAggregatedPricing.get();
                    log.trace("Found GCP aggregated custom vm pricing {} with {} components.", productId, gcpAggregatedProductPricing.getComponents().size());
                    aggregatedProductsService.addDataToCustomVm(aggregatedCustomVm, gcpAggregatedProductPricing, offerTermType);
                    return Optional.of(calculatePrices(aggregatedCustomVm, amount, offerTermType));
                } else {
                    log.warn("Can not find pricing for GCP aggregated custom vm: {}.", productId);
                    return Optional.empty();
                }
            } else {
                log.warn("Can not create custom VM from requested product. {}.", productId);
                return Optional.empty();
            }

        } else {
            Optional<GCPProductPricingRecord> maybeProductPricing = productPricingService.findProduct(productId);

            if (maybeProductPricing.isPresent()) {
                log.trace("Found basic product pricing {}", productId);
                return Optional.of(calculatePrices(maybeProductPricing.get(), amount, offerTermType));
            } else {
                log.trace("Maybe there is aggregated pricing data for that pricing? Product {}", productId);
                Optional<? extends GCPAggregatedProduct> maybeGcpAggregatedProduct = aggregatedProductsService.findProduct(productId);
                if (maybeGcpAggregatedProduct.isPresent()) {
                    GCPAggregatedProduct gcpAggregatedProduct = maybeGcpAggregatedProduct.get();
                    List<GCPBasicProduct> gcpBasicProducts = aggregatedProductsService.filterNotCustomComponents(gcpAggregatedProduct, offerTermType);
                    gcpAggregatedProduct.setComponents(gcpBasicProducts);
                    return Optional.of(calculatePrices(gcpAggregatedProduct, amount, offerTermType));
                } else {
                    return Optional.empty();
                }
            }
        }

    }

    /**
     * Calculate prices for aggregated product.
     *
     * @param aggregatedProduct
     * @param amount
     * @param offerTermType
     * @return
     */
    List<CalculatedPrice> calculatePrices(GCPAggregatedProduct aggregatedProduct, Amount amount, OfferTermType offerTermType) {
        List<CalculatedPrice> calculatedPrices = new ArrayList<>();

        if (aggregatedProduct.getOfferTermTypes().contains(offerTermType)) {
            if (aggregatedProduct instanceof GCPVmAggregatedProduct) {
                GCPVmAggregatedProduct vmProduct = (GCPVmAggregatedProduct) aggregatedProduct;
                calculatedPrices.addAll(calculatePricesForVm(vmProduct, amount, offerTermType));
            }
        }

        return calculatedPrices;
    }

    /**
     * Probably it can be more generic... Well - this is PoC.
     * <p>
     * TODO make more generic.
     *
     * @param vmProduct
     * @param amount
     * @param offerTermType
     * @return
     */
    private Collection<? extends CalculatedPrice> calculatePricesForVm(GCPVmAggregatedProduct vmProduct, Amount amount, OfferTermType offerTermType) {
        List<CalculatedPrice> calculatedPrices = new ArrayList<>();

        Optional<GCPBasicProduct> foundRam = vmProduct.getComponents().stream().filter(component -> offerTermType.equals(component.getOfferTermType())
                && ResourceGroup.RAM.is(component.getResourceGroup())).findFirst();
        Optional<GCPBasicProduct> foundCpu = vmProduct.getComponents().stream().filter(component -> offerTermType.equals(component.getOfferTermType())
                && ResourceGroup.CPU.is(component.getResourceGroup())).findFirst();

        if (foundCpu.isPresent() && (foundRam.isPresent() || "G1".equals(vmProduct.getProductFamily()) || "F1".equals(vmProduct.getProductFamily()))) {
            log.trace("CPU product for vm (): description: {}, sku id: {}", vmProduct.getId().getId(), foundCpu.get().getDescription(), foundCpu.get().getSkuId());
            Unit vmUnit = amount.getUnit();
            Optional<Unit> suggestedCpuUnit = vmProduct.suggestedCpuUnit(vmUnit);
            Optional<Unit> suggestedRamUnit = vmProduct.suggestedRamUnit(vmUnit);

            Amount vmCpuCount;

            if (vmProduct.getId().getId().contains("e2-micro")) { // e2-micro instances have individual prices (shared CPU)
                vmCpuCount = new Amount(0.25, vmProduct.getCpuCount().getUnit());
            } else if (vmProduct.getId().getId().contains("e2-small")) { // e2-small instances have individual prices (shared CPU)
                vmCpuCount = new Amount(0.5, vmProduct.getCpuCount().getUnit());
            } else {
                vmCpuCount = vmProduct.getCpuCount();
            }

            Amount cpuAmountToBePriced = suggestedCpuUnit.map(unit -> vmCpuCount.multiply(amount, unit))
                    .orElseGet(() -> vmCpuCount.multiply(amount));
            Optional<List<CalculatedPrice>> maybeCPUPrices = calculatePrice(foundCpu.get().getProductId(), cpuAmountToBePriced, offerTermType);

            Amount ramAmountToBePriced = suggestedRamUnit.map(unit -> vmProduct.getRamAmountGB().multiply(amount, unit))
                    .orElseGet(() -> vmProduct.getRamAmountGB().multiply(amount));

            Optional<List<CalculatedPrice>> maybeRAMPrices;

            // G1 and F1 instances have not prices for RAM and only CPU is important
            if ("G1".equals(vmProduct.getProductFamily()) || "F1".equals(vmProduct.getProductFamily())) {
                String currencyCode = maybeCPUPrices.isPresent() && !maybeCPUPrices.get().isEmpty() ? maybeCPUPrices.get().get(0).getPrice().getCurrencyCode() : "USD";
                maybeRAMPrices = Optional.of(List.of(new CalculatedPrice(new Price(BigDecimal.valueOf(0.0), currencyCode), "")));

            } else { // standard product family
                log.trace("Ram product for vm: description: {}, sku id: {}", foundRam.get().getDescription(), foundRam.get().getSkuId());
                maybeRAMPrices = calculatePrice(foundRam.get().getProductId(), ramAmountToBePriced, offerTermType);
            }


            if (maybeCPUPrices.isPresent() && maybeRAMPrices.isPresent()) {
                List<CalculatedPrice> cpuPrices = maybeCPUPrices.get();
                List<CalculatedPrice> ramPrices = maybeRAMPrices.get();

                if (!cpuPrices.isEmpty() && !ramPrices.isEmpty()) {
                    try {
                        Price cpuPrice = cpuPrices.get(0).price;
                        Price ramPrice = ramPrices.get(0).price;
                        if (new BigDecimal(0.0).compareTo(cpuPrice.getValue()) != 0
                                && (new BigDecimal(0.0).compareTo(ramPrice.getValue()) != 0 || "G1".equals(vmProduct.getProductFamily()) || "F1".equals(vmProduct.getProductFamily()))) {
                            Price calulatedPrice = cpuPrice.add(ramPrice);
                            log.trace("Calculated price for VM ({}) = {}. Cpu price: {}, Ram price: {}",
                                    vmProduct.getId(), calulatedPrice.getValue(), cpuPrice.getValue(), ramPrice.getValue());
                            calculatedPrices.add(new CalculatedPrice(calulatedPrice, ""));
                        } else {
                            log.error("Price of some VM {} basic product is equal to 0 (ram price: {}, cpu price: {})! Any product can not be sold for 0. Price rejected.",
                                    vmProduct.getId(), ramPrice.getValue(), cpuPrice.getValue());
                        }
                    } catch (IllegalArgumentException e) {
                        log.error(e.getMessage(), e);
                    }
                } else {
                    log.error("Cannot calculate price of VM {} because I cannot calculate prices for one of basic products: either RAM (empty?:{}) or CPU (empty?:{})", vmProduct.getId(), ramPrices.isEmpty(), cpuPrices.isEmpty());

                }
            } else {
                log.error("Cannot calculate price of VM {} because I cannot find prices for one of basic products: either RAM (found?:{}) or CPU (found?:{})", vmProduct.getId(), maybeRAMPrices.isPresent(), maybeCPUPrices.isPresent());
            }
        } else {
            log.error("Cannot calculate price of VM {} because I cannot find one of basic products: either RAM (found?:{}) or CPU (found?:{})", vmProduct.getId(), foundRam.isPresent(), foundCpu.isPresent());
        }

        return calculatedPrices;
    }


    /**
     * Responsible for calculate prices. Currently uses simple "ranges" strategy: tries to find matching value range.
     *
     * @param productPricing pricing data
     * @param amount         amount of product usage. Units are important!
     * @param offerTermType  offer
     * @return
     */
    List<CalculatedPrice> calculatePrices(GCPProductPricingRecord productPricing, Amount amount, OfferTermType offerTermType) {
        List<CalculatedPrice> calculatedPrices = new ArrayList<>();

        for (GCPProductPriceOfferRecord offer : productPricing.getOffers()) {
            for (GCPPriceOfferRate rate : offer.getRates()) {
                double quantity = amount.getQuantity();
                if (isTheSameUnit(amount.getUnit(), rate.getUsageUnit())
                        && rate.getStartUsageAmount() <= quantity && (rate.getEndUsageAmount() == null
                        || rate.getEndUsageAmount() > quantity)) {
                    String currencyCode = "USD";
                    rate.pricePerUnit(currencyCode)
                            .map(price -> price.multiply(new BigDecimal(quantity)))
                            .ifPresent(calcPriceVal -> {
                                calculatedPrices.add(new CalculatedPrice(new Price(calcPriceVal, currencyCode), ""));
                            });
                    log.trace("Found pricing rate {} and calculated price.", rate);
                    break;
                }
            }
        }

        if (calculatedPrices.isEmpty()) {
            Set<String> units = productPricing.getOffers().stream()
                    .flatMap(o -> o.getRates().stream())
                    .map(GCPPriceOfferRate::getUsageUnit)
                    .filter(Objects::nonNull)
                    .map(GCPPriceOfferRate.GCPUnit::getAbbr)
                    .collect(Collectors.toSet());
            log.warn("It was not possible to calculate prices for product {} amount {}. Is matching offer term type: yes. Units: {}",
                    productPricing.getId(), amount, units);
        }

        log.trace("Calculated {} prices for product {} amount {}: {}", calculatedPrices.size(), productPricing.getId(), amount, calculatedPrices);
        return calculatedPrices;
    }

    private boolean isTheSameUnit(Unit unit, GCPPriceOfferRate.GCPUnit usageUnit) {
        // TODO use Unit in GCPPriceOfferRate
        return unit.getUnitName().equals(usageUnit.getAbbr());
    }

    /**
     * Result: calculated price with reference to rate ID.
     */
    @Data
    @RequiredArgsConstructor
    public static class CalculatedPrice {
        private final Price price;

        private final String externalRateCodeId;
    }

}
