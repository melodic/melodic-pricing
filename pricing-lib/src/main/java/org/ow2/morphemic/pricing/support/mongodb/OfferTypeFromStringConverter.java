package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.OfferType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

/**
 * Used to read value from mongo string field.
 * Helps to read {@link OfferType} saved as text.
 */
@ReadingConverter
public class OfferTypeFromStringConverter implements Converter<String, OfferType> {

    @Override
    public OfferType convert(String source) {
        return new OfferType(source);
    }
}
