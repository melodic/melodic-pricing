package org.ow2.morphemic.pricing.support.mongodb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import java.util.ArrayList;
import java.util.List;

/**
 * Needed because some domain types must be properly saved ans primitive values in mongo. Provides
 * set of custom converters.
 */
@Configuration
@Slf4j
class MongoRepoConfiguration {

    @Bean
    MongoCustomConversions customConversions() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(new ProductIdFromStringConverter());
        converters.add(new ProductIdToStringConverter());
        converters.add(new CloudEnvironmentPricingIdFromStringConverter());
        converters.add(new CloudEnvironmentPricingIdToStringConverter());
        converters.add(new UnitToStringConverter());
        converters.add(new UnitFromStringConverter());
        converters.add(new VersionNumToLongConverter());
        converters.add(new VersionNumFromLongConverter());
        converters.add(new PlaceFromStringConverter());
        converters.add(new PlaceToStringConverter());
        converters.add(new OfferTermTypeToStringConverter());
        converters.add(new OfferTermTypeFromStringConverter());
        converters.add(new OfferTypeToStringConverter());
        converters.add(new OfferTypeFromStringConverter());

        log.debug("Custom converters for Mongo created");
        return new MongoCustomConversions(converters);
    }

}
