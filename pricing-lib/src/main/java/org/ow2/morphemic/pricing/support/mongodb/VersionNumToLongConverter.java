package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

/**
 * Helps to save {@link VersionNum} as text.
 */
@WritingConverter
class VersionNumToLongConverter implements Converter<VersionNum, Long> {

    @Override
    public Long convert(VersionNum source) {
        return source.getNum();
    }
}
