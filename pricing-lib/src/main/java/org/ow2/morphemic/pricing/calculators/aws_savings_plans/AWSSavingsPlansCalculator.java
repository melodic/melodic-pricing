package org.ow2.morphemic.pricing.calculators.aws_savings_plans;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansProductPricingService;
import org.ow2.morphemic.pricing.model.Amount;
import org.ow2.morphemic.pricing.model.Price;
import org.ow2.morphemic.pricing.model.ProductId;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Calculates product price according to given parameters.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class AWSSavingsPlansCalculator {

    private final AWSSavingsPlansProductPricingService pricingService;

    /**
     * Calculates price of given product for specified amount and units.
     *
     * @param productId     please use AWSSavingsPlansProductIdFactory
     * @param amount        how many / how much is needed (amount of product)
     * @return calculated prices, when empty then it means that calculation was not possible
     */
    public Optional<List<Price>> calculatePrice(ProductId productId, Amount amount) {
        Optional<AWSSavingsPlansProductPricingService.ProductWithPricing> maybeProductWithPricing = pricingService.findProduct(productId);
        return maybeProductWithPricing.map(AWSSavingsPlansProductPricingService.ProductWithPricing::getPricingRecord)
                .filter(e -> amount.getUnit().equals(e.getUnit()))
                .map(record -> calculatePriceForRate(record.getPricePerUnitByCurrency(), amount));
    }

    /**
     * Scans all currency map offer rates for matching records.
     * @param pricePerUnitByCurrency Map with currency and value per unit
     * @param amount amout of product
     * @return calculated Price list
     */
    private List<Price> calculatePriceForRate(Map<String, BigDecimal> pricePerUnitByCurrency, Amount amount) {
        return pricePerUnitByCurrency.entrySet().stream()
                .map(currencyValuePrice -> calculatePrice(amount, currencyValuePrice))
                .collect(Collectors.toList());
    }

    private Price calculatePrice(Amount amount, Map.Entry<String, BigDecimal> currencyValuePrice) {
        return new Price(currencyValuePrice.getValue().multiply(BigDecimal.valueOf(amount.getQuantity())), currencyValuePrice.getKey());
    }
}
