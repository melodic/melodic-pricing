package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import org.ow2.morphemic.pricing.cloud_providers.WithCloudEnvironmentPricingId;
import reactor.core.publisher.Flux;

/**
 * Allows to load AWS pricing data for some service as stream of raw jsons.
 */
interface AWSServicePricingDataStreamSupplier extends WithCloudEnvironmentPricingId {

    /**
     * Creates stream with data records which contains definitions of AWS pricing.
     *
     * @return stream of raw json data describing service pricing.
     */
    Flux<String> createDataStream();

}
