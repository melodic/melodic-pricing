package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.Place;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

/**
 * Used to read value from mongo string field.
 * Helps to read {@link Place} saved as text.
 */
@ReadingConverter
class PlaceFromStringConverter implements Converter<String, Place> {

    /**
     * @param encoded endoced strinfg value of place (whoch can be region etc).
     * @return decoded place
     * @throws IllegalArgumentException when it cannot decode place.
     */
    @Override
    public Place convert(String encoded) throws IllegalArgumentException {
        int sepIndex = encoded.indexOf(PlaceToStringConverter.SEPARATOR);
        String[] placeParts = new String[2];
        if (sepIndex < 0) {
            placeParts[0] = encoded;
            placeParts[1] = "";
        } else {
            placeParts[0] = encoded.substring(0, sepIndex);
            placeParts[1] = encoded.substring(sepIndex + 1);
        }

        Place.Type type = Place.Type.valueOf(placeParts[0]);
        switch (type) {
            case CONCRETE:
                return new Place(placeParts[1]);
            case GLOBAL:
                return Place.GLOBAL_PLACE;
            case ANY:
                return Place.ANY_PLACE;
            default:
                throw new IllegalArgumentException("Unsupported place type: " + type + "!");
        }
    }
}
