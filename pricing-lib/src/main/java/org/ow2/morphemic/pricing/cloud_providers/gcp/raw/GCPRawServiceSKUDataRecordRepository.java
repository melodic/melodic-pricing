package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Responsible for storing raw GCP data for pricing.
 */
@Repository
interface GCPRawServiceSKUDataRecordRepository extends MongoRepository<GCPRawServiceSKUDataRecord, String> {

    /**
     * Remove all records with version number less than given one (current).
     *
     * @param cloudEnvironmentPricingId id of cloud environment
     * @param currentVersion            version num
     * @return removed count
     */
    long deleteAllByCloudEnvironmentPricingIdAndVersionLessThan(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum currentVersion);

    /**
     * Universal searhing by pricing id and version less than given number. Used maily to delete related SKU records.
     * @param cloudEnvironmentPricingId
     * @param currentVersion
     * @param type
     * @return
     * @param <T>
     */
    <T> List<T> findAllByCloudEnvironmentPricingIdAndVersionLessThan(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum currentVersion, Class<T> type);

    /**
     * Remove all records with version number less than given one (current).
     *
     * @param cloudEnvironmentPricingId id of cloud environment
     * @return removed count
     */
    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId);

    <T> List<T> findAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId, Class<T> type);

    Collection<GCPRawServiceSKUDataRecord> findByCloudEnvironmentPricingIdAndVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum version);
}
