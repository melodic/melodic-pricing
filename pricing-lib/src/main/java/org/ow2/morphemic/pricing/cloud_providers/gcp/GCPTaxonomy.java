package org.ow2.morphemic.pricing.cloud_providers.gcp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * Product taxonomy.
 */
@TypeAlias("TYPE_GCPTaxonomy")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GCPTaxonomy {

    @Indexed
    private String resourceFamily;
    @Indexed
    private String resourceGroup;
    @Indexed
    private String serviceId;
    private String serviceName;
    private String serviceDisplayName;
    private String businessEntityName;
    private String productSkuName;
}
