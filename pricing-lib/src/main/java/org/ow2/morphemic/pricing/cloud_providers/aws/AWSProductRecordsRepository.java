package org.ow2.morphemic.pricing.cloud_providers.aws;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.stream.Stream;

/**
 * DB repository for AWS product data. For internal use only.
 */
interface AWSProductRecordsRepository extends MongoRepository<AWSProductRecord, ProductId> {

    /**
     * Loads all records with given projection for selected cloudEnvironmentId and version. Generic method.
     *
     * @param cloudEnvironmentPricingId cloud env pricing id
     * @param basedOnRawVersionNum      version of cloud env pricing id
     * @param projectionType            projection
     * @param <P>                       type
     * @return stream of records of projection type
     */
    @Query
    <P> Stream<P> findAllByCloudEnvironmentPricingIdAndBasedOnRawVersionNum(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                                            VersionNum basedOnRawVersionNum,
                                                                            Class<P> projectionType);

    /**
     * Loads all records with given projection for selected cloudEnvironmentPricingId and version. Generic method.
     *
     * @param cloudEnvironmentPricingId cloud env pricing id
     * @param basedOnRawVersionNum      version of cloud env pricing id
     * @param productFamily             product family name (not normalized, value from AWS)
     * @param projectionType            projection
     * @param <P>                       type
     * @return stream of records of projection type
     */
    @Query
    <P> Stream<P> findAllByCloudEnvironmentPricingIdAndBasedOnRawVersionNumAndProductFamily(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                                                            VersionNum basedOnRawVersionNum,
                                                                                            String productFamily,
                                                                                            Class<P> projectionType);

    /**
     * Loads all records with given projection for selected cloudEnvironmentPricingId.
     *
     * @param cloudEnvironmentPricingId cloud env pricing id
     * @param projectionType            projection
     * @param <P>type
     * @return stream of records of projection type
     */
    <P> Stream<P> findAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                                     Class<P> projectionType);

    /**
     * Loads all records with given projection.
     *
     * @param projectionType            projection
     * @param <P>type
     * @return stream of records of projection type
     */
    <P> Stream<P> findDistinctBy(Class<P> projectionType);

    /**
     * Loads all records with given projection.
     *
     * @param projectionType            projection
     * @param <P>type
     * @return stream of records of projection type
     */
    <P> Stream<P> findAllBy(Class<P> projectionType);

    /**
     * @param pricingId pricing id.
     * @return removed records count.
     */
    long deleteAllByCloudEnvironmentPricingId(CloudEnvironmentPricingId pricingId);

}
