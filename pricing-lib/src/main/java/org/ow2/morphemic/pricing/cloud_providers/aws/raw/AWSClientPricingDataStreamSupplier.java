package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import com.amazonaws.services.pricing.AWSPricing;
import com.amazonaws.services.pricing.model.AWSPricingException;
import com.amazonaws.services.pricing.model.GetProductsRequest;
import com.amazonaws.services.pricing.model.GetProductsResult;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import reactor.core.publisher.Flux;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Provides pricing data for given AWS service code as stream of raw string data.
 */
@RequiredArgsConstructor
@Slf4j
@ToString(exclude = "client")
class AWSClientPricingDataStreamSupplier implements AWSServicePricingDataStreamSupplier {

    private final CloudEnvironmentPricingId cloudEnvironmentPricingId;

    private final AWSPricing client;

    /**
     * Code of AWS product (service, resource). For instance "AmazonEC2"
     */
    private final String awsServiceCode;


    /**
     * Creates stream to fetch AWS pricing data using AWS REST API.
     *
     * @return stream of raw json data describing service pricing.
     */
    @Override
    public Flux<String> createDataStream() {
        return Flux.fromIterable(TokenIterator::new).flatMap(stream -> stream);
    }

    @Override
    public Optional<CloudEnvironmentPricingId> getCloudEnvironmentPricingId() {
        return Optional.of(cloudEnvironmentPricingId);
    }

    /**
     * Uses client to iterate through packs of pricing data.
     */
    private class TokenIterator implements Iterator<Flux<String>> {

        private String nextToken = "";

        private boolean beforeFirst = true;


        @Override
        public boolean hasNext() {
            return nextToken != null || beforeFirst;
        }

        /**
         * Fetches next pack of pricing data.
         *
         * @return
         */
        @Override
        public Flux<String> next() {
            beforeFirst = false;

            GetProductsRequest getProductsRequest = new GetProductsRequest().withServiceCode(awsServiceCode);
            GetProductsResult getProductsResult = null;

            for (int retryCount = 0; retryCount < 5 && getProductsResult == null; retryCount++) {
                try {
                    final StopWatch started = StopWatch.createStarted();
                    log.debug("Start reading of AWS products for servicesCode={}", awsServiceCode);
                    if (StringUtils.isBlank(nextToken)) {
                        getProductsResult = client.getProducts(getProductsRequest);
                    } else {
                        getProductsResult = client.getProducts(getProductsRequest.withNextToken(nextToken));
                    }
                    log.debug("Finished read of AWS products for servicesCode={}). It took {} seconds", awsServiceCode, getDurationInSeconds(started));
                } catch (AWSPricingException e) {
                    log.error(e.getMessage(), e);
                    try {
                        Thread.sleep(500L);
                    } catch (InterruptedException ex) {
                        log.warn(ex.getMessage());
                    }
                }
            }

            if (getProductsResult == null) {
                throw new NoSuchElementException("Cannot read pricing data!");
            }

            nextToken = getProductsResult.getNextToken();
            if (StringUtils.isBlank(nextToken)) {
                nextToken = null;
            }

            log.debug("Fetched AWS data page for service {}", awsServiceCode);
            return Flux.fromIterable(getProductsResult.getPriceList());
        }

        private double getDurationInSeconds(StopWatch started) {
            return ((double) started.getTime(TimeUnit.MILLISECONDS)) / 100D;
        }

    }

}
