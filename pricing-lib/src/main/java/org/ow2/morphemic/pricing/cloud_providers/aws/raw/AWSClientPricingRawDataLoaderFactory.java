package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.pricing.AWSPricing;
import com.amazonaws.services.pricing.AWSPricingClientBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Creates special data loader instance which will load data from given AWS cloud environment with given credentials.
 * It hides all details of creating client data supplier and loader.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class AWSClientPricingRawDataLoaderFactory {
    private final DataVersionsService dataVersionsService;

    private final AWSRawPricingDataRepository awsRawPricingDataRepository;

    /**
     * This loader can load data from AWS servers.
     *
     * @param cloudEnvironmentPricingId cloud environment id
     * @param key                       API key needed to access AWS
     * @param secret                    API secret needed to access AWS
     * @return loader
     */
    public CloseableAWSPricingRawDataLoader createPricingAWSLoader(CloudEnvironmentPricingId cloudEnvironmentPricingId, String key,
                                                                   String secret, Set<String> serviceCodes) {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(key, secret);
        AWSCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        AWSPricing client = AWSPricingClientBuilder.standard()
                .withRegion(Regions.US_EAST_1).withCredentials(credentialsProvider).build();

        log.info("AWS service codes: {}", serviceCodes);
        List<AWSServicePricingDataStreamSupplier> suppliers = serviceCodes.stream()
                .map(serviceCode -> new AWSClientPricingDataStreamSupplier(cloudEnvironmentPricingId, client, serviceCode))
                .collect(Collectors.toList());

        return new CloseableAWSPricingRawDataLoader(client, suppliers, awsRawPricingDataRepository, dataVersionsService);
    }

    /**
     * This data loader can properly shutdown network resources.
     */
    public static class CloseableAWSPricingRawDataLoader extends AWSPricingRawDataLoader implements AutoCloseable {

        private final AWSPricing client;

        public CloseableAWSPricingRawDataLoader(AWSPricing client, List<AWSServicePricingDataStreamSupplier> dataSuppliers,
                                                AWSRawPricingDataRepository awsRawPricingDataRepository,
                                                DataVersionsService dataVersionsService, boolean removeOldVersions) {
            super(dataSuppliers, awsRawPricingDataRepository, dataVersionsService, removeOldVersions);
            this.client = client;
        }

        public CloseableAWSPricingRawDataLoader(AWSPricing client, List<AWSServicePricingDataStreamSupplier> dataSuppliers, AWSRawPricingDataRepository awsRawPricingDataRepository, DataVersionsService dataVersionsService) {
            super(dataSuppliers, awsRawPricingDataRepository, dataVersionsService);
            this.client = client;
        }


        @Override
        public void close() throws Exception {
            client.shutdown();
            log.info("AWS client shutdown.");
        }
    }
}
