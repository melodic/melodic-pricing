package org.ow2.morphemic.pricing.cloud_providers.aws.raw;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.ow2.morphemic.pricing.cloud_providers.BaseDirIterator;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import reactor.core.publisher.Flux;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.ow2.morphemic.pricing.cloud_providers.aws.AWSProductPricingUpdateService.FS_LOADER_OK_FILE_NAME;

/**
 * Can load files containing AWS product data and pricings from given directory.
 * Data is loaded only when there is ".ok" file in {@link #scanDirRoot} directory.
 * This file must contain valid id of cloud environment.
 */
@Slf4j
@ToString
class AWSFilesPricingDataStreamSupplier implements AWSServicePricingDataStreamSupplier {

    private final File scanDirRoot;

    @Getter
    @Setter
    private String markerFileName = FS_LOADER_OK_FILE_NAME;

    @Setter
    @Getter
    private boolean deleteProcessedFiles = true;


    AWSFilesPricingDataStreamSupplier(File scanDirRoot, boolean deleteProcessedFiles) {
        this.scanDirRoot = scanDirRoot;
        this.deleteProcessedFiles = deleteProcessedFiles;
        log.debug("AWS pricing files loader will load data from {}, marker file name {}, delete processed files: {}",
                scanDirRoot.getAbsolutePath(), markerFileName, this.deleteProcessedFiles);
    }

    AWSFilesPricingDataStreamSupplier(File scanDirRoot) {
        this.scanDirRoot = scanDirRoot;
        log.debug("AWS pricing files loader will load data from {}, marker file name {}, delete processed files: {}",
                scanDirRoot.getAbsolutePath(), markerFileName, this.deleteProcessedFiles);
    }

    /**
     * Stream of raw data to be imported. USe also {@link #getCloudEnvironmentPricingId()} to determine destination cloud environment.
     *
     * @return non empty stream only when there is "ok" file with cloud environment id.
     */
    @Override
    public Flux<String> createDataStream() {
        Optional<CloudEnvironmentPricingId> maybeCloudEnvId = getCloudEnvironmentPricingId();
        if (maybeCloudEnvId.isPresent()) {
            return Flux.fromIterable(() -> new DirIterator(deleteProcessedFiles))
                    .filter(Optional::isPresent).map(Optional::get); // iterator can return nulls!
        } else {
            return Flux.empty();
        }
    }

    /**
     * Tries to determine id of cloud environment loading contents of {@link #markerFileName}.
     *
     * @return id when file found
     */
    @Override
    public Optional<CloudEnvironmentPricingId> getCloudEnvironmentPricingId() {
        try {
            File okFile = okFile();
            if (okFile.isFile()) {
                String id = Files.readString(okFile.toPath(), StandardCharsets.UTF_8).trim();
                log.debug("Marker file {} exists. Pricing id which was read from that file {}", okFile.getAbsolutePath(), id);
                if (!id.isBlank()) {
                    return Optional.of(new CloudEnvironmentPricingId(id));
                } else {
                    log.error("Pricing id is blank! Check file {}!", okFile.getAbsolutePath());
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    /**
     * Ok file contains id of cloud environment AND means that data is ready to import.
     *
     * @return 'ok" marker file
     */
    @SuppressFBWarnings("WEAK_FILENAMEUTILS")
    private File okFile() {
        return new File(scanDirRoot, FilenameUtils.getName(markerFileName));
    }

    /**
     * Iterates through all files and sub directories.
     */
    private final class DirIterator extends BaseDirIterator implements Iterator<Optional<String>> {

        private DirIterator(boolean deleteProcessedFiles) {
            super(scanDirRoot, deleteProcessedFiles);
            addFileToDelete(okFile());
        }

        @Override
        public boolean hasNext() {
            return hasMoreElementsToProcess();
        }

        /**
         * Reads next file fetched from queue or reads contents of directory.
         *
         * @return null for "file" which is not a typical file or file contents. Null values must be filtered out.
         */
        @Override
        public Optional<String> next() {
            File f = getFilesToProcess().poll();
            if (f != null) {
                if (f.isDirectory()) {
                    processDirectory(f);
                } else if (f.isFile()) {
                    if (f.getName().endsWith(".json")) {
                        try {
                            String json = Files.readString(f.toPath(), StandardCharsets.UTF_8);
                            if (!f.delete()) {
                                log.warn("Cannot delete file {}", f.getAbsolutePath());
                            }
                            return Optional.ofNullable(json);
                        } catch (IOException e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                }
            } else {
                throw new NoSuchElementException("No more files to process!");
            }

            return Optional.empty();
        }
    }


}
