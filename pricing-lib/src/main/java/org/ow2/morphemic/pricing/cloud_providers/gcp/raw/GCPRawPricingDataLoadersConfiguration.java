package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.util.Collections;

/**
 * Configures beans used to load raw data.
 */
@Configuration
@Slf4j
class GCPRawPricingDataLoadersConfiguration {
    /**
     * This bean allows to load AWS pricing dumps from given directory. Dumps must be saved as JSON files.
     *
     * @param gcpDataDumpDir
     * @param recordRepository
     * @param dataVersionsService
     * @return
     */
    @Bean("gcpPricingFromFiles")
    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    GCPPricingRawDataLoader gcpPricingFromFiles(@Value("${pricing.datadump.gcp.dir:/tmp/pricing/gcp}") String gcpDataDumpDir,
                                                GCPRawServiceSKUDataRecordRepository recordRepository,
                                                GCPRawSKURecordRepository gcpRawSKURecordRepository,
                                                DataVersionsService dataVersionsService) {
        File scanDir = new File(gcpDataDumpDir);
        if (!scanDir.mkdirs()) {
            log.info("Created scan dir for AWS dump loader {}", scanDir.getAbsolutePath());
        }

        return new GCPPricingRawDataLoader(Collections.singletonList(new GCPFilesServicePricingDataStreamSupplier(scanDir)),
                recordRepository, gcpRawSKURecordRepository, dataVersionsService);
    }

}
