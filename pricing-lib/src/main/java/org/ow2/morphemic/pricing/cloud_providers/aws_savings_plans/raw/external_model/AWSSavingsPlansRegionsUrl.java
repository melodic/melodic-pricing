package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Represents response from Aws savings plans pages.
 * <a href="https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/sp-offer-file.html"></a>
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSSavingsPlansRegionsUrl {

    /**
     * Region for savings plans.
     */
    @JsonProperty("regionCode")
    private String regionCode;

    /**
     * Link to this region page.
     */
    @JsonProperty("versionUrl")
    private String versionUrl;
}
