package org.ow2.morphemic.pricing.cloud_providers;

import org.ow2.morphemic.pricing.model.ProductId;

/**
 * DB row projection: fetches only product id.
 */
public interface ProductIdOnlyDbRowProjection {

    ProductId getId();

}
