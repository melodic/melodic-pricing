package org.ow2.morphemic.pricing.cloud_providers.gcp.aggregated;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 *
 */
@RequiredArgsConstructor
@Data
public class GCPAggregatedProductFamily {

    private final String familyName;

}
