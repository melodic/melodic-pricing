package org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.cloud_providers.ProductPricingUpdateService;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.AWSSavingsPlansPricingRecord.AWSSavingsPlansPricingRecordBuilder;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.AWSRawSavingsPlansManager;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.AWSRawSavingsPlansPricingDataRecord;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.AWSSavingsPlansPublicRawDataLoader;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.AWSSavingsPlansRawFilesDataLoader;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansProduct;
import org.ow2.morphemic.pricing.cloud_providers.aws_savings_plans.raw.external_model.AWSSavingsPlansRates;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.CloudProvider;
import org.ow2.morphemic.pricing.model.PriceListType;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.Unit;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.ow2.morphemic.pricing.model.aws.AWSPurchaseOption;
import org.ow2.morphemic.pricing.model.aws_savings_plans.AWSSavingsPlansProductIdFactory;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Responsible for update of aws savings plans rates. Update process consists of following steps:
 * <ol>
 *     <li>Downloading data from AWS savings plans json or from disk catalog</li>
 *     <li>Saving raw data in Mongo DB (data repository)</li>
 *     <li>Transforming raw data to domain data (savings plans pricing)</li>
 * </ol>
 */
@Service("awsSavingsPlansUpdateService")
@Slf4j
@RequiredArgsConstructor
public class AWSSavingsPlansUpdateService implements ProductPricingUpdateService {

    static final CollectionName VERSIONED_AWS_SAVINGS_PLANS_COLLECTION_NAME = new CollectionName("aws:savings_plans:pricings:" + CollectionName.TEMPLATE);

    private final AWSSavingsPlansRawFilesDataLoader filesDataLoader;
    private final AWSSavingsPlansPublicRawDataLoader awsSavingsPlansPublicRawDataLoader;

    private final DataVersionsService dataVersionsService;

    private final ProductsMetadataService productsMetadataService;

    private final AWSRawSavingsPlansManager awsRawSavingsPlansManager;
    private final AWSSavingsPlansPricingRepository awsSavingsPlansPricingRepository;
    private final AWSSavingsPlansRepository awsSavingsPlansRepository;

    private final AWSSavingsPlansProductIdFactory savingsPlansProductIdFactory = new AWSSavingsPlansProductIdFactory();

    /**
     * records to save at once.
     */
    @Getter
    @Setter
    private int bufferSize = 300;
    @Getter
    @Setter
    private int bufferSizeForPriceRatesToSave = 10000;

    @Getter
    @Setter
    private int bufferSizeForSavingsPlansToSave = 10000;

    /**
     * Used to detect Savings Plan type.
     */
    private static final Map<String, AWSSavingsPlanType> SP_TYPES = Map.of(
            "ComputeSavingsPlans", AWSSavingsPlanType.COMPUTE_SP,
            "EC2InstanceSavingsPlans", AWSSavingsPlanType.EC2_INSTANCE_SP
    );

    /**
     * It scans directory for new files containing import data.
     */
    @Transactional
    @Override
    public synchronized Optional<VersionedCloudEnvPricingId> loadPricingDumpDataFromFiles() throws IOException {
        Optional<VersionedCloudEnvPricingId> maybeVersionedCloudEnvPricingId = filesDataLoader.loadDumpToRawCache();
        maybeVersionedCloudEnvPricingId.ifPresent(this::updatePricingFromRaw);
        return maybeVersionedCloudEnvPricingId;
    }

    /**
     * Now only public savings plans rates is loaded. It will be probably final solution for savings plans.
     *
     * @param cloudEnvironmentPricingId pricing of cloud env.
     */
    @Override
    public void updateUsingExternalApi(CloudEnvironmentPricingId cloudEnvironmentPricingId, PricingDataImportStatisticCollector pricingDataImportStatistic) {
        for (VersionedCloudEnvPricingId versionedSpCloudEnvPricingId : awsSavingsPlansPublicRawDataLoader.loadRawDataToCache()) {
            updatePricingFromRaw(versionedSpCloudEnvPricingId);
        }
    }

    /**
     * Converts all raw pricing records for given versioned pricing to records describing pricing data.
     *
     * @param versionedCloudEnvPricingId new versioned cloud environment pricing id
     */
    private void updatePricingFromRaw(VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        int savedSpCount = saveSavingsPlans(versionedCloudEnvPricingId);
        int savedProductPriceRatesCount = saveSavingsPlansPricing(versionedCloudEnvPricingId);

        dataVersionsService.registerNewVersion(versionedCloudEnvPricingId, VERSIONED_AWS_SAVINGS_PLANS_COLLECTION_NAME);
        log.info("Updated products: {}, rates: {} for version {}", savedSpCount,
                savedProductPriceRatesCount, versionedCloudEnvPricingId);
    }

    /**
     * Scans all collected raw pricing data of given Savings Plan pricing and adds entries with discounted products prices.
     *
     * @param versionedCloudEnvPricingId Savings Plan pricing (versioned)
     * @return number of saved pricing rates.
     */
    private int saveSavingsPlansPricing(VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        AtomicInteger counter = new AtomicInteger();

        @RequiredArgsConstructor
        class SavingsPlanAndRate {
            private final AWSRawSavingsPlansPricingDataRecord savingsPlan;
            private final AWSSavingsPlansRates rate;
        }


        awsRawSavingsPlansManager.rawPricingDataAsFluxForVersion(versionedCloudEnvPricingId)
                .filter(sp -> sp.getRawRates() != null)
                .flatMap(sp -> Flux.fromIterable(sp.getRawRates().getRates())
                        .map(rate -> new SavingsPlanAndRate(sp, rate)))
                .map(savingsPlanAndRate -> newAwsSavingsPlanProductPricing(versionedCloudEnvPricingId.getVersionNum(), savingsPlanAndRate.rate,
                        savingsPlanAndRate.savingsPlan))
                .buffer(bufferSizeForPriceRatesToSave)
                .map(rates -> {
                    counter.addAndGet(rates.size());
                    log.debug("Fetched and saved {} savings plans rates records for {}.", counter.get(), versionedCloudEnvPricingId.getCloudEnvironmentPricingId());
                    awsSavingsPlansPricingRepository.saveAll(rates);
                    productsMetadataService.createProductsMetadata(rates.stream()
                            .map(AWSSavingsPlansPricingRecord::getId).collect(Collectors.toSet()), CloudProvider.AWS, PriceListType.AWS_SAVINGS_PLANS);
                    return true;
                }).count().block();

        return counter.get();
    }

    /**
     * Scans all collected raw pricing data of given Savings Plan pricing and adds entries with metadata of all Savings Plans.
     *
     * @param versionedCloudEnvPricingId Savings Plan pricing (versioned)
     * @return number of saved Savings Plans.
     */
    private int saveSavingsPlans(VersionedCloudEnvPricingId versionedCloudEnvPricingId) {
        AtomicInteger counter = new AtomicInteger();
        Set<VersionedCloudEnvPricingId> createdSavingsPlansPricingIds = new HashSet<>();

        awsRawSavingsPlansManager.rawProductDataAsFluxForVersion(versionedCloudEnvPricingId)
                .map(savingPlanProduct -> findOrCreateNewSavingsPlan(versionedCloudEnvPricingId.getVersionNum(), savingPlanProduct))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(savingsPlan -> {
                    VersionedCloudEnvPricingId versionedSpId = new VersionedCloudEnvPricingId(savingsPlan.getCloudEnvironmentPricingId(), savingsPlan.getVersion());
                    if (createdSavingsPlansPricingIds.contains(versionedSpId)) {
                        return false;
                    } else {
                        createdSavingsPlansPricingIds.add(versionedSpId);
                        return true;
                    }
                })
                .buffer(bufferSizeForSavingsPlansToSave)
                .map(records -> {
                    counter.addAndGet(records.size());
                    log.debug("Fetched and saved {} savings plans product records for {}.", counter.get(), versionedCloudEnvPricingId.getCloudEnvironmentPricingId());
                    awsSavingsPlansRepository.saveAll(records);
                    return true;
                }).count().block();
        return counter.get();
    }

    private Optional<AWSSavingsPlansRecord> findOrCreateNewSavingsPlan(VersionNum versionNum, AWSSavingsPlansProduct savingPlanProduct) {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.AWS, savingPlanProduct.getSkuId());
        Optional<AWSSavingsPlansRecord> foundSp = awsSavingsPlansRepository.findByCloudEnvironmentPricingIdAndVersion(cloudEnvironmentPricingId, versionNum);
        if (foundSp.isPresent()) {
            return foundSp;
        } else {
            return newSavingsPlan(versionNum, savingPlanProduct, cloudEnvironmentPricingId);
        }
    }

    private Optional<AWSSavingsPlansRecord> newSavingsPlan(VersionNum versionNum, AWSSavingsPlansProduct savingPlanProduct, CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        AWSSavingsPlanType spType = SP_TYPES.getOrDefault(savingPlanProduct.getProductFamily(), AWSSavingsPlanType.UNKNOWN_SP);
        switch (spType) {
            case COMPUTE_SP:
                return Optional.of(new AWSSavingsPlansRecord(cloudEnvironmentPricingId, versionNum,
                        AWSPurchaseOption.fromName(savingPlanProduct.getAttributes().getPurchaseOption()),
                        savingPlanProduct.getAttributes().getInstanceType(),
                        savingPlanProduct.getAttributes().getPurchaseTermAsPeriod()));
            case EC2_INSTANCE_SP:
                return Optional.of(new AWSSavingsPlansRecord(cloudEnvironmentPricingId, versionNum,
                        AWSPurchaseOption.fromName(savingPlanProduct.getAttributes().getPurchaseOption()),
                        savingPlanProduct.getAttributes().getInstanceType(),
                        savingPlanProduct.getAttributes().getPurchaseTermAsPeriod(),
                        savingPlanProduct.getAttributes().getRegionCode()));

            default:
                return Optional.empty();
        }
    }

    private AWSSavingsPlansPricingRecord newAwsSavingsPlanProductPricing(VersionNum version, AWSSavingsPlansRates rate, AWSRawSavingsPlansPricingDataRecord rawSavingsPlan) {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = new CloudEnvironmentPricingId(CloudProvider.AWS, rawSavingsPlan.getRawRates().getSku());
        VersionedCloudEnvPricingId versionedCloudEnvPricingId = new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, version);

        ProductId savingsPlansProductId = savingsPlansProductIdFactory.createSavingsPlansProductId(versionedCloudEnvPricingId, rate.getDiscountedSku());

        AWSSavingsPlansPricingRecord awsSavingsPlansPricingRecord = awsSavingsPlansPricingRepository.findByIdAndVersion(savingsPlansProductId, version)
                .orElseGet(() -> {
                    AWSSavingsPlansPricingRecordBuilder pricingRecBldr = AWSSavingsPlansPricingRecord.builder()
                            .id(savingsPlansProductId)
                            .cloudEnvironmentPricingId(cloudEnvironmentPricingId)
                            .productSkuId(rate.getDiscountedSku())
                            .rateCode(rate.getRateCode())
                            .version(version)
                            .unit(new Unit(rate.getUnit()))
                            .pricePerUnitByCurrency(Map.of(rate.getDiscountedRate().getCurrency(), new BigDecimal(rate.getDiscountedRate().getPrice())))
                            .regionCode(rawSavingsPlan.getRegionCode());

                    if (rawSavingsPlan.getRawProduct() != null && rawSavingsPlan.getRawProduct().getAttributes() != null) {
                        pricingRecBldr
                                .purchaseTerm(rawSavingsPlan.getRawProduct().getAttributes().getPurchaseTermAsPeriod());
                    }

                    return pricingRecBldr.build();
                });

        if (!version.equals(awsSavingsPlansPricingRecord.getVersion())) {
            awsSavingsPlansPricingRecord.setVersion(version);
            awsSavingsPlansPricingRecord.setPricePerUnitByCurrency(Map.of(rate.getDiscountedRate().getCurrency(), new BigDecimal(rate.getDiscountedRate().getPrice())));
        }

        return awsSavingsPlansPricingRecord;
    }

}
