package org.ow2.morphemic.pricing.versions;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

/**
 * Support for data versioning. Pair; (cloud provider, collection of data) can have assigned a version number.
 * Each version for collection must be greater then previous one.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DataVersionsService {

    private final DataVersionsRepository dataVersionsRepository;

    /**
     * Tries to find latest version of data for given cloud environment and collection name.
     *
     * @param cloudEnvironmentPricingId cloud environment id
     * @param collectionName            collection name (may be correlated with Mongo's collection name)
     * @return empty when there is no version or collection
     */
    public Optional<VersionNum> lastVersionOf(CloudEnvironmentPricingId cloudEnvironmentPricingId, String collectionName) {
        return dataVersionsRepository.findFirstByCloudEnvironmentPricingIdAndCollectionNameOrderByVersionDesc(cloudEnvironmentPricingId, collectionName)
                .map(v -> new VersionNum(v.getVersion()));
    }

    /**
     * Full metadata of last version.
     *
     * @param cloudEnvironmentPricingId cloud environment id
     * @param collectionName            collection name (may be correlated with Mongo's collection name)
     * @return empty when there is no version or collection
     */
    public Optional<VersionMetadata> lastVersionMetadata(CloudEnvironmentPricingId cloudEnvironmentPricingId, CollectionName collectionName) {
        String name = collectionName.asEnvCollectionName(cloudEnvironmentPricingId);
        return dataVersionsRepository.findFirstByCloudEnvironmentPricingIdAndCollectionNameOrderByVersionDesc(cloudEnvironmentPricingId, name)
                .map(v -> new VersionMetadata(v.getCreated(), new VersionNum(v.getVersion())));
    }



    /**
     * Tries to find latest version of data for given cloud environment and collection name.
     *
     * @param cloudEnvironmentPricingId cloud environment id
     * @param collectionName            collection name (may be correlated with Mongo's collection name)
     * @return empty when there is no version or collection
     */
    public Optional<VersionNum> lastVersionOf(CloudEnvironmentPricingId cloudEnvironmentPricingId, CollectionName collectionName) {
        String name = collectionName.asEnvCollectionName(cloudEnvironmentPricingId);
        Optional<VersionRecord> maybeVersion = dataVersionsRepository.findFirstByCloudEnvironmentPricingIdAndCollectionNameOrderByVersionDesc(cloudEnvironmentPricingId, name);
        return maybeVersion.map(v -> new VersionNum(v.getVersion()));
    }

    /**
     * Write new version data for given collection in cloud environment.
     *
     * @param cloudEnvironmentPricingId cloud environment pricing id
     * @param collectionName            internal logical name of data collection
     * @param versionNum                new version
     */
    private void registerNewVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, String collectionName, VersionNum versionNum) {
        dataVersionsRepository.save(new VersionRecord(cloudEnvironmentPricingId, collectionName, versionNum, new Date()));
        log.debug("Registered new version {} of data collection '{}'", versionNum, collectionName);
    }

    /**
     * Write new version data for given collection in cloud environment.
     *
     * @param versionedCloudEnvPricingId versioned cloud environment id
     * @param collectionName             internal logical name of data collection
     */
    @Transactional
    public void registerNewVersion(VersionedCloudEnvPricingId versionedCloudEnvPricingId, CollectionName collectionName) {
        CloudEnvironmentPricingId cloudEnvironmentPricingId = versionedCloudEnvPricingId.getCloudEnvironmentPricingId();
        registerNewVersion(cloudEnvironmentPricingId,
                collectionName.asEnvCollectionName(cloudEnvironmentPricingId),
                versionedCloudEnvPricingId.getVersionNum());
    }

    /**
     * Used in tests.
     */
    public void removeAllVersions() {
        dataVersionsRepository.deleteAll();
        log.info("All versions have been removed!");
    }

    /**
     * Removes all versions of given pricing.
     *
     * @param pricingId pricing to be cleared
     */
    @Transactional
    public void removeAllVersions(CloudEnvironmentPricingId pricingId) {
        dataVersionsRepository.deleteByCloudEnvironmentPricingId(pricingId);
        log.info("All versions of pricing {} have been removed!", pricingId);
    }

    @Transactional
    public void removeVersion(CloudEnvironmentPricingId pricingId, VersionNum version) {
        dataVersionsRepository.deleteByCloudEnvironmentPricingIdAndVersion(pricingId, version);
        log.info("Version {} of pricing {} have been removed!", version, pricingId);
    }

    /**
     * Checks if given cloud environment pricing id has defined any data collection version.
     *
     * @param cloudEnvironmentPricingId cloud environment pricing id
     * @return
     */
    @Transactional
    public boolean hasAnyVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        return dataVersionsRepository.countByCloudEnvironmentPricingId(cloudEnvironmentPricingId) > 0;
    }

    @Transactional
    public boolean checkIfThisVersionNonExist(CloudEnvironmentPricingId cloudEnvironmentPricingId, VersionNum versionNum, CollectionName collectionName) {
        String name = collectionName.asEnvCollectionName(cloudEnvironmentPricingId);
        return !dataVersionsRepository.existsByCloudEnvironmentPricingIdAndVersionAndCollectionName(cloudEnvironmentPricingId, versionNum.getNum(), name);
    }

    @Transactional
    public void setEndOfValidityForLastVersion(CloudEnvironmentPricingId cloudEnvironmentPricingId, CollectionName collectionName) {
        String name = collectionName.asEnvCollectionName(cloudEnvironmentPricingId);
        dataVersionsRepository.findFirstByCloudEnvironmentPricingIdAndCollectionNameOrderByVersionDesc(cloudEnvironmentPricingId, name)
                .ifPresent(lastVersion -> {
                    lastVersion.setEndOfValidity(new Date());
                    dataVersionsRepository.save(lastVersion);
                });
    }

    @Data
    public static class VersionMetadata {
        private final Date created;
        private final VersionNum versionNum;
    }
}
