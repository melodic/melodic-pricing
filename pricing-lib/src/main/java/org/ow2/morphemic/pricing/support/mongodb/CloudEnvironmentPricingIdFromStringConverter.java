package org.ow2.morphemic.pricing.support.mongodb;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

/**
 * Used to read value from mongo string field.
 */
@ReadingConverter
class CloudEnvironmentPricingIdFromStringConverter implements Converter<String, CloudEnvironmentPricingId> {

    @Override
    public CloudEnvironmentPricingId convert(String textValue) {
        return new CloudEnvironmentPricingId(textValue);
    }
}
