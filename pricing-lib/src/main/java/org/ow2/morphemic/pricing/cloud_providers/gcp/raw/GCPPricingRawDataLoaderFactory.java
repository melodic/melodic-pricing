package org.ow2.morphemic.pricing.cloud_providers.gcp.raw;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingDataImportStatisticCollector;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;

/**
 * Creates various loaders on the fly.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class GCPPricingRawDataLoaderFactory {

    private final GCPRawServiceSKUDataRecordRepository gcpRawServiceSKUDataRecordRepository;

    private final GCPRawSKURecordRepository gcpRawSKURecordRepository;

    private final DataVersionsService dataVersionsService;

    @Getter
    @Setter
    @Value("${pricing.cloudproviders.gcp.applicationName:pricing-lib}")
    private String applicationName;

    /**
     * Creates raw fata loader which can fetch data from GCP REST API.
     *
     * @param apiKey                    api key needed to authorize
     * @param servicesIdsToFetch        ids of services to fetch from GCP
     * @param servicesFetchingDelayMs   delay in ms used to schedule next request to GCP
     * @param cloudEnvironmentPricingId pricing id for that clous env.
     * @param pricingDataImportStatistic
     * @return loader ready for use
     */
    public GCPPricingRawDataLoader createPricingGCPApiLoader(
            String apiKey,
            Set<String> servicesIdsToFetch,
            long servicesFetchingDelayMs,
            CloudEnvironmentPricingId cloudEnvironmentPricingId, PricingDataImportStatisticCollector pricingDataImportStatistic) {
        GCPClientServicePricingDataStreamSupplier dataSupplier = new GCPClientServicePricingDataStreamSupplier(apiKey, servicesIdsToFetch,
                servicesFetchingDelayMs, cloudEnvironmentPricingId, applicationName, pricingDataImportStatistic);
        return new GCPPricingRawDataLoader(
                Collections.singletonList(dataSupplier),
                gcpRawServiceSKUDataRecordRepository,
                gcpRawSKURecordRepository,
                dataVersionsService,
                true);

    }

}
