package org.ow2.morphemic.pricing.cloud_providers.azure;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.ow2.morphemic.pricing.cloud_providers.PricingIdOnlyDbRowProjection;
import org.ow2.morphemic.pricing.cloud_providers.ProductIdOnlyDbRowProjection;
import org.ow2.morphemic.pricing.cloud_providers.ProductsMetadataService;
import org.ow2.morphemic.pricing.cloud_providers.azure.raw.AzureRawProductPricingManager;
import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.versions.CollectionName;
import org.ow2.morphemic.pricing.versions.DataVersionsService;
import org.ow2.morphemic.pricing.versions.VersionNum;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides pricing data for AZURE products. Azure product is identified by "meterId".
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class AzureProductPricingService {
    static final CollectionName VERSIONED_AZURE_PRICING_COLLECTION_NAME = new CollectionName("azure:pricings:" + CollectionName.TEMPLATE);

    private final AzurePricingRecordsRepository azurePricingRecordsRepository;

    private final DataVersionsService dataVersionsService;

    private final AzureRawProductPricingManager azureRawProductPricingManager;

    private final ProductsMetadataService productsMetadataService;

    /**
     * Allows to fetch all IDs of LATEST product pricing records for given Cloud Environment.
     * Each cloud environment may have different pricing.
     *
     * @param cloudEnvironmentPricingId cloud env
     * @return set of product ids
     */
    @Transactional
    public Set<ProductId> allProductIds(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        Optional<VersionNum> maybeLastVersionNum = dataVersionsService.lastVersionOf(cloudEnvironmentPricingId, VERSIONED_AZURE_PRICING_COLLECTION_NAME);

        if (maybeLastVersionNum.isPresent()) {
            return azurePricingRecordsRepository.findAllByCloudEnvironmentPricingIdAndBasedOnRawVersionNum(cloudEnvironmentPricingId,
                    maybeLastVersionNum.get(), ProductIdOnlyDbRowProjection.class)
                    .map(ProductIdOnlyDbRowProjection::getId)
                    .collect(Collectors.toSet());

        } else {
            return Collections.emptySet();
        }
    }

    /**
     * Tries to find product pricing record for given Cloud Provider's client and product ID for that client.
     *
     * @param prodId unique product id
     * @return maybe found record
     */
    @Transactional
    public Optional<AzureProductPricingRecord> findProductPricing(ProductId prodId) {
        return azurePricingRecordsRepository.findById(prodId);
    }

    @Transactional
    void save(AzureProductPricingRecord record) {
        azurePricingRecordsRepository.save(record);
    }

    /**
     * To be used only by tests. It must remove every product data from azure pricings from:
     * raw product cache, products metadata repository and all AZURE pricing and products records.
     */
    @Transactional
    public void deleteAll() {
        azureRawProductPricingManager.deleteAll();
        azurePricingRecordsRepository.findDistinctBy(PricingIdOnlyDbRowProjection.class)
                .map(PricingIdOnlyDbRowProjection::getCloudEnvironmentPricingId).distinct()
                .forEach(pricingId -> {
                    dataVersionsService.removeAllVersions(pricingId);
                    azureRawProductPricingManager.deletePricing(pricingId);
                });
        azurePricingRecordsRepository.findAllBy(ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId)
                .forEach(productsMetadataService::removeProduct);
        azurePricingRecordsRepository.deleteAll();
        log.info("Removed all AZURE pricing data.");
    }

    /**
     * To be used only by tests. It must remove every product associated with given pricingId data from azure pricings from:
     * raw product cache, products metadata repository and all AZURE pricing and products records.
     */
    @Transactional
    public void deletePricing(CloudEnvironmentPricingId pricingId) {
        log.debug("AZURE pricing data will be deleted {}", pricingId);
        dataVersionsService.removeAllVersions(pricingId);
        azureRawProductPricingManager.deletePricing(pricingId);
        azurePricingRecordsRepository.findAllByCloudEnvironmentPricingId(pricingId, ProductIdOnlyDbRowProjection.class)
                .map(ProductIdOnlyDbRowProjection::getId)
                .forEach(productsMetadataService::removeProduct);
        long removed = azurePricingRecordsRepository.deleteAllByCloudEnvironmentPricingId(pricingId);
        log.info("Removed AZURE pricing data for {}. Removed records {}", pricingId, removed);
    }

}
