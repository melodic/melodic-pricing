package org.ow2.morphemic.pricing.cloud_providers.aws.raw.conversion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AWSIntermediateProductAndTerms {
    private AWSIntermediateProduct product;
    private AWSIntermediateTerms terms;
    private String version;
    private String serviceCode;
    private Date publicationDate;
}
