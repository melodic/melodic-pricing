# Pricing module

This library allows computing price of a product (service, vm, storage) provided by cloud provider. 
Currently, it supports calculating prices for Amazon AWS, Microsoft AZURE and GCP (Google CP). It is open to extend and adding new cloud providers.

# License

This code is licensed under Apache 2.0 license. See LICENSE-2.0.txt file for details.

## How to build

You need Java 11 (openjdk) to build this module and MongoDB. See docker-compose.yml.
Maven: 3.6.3 (look at .gitlab-ci.yml file)

### Building locally

1. docker-compose start 
2. mvn clean install verify

### Generating documentation

Run:

mvn clean install site:site site:stage -Dmaven.test.skip=true

in order to generate documentation in target/staging

## How to use

Ths is library, so it needs to cooperate with your code in order to provide results. At first needs credentials to connect to account in cloud provider's infrastructure in order to download pricing data. 
Then it must be integrated with your DI framework (usually spring) to update periodically pricing data.
Finally, it provides useful spring beans (services) which can compute product price according to given query.  


