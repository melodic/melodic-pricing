package org.ow2.morphemic.pricing.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CloudEnvironmentIdTest {

    @Test
    public void coverageTest() {
        CloudEnvironmentId id1 = new CloudEnvironmentId();
        CloudEnvironmentId id2 = new CloudEnvironmentId(CloudProvider.AWS);
        assertNotEquals(id1, id2);
    }

}