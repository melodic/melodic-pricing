package org.ow2.morphemic.pricing.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class PriceTest {

    @Test(expected = IllegalArgumentException.class)
    public void addingDifferentCurrenciesShouldThrowException() {
        new Price(new BigDecimal("1"), "c1").add(new Price(new BigDecimal("1"), "c2"));
    }

    @Test
    public void shouldAddPricesWithTheSameCurrency() {
        assertEquals(new Price(new BigDecimal("3"), "c1"), new Price(new BigDecimal("1"), "c1").add(new Price(new BigDecimal("2"), "c1")));
    }

}