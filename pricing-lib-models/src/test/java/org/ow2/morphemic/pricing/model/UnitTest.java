package org.ow2.morphemic.pricing.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class UnitTest {

    @Test
    public void testUnitsMultiplication() {
        assertEquals(new Unit("h"), new Unit("").multiply(new Unit("h")));
        assertEquals(new Unit("h"), new Unit("h").multiply(new Unit("")));
        assertEquals(new Unit("h"), new Unit("h").multiply(new Unit(" ")));
        assertEquals(new Unit("h^2"), new Unit("h").multiply(new Unit("h")));
        assertEquals(new Unit("GB.h"), new Unit("GB").multiply(new Unit("h")));
    }

}