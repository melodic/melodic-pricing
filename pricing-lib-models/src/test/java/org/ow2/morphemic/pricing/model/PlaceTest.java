package org.ow2.morphemic.pricing.model;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PlaceTest {

    @Test
    public void anyMustMatchAnything() {
        assertTrue(Place.ANY_PLACE.matches(Place.GLOBAL_PLACE));
        assertTrue(Place.ANY_PLACE.matches(Place.ANY_PLACE));
        assertTrue(Place.ANY_PLACE.matches(new Place("Poland")));
    }

    @Test
    public void globalMustMatchGlobal() {
        assertTrue(Place.GLOBAL_PLACE.matches(Place.GLOBAL_PLACE));
        assertFalse(Place.GLOBAL_PLACE.matches(Place.ANY_PLACE));
        assertFalse(Place.GLOBAL_PLACE.matches(new Place("global")));
        assertFalse(Place.GLOBAL_PLACE.matches(new Place("Poland")));
    }

    @Test
    public void checkConcretePlaceMatching() {
        assertTrue(new Place("global").matches(new Place("global")));

        assertFalse(new Place("global").matches(new Place("Global")));
        assertFalse(new Place("global").matches(Place.ANY_PLACE));

        assertFalse(new Place("global").matches(new Place("Poland")));
        assertTrue(new Place("global").matches(Place.GLOBAL_PLACE));
    }


}