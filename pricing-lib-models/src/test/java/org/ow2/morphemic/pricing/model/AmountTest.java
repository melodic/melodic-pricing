package org.ow2.morphemic.pricing.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AmountTest {

    @Test
    public void testUnitNameEquality() {
        Amount amount = new Amount(1, new Unit("u"));
        assertTrue(amount.unitNameEqualsTo("u"));
        assertFalse(amount.unitNameEqualsTo("U"));
    }

    @Test
    public void testMultiplication() {
        assertEquals(new Amount(7, new Unit("h")), new Amount(3.5, new Unit("h")).multiply(2));
        assertEquals(new Amount(7, new Unit("h")), new Amount(3.5, new Unit("h")).multiply(new Amount(2, new Unit(""))));
        assertEquals(new Amount(7, new Unit("GB.h")), new Amount(2, new Unit("GB")).multiply(new Amount(3.5, new Unit("h"))));
    }

}