package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

import java.io.Serializable;

/**
 * Unique client id of Cloud Provider.
 */
@Data
public class CloudProviderClientId implements Serializable {
    private final static long serialVersionUID = -1;

    @JsonValue
    private final String clientId;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public CloudProviderClientId(String clientId) {
        this.clientId = clientId;
    }

}
