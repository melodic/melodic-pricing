package org.ow2.morphemic.pricing.model.aws;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

import java.util.Arrays;

@Getter
public enum AWSPurchaseOption {
    NO_UPFRONT("No Upfront"),
    PARTIAL_UPFRONT("Partial Upfront"),
    ALL_UPFRONT("All Upfront"),
    UNKNOWN("");

    @JsonValue
    private final String displayName;

    AWSPurchaseOption(String displayName) {
        this.displayName = displayName;
    }

    public static AWSPurchaseOption fromName(String name) {
        if (name == null) {
            return UNKNOWN;
        }
        return Arrays.stream(AWSPurchaseOption.values())
                .filter(offerType -> offerType.getDisplayName().equals(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unknown aws purchase option: %s", name)));
    }
}
