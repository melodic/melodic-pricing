package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Amount of something. Usually required amount of some resource.
 */
@Data
public class Amount {

    private final double quantity;

    private final Unit unit;

    @JsonCreator
    public Amount(@JsonProperty("quantity") double quantity, @JsonProperty("unit") Unit unit) {
        this.quantity = quantity;
        this.unit = unit;
    }

    public boolean unitNameEqualsTo(String unitName) {
        return unit != null && unit.getUnitName().equals(unitName);
    }

    /**
     * Multiplies by constant.
     *
     * @param constQuantity
     * @return
     */
    public Amount multiply(double constQuantity) {
        return new Amount(quantity * constQuantity, unit);
    }

    /**
     * Multiplies by another amount.
     *
     * @param amount2
     * @return
     */
    public Amount multiply(Amount amount2) {
        return multiply(amount2, unit.multiply(amount2.unit));
    }

    public Amount multiply(Amount amount2, Unit destUnit) {
        return new Amount(quantity * amount2.quantity, destUnit);
    }

}
