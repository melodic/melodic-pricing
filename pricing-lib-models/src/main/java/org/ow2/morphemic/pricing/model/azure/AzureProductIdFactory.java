package org.ow2.morphemic.pricing.model.azure;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.ProductId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Universal way to create azure product id.
 */
public class AzureProductIdFactory {

    /**
     *
     * @param clEnvPricingId in this cloud env. pricing id meterId is unique
     * @param meterId meterId field describes unique product
     * @return productId
     */
    public ProductId createProductId(CloudEnvironmentPricingId clEnvPricingId, String meterId) {
        return new ProductId(clEnvPricingId, meterId);
    }

}
