package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

import java.io.Serializable;

/**
 * Unique, universal ID of a product in Cloud Environment.
 */
@Data
public class ProductId implements Serializable {
    private final static long serialVersionUID = -1;

    @JsonValue
    private final String id;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public ProductId(String id) {
        this.id = id;
    }

    /**
     * Creates meaningful id using cloud env and external id.
     *
     * @param cloudEnvironmentPricingId cloud environment where we found this product.
     * @param externalId                external id of product (SKU, service id), unique id of "product" or "service"
     */
    public ProductId(CloudEnvironmentPricingId cloudEnvironmentPricingId, String externalId) {
        this.id = "p:" + cloudEnvironmentPricingId.getId() + "/extId:" + externalId;
    }

    /**
     * Creates meaningful id using versioned cloud env pricing id and external id.
     *
     * @param versionedCloudEnvPricingId versioned cloud environment.
     * @param externalId                external id of product (SKU, service id), unique id of "product" or "service"
     */
    public ProductId(VersionedCloudEnvPricingId versionedCloudEnvPricingId, String externalId) {
        this.id = "p:" + versionedCloudEnvPricingId.getCloudEnvironmentPricingId().getId() + "/v:" + versionedCloudEnvPricingId.getVersionNum().getNum() + "/extId:" + externalId;
    }
}
