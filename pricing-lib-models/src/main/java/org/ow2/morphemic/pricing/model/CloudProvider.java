package org.ow2.morphemic.pricing.model;

/**
 * Unique identifier for each supported cloud provider
 */
public enum CloudProvider {
    /**
     * AWS
     */
    AWS,
    /**
     * Google Cloud Platform
     */
    GCP,
    /**
     * AZURE (generic)
     */
    AZURE,
    /**
     * ON PREMISE cloud; internal, private cloud
     */
    ON_PREMISE

}
