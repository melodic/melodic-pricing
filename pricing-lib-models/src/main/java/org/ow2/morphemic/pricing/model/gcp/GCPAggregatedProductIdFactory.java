package org.ow2.morphemic.pricing.model.gcp;

import org.ow2.morphemic.pricing.model.CloudEnvironmentPricingId;
import org.ow2.morphemic.pricing.model.Place;
import org.ow2.morphemic.pricing.model.ProductId;

/**
 * Saome products in GCP are defined as aggregates: as amounts of other products, and that can be used to calculate their price.
 * For instance; virtual machines consist of CPUs and some amount of RAM.
 * Here we provie possibiloty to create id of such products.
 */
public class GCPAggregatedProductIdFactory {

    /**
     * Use it to create product id for virtual machine. Yes: it is simple, but its implementation probably will evolve.
     *
     * @param pricingId  pricing id (used by many cloud environments).
     * @param vmCodeName code name of M (n1-standard-c3 or something).
     * @param place      place: usually region. Beware that there are special places!
     * @return product id
     */
    public ProductId createVMId(CloudEnvironmentPricingId pricingId, String vmCodeName, Place place) {
        return new ProductId(pricingId, "vm:" + vmCodeName + "/r:" + place.getType() + "/" + place.getCode());
    }

}
