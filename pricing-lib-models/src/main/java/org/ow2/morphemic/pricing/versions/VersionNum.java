package org.ow2.morphemic.pricing.versions;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.Instant;

/**
 * Version number.
 */
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.DELEGATING)))
@Data
public class VersionNum implements Comparable<VersionNum> {

    private final long num;

    public VersionNum() {
        num = Instant.now().toEpochMilli();
    }

    @Override
    public int compareTo(VersionNum o) {
        long delta = num - o.num;
        if (delta == 0) {
            return 0;
        } else {
            if (delta < 0) return -1;
            return 1;
        }
    }
}
