package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

/**
 * Offer type: Consumption, Reservation, DevTestConsumption etc. Exact value depends on Cloud Provider.
 */
@Data
public class OfferType {

    /**
     * Product type.
     */
    @JsonValue
    private final String name;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public OfferType(String name) {
        this.name = name;
    }
}
