package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

/**
 * Unit of measurement. Can be quantity. It is only name (id) of unit.
 */
@Data
public class Unit {

    @JsonValue
    private final String unitName;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public Unit(String unitName) {
        this.unitName = unitName;
    }

    /**
     * Very simple multiplication operation for units. Yes: VERY SIMPLE.
     * It supports h*h
     *
     * @param unit another unit
     * @return another unit
     */
    public Unit multiply(Unit unit) {
        if (equals(unit)) {
            return new Unit(unitName + "^2");
        } else if (unitName == null || unitName.isBlank()) {
            return unit;
        } else if (unit.unitName == null || unit.unitName.isBlank()) {
            return this;
        } else {
            return new Unit(unitName + "." + unit.unitName);
        }
    }
}
