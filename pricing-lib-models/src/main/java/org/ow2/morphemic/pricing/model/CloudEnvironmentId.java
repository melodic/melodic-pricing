package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

/**
 * Unique ID of cloud environment (which is owned by user). Environment is somehow connected with "accounts", but it is not the same.
 */
@Data
public class CloudEnvironmentId implements Serializable {
    private final static long serialVersionUID = -1;

    @JsonValue
    private final String id;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public CloudEnvironmentId(String id) {
        this.id = id;
    }

    public CloudEnvironmentId() {
        this.id = "clenv:unknown:" + UUID.randomUUID().toString();
    }

    public CloudEnvironmentId(CloudProvider cloudProvider) {
        this.id = "clenv:" + cloudProvider.name() + ":" + UUID.randomUUID().toString();
    }

}
