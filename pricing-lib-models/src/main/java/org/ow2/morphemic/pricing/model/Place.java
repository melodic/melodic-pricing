package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * Place where a product (or "service") is launched, installed or billed.
 * It can be "region".
 */
@Data
@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Place {
    public static final Place GLOBAL_PLACE = new Place(Type.GLOBAL, "");
    public static final Place ANY_PLACE = new Place(Type.ANY, "");
    @JsonProperty
    private final Type type;
    /**
     * place code
     */
    @JsonProperty
    private final String code;

    public Place(String code) {
        this.code = code;
        this.type = Type.CONCRETE;
    }

    @JsonCreator
    public static Place deserialize(@JsonProperty("type") Type type, @JsonProperty("code") String code) {
        return new Place(type, code);
    }

    /**
     * If this place matches (contains) another place.
     *
     * <ul>
     *     <li>ANY matches anything</li>
     *     <li>GLOBAL matches only GLOBAL</li>
     *     <li>CONCRETE matches another CONCRETE with the same code OR GLOBAL</li>
     * </ul>
     *
     * @param p2 another place
     * @return true if matches
     */
    public boolean matches(Place p2) {
        switch (type) {
            case ANY:
                return true;
            case GLOBAL:
                return p2.type == type;
            case CONCRETE:
                return p2.type == Type.GLOBAL || (p2.type == type && Objects.equals(code, p2.code));

            default:
                log.warn("Type is not checked {}!", type);
                return false;
        }
    }

    public enum Type {
        GLOBAL, ANY, CONCRETE
    }

}
