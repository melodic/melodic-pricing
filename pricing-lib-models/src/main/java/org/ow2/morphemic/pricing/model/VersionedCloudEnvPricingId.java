package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.ow2.morphemic.pricing.versions.VersionNum;

import java.io.Serializable;
import java.util.Objects;

/**
 * Describes versioned set of data for {@link CloudEnvironmentPricingId}. Can be used as composite id.
 * Meaning: sometimes data fetched for the same environment id may change and we must distinguish between two different 'versions" of data
 * fetched from the same "cloud environment".
 */
@Data
public class VersionedCloudEnvPricingId implements Serializable {
    private final static long serialVersionUID = -1;

    private final CloudEnvironmentPricingId cloudEnvironmentPricingId;

    private final VersionNum versionNum;

    @JsonCreator
    public VersionedCloudEnvPricingId(@JsonProperty("cloudEnvironmentPricingId") CloudEnvironmentPricingId cloudEnvironmentPricingId,
                                      @JsonProperty("versionNum") VersionNum versionNum) {
        this.cloudEnvironmentPricingId = cloudEnvironmentPricingId;
        this.versionNum = versionNum;
    }

    /**
     * Creates random new version for given cloudEnvironmentId.
     *
     * @param cloudEnvironmentPricingId cloud env
     */
    public VersionedCloudEnvPricingId(CloudEnvironmentPricingId cloudEnvironmentPricingId) {
        this(cloudEnvironmentPricingId, new VersionNum());
    }

    /**
     * Creates new versioned environment id using current cloudEnvironmentId.
     *
     * @param versionNum new version
     * @return new object
     */
    public VersionedCloudEnvPricingId newVersion(VersionNum versionNum) {
        Objects.requireNonNull(versionNum);
        return new VersionedCloudEnvPricingId(cloudEnvironmentPricingId, versionNum);
    }
}
