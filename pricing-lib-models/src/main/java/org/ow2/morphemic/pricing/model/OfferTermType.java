package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

/**
 * Offer term type: OnDemand, Reserved etc. Exact value depends on Cloud Provider.
 */
@Data
public class OfferTermType {

    /**
     * Internal term type name.
     */
    @JsonValue
    private final String name;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public OfferTermType(String name) {
        this.name = name;
    }
}
