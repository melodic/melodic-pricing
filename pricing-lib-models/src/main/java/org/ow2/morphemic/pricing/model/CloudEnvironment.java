package org.ow2.morphemic.pricing.model;

import lombok.Data;

/**
 * Describes cloud environment. For informational purposes only.
 */
@Data
public class CloudEnvironment {

    private CloudEnvironmentId id;

    /**
     * Pricing id for that environment. It may be shared by many {@link CloudEnvironment}s (m:1 relation).
     * Prices are calculated using that id.
     */
    private CloudEnvironmentPricingId cloudEnvironmentPricingId;

    /**
     * This cloud environment belongs to this "client". This is universal id - it may represent organization, person etc.
     * It is not important here.
     */
    private CloudProviderClientId clientId;

    private CloudProvider cloudProviderCode;

}
