package org.ow2.morphemic.pricing.model.aws_savings_plans;

import org.ow2.morphemic.pricing.model.ProductId;
import org.ow2.morphemic.pricing.model.VersionedCloudEnvPricingId;

/**
 * Products in AWS savings plans are defined as aggregates.
 * Here we provide possibility to create id of such products.
 */
public class AWSSavingsPlansProductIdFactory {

    /**
     * Use it to create product id for calculate price.
     *
     * @param versionedCloudEnvPricingId  for obtain correct versionedCloudEnvPricingId plese use ->
     * please use AWSSavingsPlansProductPricingService#findSavingsPlansProductIdByPurchaseOptionPurchaseTermInstanceTypeForLastVersion
     * @param skuForProduct sku for specific product
     * @return product id
     */
    public ProductId createSavingsPlansProductId(VersionedCloudEnvPricingId versionedCloudEnvPricingId, String skuForProduct) {
        return new ProductId(versionedCloudEnvPricingId.getCloudEnvironmentPricingId(),
                "ver:" + versionedCloudEnvPricingId.getVersionNum().getNum() + "/sku:" + skuForProduct);
    }
}
