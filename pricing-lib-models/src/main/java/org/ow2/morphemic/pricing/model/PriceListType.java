package org.ow2.morphemic.pricing.model;

/**
 * Unique identifier for each supported price list.
 */
public enum PriceListType {
    /**
     * Aws savings plans price list.
     */
    AWS_SAVINGS_PLANS
}
