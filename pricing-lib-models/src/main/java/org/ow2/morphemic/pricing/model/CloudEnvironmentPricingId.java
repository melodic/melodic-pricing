package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

/**
 * Unique ID of cloud environment pricing. Each {@link CloudEnvironment} must have one pricing, but pricing may be shared by many {@link CloudEnvironment}s.
 */
@Data
public class CloudEnvironmentPricingId implements Serializable {
    private final static long serialVersionUID = -1;
    @JsonValue
    private final String id;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public CloudEnvironmentPricingId(String id) {
        this.id = id;
    }

    public CloudEnvironmentPricingId() {
        this.id = "clevpr:unknown:" + UUID.randomUUID().toString();
    }

    public CloudEnvironmentPricingId(CloudProvider cloudProvider) {
        this.id = "clevpr:" + cloudProvider.name() + ":" + UUID.randomUUID().toString();
    }

    public CloudEnvironmentPricingId(CloudProvider cloudProvider, String externalId) {
        this.id = "clevpr:" + cloudProvider.name() + ":" + externalId;
    }
}
