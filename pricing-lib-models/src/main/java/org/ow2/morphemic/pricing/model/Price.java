package org.ow2.morphemic.pricing.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Price as value and currency.
 */
@Data
public class Price {

    private final BigDecimal value;

    private final String currencyCode;

    @JsonCreator
    public Price(@JsonProperty("value") BigDecimal value, @JsonProperty("currencyCode") String currencyCode) {
        this.value = value;
        this.currencyCode = currencyCode;
    }

    /**
     * Can only add prices with the same currencies.
     *
     * @param p
     * @return
     * @throws IllegalArgumentException
     */
    public Price add(Price p) throws IllegalArgumentException {
        if (!Objects.equals(currencyCode, p.currencyCode)) {
            throw new IllegalArgumentException("Cannot add prices with different currencies: " + currencyCode + " and " + p.currencyCode);
        }
        return new Price(value.add(p.value), currencyCode);
    }
}
