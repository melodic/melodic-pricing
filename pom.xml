<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.ow2.melodic</groupId>
	<artifactId>melodic-pricing-parent</artifactId>
	<version>0.51-SNAPSHOT</version>
	<name>Melodic Pricing Module</name>
	<packaging>pom</packaging>
	<description>Fetching pricing data for various cloud providers.</description>

	<properties>
		<java.version>11</java.version>
		<resource.delimiter>@</resource.delimiter>
		<maven.compiler.source>${java.version}</maven.compiler.source>
		<maven.compiler.target>${java.version}</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<delombok.output>target/delombok</delombok.output>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-starter-parent</artifactId>
				<version>2.3.12.RELEASE</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>org.javamoney</groupId>
				<artifactId>moneta</artifactId>
				<version>1.4</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>ma.glasnost.orika</groupId>
				<artifactId>orika-core</artifactId>
				<version>1.5.2</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security.oauth.boot</groupId>
				<artifactId>spring-security-oauth2-autoconfigure</artifactId>
				<version>2.1.4.RELEASE</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-jwt</artifactId>
				<version>1.0.10.RELEASE</version>
			</dependency>
			<dependency>
				<groupId>com.google.guava</groupId>
				<artifactId>guava</artifactId>
				<version>28.2-jre</version>
			</dependency>
			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-lang3</artifactId>
				<version>3.9</version>
			</dependency>
			<dependency>
				<groupId>org.openapitools</groupId>
				<artifactId>jackson-databind-nullable</artifactId>
				<version>0.2.1</version>
			</dependency>
			<dependency>
				<groupId>org.springdoc</groupId>
				<artifactId>springdoc-openapi-ui</artifactId>
				<version>1.2.32</version>
			</dependency>
			<dependency>
				<groupId>org.springdoc</groupId>
				<artifactId>springdoc-openapi-security</artifactId>
				<version>1.2.32</version>
			</dependency>
			<!-- https://mvnrepository.com/artifact/commons-io/commons-io -->
			<dependency>
				<groupId>commons-io</groupId>
				<artifactId>commons-io</artifactId>
				<version>2.6</version>
			</dependency>
			<dependency>
				<groupId>io.jsonwebtoken</groupId>
				<artifactId>jjwt-api</artifactId>
				<version>0.10.5</version>
			</dependency>
			<dependency>
				<groupId>io.jsonwebtoken</groupId>
				<artifactId>jjwt-impl</artifactId>
				<version>0.10.5</version>
			</dependency>
			<dependency>
				<groupId>io.jsonwebtoken</groupId>
				<artifactId>jjwt-jackson</artifactId>
				<version>0.10.5</version>
			</dependency>
			<dependency>
				<groupId>net.jcip</groupId>
				<artifactId>jcip-annotations</artifactId>
				<version>1.0</version>
			</dependency>
			<dependency>
				<groupId>com.github.spotbugs</groupId>
				<artifactId>spotbugs-annotations</artifactId>
				<version>4.0.0-beta3</version>
			</dependency>
			<!-- https://mvnrepository.com/artifact/com.tngtech.archunit/archunit-junit4 -->
			<dependency>
				<groupId>com.tngtech.archunit</groupId>
				<artifactId>archunit-junit4</artifactId>
				<version>0.14.1</version>
			</dependency>

			<dependency>
				<groupId>com.github.cloudyrock.mongock</groupId>
				<artifactId>mongock-bom</artifactId>
				<version>4.1.17</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	<dependencies>
		<!-- Compatibility with Findbugs annotations for Spotbugs -->
		<dependency>
			<groupId>net.jcip</groupId>
			<artifactId>jcip-annotations</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>com.github.spotbugs</groupId>
			<artifactId>spotbugs-annotations</artifactId>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.projectlombok</groupId>
				<artifactId>lombok-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.jacoco</groupId>
					<artifactId>jacoco-maven-plugin</artifactId>
					<version>0.8.4</version>
					<executions>
						<execution>
							<goals>
								<goal>prepare-agent</goal>
							</goals>
						</execution>
						<execution>
							<id>generate-code-coverage-report</id>
							<phase>test</phase>
							<goals>
								<goal>report</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<excludes>
						</excludes>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-checkstyle-plugin</artifactId>
					<version>2.17</version>
					<configuration>
						<sourceDirectory>${project.build.sourceDirectory}</sourceDirectory>
						<configLocation>${project.basedir}/conf/checkstyle.xml</configLocation>
						<encoding>UTF-8</encoding>
						<consoleOutput>true</consoleOutput>
						<failsOnError>true</failsOnError>
						<excludes>
						</excludes>
					</configuration>
					<executions>
						<execution>
							<id>validate</id>
							<phase>validate</phase>
							<goals>
								<goal>check</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<!-- SpotBugs Static Analysis -->
				<plugin>
					<groupId>com.github.spotbugs</groupId>
					<artifactId>spotbugs-maven-plugin</artifactId>
					<version>3.1.12</version>
					<executions>
						<execution>
							<goals>
								<goal>check</goal>
							</goals>
						</execution>
					</executions>
					<configuration>
						<effort>Max</effort>
						<threshold>Low</threshold>
						<failOnError>true</failOnError>
						<excludeFilterFile>${project.basedir}/conf/spotbugs-exclude.xml</excludeFilterFile>
						<xmlOutput>true</xmlOutput>
						<xmlOutputDirectory>target/site</xmlOutputDirectory>
						<plugins>
							<plugin>
								<groupId>com.h3xstream.findsecbugs</groupId>
								<artifactId>findsecbugs-plugin</artifactId>
								<version>LATEST</version> <!-- Auto-update to the latest stable -->
							</plugin>
						</plugins>
					</configuration>
				</plugin>
				<plugin>
					<groupId>pl.project13.maven</groupId>
					<artifactId>git-commit-id-plugin</artifactId>
					<executions>
						<execution>
							<id>get-the-git-infos</id>
							<goals>
								<goal>revision</goal>
							</goals>
							<phase>initialize</phase>
						</execution>
					</executions>
					<configuration>
						<generateGitPropertiesFile>false</generateGitPropertiesFile>
						<!--					<generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties</generateGitPropertiesFilename>-->
						<includeOnlyProperties>
							<includeOnlyProperty>^git.build.(time|version)$</includeOnlyProperty>
							<includeOnlyProperty>^git.commit.id.(abbrev|full)$</includeOnlyProperty>
							<includeOnlyProperty>^git.commit.id$</includeOnlyProperty>
							<includeOnlyProperty>^git.branch$</includeOnlyProperty>
						</includeOnlyProperties>
						<commitIdGenerationMode>full</commitIdGenerationMode>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-jar-plugin</artifactId>
					<version>3.0.2</version>
					<configuration>
						<archive>
							<manifestEntries>
								<Implementation-Version>${project.version}_${git.commit.id.full}</Implementation-Version>
							</manifestEntries>
						</archive>
					</configuration>
				</plugin>
				<plugin>
					<artifactId>maven-deploy-plugin</artifactId>
					<version>2.8.1</version>
					<executions>
						<execution>
							<id>default-deploy</id>
							<phase>deploy</phase>
							<goals>
								<goal>deploy</goal>
							</goals>
						</execution>
					</executions>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-site-plugin</artifactId>
					<version>3.12.1</version>
					<configuration>
						<locales>en</locales>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.projectlombok</groupId>
					<artifactId>lombok-maven-plugin</artifactId>
					<version>1.18.20.0</version>
					<configuration>
						<sourceDirectory>${project.basedir}/src/main/java</sourceDirectory>
						<outputDirectory>${delombok.output}</outputDirectory>
						<addOutputDirectory>false</addOutputDirectory>
					</configuration>
					<executions>
						<execution>
							<phase>generate-sources</phase>
							<goals>
								<goal>delombok</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-javadoc-plugin</artifactId>
					<version>3.4.1</version>
					<configuration>
						<doclint>none</doclint>
						<sourcepath>${delombok.output}:${project.build.directory}/generated-sources/openapi/src/main/java</sourcepath>
					</configuration>
					<executions>
						<execution>
							<goals>
								<goal>javadoc</goal>
							</goals>
						</execution>
						<execution>
							<id>aggregate</id>
							<goals>
								<goal>aggregate</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.4.1</version>
				<configuration>
					<doclint>none</doclint>
					<show>private</show>
					<sourcepath>${delombok.output}:${project.build.directory}/generated-sources/openapi/src/main/java</sourcepath>
				</configuration>
				<reportSets>
					<reportSet><!-- by default, id = "default" -->
						<id>default</id>
						<reports><!-- select non-aggregate reports -->
							<report>javadoc</report>
						</reports>
					</reportSet>
					<reportSet><!-- aggregate reportSet, to define in poms having modules -->
						<id>aggregate</id>
						<inherited>false</inherited><!-- don't run aggregate in child modules -->
						<reports>
							<report>aggregate</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>
				<version>3.4.1</version>
			</plugin>
		</plugins>

	</reporting>


	<repositories>
		<repository>
			<id>eu.7bulls</id>
			<name>Melodic 7bulls repository</name>
			<url>https://nexus.7bulls.eu:8443/repository/maven-public/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</snapshots>
		</repository>

		<repository>
			<id>maven-central</id>
			<url>https://repo1.maven.org/maven2/</url>
			<snapshots><updatePolicy>daily</updatePolicy></snapshots>
		</repository>

		<repository>
			<id>ow2</id>
			<name>OW2 repository</name>
			<url>http://repository.ow2.org/nexus/content/repositories/public/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</snapshots>
		</repository>
		<repository>
			<id>ossrh.snapshots</id>
			<name>OSSRH snapshots repository</name>
			<url>https://oss.sonatype.org/content/repositories/snapshots</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</snapshots>
		</repository>
	</repositories>
	<distributionManagement>
		<repository>
			<id>eu.7bulls</id>
			<name>Melodic 7bulls repository</name>
			<url>https://nexus.7bulls.eu:8443/repository/maven-releases/</url>
		</repository>
		<snapshotRepository>
			<id>eu.7bulls</id>
			<name>Melodic 7bulls repository</name>
			<url>https://nexus.7bulls.eu:8443/repository/maven-snapshots/</url>
		</snapshotRepository>
		<site>
			<id>test-site</id>
			<url>file:///tmp/site_stage</url>
		</site>
	</distributionManagement>

	<scm>
		<connection>scm:git:git@gitlab.ow2.org:melodic/melodic-pricing.git</connection>
		<developerConnection>scm:git:git@gitlab.ow2.org:melodic/melodic-pricing.git</developerConnection>
	  <tag>melodic-pricing-parent-0.30</tag>
  </scm>

	<modules>
	    <module>pricing-lib</module>
	    <module>pricing-usvc</module>
		<module>pricing-lib-models</module>
		<module>pricing-lib-models-jackson</module>
	</modules>
</project>
